<!-- 
- @author: Genaro Bermúdez [31/01/2018]
- @updated: ---
- @description: init page > redirect
-->

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Página de inicio</title>
</head>
<script type="text/javascript">
	function loginRedirect() {
		window.document.redirectForm.operation.value = "first";
		window.document.redirectForm.submit();
	}
</script>
<body onload="loginRedirect();">

	<!-- ============================================================ -->
	<!-- Formulario para redirect -->
	<!-- ============================================================ -->
	<s:form action="login" method="post" name="redirectForm">
		<input type="hidden" value="" name="operation">
	</s:form>

</body>
</html>