<!-- 
- @author: Genaro Bermúdez [12/02/2018]
- @updated: Mauricio Rivera [12/04/2018]
- @description: not allowed page
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	function paginator(pageNumber) {
		var paginatorForm = window.document.paginatorForm;
		paginatorForm['attributesPaginator.pageNumber'].value = pageNumber;
		find();
	}

	function sort(sortColumn, sortOrder) {
		var paginatorForm = window.document.paginatorForm;
		if (sortOrder == '') {
			paginatorForm['attributesPaginator.sortColumn'].value = '';
			paginatorForm['attributesPaginator.sortOrder'].value = '';
		} else {
			paginatorForm['attributesPaginator.sortColumn'].value = sortColumn;
			paginatorForm['attributesPaginator.sortOrder'].value = sortOrder;
		}
		find();
	}

	function find() {
		var paginatorForm = window.document.paginatorForm;
		var inputFiltro = window.document.getElementById('${param.idCampoFiltro}');
		if (inputFiltro != undefined) {
			paginatorForm['attributesPaginator.filter'].value = inputFiltro.value;
		}
		paginatorForm['attributesPaginator.fieldsToFilter'].value = '${param.fieldsToFilter}';
		paginatorForm.action = '${param.action}';
		paginatorForm.submit();
	}
	
</script>

<!-- ============================================================ -->
<!-- Paginator Form -->
<!-- ============================================================ -->
<s:form id="paginatorForm" method="post" hidden="true" style="display:none;">
	<s:hidden name="attributesPaginator.filter" />
	<s:hidden name="attributesPaginator.pageNumber" />
	<s:hidden name="attributesPaginator.sortColumn" />
	<s:hidden name="attributesPaginator.sortOrder" />
	<s:hidden name="attributesPaginator.fieldsToFilter" />
</s:form>

<!-- ============================================================ -->
<!-- Paginator -->
<!-- ============================================================ -->
<table align="left" border="0" cellpadding="0" cellspacing="0">
	<tr align="center">
		<td width="5px"></td>

		<s:if test="attributesPaginator.numberElements == 0">
			&nbsp;<span class="listContainerHeaderMouseOut">No hay elementos a mostrar</span>
		</s:if>
		<s:else>
			<!-- First Page -->
			<s:if test="attributesPaginator.firstPageSelectable">
				<td width="20px" title="Primera página"><a href="javascript:void(0)" onclick="paginator(1);"><img src="<s:property value="resourcesPath+'MODULES/commons/images/principio.gif'"/>" border="0" width="10" height="7"></a></td>
			</s:if>
			<s:else>
				<td width="20px" title="Primera página"><img src="<s:property value="resourcesPath+'MODULES/commons/images/principioTenue.png'"/>" border="0" width="10" height="7"></td>
			</s:else>

			<!-- Previous Page -->
			<s:if test="attributesPaginator.previousPageSelectable">
<!-- 				<td width="20px" title="Página anterior"> -->
<%-- 				<a href="javascript:void(0)" onclick='paginator(<s:property value="attributesPaginator.previousPage" />);'><img src="<s:property value="resourcesPath+'MODULES/commons/images/atras.gif'"/>" border="0" width="10" height="7"></a> --%>
<!-- 				</td> -->
			</s:if>
			<s:else>
<%-- 				<td width="20px" title="Página anterior"><img src="<s:property value="resourcesPath+'MODULES/commons/images/atrasTenue.png'"/>" border="0" width="10" height="7"></td> --%>
			</s:else>

			<!-- Pages -->
			<s:iterator var="counter" begin="1" end="attributesPaginator.pagesNumber">
				<s:if test="attributesPaginator.pagesNumber > 30">
					<s:set name="first" value="%{30}"></s:set>
					<s:set name="min" value="%{attributesPaginator.pageNumber-15}"></s:set>
					<s:set name="max" value="%{attributesPaginator.pageNumber+15}"></s:set>
					<s:set name="last" value="%{attributesPaginator.pagesNumber-30}"></s:set>
					<s:if test="%{ (#counter <= 30) && (#counter >= #min) }">
						<td width="30px"><a onMouseOver="this.className='paginatorMouseOver'" onMouseOut="this.className='paginatorMouseOut'" class="paginatorMouseOut" href="javascript:void(0)" onclick='paginator(<s:property value="top" />);'> <s:property value="top" />
						</a></td>
					</s:if>
					<s:elseif test="%{ (#counter > 30) && (#counter >= #min) && (#counter <= #max) && (#counter < #last)}">
						<td width="30px"><a onMouseOver="this.className='paginatorMouseOver'" onMouseOut="this.className='paginatorMouseOut'" class="paginatorMouseOut" href="javascript:void(0)" onclick='paginator(<s:property value="top" />);'> <s:property value="top" />
						</a></td>
					</s:elseif>
					<s:elseif test="%{ (#counter > 30) && (#counter >= #min) && (#counter <= #max) && (#counter > #last)}">
						<s:if test="attributesPaginator.pageNumber == top">
							<td width="30px" style="cursor: default;">&nbsp;<b><u><s:property value="top" /></u></b>&nbsp;
							</td>
						</s:if>
						<s:else>
							<td width="30px"><a onMouseOver="this.className='paginatorMouseOver'" onMouseOut="this.className='paginatorMouseOut'" class="paginatorMouseOut" href="javascript:void(0)" onclick='paginator(<s:property value="top" />);'> <s:property value="top" />
							</a></td>
						</s:else>
					</s:elseif>
				</s:if>
				<s:else>
					<s:if test="attributesPaginator.pageNumber == top">
						<td width="30px" style="cursor: default;">&nbsp;<b><u><s:property value="top" /></u></b>&nbsp;
						</td>
					</s:if>
					<s:else>
						<td width="30px"><a onMouseOver="this.className='paginatorMouseOver'" onMouseOut="this.className='paginatorMouseOut'" class="paginatorMouseOut" href="javascript:void(0)" onclick='paginator(<s:property value="top" />);'> <s:property value="top" />
						</a></td>
					</s:else>
				</s:else>
			</s:iterator>

			<!-- Next Page -->
			<s:if test="attributesPaginator.nextPageSelectable">
<!-- 				<td width="20px" title="Página siguiente"> -->
<%-- 				<a href="javascript:void(0)" onclick='paginator(<s:property value="attributesPaginator.nextPageSelectable" />);'><img src="<s:property value="resourcesPath+'MODULES/commons/images/siguiente.gif'"/>" border="0" width="10" height="7"></a> --%>
<!-- 				</td> -->
			</s:if>
			<s:else>
<%-- 				<td width="20px" title="Página siguiente"><img src="<s:property value="resourcesPath+'MODULES/commons/images/siguienteTenue.png'"/>" border="0" width="10" height="7"></td> --%>
			</s:else>

			<!-- Last Page -->
			<s:if test="attributesPaginator.lastPageSelectable">
				<td width="20px" title="Última página"><a href="javascript:void(0)" onclick='paginator(<s:property value="attributesPaginator.pagesNumber" />);'><img src="<s:property value="resourcesPath+'MODULES/commons/images/final.gif'"/>" border="0" width="10" height="7"></a></td>
			</s:if>
			<s:else>
				<td width="20px" title="Última página"><img src="<s:property value="resourcesPath+'MODULES/commons/images/finalTenue.png'"/>" border="0" width="10" height="7"></td>
			</s:else>
			<td width="20px" onMouseOver="this.className='paginatorMouseOver'" onMouseOut="this.className='paginatorMouseOver'" class="paginatorMouseOver">Total:</td>
			<td width="20px" onMouseOver="this.className='paginatorMouseOver'" onMouseOut="this.className='paginatorMouseOver'" class="paginatorMouseOver"><s:property value="attributesPaginator.pagesNumber" /></td>
		</s:else>
	</tr>
</table>
