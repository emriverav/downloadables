<!-- 
- @author: Genaro Bermúdez [31/01/2018]
- @updated: ---
- @description: error unespected
-->

<%@ page language="java" isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device−width, initial−scale=1.0" />
<title>Liverpool | Error inesperado</title>
<link href="<s:property value="resourcesPath+'MODULES/commons/styles/commons.css'"/>" rel="stylesheet" type="text/css">
</head>
<body>

	<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		<tr height="65px" align="left">
			<td>&nbsp;</td>
			<td><img src="<s:property value="resourcesPath+'MODULES/commons/images/imgLogoLiverpool.gif'"/>"></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td><font style="font: 1em arial, Helvetica, sans-serif; font-stretch: extra-condensed; color: #666;">¡Lo sentimos! Ha ocurrido un error severo en nuestro sistema.</font></br> <font style="font: 1em arial, Helvetica, sans-serif; font-stretch: extra-condensed; color: #666;">Si necesitas ayuda comunícate al teléfono: 52 62 99 99</font></br</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="65px" align="center">
			<td>&nbsp;</td>
			<td><font style="font: 1em arial, Helvetica, sans-serif; font-stretch: extra-condensed; color: #666;"><a style="color: #E10098;" href="<s:url value="/"/>">Regresar a la aplicación Monitor downloadables</a></font></td>
			<td>&nbsp;</td>
		</tr>
	</table>

</body>
</html>