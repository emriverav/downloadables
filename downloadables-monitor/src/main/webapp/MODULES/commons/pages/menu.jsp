<!-- 
- @author: Genaro Bermúdez [12/02/2018]
- @updated:
- @description: main menu
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	function redirect(action) {
		window.document.navForm.action = action;	
		window.document.navForm.submit();
	}
	function viewHideChildMenu(objeto) {
		for(var i=0; i < document.getElementsByTagName("div").length; i++) {
		  var subMenu = document.getElementsByTagName("div")[i];
		 	if (subMenu.id.substring(0,9) == "childMenu") {
		 		subMenu.style.display = "none";
		 	}
		}
		var objectChildGeneric = window.document.getElementById("childMenu" + objeto.id.replace("parentMenu", ""));
		if ((objectChildGeneric != null) && (objectChildGeneric.style.display != "block"))  {
			objectChildGeneric.style.marginTop = ((objeto.getBoundingClientRect().top) + (objeto.offsetHeight) + 2) + "px";
			objectChildGeneric.style.marginLeft = ((objeto.getBoundingClientRect().left) -3) + "px";
			objectChildGeneric.style.display = "block";
		}
	}
</script>

<tr>
	<td class="menuBaseContainer">
	
		<!-- ============================================================ -->
		<!-- Redirect form -->
		<!-- ============================================================ -->
		<s:form method="post" name="navForm" style="display:none;" >
			<input type="hidden" value="" name="operation">
		</s:form>

		<!-- ============================================================ -->
		<!-- Options -->
		<!-- ============================================================ -->
		<table width="100%" border="0"  cellpadding="0" cellpadding="0">
			<tr>
				<td>
					<!-- ============================================================ -->
					<!-- First level options -->
					<!-- ============================================================ -->
					<a onclick="redirect('home')" onMouseOver="this.className='menuMouseOver'; viewHideChildMenu(this);" onMouseOut="this.className='menuMouseOut'; viewHideChildMenu(this);" class="menuMouseOut" >Inicio</a>
					<span class="pipeLine">&nbsp;</span>
					<s:iterator value="#session.toLogin.menus" var="menu">
						<s:if test="#menu.idParentMenu == 0">
							<s:if test="#menu.action != null">
								<a onclick="redirect('<s:property value="#menu.action"/>')" onMouseOver="this.className='menuMouseOver'; viewHideChildMenu(this);" onMouseOut="this.className='menuMouseOut'; viewHideChildMenu(this);" class="menuMouseOut" ><s:property value="#menu.name"/></a>
							</s:if>
							<s:else>
								<a onMouseOver="this.className='menuMouseOver'; viewHideChildMenu(this);" id='parentMenu<s:property value="#menu.name"/>' onMouseOut="this.className='menuMouseOut'; viewHideChildMenu(this);"  class="menuMouseOut"><s:property value="#menu.name"/> &nbsp;&nbsp;<img src="<s:property value="resourcesPath+'MODULES/commons/images/imgBajar.png'"/>" ></a>
							</s:else>
							<span class="pipeLine">&nbsp;</span>
						</s:if>
					</s:iterator>
				</td>
				<td>
					&nbsp;
				</td>
				<td align="right">
					<a class="menuMouseOut"><b>Usuario</b>: <s:property value="#session.toLogin.user.login" /></a>
					<span class="pipeLine">&nbsp;</span>
					<a onMouseOver="this.className='menuMouseOver';" onMouseOut="this.className='menuMouseOut'" class="menuMouseOut" onclick="redirect('login');">Cerrar sesión</a>
				</td>
				
			</tr>
		</table>
		
	</td>
</tr>

<!-- ============================================================ -->
<!-- Second level options -->
<!-- ============================================================ -->
<s:iterator value="#session.toLogin.menus" var="menu">
	<s:if test="#menu.idParentMenu == 0 && #menu.action == null">
		<div class="subMenuContainer" style="display:none; position: absolute; z-index: 1000;" id="childMenu<s:property value="#menu.name"/>" onMouseLeave="this.style.display='none';">
			<s:iterator value="#session.toLogin.menus" var="subMenu">
				<s:if test="#subMenu.idParentMenu > 0 && #subMenu.idParentMenu == #menu.idMenu">
					<s:if test='#subMenu.url.equals("S")'>
						<s:if test='#subMenu.modal.equals("S")'>
							<div onMouseOver="this.className='subMenuMouseOver';" onMouseOut="this.className='subMenuMouseOut'" class="subMenuMouseOut" onclick="abrirVentanaModal('<s:property value="#subMenu.action"/>', '<s:property value="#subMenu.name"/>');" ><s:property value="#subMenu.name"/></div>
						</s:if>
						<s:else>
							<div onMouseOver="this.className='subMenuMouseOver';" onMouseOut="this.className='subMenuMouseOut'" class="subMenuMouseOut" onclick="redirectURL('<s:property value="#subMenu.action"/>');" ><s:property value="#subMenu.name"/></div>
						</s:else>
					</s:if>
					<s:else>
						<div onMouseOver="this.className='subMenuMouseOver';" onMouseOut="this.className='subMenuMouseOut'" class="subMenuMouseOut" onclick="redirect('<s:property value="#subMenu.action"/>');" ><s:property value="#subMenu.name"/></div>
					</s:else>
				</s:if>
			</s:iterator>
		</div>
	</s:if>
</s:iterator>
