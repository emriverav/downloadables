<!-- 
- @author: Genaro Bermúdez [12/02/2018]
- @updated:
- @description: subcontainers page
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!-- ============================================================ -->
<!-- Subcontainer validations -->
<!-- ============================================================ -->
<div style="display: block; position: absolute; z-index: 100; left: 29%; padding-top: 35%; _padding-top: 50%">
	<s:iterator value="errorMessages" var="message">
		<div class="validationsContainer" onclick="this.style.display='none';">
			<img style="position: absolute; margin-left: -50px; margin-top: -12px;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgError.png'"/>"> <img style="position: absolute; margin-left: 480px; margin-top: 0px;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgCerrar.png'"/>">
			<s:property />
		</div>
	</s:iterator>
</div>

<!-- ============================================================ -->
<!-- Subcontainer loader -->
<!-- ============================================================ -->
<div id="genericLoaderContainer" style="z-index: 1001; position: absolute; background-color: #CCCCCC; display: none; top: 0px; left: 0px; width: 100%; height: 100%; opacity: 0.7;"></div>
<div id="genericLoaderImage" style="text-align: center; z-index: 1002; padding: 20px 70px 20px 70px; border: thin solid #808080; border-radius: 5px; border-width: 1px 1px 1px 1px; background-color: #f2f2f2; position: absolute; display: none;"><img src="<s:property value="resourcesPath+'MODULES/commons/images/imgLoader.gif'"/>"><span style="text-align: center;"><br> <span class="formSubContainer">cargando...</span></div>

<!-- ============================================================ -->
<!-- Subcontainer genericTooltip -->
<!-- ============================================================ -->
<div class="genericToolTip" id="genericToolTip"></div>
<div id="genericToolTipSide" style="z-index:1000; position:absolute; display:none; width:15px; height:auto; background-repeat: no-repeat; background-image:url('<s:property value="resourcesPath+'MODULES/commons/images/imgToolTipGenericoPunta.png'"/>');"></div>

<!-- ============================================================ -->
<!-- Subcontainer popup -->
<!-- ============================================================ -->
<div id="genericPopupBaseContainer" style="z-index: 100; position: absolute; background-color: #CCCCCC; display: none; top: 0px; left: 0px; width: 100%; height: 100%; opacity: 0.7;"></div>
<div id="genericPopupContainer" style="z-index: 1000; position: absolute; display: none;">
	<table class="formContainer" width="470px" id="genericPopupTable">
		<tr>
			<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;<span id="genericPopupTitle">Título</span>
				<div style="position: absolute; margin-left: 435px; margin-top: -18px; cursor: pointer;">
					<img src="<s:property value="resourcesPath+'MODULES/commons/images/imgCerrar.png'"/>" onclick="showHidePopup();">
				</div>
			</td>
		</tr>
		<tr class="formSubContainer">
			<td><br>
				<form class="formSubContainer" action="editPopup" method="post" name="popupForm">
					<table align="center" cellpadding="3" cellspacing="0" width="100%">
						<tr>
							<td align="center"><span id="genericPopupMessage">¿Seguro deseas eliminar el área comercial?</span></td>
						</tr>
						<tr height="20px">
							<td></td>
						</tr>
						<tr>
							<td align="center"><span id="genericPopupButtons"></span></td>
						</tr>
						<tr height="15px">
							<td></td>
						</tr>
					</table>
				</form></td>
		</tr>
	</table>
</div>

<script type="text/javascript">
	function evaluateInformativeMessage() {
		var title = "<s:property value='popup.title' />";
		var message = "<s:property value='popup.message' escapeHtml='false' />";
		var buttons = new Array();
		<s:iterator value='popup.buttons' var='button'>
			buttons.push(new Button("<s:property value='label' />", "<s:property value='callback' />"));
		</s:iterator>
		if (message != "") {
			showHidePopup(message, title, buttons);
		}
	}

	function hideElements(elementos) {
		if (elements != undefined) {
			for (var idx = 0; idx < elements.length; idx++) {
				elements[idx].style.display = "none";
			}
		}
	}

	function disabledElementWithImage(elements) {
		if (elements != undefined) {
			for (var idx = 0; idx < elements.length; idx++) {
				var nodesChild = elements[idx].childNodes;
				for (var jdx = 0; jdx < nodesChild.length; jdx++) {
					if (nodesChild[jdx].tagName == "IMG") {
						nodesChild[jdx].src = nodesChild[jdx].src.replace(
								".png", "Disabled.png");
						nodesChild[jdx].style.cursor = "default";
						break;
					}
				}
				elements[idx].onclick = null;
			}
		}
	}

	evaluateInformativeMessage();
</script>

