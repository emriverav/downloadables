<!-- 
- @author: Genaro Bermúdez [12/02/2018]
- @updated: 
- @description: header
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script>
	function hideChildSubMenu() {
		for (var i = 0; i < document.getElementsByClassName("subMenuContainer").length; i++) {
			var subMenu = document.getElementsByClassName("subMenuContainer")[i];
			subMenu.style.display = "none";
		}
	}
</script>

<table width="100%" onmouseover="hideChildSubMenu();">
	<tr>
		<td align="left"><img height="47px" src="<s:property value="resourcesPath+'MODULES/commons/images/imgLogoLiverpool.gif'"/>"></td>
		<td align="center" width="40%">Monitor Descargables</td>
		<td align="right" width="30%"></td>
	</tr>
</table>