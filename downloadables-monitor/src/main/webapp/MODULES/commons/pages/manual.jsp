<!-- 
- @author: Genaro Bermúdez [31/01/2018]
- @updated: ---
- @description: user manual
-->

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<embed src="<s:url value="/MODULES/commons/docs/Manual.pdf"/>" width="100%" height="100%" href="<s:url value="/MODULES/commons/docs/Manual.pdf"/>"></embed>