<!-- 
- @author: Genaro Bermúdez [12/02/2018]
- @updated: 
- @description: footer
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	function openManual() {
		openPopup("manual", "Manual", 600, 550);
	}
</script>

<table width="100%" colspan="5">
	<tr>
		<td align="left"><span class="footerTextBaseContainer" style="cursor: default;">{ v.0.0.2 monitor downloadables }</span></td>
		<td align="right" colspan="2"><span><a class="footerTextBaseContainer" style="cursor: pointer;" onClick="openManual();"><img style="position: relative; top: 2px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgManual.png'"/>" title="Manual online"> Manual online</a>&nbsp;&nbsp; </span></td>
	</tr>
</table>