<!-- 
- @author: Genaro Bermúdez [31/01/2018]
- @updated:
- @description: principal container 
-->

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags" %>

<!DOCTYPE HTML>
<html>

<head>
	<link rel="icon" href="<s:property value="resourcesPath+'MODULES/commons/images/favicon.ico'"/>" type="image/x-icon" />
	<sx:head />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><tiles:insertAttribute name="title" ignore="true" /></title>
	<link href="<s:property value="resourcesPath+'MODULES/commons/styles/commons.css'"/>" rel="stylesheet" type="text/css" />
	<script src="<s:property value="resourcesPath+'MODULES/commons/scripts/jquery-2.1.3.min.js'"/>" type="text/javascript"></script>
	<script src="<s:property value="resourcesPath+'MODULES/commons/scripts/commons.js'"/>" type="text/javascript"></script>
</head>

<body style="margin: 0px 0px 0px 0px; overflow: hidden">

	<!-- Main container -->
	<table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%; border-width: 1px; border-style: solid; border-color: #666666; position: absolute; margin: auto;">

		<!-- ============================================================ -->
		<!-- Header -->
		<!-- ============================================================ -->
		<tr>
			<td class="headerBaseContainer"><tiles:insertAttribute name="header" /></td>
		</tr>

		<!-- ============================================================ -->
		<!-- Menu -->
		<!-- ============================================================ -->
		<tiles:insertAttribute name="menu" />

		<!-- ============================================================ -->
		<!-- Body -->
		<!-- ============================================================ -->
		<tr>
			<td align="center"><div class="bodyBaseContainer">
					<tiles:insertAttribute name="body" />
				</div></td>
		</tr>

		<!-- ============================================================ -->
		<!-- Footer -->
		<!-- ============================================================ -->
		<tr>
			<td class="footerBaseContainer"><tiles:insertAttribute name="footer" /></td>
		</tr>
	</table>

	<!-- ============================================================ -->
	<!-- Subcontainers of validators / tooltip / popup -->
	<!-- ============================================================ -->
	<tiles:insertAttribute name="subcontainers" />

</body>

</html>