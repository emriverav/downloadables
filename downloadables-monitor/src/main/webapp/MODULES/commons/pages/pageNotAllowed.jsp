<!-- 
- @author: Genaro Bermúdez [12/02/2018]
- @updated: 
- @description: not allowed page
-->

<%@ page language="java" isErrorPage="true" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Liverpool | Página no permitida</title>
</head>
<body>

	<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		<tr height="65px" align="left">
			<td>&nbsp;</td>
			<td><img src="<s:property value="resourcesPath+'MODULES/commons/images/imgLogoLiverpool.gif'"/>"></td>
			<td>&nbsp;</td>
		</tr>
		<tr style="height: 480px; text-align: center;">
			<td>&nbsp;</td>
			<td><font style="display: block; font-size: 4em; font-family: arial, Helvetica, sans-serif; font-weight: 800; color: #636;">Página no permitida</font></br> <font style="font: 1em arial, Helvetica, sans-serif; font-stretch: extra-condensed; color: #666;">¡Lo sentimos! No tienes los permisos necesarios para acceder a esta página.</font></br> <font style="font: 1em arial, Helvetica, sans-serif; font-stretch: extra-condensed; color: #666;">Si necesitas ayuda comunícate al teléfono: 52 62 99 99</font></br</td>
			<td>&nbsp;</td>
		</tr>
		<tr height="65px" align="center">
			<td>&nbsp;</td>
			<td><font style="font: 1em arial, Helvetica, sans-serif; font-stretch: extra-condensed; color: #666;"><a style="color: #636;" href="<s:url value="/inicio"/>">Regresar a la aplicación Monitor downloadables</a></font></td>
			<td>&nbsp;</td>
		</tr>
	</table>

</body>
</html>