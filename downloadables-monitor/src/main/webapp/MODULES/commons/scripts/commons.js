/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: 
 * @description: js commons
 * @since-version: 1.0
 */

window.onclick = function(event) {
	if (event.target.nodeName == "A")  {
		if (event.target.id == "popupButton") return;
		if (event.target.onclick == null) return;
		window.document.getElementById("genericLoaderContainer").style.display="block";
		window.document.getElementById("genericLoaderImage").style.display="block";
		window.document.getElementById("genericLoaderImage").style.marginTop = ((screen.height/2)-100) + "px";
		window.document.getElementById("genericLoaderImage").style.marginLeft= ((screen.width/2)-110) + "px";
	}
}

function showLoading(){
	window.document.getElementById("genericLoaderContainer").style.display="block";
	window.document.getElementById("genericLoaderImage").style.display="block";
	window.document.getElementById("genericLoaderImage").style.marginTop = ((screen.height/2)-100) + "px";
	window.document.getElementById("genericLoaderImage").style.marginLeft= ((screen.width/2)-110) + "px";	
}

function noBackNavegation() {
	window.location.hash = "no-back-button";
	window.location.hash = "Again-No-back-button";
	window.onhashchange = function() {
		window.location.hash = "no-back-button";
	}
}

function redirectURL(url) {
	window.location.href=url;
}

function openPopup(url, name, width, height) {
	location.reload(true);
	var param = '';
	if ((width != null) && (width != '')) {
		param = ', width=' + width;
	} else {
		param = ', width=' + ((screen.width)-100);
	}
	if ((height != null) && (height != '')) {
		param = param + ', height=' + height;
	} else {
		param = param + ', height=' + ((screen.height)-150);
	}
	param = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable =yes, left=50, top=25 " + param;	
	windowFrame = window.open(url, name, param);
	windowFrame.focus();
}

function showHideToolTip(object, text) {
	var genericToolTip = window.document.getElementById("genericToolTip");
	var genericToolTipSide = window.document.getElementById("genericToolTipSide");
	if ((object == null) || (texto == "")) {
		genericToolTip.style.display = "none";
		genericToolTipSide.style.display = "none";
		return;
	}
	if (text.length < 20) {
		genericToolTip.style.width = "120px";
	}
	genericToolTip.style.marginTop = (object.getBoundingClientRect().top-10) + "px";
	genericToolTip.style.marginLeft= object.getBoundingClientRect().left + 25 + "px";
	genericToolTip.innerHTML = text;
	genericToolTip.style.display = "block";
	genericToolTipSide.style.display = "block";
}

function Button(label, callback) {
	this.label = label;
	this.callback = callback;
}

function showHidePopup(message, title, buttons) {
	window.document.getElementById("genericLoaderContainer").style.display="none";
	window.document.getElementById("genericLoaderImage").style.display="none";
	if ((message == null) || (title == null) || (buttons == null)) {
		window.document.getElementById("genericPopupBaseContainer").style.display = "none";
		window.document.getElementById("genericPopupContainer").style.display = "none";
		return; 
	}
	var frameButtons = "";
	for(var i=0; i<buttons.length; i++) {
		frameButtons = frameButtons + "<a id='popupButton' onclick=\"" + buttons[i].callback + "\" onMouseOver=\"this.className=\'saveButtonMouseOver\'\" onMouseOut=\"this.className=\'saveButtonMouseOut\'\" class=\"saveButtonMouseOut\">" + buttons[i].label + "</a>" + "&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	window.document.getElementById("genericPopupMessage").innerHTML = message;
	window.document.getElementById("genericPopupTitle").innerHTML = title;
	window.document.getElementById("genericPopupButtons").innerHTML = frameButtons;
	window.document.getElementById("genericPopupBaseContainer").style.display = "block";
	window.document.getElementById("genericPopupContainer").style.marginTop = ((screen.height/2) - 160) + "px";
	window.document.getElementById("genericPopupContainer").style.marginLeft= ((screen.width/2)- ((window.document.getElementById("genericPopupTable").width.replace("px", "")/2))) + "px";
	window.document.getElementById("genericPopupContainer").style.display = "block";
}

function changeArrowsCalendar(){	
	var elements;
	elements = document.getElementsByClassName('decrease');
	if(elements != null){
		for(var i=0, l=elements.length; i<l; i++){
			elements[i].innerHTML = "";
		}
		for(var i=0, l=elements.length; i<l; i++){
			if(elements[i].getAttribute("dojoattachevent")!="onClick: onIncrementWeek;"){
				elements[i].innerHTML ="&nbsp;&nbsp;&nbsp;&nbsp;";
			}
		}
	}
	elements = document.getElementsByClassName('increase');
	if(elements!=null){
		for(var i=0, l=elements.length; i<l; i++){
			elements[i].innerHTML = "";
		}
		for(var i=0, l=elements.length; i<l; i++){
			if(elements[i].getAttribute("dojoattachevent")!="onClick: onIncrementWeek;"){
				elements[i].innerHTML ="&nbsp;&nbsp;&nbsp;&nbsp;";
			}
		}
	}
}

function chanseFieldOlnyReadForName(fieldName) {
	var elements = window.document.getElementsByName(fieldName);
	var f = new Date();
	if (elements.length <= 0) {
		elements[0].value = (f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear());
	}else{
		elements[0].value = '01/01/2018'
	}
}

function numericalCapture(event){
	var charCode = (event.charCode != undefined) ? event.charCode : ((event.keyCode != null) ? event.keyCode : event.which);
	return (charCode >= 48 && charCode <= 57) || charCode == 0;
}

function amountCapture(event, object){
	var characterPoint = 46;
	var number = object.value;
	var charCode = (event.charCode != undefined) ? event.charCode : ((event.keyCode != null) ? event.keyCode : event.which);
	if (number.indexOf('.')>=0) {
		characterPoint = 48;
	}
	return (charCode >= 48 && charCode <= 57) || charCode == 0 || charCode == characterPoint;
}

function amountFormat(idObject, option){
	var object = window.document.getElementById(idObject);
	if (object != undefined) {
		if (option == 1) {
			amountFormatApply(object);
		} else if (option == 2) {
			amountFormatRemove(object);
		}
	}
}

function amountFormatApply(object) {
	var number = object.value;
	var digits = 0;
	var decimalPlaces = 2;
	var conComas = true;
	object.value = formatearNumero(number, digits, decimalPlaces, conComas);
}

function numberFormatter(number, digits, decimalPlaces, conComas){
	number = number.toString();
    var simpleNumber = '';
    for (var i = 0; i < number.length; ++i){
    	if ("0123456789.".indexOf(numero.charAt(i)) >= 0)
    		simpleNumber += number.charAt(i);
    }
    number = parseFloat(simpleNumber);
    if (isNaN(number))    number     = 0;
    if (conComas == null) conComas = false;
    if (digits == 0)     digits     = 1;
    var integerPart = (decimalPlaces > 0 ? Math.floor(number) : Math.round(number));
    var string      = "";
    for (var i = 0; i < digits || integerPart > 0; ++i){
    	if (conComas && string.match(/^\d\d\d/)) {
    		string = "," + string;
    	}
    	string      = (integerPart % 10) + string;
    	integerPart = Math.floor(integerPart / 10);
    }
    if (decimalPlaces > 0){
    	number -= Math.floor(numero);
    	number *= Math.pow(10, decimalPlaces);
    	string += "." + numberFormatter(number, decimalPlaces, 0);
    }
    return string;
}

function amountFormatRemove(object) {
	var number = object.value;
	var simpleNumber = '';
	var start = object.selectionStart;
    var end = object.selectionEnd;
    var numeroComas = 0;
    for (var i = 0; i < start - 1; ++i){
    	if (",".indexOf(number.charAt(i)) >= 0){
    		numeroComas++;
    	}
    }
    end = end - numeroComas;
	 for (var i = 0; i < number.length; ++i){
		 if ("0123456789.".indexOf(number.charAt(i)) >= 0){
			 simpleNumber += number.charAt(i);
		 }
	 }
	 object.value = simpleNumber;
	 if (object.setSelectionRange) {
		 object.setSelectionRange(start, end);
	 }
}

if (typeof document.getElementsByClassName!='function') {
    document.getElementsByClassName = function() {
        var elms = document.getElementsByTagName('*');
        var ei = new Array();
        for (i=0;i<elms.length;i++) {
            if (elms[i].getAttribute('class')) {
                ecl = elms[i].getAttribute('class').split(' ');
                for (j=0;j<ecl.length;j++) {
                    if (ecl[j].toLowerCase() == arguments[0].toLowerCase()) {
                        ei.push(elms[i]);
                    }
                }
            } else if (elms[i].className) {
                ecl = elms[i].className.split(' ');
                for (j=0;j<ecl.length;j++) {
                    if (ecl[j].toLowerCase() == arguments[0].toLowerCase()) {
                        ei.push(elms[i]);
                    }
                }
            }
        }
        return ei;
    }
}