/**
 * ============================================================ 
 * Autor: Genaro Bermúdez [20/01/2016]
 * Modificado: Oscar Longino [27/01/2017]
 * Descripción: scripts principal para las páginas comunes
 * ============================================================
 */

window.onclick = function(event) {
	if (event.target.nodeName == "A")  {
		if (event.target.id == "botonVentanaEmergente") return;
		if (event.target.onclick == null) return;
		window.document.getElementById("contenedorLoaderGenerico").style.display="block";
		window.document.getElementById("imagenLoaderGenerico").style.display="block";
		window.document.getElementById("imagenLoaderGenerico").style.marginTop = ((screen.height/2)-100) + "px";
		window.document.getElementById("imagenLoaderGenerico").style.marginLeft= ((screen.width/2)-110) + "px";
	}
}

function mostrarLoading(){
	window.document.getElementById("contenedorLoaderGenerico").style.display="block";
	window.document.getElementById("imagenLoaderGenerico").style.display="block";
	window.document.getElementById("imagenLoaderGenerico").style.marginTop = ((screen.height/2)-100) + "px";
	window.document.getElementById("imagenLoaderGenerico").style.marginLeft= ((screen.width/2)-110) + "px";	
}

function noBackNavegacion() {
	window.location.hash = "no-back-button";
	window.location.hash = "Again-No-back-button";
	window.onhashchange = function() {
		window.location.hash = "no-back-button";
	}
}

function redireccionarURL(url) {
	window.location.href=url;
}

function abrirVentanaModal(url, nombre, ancho, alto) {
	location.reload(true);
	var parametros = '';
	if ((ancho != null) && (ancho != '')) {
		parametros = ', width=' + ancho;
	} else {
		parametros = ', width=' + ((screen.width)-100);
	}
	if ((alto != null) && (alto != '')) {
		parametros = parametros + ', height=' + alto;
	} else {
		parametros = parametros + ', height=' + ((screen.height)-150);
	}
	parametros = "toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable =yes, left=50, top=25 " + parametros;	
	ventana = window.open(url,nombre,parametros);
	ventana.focus();
}

function calcularFecha() {
	marcacion = new Date();
	Hora = marcacion.getHours();
	Minutos = marcacion.getMinutes();
	dn = "a.m";
	if (Hora > 12) {
		dn = "p.m";
		Hora = Hora - 12;
	}
	if (Hora == 0) { Hora = 12; }
	if (Hora <= 9) { Hora = "0" + Hora; }
	if (Minutos <= 9) { Minutos = "0" + Minutos; }
	var Dia = new Array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
	var Mes = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",  "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
	var Hoy = new Date();
	var Anio = Hoy.getFullYear();
	var Fecha = Dia[Hoy.getDay()] + ", " + Hoy.getDate() + " de " + Mes[Hoy.getMonth()] + " de " + Anio + " &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; ";
	var Script, Total
	Script = Fecha + Hora + ":" + Minutos + " " + dn;
	Total = Script;
	document.getElementById('pie').innerHTML = Total;
	setTimeout("calcularFecha()", 60000);
}

function mostrarOcultarToolTip(objeto, texto) {
	var toolTipGenerico = window.document.getElementById("toolTipGenerico");
	var toolTipGenericoPunta = window.document.getElementById("toolTipGenericoPunta");
	if ((objeto == null) || (texto == "")) {
		toolTipGenerico.style.display = "none";
		toolTipGenericoPunta.style.display = "none";
		return;
	}
	if (texto.length < 20) {
		toolTipGenerico.style.width = "120px";
	}
	toolTipGenerico.style.marginTop = (objeto.getBoundingClientRect().top-10) + "px";
	toolTipGenerico.style.marginLeft= objeto.getBoundingClientRect().left + 25 + "px";
	toolTipGenerico.innerHTML = texto;
	toolTipGenerico.style.display = "block";
	toolTipGenericoPunta.style.display = "block";
}

function Boton(nombre, callback) {
	this.nombre = nombre;
	this.callback = callback;
}

function mostrarOcultarVentanaEmergente(mensaje, titulo, botones) {
	window.document.getElementById("contenedorLoaderGenerico").style.display="none";
	window.document.getElementById("imagenLoaderGenerico").style.display="none";
	if ((mensaje == null) || (titulo == null) || (botones == null)) {
		window.document.getElementById("contenedorBaseVentanaEmergenteGenerica").style.display = "none";
		window.document.getElementById("contenedorVentanaEmergenteGenerica").style.display = "none";
		return; 
	}
	var botonera = "";
	for(var i=0; i<botones.length; i++) {
		botonera = botonera + "<a id='botonVentanaEmergente' onclick=\"" + botones[i].callback + "\" onMouseOver=\"this.className=\'mouseEncimaBotonGuardar\'\" onMouseOut=\"this.className=\'mouseFueraBotonGuardar\'\" class=\"mouseFueraBotonGuardar\">" + botones[i].nombre + "</a>" + "&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	window.document.getElementById("mensajeVentanaEmergenteGenerica").innerHTML = mensaje;
	window.document.getElementById("tituloVentanaEmergenteGenerica").innerHTML = titulo;
	window.document.getElementById("botonesVentanaEmergenteGenerica").innerHTML = botonera;
	window.document.getElementById("contenedorBaseVentanaEmergenteGenerica").style.display = "block";
	window.document.getElementById("contenedorVentanaEmergenteGenerica").style.marginTop = ((screen.height/2) - 160) + "px";
	window.document.getElementById("contenedorVentanaEmergenteGenerica").style.marginLeft= ((screen.width/2)- ((window.document.getElementById("tablaVentanaEmergenteGenerica").width.replace("px", "")/2))) + "px";
	window.document.getElementById("contenedorVentanaEmergenteGenerica").style.display = "block";
}

function cambiarflechasCalendar(){	
	var elements;
	elements = document.getElementsByClassName('decrease');
	if(elements != null){
		for(var i=0, l=elements.length; i<l; i++){
			elements[i].innerHTML = "";
		}
		for(var i=0, l=elements.length; i<l; i++){
			if(elements[i].getAttribute("dojoattachevent")!="onClick: onIncrementWeek;"){
				elements[i].innerHTML ="&nbsp;&nbsp;&nbsp;&nbsp;";
			}
		}
	}
	elements = document.getElementsByClassName('increase');
	if(elements!=null){
		for(var i=0, l=elements.length; i<l; i++){
			elements[i].innerHTML = "";
		}
		for(var i=0, l=elements.length; i<l; i++){
			if(elements[i].getAttribute("dojoattachevent")!="onClick: onIncrementWeek;"){
				elements[i].innerHTML ="&nbsp;&nbsp;&nbsp;&nbsp;";
			}
		}
	}
}

function cambiarCampoSoloLecturaPorNombre(nombreCampo) {
	var elementos = window.document.getElementsByName(nombreCampo);
	if (elementos.length > 0) {
		elementos[0].readOnly = true;
	}
}

function capturaNumerica(event){
	var charCode = (event.charCode != undefined) ? event.charCode : ((event.keyCode != null) ? event.keyCode : event.which);
	return (charCode >= 48 && charCode <= 57) || charCode == 0;
}

function capturaMonto(event, object){
	var caracterPunto = 46;
	var numero = object.value;
	var charCode = (event.charCode != undefined) ? event.charCode : ((event.keyCode != null) ? event.keyCode : event.which);
	if (numero.indexOf('.')>=0) {
		caracterPunto = 48;
	}
	return (charCode >= 48 && charCode <= 57) || charCode == 0 || charCode == caracterPunto;
}

function formatoMonto(idObject, option){
	var object = window.document.getElementById(idObject);
	if (object != undefined) {
		if (option == 1) {
			aplicarFormatoMonto(object);
		} else if (option == 2) {
			quitarFormatoMonto(object);
		}
	}
}

function aplicarFormatoMonto(object) {
	var number = object.value;
	var digitos = 0;
	var decimalPlaces = 2;
	var conComas = true;
	object.value = formatearNumero(number, digitos, decimalPlaces, conComas);
}

function formatearNumero(numero, digitos, decimalPlaces, conComas){
	numero = numero.toString();
    var numeroSimple = '';
    for (var i = 0; i < numero.length; ++i){
    	if ("0123456789.".indexOf(numero.charAt(i)) >= 0)
    		numeroSimple += numero.charAt(i);
    }
    numero = parseFloat(numeroSimple);
    if (isNaN(numero))    numero     = 0;
    if (conComas == null) conComas = false;
    if (digitos == 0)     digitos     = 1;
    var integerPart = (decimalPlaces > 0 ? Math.floor(numero) : Math.round(numero));
    var string      = "";
    for (var i = 0; i < digitos || integerPart > 0; ++i){
    	if (conComas && string.match(/^\d\d\d/)) {
    		string = "," + string;
    	}
    	string      = (integerPart % 10) + string;
    	integerPart = Math.floor(integerPart / 10);
    }
    if (decimalPlaces > 0){
    	numero -= Math.floor(numero);
    	numero *= Math.pow(10, decimalPlaces);
    	string += "." + formatearNumero(numero, decimalPlaces, 0);
    }
    return string;
}

function quitarFormatoMonto(object) {
	var numero = object.value;
	var numeroSimple = '';
	var start = object.selectionStart;
    var end = object.selectionEnd;
    var numeroComas = 0;
    for (var i = 0; i < start - 1; ++i){
    	if (",".indexOf(numero.charAt(i)) >= 0){
    		numeroComas++;
    	}
    }
    end = end - numeroComas;
	 for (var i = 0; i < numero.length; ++i){
		 if ("0123456789.".indexOf(numero.charAt(i)) >= 0){
			 numeroSimple += numero.charAt(i);
		 }
	 }
	 object.value = numeroSimple;
	 if (object.setSelectionRange) {
		 object.setSelectionRange(start, end);
	 }
}

if (typeof document.getElementsByClassName!='function') {
    document.getElementsByClassName = function() {
        var elms = document.getElementsByTagName('*');
        var ei = new Array();
        for (i=0;i<elms.length;i++) {
            if (elms[i].getAttribute('class')) {
                ecl = elms[i].getAttribute('class').split(' ');
                for (j=0;j<ecl.length;j++) {
                    if (ecl[j].toLowerCase() == arguments[0].toLowerCase()) {
                        ei.push(elms[i]);
                    }
                }
            } else if (elms[i].className) {
                ecl = elms[i].className.split(' ');
                for (j=0;j<ecl.length;j++) {
                    if (ecl[j].toLowerCase() == arguments[0].toLowerCase()) {
                        ei.push(elms[i]);
                    }
                }
            }
        }
        return ei;
    }
}