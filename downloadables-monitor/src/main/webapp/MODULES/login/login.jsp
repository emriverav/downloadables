<!-- 
- @author: Genaro Bermúdez [12/02/2018]
- @updated: 
- @description: login form
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	function login() {
		window.document.loginForm.operation.value = "userValidate";
		window.document.loginForm.submit();
	}
	window.onload = function() {
		noBackNavegation();
	}	
</script>

<!-- ============================================================ -->
<!-- Forma base -->
<!-- ============================================================ -->
<br><br><br><br><br><br>
<table class="formContainer" width="308px">

	<!-- ============================================================ -->
	<!-- Título -->
	<!-- ============================================================ -->
	<tr>
		<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;Login de la aplicación</td>
	</tr>
	
	<tr class="formSubContainer">
		<td>
			<br>
		
			<!-- ============================================================ -->
			<!-- Formulario  -->
			<!-- ============================================================ -->
			<s:form class="formSubContainer" action="login" method="post" name="loginForm" theme="simple">
				<s:hidden name="operation" />
				<table align="right" cellpadding="3" cellspacing="0" width="95%">
					<tr>
						<td><span class="validationsErrors" >*</span> Usuario</td>
						<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toLogin.user.login" size="30" /></td>
					</tr>
					<tr>
						<td><span class="validationsErrors" >*</span> Clave</td>
						<td><s:password class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toLogin.user.password" size="30"/></td>
					</tr>
		
					<!-- ============================================================ -->
					<!-- Separador intermedio -->
					<!-- ============================================================ -->
					<tr height="15px" >
						<td colspan="2" ></td>
					</tr>
					
					<!-- ============================================================ -->
					<!-- Botones de acción -->
					<!-- ============================================================ -->
					<tr>
						<td colspan="2" align="right" >
							<a onclick="login();" onMouseOver="this.className='loginButtonMouseOver';" onMouseOut="this.className='loginButtonMouseOut';" class="loginButtonMouseOut">Inicia Sesión &nbsp;&nbsp;<img src="<s:property value="resourcesPath+'MODULES/commons/images/imgAvanzar.png'"/>" ></a>
							&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
					
					<!-- ============================================================ -->
					<!-- Separador inferior -->
					<!-- ============================================================ -->
					<tr height="15px" >
						<td colspan="2" ></td>
					</tr>
					
				</table>			
			</s:form>
		</td>
	</tr>
</table>

