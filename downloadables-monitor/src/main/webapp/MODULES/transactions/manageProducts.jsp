<!-- 
- @author: Genaro Bermúdez [21/02/2018]
- @updated: Mauricio Rivera [26/03/2018]
- @description: manage transactions
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	var customerFirstChangeEmail = '';
	var customerSecondChangeEmail = '';
	window.onload = function() {
		customerFirstChangeEmail = '<s:property value="toProducts.product.customerFirstChangeEmail" />';
		customerSecondChangeEmail = '<s:property value="toProducts.product.customerSecondChangeEmail" />';
		var showGenerateOrderButton = '<s:property value="toProducts.product.showGenerateOrderButton" />';
		var showSupplyOrderButton = '<s:property value="toProducts.product.showSupplyOrderButton" />';
		var showSendCodesButton = '<s:property value="toProducts.product.showSendCodesButton" />';
		var showEditEmail = '<s:property value="toProducts.product.showEditEmail" />'
		var sendCodeRetries = <s:property value="toProducts.product.send_code_retries" />;
		var emailContainer = window.document.getElementById('emailContainer');
		var emailFirstChangeContainer = window.document
				.getElementById('emailFirstChangeContainer');
		var emailSecondChangeContainer = window.document
				.getElementById('emailSecondChangeContainer');
		var imagesContainerEdit = window.document
				.getElementById('imagesContainerEdit');
		if (customerSecondChangeEmail == "") {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = imagesContainerEdit.innerHTML;
			emailSecondChangeContainer.innerHTML = '';
		}
		if (customerFirstChangeEmail == "") {
			emailContainer.innerHTML = imagesContainerEdit.innerHTML;
			emailFirstChangeContainer.innerHTML = '';
			emailSecondChangeContainer.innerHTML = '';
		}
		if (showEditEmail == 'false') {
			emailContainer.style.display = "none";
			emailFirstChangeContainer.style.display = "none";
			emailSecondChangeContainer.style.display = "none";
		}
		if (showGenerateOrderButton == 'true') {
			window.document.getElementById('containerGenerateOrder').style.display = "block";
			emailContainer.style.display = "none";
			emailFirstChangeContainer.style.display = "none";
			emailSecondChangeContainer.style.display = "none";
		}
		if (showSupplyOrderButton == 'true') {
			window.document.getElementById('containerSupplyOrder').style.display = "block";
			emailContainer.style.display = "none";
			emailFirstChangeContainer.style.display = "none";
			emailSecondChangeContainer.style.display = "none";
		}
		if (showSendCodesButton == 'true') {
			window.document.getElementById('containerSendCodes').style.display = "block";
		}
		if (sendCodeRetries >= 3) {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = '';
			emailSecondChangeContainer.innerHTML = '';
		}
	}

	function editEmail() {
		var txtCustomerFirstChangeEmail = window.document
				.getElementById('txtCustomerFirstChangeEmail');
		var txtCustomerSecondChangeEmail = window.document
				.getElementById('txtCustomerSecondChangeEmail');
		var imagesContainerEdit = window.document
				.getElementById('imagesContainerEdit');
		var imagesContainerSave = window.document
				.getElementById('imagesContainerSave');
		var imagesContainerCancel = window.document
				.getElementById('imagesContainerCancel');
		if (customerFirstChangeEmail == "") {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = imagesContainerSave.innerHTML
					+ imagesContainerCancel.innerHTML;
			emailSecondChangeContainer.innerHTML = '';
			txtCustomerFirstChangeEmail.setAttribute("class", "textMouseOut");
			txtCustomerFirstChangeEmail.setAttribute("onmouseover",
					"this.className='textMouseOver'");
			txtCustomerFirstChangeEmail.setAttribute("onmouseout",
					"this.className='textMouseOut'");
			txtCustomerFirstChangeEmail.removeAttribute("readonly");
			txtCustomerFirstChangeEmail.focus();
			txtCustomerSecondChangeEmail.setAttribute("class",
					"textMouseDisabled");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseover");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseout");
			txtCustomerSecondChangeEmail.setAttribute("readonly", "readonly");
		} else {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = '';
			emailSecondChangeContainer.innerHTML = imagesContainerSave.innerHTML
					+ imagesContainerCancel.innerHTML;
			txtCustomerSecondChangeEmail.setAttribute("class", "textMouseOut");
			txtCustomerSecondChangeEmail.setAttribute("onmouseover",
					"this.className='textMouseOver'");
			txtCustomerSecondChangeEmail.setAttribute("onmouseout",
					"this.className='textMouseOut'");
			txtCustomerSecondChangeEmail.removeAttribute("readonly");
			txtCustomerSecondChangeEmail.focus();
			txtCustomerFirstChangeEmail.setAttribute("class",
					"textMouseDisabled");
			txtCustomerFirstChangeEmail.removeAttribute("onmouseover");
			txtCustomerFirstChangeEmail.removeAttribute("onmouseout");
			txtCustomerFirstChangeEmail.setAttribute("readonly", "readonly");
		}
	}

	function saveEmail() {
		window.document.formProducts.operation.value = 'saveEmail';
		if (customerFirstChangeEmail == "") {
			window.document.formProducts.operationEmail.value = '1';
		} else {
			window.document.formProducts.operationEmail.value = '2';
		}
		window.document.formProducts.submit();
	}

	function cancelEmail() {
		var txtCustomerFirstChangeEmail = window.document
				.getElementById('txtCustomerFirstChangeEmail');
		var txtCustomerSecondChangeEmail = window.document
				.getElementById('txtCustomerSecondChangeEmail');
		var imagesContainerEdit = window.document
				.getElementById('imagesContainerEdit');
		var imagesContainerSave = window.document
				.getElementById('imagesContainerSave');
		var imagesContainerCancel = window.document
				.getElementById('imagesContainerCancel');
		if (customerFirstChangeEmail == "") {
			emailContainer.innerHTML = imagesContainerEdit.innerHTML;
			emailFirstChangeContainer.innerHTML = '';
			emailSecondChangeContainer.innerHTML = '';
			txtCustomerFirstChangeEmail.setAttribute("class",
					"textMouseDisabled");
			txtCustomerFirstChangeEmail.removeAttribute("onmouseover");
			txtCustomerFirstChangeEmail.removeAttribute("onmouseout");
			txtCustomerFirstChangeEmail.setAttribute("readonly", "readonly");
			txtCustomerFirstChangeEmail.value = "";
			txtCustomerSecondChangeEmail.setAttribute("class",
					"textMouseDisabled");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseover");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseout");
			txtCustomerSecondChangeEmail.setAttribute("readonly", "readonly");
			txtCustomerSecondChangeEmail.value = "";
		} else {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = imagesContainerEdit.innerHTML;
			emailSecondChangeContainer.innerHTML = '';
			txtCustomerSecondChangeEmail.setAttribute("class",
					"textMouseDisabled");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseover");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseout");
			txtCustomerSecondChangeEmail.setAttribute("readonly", "readonly");
			txtCustomerSecondChangeEmail.value = "";
		}
	}

	function manageTransactions() {
		window.document.formProducts.operation.value = "back";
		window.document.formProducts.action = 'manageTransactions';
		window.document.formProducts.submit();
	}

	function reportPdf() {
		openPopup('reportProducts', 'Products');
	}

	function reportExcel() {
		openPopup('reportProductsExcel', 'Products');
	}

	function generateOrder() {
		window.document.formProducts.operation.value = 'generateOrder';
		window.document.formProducts.submit();
	}

	function supplyOrder() {
		window.document.formProducts.operation.value = 'generateOrder';
		window.document.formProducts.submit();
	}

	function sendCodes() {
		window.document.formProducts.operation.value = 'sendCodes';
		window.document.formProducts.submit();
	}
</script>

<!-- ============================================================ -->
<!-- Forma base -->
<!-- ============================================================ -->
<table class="formContainer" width="1250px">

	<!-- ============================================================ -->
	<!-- Título -->
	<!-- ============================================================ -->
	<tr>
		<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;Detalle de la orden</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Search -->
	<!-- ============================================================ -->
	<tr class="filtersBarFormContainer">
		<td><s:form action="manageProducts" method="post" name="formProducts" theme="simple">
				<s:hidden name="refTransId" />
				<s:hidden name="operation" />
				<s:hidden name="operationEmail" />
				<table class="filtersBarFormContainer" cellpadding="5" cellspacing="5" width="100%">
					<tr valign="top">

						<!-- Order info -->
						<td>
							<div style="width: 395px; height: 235px; border: thin solid #999999;">
								<table cellpadding="3" cellspacing="3" width="100%">
									<tr>
										<td colspan="2"><b>Datos de la Orden</b></td>
									</tr>
									<tr>
										<td width="90px">Nro.:</td>
										<td><s:textfield readonly="true" style="text-align:center;" class="textMouseDisabled" name="toProducts.product.orderId" size="20" /></td>
									</tr>
									<tr>
										<td>Referencia:</td>
										<td><s:textfield readonly="true" style="text-align:center;" class="textMouseDisabled" name="toProducts.product.refTransId" size="20" /></td>
									</tr>
									<tr>
										<td>Estatus:</td>
										<td><s:textfield readonly="true" class="textMouseDisabled" name="toProducts.product.orderStatus" size="40" /></td>
									</tr>
									<tr>
										<td>Reenvíos de correo:</td>
										<td><s:textfield readonly="true" class="textMouseDisabled" name="toProducts.product.send_code_retries" size="2" /></td>
									</tr>
								</table>
							</div>
						</td>

						<!-- Customer info -->
						<td>
							<div style="width: 420px; height: 235px; border: thin solid #999999;">
								<table cellpadding="3" cellspacing="3" width="100%">
									<tr>
										<td colspan="2"><b>Datos del Cliente</b></td>
									</tr>
									<tr>
										<td width="140px">Nombres y apellidos:</td>
										<td><s:textfield readonly="true" class="textMouseDisabled" name="toProducts.product.customerFirstName" value="%{toProducts.product.customerFirstName} %{toProducts.product.customerApaterno} %{toProducts.product.customerAMaterno}" size="30" /></td>
									</tr>
									<tr>
										<td>Tipo de usuario:</td>
										<td><s:textfield readonly="true" class="textMouseDisabled" name="toProducts.product.loginId" size="30" /></td>
									</tr>
									<tr>
										<td>Email:</td>
										<td><s:textfield readonly="true" class="textMouseDisabled" name="toProducts.product.customerEmail" size="30" /> <span id="emailContainer"></span></td>
									</tr>
									<tr>
										<td>1er. cambio de correo:</td>
										<td><s:textfield class="textMouseDisabled" readonly="true" id="txtCustomerFirstChangeEmail" name="toProducts.product.customerFirstChangeEmail" size="30" /> <span id="emailFirstChangeContainer"></span></td>
									</tr>
									<tr>
										<td>2do. cambio de correo:</td>
										<td><s:textfield class="textMouseDisabled" readonly="true" id="txtCustomerSecondChangeEmail" name="toProducts.product.customerSecondChangeEmail" size="30" /> <span id="emailSecondChangeContainer"></span></td>
									</tr>
									<tr>
										<td>Teléfono:</td>
										<td><s:textfield readonly="true" class="textMouseDisabled" name="toProducts.product.customerCelphone" size="30" /></td>
									</tr>
								</table>
							</div>
						</td>

						<td width="250px"></br> <a onclick="manageTransactions();" onMouseOver="this.className='backButtonMouseOver'" onMouseOut="this.className='backButtonMouseOut'" class="backButtonMouseOut"><img src="<s:property value="resourcesPath+'MODULES/commons/images/imgRegresar.png'"/>">&nbsp;&nbsp;Regresar</a></td>

						<td width="20%" align="right">
							<table cellpadding="0" align="right" cellspacing="0" width="100%">
								<tr height="215px" valign="top">
									<td align="right"><img style="position: relative; top: 3px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgReportePdf.gif'"/>" title="Ver PDF" onclick="reportPdf();"> <a onclick="reportPdf();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Pdf</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img
										style="position: relative; top: 3px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgReporteExcel.gif'"/>" title="Ver EXCEL" onclick="reportExcel();"> <a onclick="reportExcel();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Excel</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								</tr>
								<tr height="20px" valign="bottom">
									<td align="right"><span id="containerGenerateOrder" style="display: none;"><a onclick="generateOrder();" onMouseOver="this.className='findButtonMouseOver';" onMouseOut="this.className='findButtonMouseOut';" class="findButtonMouseOut">Generar orden</a></span> <span id="containerSupplyOrder" style="display: none;"><a onclick="supplyOrder();" onMouseOver="this.className='findButtonMouseOver';" onMouseOut="this.className='findButtonMouseOut';" class="findButtonMouseOut">Surtir
												orden</a></span> <span id="containerSendCodes" style="display: none;"><a onclick="sendCodes();" onMouseOver="this.className='findButtonMouseOver';" onMouseOut="this.className='findButtonMouseOut';" class="findButtonMouseOut">Reenviar códigos</a></span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</s:form></td>
	</tr>

	<!-- ============================================================ -->
	<!-- Aux buttons -->
	<!-- ============================================================ -->
	<div id="imagesContainerEdit" style="display: none;">
		&nbsp;&nbsp;<img style="position: relative; top: 3px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgEditar.png'"/>" title="Editar correo" onclick="editEmail();">
	</div>
	<div id="imagesContainerSave" style="display: none;">
		&nbsp;&nbsp;<img style="position: relative; top: 3px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgGuardar.png'"/>" title="Guardar correo" onclick="saveEmail();">
	</div>
	<div id="imagesContainerCancel" style="display: none;">
		&nbsp;&nbsp;<img style="position: relative; top: 3px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgNoVerificada.png'"/>" title="Cancelar edición del correo" onclick="cancelEmail();">
	</div>

	<!-- ============================================================ -->
	<!-- Registros -->
	<!-- ============================================================ -->
	<tr>
		<td>
			<div style="overflow: auto; height: 185px;">
				<table class="listContainer" cellpadding="5" cellspacing="1">
					<tr class="listContainerHeader">
						<td align="center" width="9%">Sku Liverpool</td>
						<td align="center" width="20%">Descripción</td>
						<td align="center" width="9%">Sku Proveedor</td>
						<td align="center" width="6%">Cantidad</td>
						<td align="center" width="13%">Fecha de creación</td>
						<td align="center" width="10%">Proveedor</td>
						<td align="center" width="9%">¿Generado?</td>
						<td align="center" width="11%">¿Redimido?</td>
						<td align="center" width="13%">Fecha</td>
					</tr>
					<s:iterator value="toProducts.products">
						<tr class="listContainerBodyMouseOut" onMouseOver="this.className='listContainerBodyMouseOver'" onMouseOut="this.className='listContainerBodyMouseOut'">
							<td align="center"><s:property value="sku" /></td>
							<td><s:property value="description" /></td>
							<td align="center"><s:property value="externalId" /></td>
							<td align="center"><s:property value="quantity" /></td>
							<td align="center"><s:date format="dd/MM/yyyy (hh:mm a)" name="creation_date" /></td>
							<td align="center"><s:property value="supplierName" default="---" /></td>
							<td align="center"><s:property value="codeMessage" /></td>
							<td align="center"><s:property value="redeemStatus" /></td>
							<td align="center"><s:property value="redeemDate" /></td>
							<td align="center"><s:date format="dd/MM/yyyy" name="redeemDateBegin" /> <s:if test="%{redeemDateEnd != null}"> - <s:date format="dd/MM/yyyy" name="redeemDateEnd" />
								</s:if></td>
							</td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</td>
	</tr>
</table>