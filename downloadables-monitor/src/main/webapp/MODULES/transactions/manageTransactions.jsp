<!-- 
- @author: Genaro Bermúdez [19/02/2018]
- @updated: Genaro Bermúdez [23/02/2018]
- @description: manage transactions
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<%@ taglib uri="/struts-dojo-tags" prefix="sx"%>

<script type="text/javascript">
// window.onload = function() {
// 	chanseFieldOlnyReadForName('dojo.toTransactions.transactionsFilters.start');
// 	chanseFieldOlnyReadForName('dojo.toTransactions.transactionsFilters.end');
// };

	function findTransactions() {
		window.document.formTransactions.operation.value = "search";
		window.document.formTransactions.submit();
	}

	function cleanFilters() {
		window.document.formTransactions.operation.value = "clean";
		window.document.formTransactions.submit();
	}

	function manageProducts(refTransId) {
		window.document.formTransactions.refTransId.value = refTransId;
		window.document.formTransactions.action = 'manageProducts';
		window.document.formTransactions.submit();
	}

	function reportPdf() {
		openPopup('reportTransactions', 'Transactions');
	}

	function reportExcel() {
		openPopup('reportTransactionsExcel', 'Transactions');
	}

	function expandContract() {
		var imgObject = window.document.getElementById('imgFilter');
		var divObject = window.document.getElementById('divFilter');
		var divObjectBr = window.document.getElementById('divFilterBr');
		var rowsContainer = window.document.getElementById('rowsContainer');
		var n = imgObject.src.indexOf("Desplegado");
		if (n == -1) {
			imgObject.src = "<s:property value='resourcesPath'/>"
					+ "MODULES/commons/images/imgDesplegado.png";
			divObject.style.display = "block";
			divObjectBr.style.display = "block";
			rowsContainer.style.height = "200px";
		} else {
			imgObject.src = "<s:property value='resourcesPath'/>"
					+ "MODULES/commons/images/imgReplegado.png";
			divObject.style.display = "none";
			divObjectBr.style.display = "none";
			rowsContainer.style.height = "390px";
		}
	}
</script>

<!-- ============================================================ -->
<!-- Forma base -->
<!-- ============================================================ -->
<table class="formContainer" width="1250px">

	<!-- ============================================================ -->
	<!-- Título -->
	<!-- ============================================================ -->
	<tr>
		<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;Transacciones recibidas por el ADAPTER</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Search -->
	<!-- ============================================================ -->
	<tr class="filtersBarFormContainer">
		<td>
			<table class="filtersBarFormContainer" cellpadding="5" cellspacing="5">
				<tr>
					<td width="200px" onclick="expandContract();" style="cursor: pointer;"><a onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Filtros de búsqueda</a> <img id="imgFilter" style="position: relative; top: 3px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgDesplegado.png'"/>" title="Expandir / Contraer filtros de búsqueda"></td>
					<td align="right"><img style="position: relative; top: 3px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgReportePdf.gif'"/>" title="Ver PDF" onclick="reportPdf();"> <a onclick="reportPdf();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Pdf</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <img
						style="position: relative; top: 3px; cursor: pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgReporteExcel.gif'"/>" title="Ver EXCEL" onclick="reportExcel();"> <a onclick="reportExcel();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Excel</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</table>

			<div id="divFilter">
				<s:form action="manageTransactions" method="post" name="formTransactions" theme="simple">
					<s:hidden name="refTransId" />
					<s:hidden name="operation" />
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<tr valign="top">

							<td width="10px">&nbsp;</td>

							<!-- Creation Date -->
							<td>
								<div style="width: 160px; height: 170px; border: thin solid #999999;">
									<table cellpadding="3" cellspacing="3" width="100%">
										<tr>
											<td colspan="2"><b>Fecha de creación</b></td>
										</tr>
										<tr>
											<td>Inicio:</td>
											<td><sx:datetimepicker startDate="2017" name="toTransactions.transactionsFilters.start" disabled="%{disabled}" displayFormat="dd/MM/yyyy" value="%{toTransactions.transactionsFilters.start}" style="width:75px; text-align:center;" weekStartsOn="0" iconPath="%{resourcesPath+'MODULES/commons/images/imgCalendario.png'}" cssClass="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" /></td>
										</tr>
										<tr>
											<td>Fin:</td>
											<td><sx:datetimepicker startDate="2017/01/01" name="toTransactions.transactionsFilters.end" disabled="%{disabled}" displayFormat="dd/MM/yyyy" value="%{toTransactions.transactionsFilters.end}" style="width:75px; text-align:center;" weekStartsOn="0" iconPath="%{resourcesPath+'MODULES/commons/images/imgCalendario.png'}" cssClass="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" /></td>
										</tr>
									</table>
								</div>
							</td>

							<td width="10px">&nbsp;</td>

							<!-- Channel -->
							<td>
								<div style="width: 116px; height: 170px; border: thin solid #999999;">
									<table cellpadding="3" cellspacing="3" width="100%">
										<tr>
											<td colspan="2"><b>Canal de venta</b></td>
										</tr>
										<tr>
											<td><s:checkbox name="toTransactions.transactionsFilters.channelWeb" value="toTransactions.transactionsFilters.channelWeb" /></td>
											<td>Web</td>
										</tr>
										<tr>
											<td><s:checkbox name="toTransactions.transactionsFilters.channelWap" value="toTransactions.transactionsFilters.channelWap" /></td>
											<td>Wap</td>
										</tr>
										<tr>
											<td><s:checkbox name="toTransactions.transactionsFilters.channelApp" value="toTransactions.transactionsFilters.channelApp" /></td>
											<td>App</td>
										</tr>
										<tr>
											<td><s:checkbox name="toTransactions.transactionsFilters.channelCsc" value="toTransactions.transactionsFilters.channelCsc" /></td>
											<td>Csc</td>
										</tr>
									</table>
								</div>
							</td>

							<td width="10px">&nbsp;</td>

							<!-- Customer info -->
							<td>
								<div style="width: 235px; height: 170px; border: thin solid #999999;">
									<table cellpadding="3" cellspacing="3" width="100%">
										<tr>
											<td colspan="2"><b>Datos personales</b></td>
										</tr>
										<tr>
											<td>Nombres:</td>
											<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toTransactions.transactionsFilters.customerFirstName" size="20" /></td>
										</tr>
										<tr>
											<td>Apellidos:</td>
											<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toTransactions.transactionsFilters.customerApaterno" size="20" /></td>
										</tr>
										<tr>
											<td>Email:</td>
											<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toTransactions.transactionsFilters.customerEmail" size="20" /></td>
										</tr>
										<tr>
											<td>Teléfono:</td>
											<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toTransactions.transactionsFilters.customerCelphone" size="20" /></td>
										</tr>
									</table>
								</div>
							</td>

							<td width="10px">&nbsp;</td>

							<!-- User type -->
							<td>
								<div style="width: 125px; height: 170px; border: thin solid #999999;">
									<table cellpadding="3" cellspacing="3" width="100%">
										<tr>
											<td colspan="2"><b>Tipo de usuario</b></td>
										</tr>
										<tr>
											<td><s:checkbox name="toTransactions.transactionsFilters.userTypeRegistered" value="toTransactions.transactionsFilters.userTypeRegistered" /></td>
											<td>Registrado</td>
										</tr>
										<tr>
											<td><s:checkbox name="toTransactions.transactionsFilters.userTypeInvited" value="toTransactions.transactionsFilters.userTypeInvited" /></td>
											<td>Invitado</td>
										</tr>
									</table>
								</div>
							</td>

							<td width="10px">&nbsp;</td>

							<!-- Order info -->
							<td>
								<div style="width: 275px; height: 170px; border: thin solid #999999;">
									<table cellpadding="3" cellspacing="3" width="100%">
										<tr>
											<td colspan="2"><b>Dato de compra</b></td>
										</tr>
										<tr>
											<td>Sku:</td>
											<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toTransactions.transactionsFilters.sku" size="20" /></td>
										</tr>
										<tr>
											<td>Referencia:</td>
											<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toTransactions.transactionsFilters.refTransId" size="20" /></td>
										</tr>
										<tr>
											<td>Orden proveedor:</td>
											<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="toTransactions.transactionsFilters.orderId" size="20" /></td>
										</tr>
									</table>
								</div>
							</td>

							<!-- Find / Clean buttons -->
							<td width="20%"></br> <a onclick="findTransactions();" onMouseOver="this.className='findButtonMouseOver';" onMouseOut="this.className='findButtonMouseOut';" class="findButtonMouseOut">Buscar</a> </br> </br> <a onclick="cleanFilters();" onMouseOver="this.className='findButtonMouseOver';" onMouseOut="this.className='findButtonMouseOut';" class="findButtonMouseOut">Limpiar</a></td>

						</tr>
					</table>
				</s:form>
			</div>

			<div id="divFilterBr">
				<br>
			</div>

		</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Registros -->
	<!-- ============================================================ -->
	<tr>
		<td>
			<div style="overflow: auto; height: 200px;" id="rowsContainer">
				<table class="listContainer" cellpadding="5" cellspacing="1">
					<tr class="listContainerHeader">
						<td align="center" width="10%">Referencia</td>
						<td align="center" width="10%">Pedido</td>
						<td align="center" width="9%">Tipo usuario</td>
						<td align="center" width="25%">Email</td>
						<td align="center" width="24%">Nombre</td>
						<td align="center" width="11%">Canal de venta</td>
						<td align="center" width="11%">Fecha de creación</td>
					</tr>
					<s:iterator value="toTransactions.transactions">
						<tr class="listContainerBodyMouseOut" onMouseOver="this.className='listContainerBodyMouseOver'" onMouseOut="this.className='listContainerBodyMouseOut'">
							<td align="center"><a style="color: blue; text-decoration: underline; cursor: pointer;" onclick="manageProducts('<s:property value="refTransId"/>');"><s:property value="refTransId" /></a></td>
							<td align="center"><s:property value="shippingGroup" /></td>
							<td align="center"><s:property value="loginId" /></td>
							<td><s:property value="customerEmail" /></td>
							<td><s:property value="customerFirstName" /> <s:property value="customerApaterno" /> <s:property value="customerAMaterno" /></td>
							<td align="center"><s:property value="channel" /></td>
							<td align="center"><s:date format="dd/MM/yyyy" name="creationDate" /></td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Paginador -->
	<!-- ============================================================ -->
	<tr class="paginatorContainer">
		<td><s:include value="/MODULES/commons/pages/paginator.jsp">
				<s:param name="fieldsToFilter"></s:param>
				<s:param name="idCampoFiltro"></s:param>
				<s:param name="action">manageTransactions</s:param>
			</s:include></td>
	</tr>
</table>