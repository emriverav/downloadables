<!-- 
- @author: Genaro Bermúdez [21/02/2018]
- @updated: Genaro Bermúdez [24/02/2018]
- @description: manage transactions
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	
	var customerFirstChangeEmail = '';
	var customerSecondChangeEmail = '';
	window.onload = function(){
		customerFirstChangeEmail = '<s:property value="toStatus.status.customerFirstChangeEmail" />';
		customerSecondChangeEmail = '<s:property value="toStatus.status.customerSecondChangeEmail" />';
		var emailContainer = window.document.getElementById('emailContainer');
		var emailFirstChangeContainer = window.document.getElementById('emailFirstChangeContainer');
		var emailSecondChangeContainer = window.document.getElementById('emailSecondChangeContainer');
		var imagesContainerEdit = window.document.getElementById('imagesContainerEdit');
		if (customerSecondChangeEmail == "") {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = imagesContainerEdit.innerHTML;
			emailSecondChangeContainer.innerHTML = '';
		}
		if (customerFirstChangeEmail == "") {
			emailContainer.innerHTML = imagesContainerEdit.innerHTML;
			emailFirstChangeContainer.innerHTML = '';
			emailSecondChangeContainer.innerHTML = '';
		} 
	}

	function manageProducts() {
		window.document.formStatus.action = 'manageProducts';
		window.document.formStatus.submit();
	}

	function reportPdf() {		
		openPopup('reportStatus', 'Status');
	}

	function reportExcel() {		
		openPopup('reportStatusExcel', 'Status');
	}

	function sendCode(sku){
		window.document.formStatus.operation.value = 'sendCode';
		window.document.formStatus.sku.value = sku;
		window.document.formStatus.submit();
	}

	function editEmail(){
		var txtCustomerFirstChangeEmail = window.document.getElementById('txtCustomerFirstChangeEmail');
		var txtCustomerSecondChangeEmail = window.document.getElementById('txtCustomerSecondChangeEmail');
		var imagesContainerEdit = window.document.getElementById('imagesContainerEdit');
		var imagesContainerSave = window.document.getElementById('imagesContainerSave');
		var imagesContainerCancel = window.document.getElementById('imagesContainerCancel');
		if (customerFirstChangeEmail == "") {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = imagesContainerSave.innerHTML + imagesContainerCancel.innerHTML;
			emailSecondChangeContainer.innerHTML = '';
			txtCustomerFirstChangeEmail.setAttribute("class","textMouseOut");
			txtCustomerFirstChangeEmail.setAttribute("onmouseover","this.className='textMouseOver'");
			txtCustomerFirstChangeEmail.setAttribute("onmouseout","this.className='textMouseOut'");
			txtCustomerFirstChangeEmail.removeAttribute("readonly");
			txtCustomerFirstChangeEmail.focus();
			txtCustomerSecondChangeEmail.setAttribute("class","textMouseDisabled");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseover");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseout");
			txtCustomerSecondChangeEmail.setAttribute("readonly","readonly");
		} else {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = '';
			emailSecondChangeContainer.innerHTML = imagesContainerSave.innerHTML + imagesContainerCancel.innerHTML;
			txtCustomerSecondChangeEmail.setAttribute("class","textMouseOut");
			txtCustomerSecondChangeEmail.setAttribute("onmouseover","this.className='textMouseOver'");
			txtCustomerSecondChangeEmail.setAttribute("onmouseout","this.className='textMouseOut'");
			txtCustomerSecondChangeEmail.removeAttribute("readonly");
			txtCustomerSecondChangeEmail.focus();
			txtCustomerFirstChangeEmail.setAttribute("class","textMouseDisabled");
			txtCustomerFirstChangeEmail.removeAttribute("onmouseover");
			txtCustomerFirstChangeEmail.removeAttribute("onmouseout");
			txtCustomerFirstChangeEmail.setAttribute("readonly","readonly");
		}
	}

	function saveEmail(){
		window.document.formStatus.operation.value = 'saveEmail';
		if (customerFirstChangeEmail == "") {
			window.document.formStatus.operationEmail.value = '1';
		} else {
			window.document.formStatus.operationEmail.value = '2';
		}
		window.document.formStatus.submit();
	}

	function cancelEmail(){
		var txtCustomerFirstChangeEmail = window.document.getElementById('txtCustomerFirstChangeEmail');
		var txtCustomerSecondChangeEmail = window.document.getElementById('txtCustomerSecondChangeEmail');
		var imagesContainerEdit = window.document.getElementById('imagesContainerEdit');
		var imagesContainerSave = window.document.getElementById('imagesContainerSave');
		var imagesContainerCancel = window.document.getElementById('imagesContainerCancel');
		if (customerFirstChangeEmail == "") {
			emailContainer.innerHTML = imagesContainerEdit.innerHTML;
			emailFirstChangeContainer.innerHTML = '';
			emailSecondChangeContainer.innerHTML = '';
			txtCustomerFirstChangeEmail.setAttribute("class","textMouseDisabled");
			txtCustomerFirstChangeEmail.removeAttribute("onmouseover");
			txtCustomerFirstChangeEmail.removeAttribute("onmouseout");
			txtCustomerFirstChangeEmail.setAttribute("readonly","readonly");
			txtCustomerFirstChangeEmail.value = "";
			txtCustomerSecondChangeEmail.setAttribute("class","textMouseDisabled");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseover");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseout");
			txtCustomerSecondChangeEmail.setAttribute("readonly","readonly");
			txtCustomerSecondChangeEmail.value = "";
		} else {
			emailContainer.innerHTML = '';
			emailFirstChangeContainer.innerHTML = imagesContainerEdit.innerHTML;
			emailSecondChangeContainer.innerHTML = '';
			txtCustomerSecondChangeEmail.setAttribute("class","textMouseDisabled");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseover");
			txtCustomerSecondChangeEmail.removeAttribute("onmouseout");
			txtCustomerSecondChangeEmail.setAttribute("readonly","readonly");
		}
	}
	
</script>

<!-- ============================================================ -->
<!-- Forma base -->
<!-- ============================================================ -->
<table class="formContainer" width="1250px">

	<!-- ============================================================ -->
	<!-- Título -->
	<!-- ============================================================ -->
	<tr>
		<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;Estatus</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Search -->
	<!-- ============================================================ -->
	<tr class="filtersBarFormContainer">
		<td>
			<s:form action="manageStatus" method="post" name="formStatus" theme="simple">
				<s:hidden name="sku" />
				<s:hidden name="shippingGroup" />
				<s:hidden name="operation" />
				<s:hidden name="operationEmail" />
				<table class="filtersBarFormContainer" cellpadding="5" cellspacing="5" width="100%">
					<tr valign="top" >
						
						<!-- Status info -->
						<td>
							<div style="width:440px; border:thin solid #999999; ">
								<table cellpadding="3" cellspacing="3" width="100%">
									<tr><td colspan="2"><b>Consulta de estatus</b></td></tr>
									<tr><td width="170px">Correo electrónico:</td><td><s:textfield class="textMouseDisabled" readonly="true" name="toStatus.status.customerEmail" size="30" /> <span id="emailContainer"></span></td></tr>
									<tr><td>Primer cambio de correo:</td><td><s:textfield class="textMouseDisabled" readonly="true" id="txtCustomerFirstChangeEmail" name="toStatus.status.customerFirstChangeEmail" size="30" /> <span id="emailFirstChangeContainer"></span></td></tr>
									<tr><td>Segundo cambio de correo:</td><td><s:textfield class="textMouseDisabled" readonly="true" id="txtCustomerSecondChangeEmail" name="toStatus.status.customerSecondChangeEmail" size="30" /> <span id="emailSecondChangeContainer"></span></td></tr>
									<tr><td>Estado de la transacción:</td><td><s:textfield class="textMouseDisabled" readonly="true" name="toStatus.status.status" size="25" /></td></tr>
									<tr><td>Último resultado:</td><td><s:textfield class="textMouseDisabled" readonly="true" name="toStatus.status.lastStatus" size="25" /></td></tr>
								</table>
							</div>
						</td>
						
						<td width="250px">
							</br>
							<a onclick="manageProducts();" onMouseOver="this.className='backButtonMouseOver'" onMouseOut="this.className='backButtonMouseOut'" class="backButtonMouseOut"><img src="<s:property value="resourcesPath+'MODULES/commons/images/imgRegresar.png'"/>">&nbsp;&nbsp;Regresar</a>
						</td>
							
						<td width="50%" align="right">
							<img style="position:relative; top:3px; cursor:pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgReportePdf.gif'"/>" title="Ver PDF" onclick="reportPdf();"> <a onclick="reportPdf();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Pdf</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<img style="position:relative; top:3px; cursor:pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgReporteExcel.gif'"/>" title="Ver EXCEL" onclick="reportExcel();"> <a onclick="reportExcel();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Excel</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>
				</table>
			</s:form>
		</td>
	</tr>
	
	<!-- ============================================================ -->
	<!-- Aux buttons -->
	<!-- ============================================================ -->
	<div id="imagesContainerEdit" style="display:none;">&nbsp;&nbsp;<img style="position:relative; top:3px; cursor:pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgEditar.png'"/>" title="Editar correo" onclick="editEmail();"></div>
	<div id="imagesContainerSave" style="display:none;">&nbsp;&nbsp;<img style="position:relative; top:3px; cursor:pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgGuardar.png'"/>" title="Guardar correo" onclick="saveEmail();"></div>
	<div id="imagesContainerCancel" style="display:none;">&nbsp;&nbsp;<img style="position:relative; top:3px; cursor:pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgNoVerificada.png'"/>" title="Cancelar edición del correo" onclick="cancelEmail();"></div>
	
	<!-- ============================================================ -->
	<!-- Registros -->
	<!-- ============================================================ -->
	<tr>
		<td>
			<div style="overflow: auto; height: 220px;">
				<table class="listContainer" cellpadding="5" cellspacing="1">
					<tr class="listContainerHeader">
						<td align="center" width="8%" colspan="2">Acciones</td>
						<td align="center" width="12%">Sku Liverpool</td>
						<td align="center" width="31%">Descripción</td>
						<td align="center" width="12%">Sku Proveedor</td>
						<td align="center" width="13%">Índice</td>
						<td align="center" width="10%">Estatus</td>
						<td align="center" width="12%">Fecha redención</td>
						<td align="center" width="12%">Intentos</td>
					</tr>
					<s:iterator value="toStatus.status">
						<tr class="listContainerBodyMouseOut" onMouseOver="this.className='listContainerBodyMouseOver'" onMouseOut="this.className='listContainerBodyMouseOut'">
							<td align="center"><a onclick="sendCode(<s:property value="sku"/>);"><img style="cursor: pointer" src="<s:property value="resourcesPath+'MODULES/commons/images/imgEnviar.png'"/>" title="Reenviar código"></a></td>
							<td align="center"><a onclick="generateCode(<s:property value="sku"/>);"><img style="cursor: pointer" src="<s:property value="resourcesPath+'MODULES/commons/images/imgGenerar.png'"/>" title="Generar código"></a></td>
							<td align="center"><s:property value="sku" /></td>
							<td><s:property value="description" /></td>
							<td align="center"><s:property value="externalId" /></td>
							<td align="center"><s:property value="indexId" /></td>
							<td align="center"><s:property value="redeemStatus" /></td>
							<td align="center"><s:property value="redeemDate" /></td>
							<td align="center"><s:property value="redeemRetries" /></td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</td>
	</tr>
	
</table>