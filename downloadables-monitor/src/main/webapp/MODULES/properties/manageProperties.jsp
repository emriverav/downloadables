<!-- 
- @author: Genaro Bermúdez [19/02/2018]
- @updated: ---
- @description: manage properties
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	
	function editProperty(value) {
		window.document.formProperties.operation.value = "edit";
		window.document.formProperties.propertyId.value = value;
		window.document.formProperties.submit();
	}

	function report() {		
		openPopup('reportProperties', 'Properties');
	}
	
</script>

<!-- ============================================================ -->
<!-- Formulario para los redirect -->
<!-- ============================================================ -->
<s:form action="editProperty" method="post" name="formProperties" style="display:none;" theme="simple">
	<s:hidden name="operation" />
	<s:hidden name="propertyId" />
</s:form>

<!-- ============================================================ -->
<!-- Forma base -->
<!-- ============================================================ -->
<table class="formContainer" width="1250px">

	<!-- ============================================================ -->
	<!-- Título -->
	<!-- ============================================================ -->
	<tr>
		<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;Propiedades</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Search -->
	<!-- ============================================================ -->
	<tr class="filtersBarFormContainer">
		<td>
			<table class="filtersBarFormContainer" cellpadding="5" cellspacing="5" border="0" >
				<tr>
					<td>
						Nombre&nbsp;&nbsp;
						<s:textfield theme="simple" class="textMouseOver" id="filtroCuadroTexto" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="attributesPaginator.filter" size="25" />&nbsp;&nbsp;
						<a onclick="find();" onMouseOver="this.className='findButtonMouseOver';" onMouseOut="this.className='findButtonMouseOut';" class="findButtonMouseOut">Buscar</a>
					</td>
					<td align="right">
						<img style="position:relative; top:3px; cursor:pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgReporte.png'"/>" title="Ver reporte" onclick="report();"> <a onclick="report();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Ver reporte</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<!-- ============================================================ -->
	<!-- Registros -->
	<!-- ============================================================ -->
	<tr>
		<td>
			<div style="overflow: auto; height: 360px;">
				<table class="listContainer" cellpadding="5" cellspacing="1">
					<tr class="listContainerHeader">
						<td align="center" width="8%" colspan="2">Acciones</td>
						<td align="center" width="47%" style="cursor: pointer;" onclick='sort("propertyKey", "<s:property value="attributesPaginator.getOrder('propertyKey')" />");'><a href="#" onMouseOver="this.className='listContainerHeaderMouseOver'" onMouseOut="this.className='listContainerHeaderMouseOut'" class="listContainerHeaderMouseOut">Clave <img border="0" src="<s:property value="resourcesPath + attributesPaginator.getImage('propertyKey')" />"/></a></td>
						<td align="center" width="45%" style="cursor: pointer;" onclick='sort("propertyValue", "<s:property value="attributesPaginator.getOrder('propertyValue')" />");'><a href="#" onMouseOver="this.className='listContainerHeaderMouseOver'" onMouseOut="this.className='listContainerHeaderMouseOut'" class="listContainerHeaderMouseOut">Valor <img border="0" src="<s:property value="resourcesPath + attributesPaginator.getImage('propertyValue')" />"/></a></td>
					</tr>
					<s:iterator value="toProperties.properties">
						<tr class="listContainerBodyMouseOut" onMouseOver="this.className='listContainerBodyMouseOver'" onMouseOut="this.className='listContainerBodyMouseOut'">
							<td align="center"><a onclick="editProperty(<s:property value="propertyId"/>);"><img style="cursor: pointer" src="<s:property value="resourcesPath+'MODULES/commons/images/imgEditar.png'"/>" title="Editar"></a></td>
							<td align="center"><img style="cursor: pointer" src="<s:property value="resourcesPath+'MODULES/commons/images/imgEliminarDisabled.png'"/>" title="No se puede eliminar"></td>
							<td><s:property value="propertyKey" /></td>
							<td><s:property value="propertyValue" /></td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Paginador -->
	<!-- ============================================================ -->
	<tr class="paginatorContainer">
		<td>
			<s:include value="/MODULES/commons/pages/paginator.jsp">
				<s:param name="fieldsToFilter">propertyKey</s:param>
				<s:param name="idCampoFiltro">filtroCuadroTextos</s:param>
				<s:param name="action">manageProperties</s:param>
			</s:include>
		</td>
	</tr>

</table>


