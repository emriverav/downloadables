<!-- 
- @author: Genaro Bermúdez [19/02/2018]
- @updated: ---
- @description: edit property
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	function save() {
		window.document.formProperties.operation.value = "save";
		window.document.formProperties.submit();
	}

	function back() {
		window.document.formProperties.action = "manageProperties";
		window.document.formProperties.operation.value = "back";
		window.document.formProperties.submit();
	}
</script>

<!-- ============================================================ -->
<!-- Base form -->
<!-- ============================================================ -->
<br>
<br>
<table class="formContainer" width="670px">

	<!-- ============================================================ -->
	<!-- Title -->
	<!-- ============================================================ -->
	<tr>
		<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;<s:property value="formTitle" /></td>
	</tr>

	<tr class="formSubContainer">
		<td><br> 
		
				<!-- ============================================================ -->
				<!-- Form  -->
				<!-- ============================================================ -->
				<s:form class="formSubContainer" action="editProperty" method="post" name="formProperties" theme="simple">
				<s:hidden name="operation" />
				<s:hidden name="formTitle" />
				<s:hidden name="property.propertyId" />
				<table align="right" cellpadding="3" cellspacing="0" width="95%">
					<tr>
						<td>Clave</td>
						<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="property.propertyKey" readonly="true" size="50" /></td>
					</tr>
					<tr>
						<td><span class="validationsErrors">*</span> Valor</td>
						<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="property.propertyValue" size="50" /></td>
					</tr>
					<tr>
						<td><span class="validationsErrors">*</span> Descripción</td>
						<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="property.description" size="80" /></td>
					</tr>
               
					<!-- ============================================================ -->
					<!-- Space separator -->
					<!-- ============================================================ -->
					<tr height="15px">
						<td colspan="2"></td>
					</tr>

					<!-- ============================================================ -->
					<!-- Actions buttons -->
					<!-- ============================================================ -->
					<tr>
						<td colspan="2" align="right"><a onclick="back();" onMouseOver="this.className='backButtonMouseOver'" onMouseOut="this.className='backButtonMouseOut'" class="backButtonMouseOut"><img src="<s:property value="resourcesPath+'MODULES/commons/images/imgRegresar.png'"/>">&nbsp;&nbsp;Regresar</a> &nbsp; <a onclick="save();" onMouseOver="this.className='saveButtonMouseOver'" onMouseOut="this.className='saveButtonMouseOut'" class="saveButtonMouseOut">&nbsp;&nbsp;&nbsp;Guardar&nbsp;&nbsp;&nbsp;</a>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>

					<!-- ============================================================ -->
					<!-- Space separator -->
					<!-- ============================================================ -->
					<tr height="15px">
						<td colspan="2"></td>
					</tr>

				</table>
			</s:form></td>
	</tr>
</table>
