<!-- 
- @author: Genaro Bermúdez [31/01/2018]
- @updated: ---
- @description: manage users
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	
	function addUser() {
		window.document.formUsers.operation.value = "edit";
		window.document.formUsers.userId.value = "";
		window.document.formUsers.submit();
	}
	
	function editUser(value) {
		window.document.formUsers.operation.value = "edit";
		window.document.formUsers.userId.value = value;
		window.document.formUsers.submit();
	}

	function deleteUser(value) {
		window.document.formUsers.action = "manageUsers";
		window.document.formUsers.operation.value = "delete";
		window.document.formUsers.userId.value = value;
		window.document.formUsers.submit();
	}
	
	function deleteConfirmation(value) {
		var buttons = new Array(new Button("No", "showHidePopup();"), new Button("Si", "deleteUser("+value+");"));
		showHidePopup("¿Está seguro que desea eliminar el usuario?", "Mensaje de la aplicación", buttons);
	}

	function report() {		
		openPopup('reportUsers', 'Users');
	}
	
</script>

<!-- ============================================================ -->
<!-- Formulario para los redirect -->
<!-- ============================================================ -->
<s:form action="editUser" method="post" name="formUsers" style="display:none;" theme="simple">
	<s:hidden name="operation" />
	<s:hidden name="userId" />
</s:form>

<!-- ============================================================ -->
<!-- Forma base -->
<!-- ============================================================ -->
<table class="formContainer" width="1250px">

	<!-- ============================================================ -->
	<!-- Título -->
	<!-- ============================================================ -->
	<tr>
		<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;Usuarios</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Search -->
	<!-- ============================================================ -->
	<tr class="filtersBarFormContainer">
		<td>
			<table class="filtersBarFormContainer" cellpadding="5" cellspacing="5" border="0" >
				<tr>
					<td>
						Nombre&nbsp;&nbsp;
						<s:textfield theme="simple" class="textMouseOver" id="filtroCuadroTexto" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="attributesPaginator.filter" size="25" />&nbsp;&nbsp;
						<a onclick="find();" onMouseOver="this.className='findButtonMouseOver';" onMouseOut="this.className='findButtonMouseOut';" class="findButtonMouseOut">Buscar</a>
					</td>
					<td align="right">
						<img style="position:relative; top:3px; cursor:pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgAgregar.png'"/>" title="Agregar usuario" onclick="addUser();"> <a onclick="addUser();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Agregar usuario</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<img style="position:relative; top:3px; cursor:pointer;" src="<s:property value="resourcesPath+'MODULES/commons/images/imgReporte.png'"/>" title="Ver reporte" onclick="report();"> <a onclick="report();" onMouseOver="this.className='filtersBarFormContainerMouseOver'" onMouseOut="this.className='filtersBarFormContainerMouseOut'" class="filtersBarFormContainerMouseOut">Ver reporte</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<!-- ============================================================ -->
	<!-- Registros -->
	<!-- ============================================================ -->
	<tr>
		<td>
			<div style="overflow: auto; height: 360px;">
				<table class="listContainer" cellpadding="5" cellspacing="1">
					<tr class="listContainerHeader">
						<td align="center" width="8%" colspan="2">Acciones</td>
						<td align="center" width="30%" style="cursor: pointer;" onclick='sort("firstName", "<s:property value="attributesPaginator.getOrder('firstName')" />");'><a href="#" onMouseOver="this.className='listContainerHeaderMouseOver'" onMouseOut="this.className='listContainerHeaderMouseOut'" class="listContainerHeaderMouseOut">Nombre <img border="0" src="<s:property value="resourcesPath + attributesPaginator.getImage('firstName')" />"/></a></td>
						<td align="center" width="30%" style="cursor: pointer;" onclick='sort("lastName", "<s:property value="attributesPaginator.getOrder('lastName')" />");'><a href="#" onMouseOver="this.className='listContainerHeaderMouseOver'" onMouseOut="this.className='listContainerHeaderMouseOut'" class="listContainerHeaderMouseOut">Apellido <img border="0" src="<s:property value="resourcesPath + attributesPaginator.getImage('lastName')" />"/></a></td>
						<td align="center" width="32%"><a class="listContainerHeaderMouseOut">Login</a></td>
					</tr>
					<s:iterator value="toUsers.users">
						<tr class="listContainerBodyMouseOut" onMouseOver="this.className='listContainerBodyMouseOver'" onMouseOut="this.className='listContainerBodyMouseOut'">
							<td align="center"><a onclick="editUser(<s:property value="userId"/>);"><img style="cursor: pointer" src="<s:property value="resourcesPath+'MODULES/commons/images/imgEditar.png'"/>" title="Editar"></a></td>
							<td align="center"><a onclick="deleteConfirmation(<s:property value="userId"/>);"><img style="cursor: pointer" src="<s:property value="resourcesPath+'MODULES/commons/images/imgEliminar.png'"/>" title="Eliminar"></a></td>
							<td><s:property value="firstName" /></td>
							<td><s:property value="lastName" /></td>
							<td><s:property value="login" /></td>
						</tr>
					</s:iterator>
				</table>
			</div>
		</td>
	</tr>

	<!-- ============================================================ -->
	<!-- Paginador -->
	<!-- ============================================================ -->
	<tr class="paginatorContainer">
		<td>
			<s:include value="/MODULES/commons/pages/paginator.jsp">
				<s:param name="fieldsToFilter">first_Name</s:param>
				<s:param name="idCampoFiltro">filtroCuadroTexto</s:param>
				<s:param name="action">manageUsers</s:param>
			</s:include>
		</td>
	</tr>

</table>


