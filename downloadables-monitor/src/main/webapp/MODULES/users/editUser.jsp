<!-- 
- @author: Genaro Bermúdez [31/01/2018]
- @updated: ---
- @description: edit user
-->

<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>

<script type="text/javascript">
	function save() {
		window.document.formUsers.operation.value = "save";
		window.document.formUsers.submit();
	}

	function back() {
		window.document.formUsers.action = "manageUsers";
		window.document.formUsers.operation.value = "back";
		window.document.formUsers.submit();
	}
</script>

<!-- ============================================================ -->
<!-- Base form -->
<!-- ============================================================ -->
<br>
<br>
<table class="formContainer" width="380px">

	<!-- ============================================================ -->
	<!-- Title -->
	<!-- ============================================================ -->
	<tr>
		<td class="topBarFormContainer">&nbsp;&nbsp;&nbsp;<s:property value="formTitle" /></td>
	</tr>

	<tr class="formSubContainer">
		<td><br> 
		
				<!-- ============================================================ -->
				<!-- Form  -->
				<!-- ============================================================ -->
				<s:form class="formSubContainer" action="editUser" method="post" name="formUsers" theme="simple">
				<s:hidden name="operation" />
				<s:hidden name="formTitle" />
				<s:hidden name="user.userId" />
				<table align="right" cellpadding="3" cellspacing="0" width="95%">
					<tr>
						<td><span class="validationsErrors">*</span> Login</td>
						<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="user.login" size="30" /></td>
					</tr>
					<tr>
						<td><span class="validationsErrors">*</span> Nombre</td>
						<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="user.firstName" size="30" /></td>
					</tr>
					<tr>
						<td><span class="validationsErrors">*</span> Apellido</td>
						<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="user.lastName" size="30" /></td>
					</tr>

					<tr>
						<td><span class="validationsErrors">*</span> Correo</td>
						<td><s:textfield class="textMouseOut" onmouseover="this.className='textMouseOver'" onmouseout="this.className='textMouseOut'" name="user.email" size="30" /></td>
					</tr>

					<tr>
					 	<td><span class="erroresValidacion" >*</span> Perfil</td>
               			<td colspan="2"><s:select name="user.profile" disabled="%{disabled}" headerValue="--- Seleccione ---" headerKey="0" list="toUsers.profilesCatalog" style="width:100%;" value="%{user.profile}"  cssClass="comboMouseOut" onmouseover="this.className = 'comboMouseOver'" onmouseout="this.className = 'comboMouseOut'"/></td>
          			</tr>
               
					<!-- ============================================================ -->
					<!-- Space separator -->
					<!-- ============================================================ -->
					<tr height="15px">
						<td colspan="2"></td>
					</tr>

					<!-- ============================================================ -->
					<!-- Actions buttons -->
					<!-- ============================================================ -->
					<tr>
						<td colspan="2" align="right"><a onclick="back();" onMouseOver="this.className='backButtonMouseOver'" onMouseOut="this.className='backButtonMouseOut'" class="backButtonMouseOut"><img src="<s:property value="resourcesPath+'MODULES/commons/images/imgRegresar.png'"/>">&nbsp;&nbsp;Regresar</a> &nbsp; <a onclick="save();" onMouseOver="this.className='saveButtonMouseOver'" onMouseOut="this.className='saveButtonMouseOut'" class="saveButtonMouseOut">&nbsp;&nbsp;&nbsp;Guardar&nbsp;&nbsp;&nbsp;</a>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>

					<!-- ============================================================ -->
					<!-- Space separator -->
					<!-- ============================================================ -->
					<tr height="15px">
						<td colspan="2"></td>
					</tr>

				</table>
			</s:form></td>
	</tr>
</table>
