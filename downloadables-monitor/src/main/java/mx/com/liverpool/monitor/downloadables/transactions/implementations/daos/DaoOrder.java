/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.implementations.daos;

import javax.ejb.Stateless;

import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.AbstracDao;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoOrder;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToProducts;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated:
 * @description: Persistence implementation
 * @since-version: 1.0
 */
@Repository
@Slf4j
@Stateless
public class DaoOrder extends AbstracDao implements IDaoOrder {

	/** DAO query constans */
	protected final static String UPDATE_ERROR_CODE = "UPDATE ORDERS SET ERROR_CODE  = 1003 WHERE REF_TRANS_ID = ? AND SHIPPING_GROUP = ?";

	public Object findAll(Object object) {
		return null;
	}

	public ToProducts update(Object object) {
		ToProducts toProducts = (ToProducts) object;
		try {
			setRowsAffected(jdbcTemplate.update(UPDATE_ERROR_CODE, new Object[] { toProducts.getOrderDi().getRefId(), toProducts.getOrderDi().getShippingGroupId() }));
			log.info("Update ErrorCode sucessfully ");
		} catch (Exception e) {
			throw e;
		}
		return toProducts;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

}
