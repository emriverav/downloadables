/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.actions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.CellStyle;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPTable;
import com.opensymphony.xwork2.ActionContext;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.commons.implementations.services.ServiceLocator;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Button;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.utils.Validator;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Transaction;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.TransactionsFilters;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToTransactions;

/**
 * @author: Genaro Bermúdez [19/02/2018]
 * @updated: Mauricio Rivera[12/04/2018]
 * @description: struts controller ManageTransactions
 * @since-version: 1.0
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = false)
public class ManageTransactions extends GapsiCommonAction {

	private static final long serialVersionUID = -6485436993072307016L;
	private ToTransactions toTransactions;

	public String execute() throws Exception {
		super.readPopup();
		if (ActionContext.getContext().getSession().get("operation") != null && (super.getOperation() != null && !super.getOperation().equals("clean") && !super.getOperation().equals("search") && !super.getOperation().equals("back") || super.getOperation() == null)) {
			if (ActionContext.getContext().getSession().get("toTransactions") != null && ActionContext.getContext().getSession().get("operation").equals("paginator")) {
				toTransactions = (ToTransactions) ActionContext.getContext().getSession().get("toTransactions");
			}
		}
		if (toTransactions == null || (super.getOperation() != null && super.getOperation().equals("clean"))) {
			toTransactions = new ToTransactions();
			toTransactions.setTransactionsFilters(new TransactionsFilters());
			toTransactions.getTransactionsFilters().setStart(Validator.addDayToDate(new Date(), -1));
			toTransactions.getTransactionsFilters().setEnd(new Date());
		}
		if (super.getOperation() != null && super.getOperation().equals("search") && validator()) {
			this.setToTransactions((ToTransactions) ActionContext.getContext().getSession().get("toTransactions"));
			return "input";
		}
		if (super.getOperation() != null && super.getOperation().equals("back")) {
			toTransactions = (ToTransactions) ActionContext.getContext().getSession().get("toTransactions");
		}
		// toTransactions.setAudit(new Audit(null, null, "8", "Consulta Trans Gral.", super.getUserLogued().getUserId()));
		this.setToTransactions((ToTransactions) ServiceLocator.getInstance().getServiceTransactions().findAll(toTransactions));
		if (!(toTransactions.getTransactions().size() > 0)) {
			ArrayList<Button> buttons = new ArrayList<Button>();
			buttons.add(new Button("Ok", "showHidePopup();"));
			super.showHidePopup("No se encontraron registros para la búsqueda específicada ", "Mensaje de la aplicación", buttons);
			super.pageList(toTransactions.getTransactions());
			ActionContext.getContext().getSession().put("toTransactions", toTransactions);
			log.info("Has been ending ManageTransactions action");
			super.readPopup();
			return "success";
		} else {
			if (toTransactions.getTransactions().size() > 10) {
				super.setOperation("paginator");
				ActionContext.getContext().getSession().put("operation", super.getOperation());
			} else {
				ActionContext.getContext().getSession().put("operation", null);
			}
			super.pageList(toTransactions.getTransactions());
			ActionContext.getContext().getSession().put("toTransactions", toTransactions);
			log.info("Has been ending ManageTransactions action");
			super.readPopup();
			return "success";
		}
	}

	private boolean validator() {
		boolean emptyFields = false;
		boolean otherFields = false;
		super.setErrorMessages(new ArrayList<String>());
		if (toTransactions.getTransactionsFilters().getChannelWeb() || toTransactions.getTransactionsFilters().getChannelCsc() || (toTransactions.getTransactionsFilters().getCustomerFirstName() != null && !toTransactions.getTransactionsFilters().getCustomerFirstName().trim().isEmpty()) || (toTransactions.getTransactionsFilters().getCustomerApaterno() != null && !toTransactions.getTransactionsFilters().getCustomerApaterno().trim().isEmpty())
				|| (toTransactions.getTransactionsFilters().getCustomerEmail() != null && !toTransactions.getTransactionsFilters().getCustomerEmail().trim().isEmpty()) || (toTransactions.getTransactionsFilters().getCustomerCelphone() != null && !toTransactions.getTransactionsFilters().getCustomerCelphone().trim().isEmpty()) || toTransactions.getTransactionsFilters().getUserTypeInvited() || toTransactions.getTransactionsFilters().getUserTypeRegistered()
				|| (toTransactions.getTransactionsFilters().getSku() != null && !toTransactions.getTransactionsFilters().getSku().trim().isEmpty()) || (toTransactions.getTransactionsFilters().getRefTransId() != null && !toTransactions.getTransactionsFilters().getRefTransId().trim().isEmpty()) || (toTransactions.getTransactionsFilters().getOrderId() != null && !toTransactions.getTransactionsFilters().getOrderId().trim().isEmpty())) {
			otherFields = true;
		}
		if (!otherFields) {
			if (toTransactions.getTransactionsFilters().getStart() == null || toTransactions.getTransactionsFilters().getStart().equals("")) {
				super.getErrorMessages().add("Debe indicar la fecha inicio de la Orden");
				emptyFields = true;
			}
			if (toTransactions.getTransactionsFilters().getEnd() == null || toTransactions.getTransactionsFilters().getEnd().equals("")) {
				super.getErrorMessages().add("Debe indicar la fecha fin de la Orden");
				emptyFields = true;
			}
			if (emptyFields) {
				return emptyFields;
			}
		}
		if (toTransactions.getTransactionsFilters().getStart() != null && toTransactions.getTransactionsFilters().getEnd() != null) {
			if ((toTransactions.getTransactionsFilters().getStart().compareTo(toTransactions.getTransactionsFilters().getEnd())) > 0) {
				toTransactions.getTransactionsFilters().setStart(Validator.addDayToDate(new Date(), -1));
				toTransactions.getTransactionsFilters().setEnd(new Date());
				super.getErrorMessages().add("La fecha fin no puede ser menor a la fecha de inicio");
				emptyFields = true;
			}
			if ((toTransactions.getTransactionsFilters().getStart().compareTo(new Date())) > 0) {
				toTransactions.getTransactionsFilters().setStart(Validator.addDayToDate(new Date(), -1));
				toTransactions.getTransactionsFilters().setEnd(new Date());
				super.getErrorMessages().add("La fecha inicio no puede ser mayor a la fecha actual");
				emptyFields = true;
			}
		}
		if (emptyFields) {
			return emptyFields;
		}
		return false;
	}

	public String buildReport() throws Exception {
		PdfPTable table = new PdfPTable(7);
		table.addCell(cell("Referencia", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Pedido", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Tipo Usuario", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Nombre", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Email", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Canal", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Fecha", Element.ALIGN_CENTER, Font.BOLD));
		for (Transaction transaction : ((ToTransactions) ActionContext.getContext().getSession().get("toTransactionsPDF")).getTransactions()) {
			table.addCell(cell(transaction.getRefTransId().toString(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(transaction.getShippingGroup(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(transaction.getLoginId(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(transaction.getCustomerFirstName() + " " + transaction.getCustomerApaterno(), Element.ALIGN_LEFT, Font.NORMAL));
			table.addCell(cell(transaction.getCustomerEmail(), Element.ALIGN_LEFT, Font.NORMAL));
			table.addCell(cell(transaction.getChannel(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(transaction.getCreationDate().toString(), Element.ALIGN_CENTER, Font.NORMAL));
		}
		log.info("Has been ending buildReport method");
		return super.buildReport("Listado de transacciones", table);
	}

	public String buildReportExcel() throws Exception {
		List<List<CellExcel>> table = new ArrayList<List<CellExcel>>();
		List<CellExcel> title = new ArrayList<CellExcel>();
		title.add(cellExcel("Referencia", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Pedido", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Tipo Usuario", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Nombre", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Email", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Canal", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Fecha", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));// PTMR
		List<List<CellExcel>> rows = new ArrayList<List<CellExcel>>();
		for (Transaction transaction : ((ToTransactions) ActionContext.getContext().getSession().get("toTransactions")).getTransactions()) {
			List<CellExcel> row = new ArrayList<CellExcel>();
			row.add(cellExcel(transaction.getRefTransId().toString(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(transaction.getShippingGroup(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(transaction.getLoginId(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(transaction.getCustomerFirstName() + " " + transaction.getCustomerApaterno(), CellStyle.ALIGN_LEFT, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(transaction.getCustomerEmail(), CellStyle.ALIGN_LEFT, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(transaction.getChannel(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(transaction.getCreationDate().toString(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			rows.add(row);
		}
		table.add(title);
		table.addAll(rows);
		log.info("Has been ending buildReportExcel method");
		return super.buildReportExcel("Listado de transacciones", table);
	}

}
