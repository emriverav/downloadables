/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.implementations.daos;

import java.util.ArrayList;

import javax.ejb.Stateless;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.AbstracDao;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoStatus;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Status;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToStatus;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated:
 * @description: Persistence implementation
 * @since-version: 1.0
 */
@Repository
@Slf4j
@Stateless
public class DaoStatus extends AbstracDao implements IDaoStatus {

	/** DAO query constans */
	protected final static String FIND_ALL = "SELECT D.INDEX_ID, D.CUSTOMER_FIRST_CHANGE_EMAIL, D.CUSTOMER_SECOND_CHANGE_EMAIL, D.REDEEM_STATUS, D.REDEEM_DATE, D.REDEEM_RETRIES, P.SKU, P.DESCRIPTION, P.EXTERNAL_ID, O.REF_TRANS_ID, O.SHIPPING_GROUP, C.CUSTOMER_FIRST_NAME, C.CUSTOMER_APATERNO, C.CUSTOMER_AMATERNO, C.CUSTOMER_EMAIL FROM PRODUCTS_DETAIL D INNER JOIN PRODUCTS P ON D.SKU = P.SKU INNER JOIN ORDERS O ON O.REF_TRANS_ID = D.REF_TRANS_ID INNER JOIN CLIENTS C ON C.REF_TRANS_ID = O.REF_TRANS_ID WHERE P.SKU = ? AND O.SHIPPING_GROUP = ?";
	protected final static String UPDATE_FIRST = "UPDATE PRODUCTS_DETAIL SET CUSTOMER_FIRST_CHANGE_EMAIL = ? WHERE SKU = ?";
	protected final static String UPDATE_SECOND = "UPDATE PRODUCTS_DETAIL SET CUSTOMER_SECOND_CHANGE_EMAIL = ? WHERE SKU = ?";

	public ToStatus findAll(Object object) {
		ToStatus toStatus = (ToStatus) object;
		toStatus.setStatusList(new ArrayList<Status>());
		try {
			toStatus.setStatusList((ArrayList<Status>) jdbcTemplate.query(FIND_ALL, new Object[] { toStatus.getStatus().getSku(), toStatus.getStatus().getShippingGroup() }, new BeanPropertyRowMapper<Status>(Status.class)));
			if (toStatus.getStatusList().size() > 0) {
				toStatus.setStatus(toStatus.getStatusList().get(0));
			}
			setRowsAffected(toStatus.getStatusList().size());
			log.info("Has been find all rows from database");
		} catch (Exception e) {
			log.info("An error has been ocurred during findAll operation with this message > " + e.getMessage());
			throw e;
		}
		return toStatus;
	}

	public ToStatus update(Object object) {
		ToStatus toStatus = (ToStatus) object;
		try {
			String UPDATE = toStatus.getStatus().getSaveFirstEmail() ? UPDATE_FIRST : UPDATE_SECOND;
			String VALUE = toStatus.getStatus().getSaveFirstEmail() ? toStatus.getStatus().getCustomerFirstChangeEmail() : toStatus.getStatus().getCustomerSecondChangeEmail();
			setRowsAffected(jdbcTemplate.update(UPDATE, new Object[] { VALUE, toStatus.getStatus().getSku() }));
			log.info("Has been update the detail database");
		} catch (Exception e) {
			log.info("An error has been ocurred during update operation with this message > " + e.getMessage());
			throw e;
		}
		return toStatus;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

}
