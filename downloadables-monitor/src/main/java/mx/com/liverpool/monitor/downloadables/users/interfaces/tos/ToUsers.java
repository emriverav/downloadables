/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.users.interfaces.tos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.tos.ToAudit;
import mx.com.liverpool.monitor.downloadables.users.interfaces.models.User;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: class that implements TransferObject pattern
 * @since-version: 1.0
 */
@EqualsAndHashCode(callSuper = false)
public class ToUsers extends ToAudit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -199944112206032843L;

	@Getter
	@Setter
	private User user;

	@Getter
	@Setter
	private ArrayList<User> users;

	private Map<String, String> profilesCatalog;

	@Getter
	@Setter
	private String typeOperation;

	@Getter
	@Setter
	private Boolean successfully;

	public Map<String, String> getProfilesCatalog() {
		profilesCatalog = new LinkedHashMap<String, String>();
		profilesCatalog.put("FULL", "Full");
		profilesCatalog.put("ADMIN", "Admin");
		profilesCatalog.put("AGENT", "Agent");
		return profilesCatalog;
	}

	public void setProfilesCatalog(Map<String, String> profilesCatalog) {
		this.profilesCatalog = profilesCatalog;
	}

}
