/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.login.interfaces.services;

import mx.com.liverpool.monitor.downloadables.login.interfaces.tos.ToLogin;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: Interface to the ServiceLogin
 * @since-version: 1.0
 */
public interface IServiceLogin {

	/**
	 * @autor: Genaro Bermúdez [02/02/2016]
	 * @updated: ---
	 * @description: Method that return the invoke persistence tier to user validation
	 * @params - key: string with the key to get
	 * @return - value: string with the value
	 */
	public ToLogin userValidation(ToLogin loginTO);

}
