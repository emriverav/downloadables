/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.audit.interfaces.tos;

import java.io.Serializable;

import lombok.Data;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.models.Audit;

/**
 * @author: Genaro Bermúdez [28/02/2018]
 * @updated: ---
 * @description: class that implements TransferObject pattern
 * @since-version: 1.0
 */
@Data
public class ToAudit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 5106435243335732839L;
	private Audit audit;

}
