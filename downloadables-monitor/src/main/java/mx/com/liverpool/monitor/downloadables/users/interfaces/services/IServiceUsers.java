/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.users.interfaces.services;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.IAbstract;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: Interface to the ServiceUsers
 * @since-version: 1.0
 */
public interface IServiceUsers extends IAbstract {

}
