/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.services;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: Interface to the ServiceTimer
 * @since-version: 1.0
 */
public interface IServiceTimer {

	/**
	 * @author: Genaro Bermúdez [02/02/2016]
	 * @updated: ---
	 * @description: Method that start the timer
	 * @params - N/A
	 * @return - N/A
	 */
	public void start();

	/**
	 * @author: Genaro Bermúdez [02/02/2016]
	 * @updated: ---
	 * @description: Method that stop the timer
	 * @params - N/A
	 * @return - N/A
	 */
	public void stop();

}
