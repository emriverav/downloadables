/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.implementations.services;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.implementations.services.ServiceAudit;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoTransactions;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.services.IServiceTransactions;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToTransactions;

/**
 * @author: Genaro Bermúdez [19/02/2018]
 * @updated:
 * @description: bussiness logic ServiceTransactions
 * @since-version: 1.0
 */
@Slf4j
@Stateless
@Interceptors(ServiceAudit.class)
public class ServiceTransactions implements IServiceTransactions {

	@EJB
	IDaoTransactions daoTransactions;

	public ToTransactions findAll(Object object) {
		ToTransactions toTransactions = (ToTransactions) object;
		try {
			toTransactions = (ToTransactions) daoTransactions.findAll(toTransactions);
			log.info("Has been find all transactions");
		} catch (Exception e) {
			log.error("Can't obtain the transactions");
		}
		return toTransactions;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

}