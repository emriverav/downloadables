/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.login.interfaces.tos;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.tos.ToAudit;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Menu;
import mx.com.liverpool.monitor.downloadables.users.interfaces.models.User;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: Genaro Bermúdez [01/02/2018]
 * @description: class that implements TransferObject pattern
 * @since-version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ToLogin extends ToAudit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 2529358176407909721L;
	private User user;
	private ArrayList<Menu> menus;
	private String resourcesPath;
	private Boolean firstTime;
	private Boolean successfully;

}
