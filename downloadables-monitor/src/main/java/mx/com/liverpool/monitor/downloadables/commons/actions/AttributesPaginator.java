/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.actions;

import lombok.Getter;
import lombok.Setter;

/**
 * @author: Mauricio Rivera [07/02/2018]
 * @updated: ---
 * @description: Class that represents the paginator's attributes
 * @since-version: 1.0
 */
public class AttributesPaginator {

	private static final String ASC = "ASC";
	private static final String DESC = "DESC";
	private static final int PAGE_NUMBER_DEFAULT = 1;
	private static final int PAGE_SIZE_DEFAULT = 50;

	@Getter
	@Setter
	private int pageNumber = PAGE_NUMBER_DEFAULT;

	@Getter
	@Setter
	private int pageSize = PAGE_SIZE_DEFAULT;

	@Getter
	@Setter
	private int pagesNumber;

	@Getter
	@Setter
	private String sortOrden;

	@Getter
	@Setter
	private String sortColumn;

	@Getter
	@Setter
	private String[] fieldsToFliter;

	@Getter
	@Setter
	private int numberElements;

	@Getter
	@Setter
	private String filter;

	public AttributesPaginator() {
		super();
	}

	public AttributesPaginator(int pageNumber, int pageSize, int pagesNumber, String sortOrden, String sortColumn, String[] fieldsToFliter, int numberElements, String filter) {
		super();
		this.pageNumber = pageNumber;
		this.pageSize = pageSize;
		this.pagesNumber = pagesNumber;
		this.sortOrden = sortOrden;
		this.sortColumn = sortColumn;
		this.fieldsToFliter = fieldsToFliter;
		this.numberElements = numberElements;
		this.filter = filter;
	}

	public boolean isFirstPageSelectable() {
		return pageNumber != 1;
	}

	public boolean isPreviousPageSelectable() {
		return pageNumber != 1;
	}

	public boolean isNextPageSelectable() {
		return pageNumber != pagesNumber;
	}

	public boolean isLastPageSelectable() {
		return pageNumber != pagesNumber;
	}

	public int getPreviouPage() {
		int previouPage = pageNumber;
		if (pageNumber != 1) {
			previouPage -= 1;
		}
		return previouPage;
	}

	public int getNextPage() {
		int nextPage = pageNumber;
		if (pageNumber != pagesNumber) {
			nextPage += 1;
		}
		return nextPage;
	}

	public String getOrder(String columName) {
		String newSortOrder = null;
		if (sortColumn != null && sortColumn.equals(columName)) {
			if (sortOrden.equals(ASC)) {
				newSortOrder = DESC;
			} else if (sortOrden.equals(DESC)) {
				newSortOrder = "";
			}
		} else {
			newSortOrder = ASC;
		}
		return newSortOrder;
	}

	public String getImage(String columnName) {
		String image = "MODULES/commons/images/";
		if (sortColumn == null || !sortColumn.equals(columnName)) {
			return image + "imgVacio.png";
		}
		if (sortOrden.equals(ASC)) {
			return image + "imgAscendente.png";
		} else if (sortOrden.equals(DESC)) {
			return image + "imgDescendente.png";
		} else {
			return image + "imgVacio.png";
		}
	}

}
