/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos;

import java.io.Serializable;

import lombok.Data;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.OrderDi;

/**
 * @author: Genaro Bermúdez [06/03/2018]
 * @updated: ---
 * @description: Wrapping to many models or pojo's class. Its represents the TransferObject Design Pattern
 * @since-version: 1.0
 */
@Data
public class ToOrders implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 1183110059645757817L;
	private OrderDi orderDi;

}
