/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.IAbstract;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToProducts;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Genaro Bermúdez [27/02/2018]
 * @description: Interface to access the persistence tier
 * @since-version: 1.0
 */
public interface IDaoProducts extends IAbstract {

	/**
	 * @autor: Genaro Bermúdez [27/02/2016]
	 * @updated: ---
	 * @description: Method that find all skus with errors 1001
	 * @params - toProducts: object with all input values
	 * @return - toProducts: object with result operation
	 */
	public ToProducts findAllGenerate(ToProducts toProducts);

	/**
	 * @autor: Genaro Bermúdez [28/02/2016]
	 * @updated: ---
	 * @description: Method that find all skus without errors 1000
	 * @params - toProducts: object with all input values
	 * @return - toProducts: object with result operation
	 */
	public ToProducts findAllSend(ToProducts toProducts);

	/**
	 * @autor: Genaro Bermúdez [28/02/2016]
	 * @updated: ---
	 * @description: Method that update the retries to send notifications
	 * @params - toProducts: object with all input values
	 * @return - toProducts: object with result operation
	 */
	public ToProducts updateRetriesSendCodes(ToProducts toProducts);

}
