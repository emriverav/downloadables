/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.home.actions;

import com.opensymphony.xwork2.ActionContext;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.login.interfaces.tos.ToLogin;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: Genaro Bermúdez [01/02/2018]
 * @description: init - home controller
 * @since-version: 1.0
 */
@Slf4j
public class Home extends GapsiCommonAction {

	private static final long serialVersionUID = -7976161838445130574L;

	public String execute() throws Exception {
		log.info("Beginning execute Home action");
		super.readPopup();
		ToLogin toLogin = (ToLogin) ActionContext.getContext().getSession().get("toLogin");
		ActionContext.getContext().getSession().clear();
		if (toLogin.getFirstTime() != null && toLogin.getFirstTime()) {
			toLogin.setFirstTime(false);
			ActionContext.getContext().getSession().put("toLogin", toLogin);
			return "manageTransactions";
		}
		ActionContext.getContext().getSession().put("toLogin", toLogin);
		log.info("Ending execute Home action");
		return "success";
	}

}
