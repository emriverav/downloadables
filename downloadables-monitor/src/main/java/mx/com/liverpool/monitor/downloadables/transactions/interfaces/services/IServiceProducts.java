/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.services;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.IAbstract;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToProducts;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated:
 * @description: Interface to the ServiceProducts
 * @since-version: 1.0
 */
public interface IServiceProducts extends IAbstract {

	/**
	 * @autor: Genaro Bermúdez [27/02/2016]
	 * @updated: ---
	 * @description: Method that invoke persistence tier to generateOrder
	 * @params - toProducts: object with all input values
	 * @return - toProducts: object with result operation
	 */
	public ToProducts generateOrder(ToProducts toProducts);
	
	/**
	 * @autor: Genaro Bermúdez [27/02/2016]
	 * @updated: ---
	 * @description: Method that send a notification
	 * @params - toProducts: object with all input values
	 * @return - toProducts: object with result operation
	 */
	public ToProducts sendCodes(ToProducts toProducts);

}
