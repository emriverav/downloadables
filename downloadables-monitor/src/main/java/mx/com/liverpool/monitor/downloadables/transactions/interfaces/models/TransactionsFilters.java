/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [19/01/2018]
 * @updated:
 * @description: config-file to set app values. Its uses YAML mapping with jackson library
 * @since-version: 1.0
 */
@Data
public class TransactionsFilters implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 8308317214754938335L;
	private Integer rows = 100;
	private Date start;
	private Date end;
	private Boolean channelWeb;
	private Boolean channelCsc;
	private Boolean channelWap;
	private Boolean channelApp;
	private String customerFirstName;
	private String customerApaterno;
	private String customerEmail;
	private String customerCelphone;
	private Boolean userTypeInvited;
	private Boolean userTypeRegistered;
	private String sku;
	private String refTransId;
	private String orderId;

}
