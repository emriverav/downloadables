/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.login.implementations.services;

import java.util.Hashtable;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

import com.opensymphony.xwork2.ActionContext;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.implementations.services.ServiceAudit;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.daos.IDaoAudit;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.models.Audit;
import mx.com.liverpool.monitor.downloadables.login.interfaces.services.IServiceLogin;
import mx.com.liverpool.monitor.downloadables.login.interfaces.tos.ToLogin;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.services.IServiceProperties;
import mx.com.liverpool.monitor.downloadables.users.interfaces.daos.IDaoUsers;
import mx.com.liverpool.monitor.downloadables.users.interfaces.models.User;
import mx.com.liverpool.monitor.downloadables.users.interfaces.tos.ToUsers;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: Mauricio Rivera [05/03/2018]
 * @description: bussiness logic Login Service
 * @since-version: 1.0
 */
@Slf4j
@Stateless
@Interceptors(ServiceAudit.class)
public class ServiceLogin implements IServiceLogin {

	@EJB
	IDaoUsers daoUsers;

	@EJB
	IServiceProperties serviceProperties;

	static final String CONF_ACTIVE_DIRECTORY_USERNAME = "CONF_ACTIVE_DIRECTORY_USERNAME";
	static final String CONF_ACTIVE_DIRECTORY_PASSWORD = "CONF_ACTIVE_DIRECTORY_PASSWORD";
	static final String CONF_ACTIVE_DIRECTORY_BASE = "CONF_ACTIVE_DIRECTORY_BASE";
	static final String CONF_ACTIVE_DIRECTORY_DN = "CONF_ACTIVE_DIRECTORY_DN";
	static final String CONF_ACTIVE_DIRECTORY_URL = "CONF_ACTIVE_DIRECTORY_URL";
	static final String CONF_ACTIVE_DIRECTORY_FACTORY = "CONF_ACTIVE_DIRECTORY_FACTORY";
	static final String CONF_ACTIVE_DIRECTORY_AUTHENTICATION = "CONF_ACTIVE_DIRECTORY_AUTHENTICATION";
	static final String CONF_ACTIVE_DIRECTORY_REFERRAL = "CONF_ACTIVE_DIRECTORY_REFERRAL";
	static final String ENVIRONMENT_LOCAL = "ENVIRONMENT_LOCAL";

	@Getter
	@Setter
	private SearchControls cons;
	@Getter
	@Setter
	private LdapContext ctx;

	@EJB
	IDaoAudit daoAudit;

	@Override
	public ToLogin userValidation(ToLogin toLogin) {
		log.info("Beginning userValidation method");
		try {
			ToUsers toUsers = new ToUsers();
			if (serviceProperties.getValue(ENVIRONMENT_LOCAL).trim().toLowerCase().equals("false")) {
				User user = getUser(toLogin.getUser().getLogin(), getLdapContext(), getSearchControls());
				if (user == null) {
					throw new Exception("User not exists");
				}
				Boolean login=loginActiveDirectory(toLogin.getUser().getLogin(),toLogin.getUser().getPassword());
				if(login) {
					log.info("Logueo de manera correcta");
					toUsers.setUser(toLogin.getUser());
					toUsers = (ToUsers) daoUsers.find(toUsers);
					toLogin.setAudit(new Audit(null, null, "1", "Login de usuario", toUsers.getUser().getUserId()));
					ActionContext.getContext().getSession().put("userLogued", toUsers.getUser().getUserId());
					log.info("Usuario logueado: " + ActionContext.getContext().getSession().get("userLogued"));
					daoAudit.insert(toLogin.getAudit());
					toLogin.getUser().setProfile(toUsers.getUser().getProfile());
					toLogin.setSuccessfully(true);
				}else {
					log.info("No existe el usaurio en LDAP");
					toUsers.setUser(toLogin.getUser());
				}
					
			}else {
			toUsers.setUser(toLogin.getUser());
			toUsers = (ToUsers) daoUsers.find(toUsers);
			toLogin.setAudit(new Audit(null, null, "1", "Login de usuario", toUsers.getUser().getUserId()));
			ActionContext.getContext().getSession().put("toLogin", toLogin);
			daoAudit.insert(toLogin.getAudit());
			toLogin.getUser().setProfile(toUsers.getUser().getProfile());
			toLogin.setSuccessfully(true);
			}
			if (!toUsers.getSuccessfully()) {
				throw new Exception("User not exits");
			}
			
			log.info("Has been validate user from database");

		} catch (Exception e) {
			toLogin.setSuccessfully(false);
			log.info("An error has been ocurred during validating user operation with this message > " + e.getMessage());
		}
		return toLogin;
	}

	private LdapContext getLdapContext() {
		LdapContext context = null;
		try {
			Hashtable<String, String> properties = new Hashtable<String, String>();
			properties.put(Context.INITIAL_CONTEXT_FACTORY, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_FACTORY));
			properties.put(Context.SECURITY_AUTHENTICATION, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_AUTHENTICATION));
			properties.put(Context.SECURITY_PRINCIPAL, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_DN));
			properties.put(Context.SECURITY_CREDENTIALS, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_PASSWORD));
			properties.put(Context.PROVIDER_URL, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_URL));
			properties.put(Context.REFERRAL, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_REFERRAL));
			context = new InitialLdapContext(properties, null);
			log.info("LDAP Connection sucessfully");
		} catch (Exception e) {
			log.info("LDAP Connection failed: " + e.getMessage());
		}
		return context;
	}

	private User getUser(String login, LdapContext context, SearchControls searchControls) {
		User user = new User();
		try {
			NamingEnumeration<SearchResult> answer = context.search("DC=Puerto,DC=liverpool,DC=com,DC=mx", "sAMAccountName=" + login, searchControls);
			boolean exitoso = false;
			if (answer.hasMore()) {
				SearchResult resultado = (SearchResult) answer.next();
				Attributes attribs = resultado.getAttributes();
				Attribute nombre = attribs.get("givenName");
				Attribute user1 = attribs.get("sAMAccountName");
				Attribute completo = attribs.get("displayName");
				String nombreCompleto = (String)completo.get();
				String nomb = (String)nombre.get();
				String us = (String)user1.get();
				
				user.setFirstName(nomb.trim());
				user.setLastName(nombreCompleto.replace(nomb.trim(), ""));
				user.setLogin(us.trim());
				user.setEmail(us.trim().toLowerCase().concat("@liverpool.com.mx"));
				exitoso = true;	
			}if(!exitoso){
				user = null;
				log.info("No se encontro ningún usuario con el identificador: "+login);
			} 
			else {
				log.info("User not found");
			}
		} catch (Exception ex) {
			log.info("User not found");
		}
		return user;
	}

	private SearchControls getSearchControls() {
		SearchControls cons = new SearchControls();
		cons.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String[] attrIDs = { "sn", "givenName", "mail", "sAMAccountName", "cn", "displayName", "distinguishedName" };
		cons.setReturningAttributes(attrIDs);
		return cons;
	}

	public Boolean loginActiveDirectory(String usuario, String password) {
		log.info("Ha entrado a loginActiveDirectory");
		boolean loginExitoso = false; 
		try{
			usuario = this.distinguishedName(usuario, getLdapContext(), getSearchControls());
			getLdapContext(usuario,password);
	        if(this.getCtx()!=null){
	    	   log.info("Login correcto a Active Directory");
	    	   loginExitoso = true;
	    	   this.getLdapContext().close();
	        }
	    }catch (Exception e){
	    	log.error("Ha ocurrido un error en loginActiveDirectory cuya causa es: "+e.getLocalizedMessage());
	    }
		return loginExitoso;
	}
	
	private String distinguishedName(String usuario, LdapContext context, SearchControls searchControls){
		getLdapContext();
		this.getSearchControls();
		boolean lpadConext =getLdapContext() != null;
		try{
			if(lpadConext){
				String cadenaBusqueda = "(&(sAMAccountName=" + usuario.toUpperCase() + "))";
				NamingEnumeration<SearchResult> answer = context.search(serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_BASE), cadenaBusqueda, getSearchControls());
				if(answer.hasMore()) {
					SearchResult resultado = (SearchResult) answer.next();
					Attributes attribs = resultado.getAttributes();
					Attribute attr = attribs.get("distinguishedName");
					usuario = (String)attr.get();
				}
				context.close();
			}
		}catch(NamingException ex){
			usuario = null;
			log.error("Ha ocurrido un error en distinguishedName cuya causa es: "+ex.getExplanation());
		}
		return usuario;
	}
	
	private void getLdapContext(String usuario, String password) {
		LdapContext ctx = null;
		try {
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_FACTORY));
	        env.put(Context.PROVIDER_URL, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_URL));
	        env.put(Context.SECURITY_AUTHENTICATION, serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_AUTHENTICATION));
	        env.put(Context.SECURITY_PRINCIPAL, usuario != null ? usuario : serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_DN)); 
	        env.put(Context.SECURITY_CREDENTIALS, password !=null ? password : serviceProperties.getValue(CONF_ACTIVE_DIRECTORY_PASSWORD));
			ctx = new InitialLdapContext(env, null);
		} catch (NamingException ex) {
			log.warn(ex.getMessage().contains("52e") ? "Active Directory - Credenciales invalidas":
						 ex.getMessage().contains("525") ? "Active Directory - Usuario no encontrado":
					     ex.getMessage().contains("530") ? "Active Directory - No tiene permitido hacer login en este momento":
					     ex.getMessage().contains("531") ? "Active Directory - No tiene permitido hacer login desde este equipo":
						 ex.getMessage().contains("532") ? "Active Directory - La contraseña a expirado":
						 ex.getMessage().contains("533") ? "Active Directory - La cuenta ha sido deshabilitada":
						 ex.getMessage().contains("701") ? "Active Directory - La cuenta ha expirado": ex.getMessage());
		}
		this.setCtx(ctx);
	}
}
