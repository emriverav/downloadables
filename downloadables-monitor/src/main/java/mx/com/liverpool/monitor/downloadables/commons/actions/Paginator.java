/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.actions;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: Class for pagination logic manager in the views
 * @since-version: 1.0
 */
public class Paginator {

	private AttributesPaginator attributesPaginator;

	public Paginator(AttributesPaginator attributesPaginator) {
		super();
		this.attributesPaginator = attributesPaginator;
	}

	@SuppressWarnings("unchecked")
	public void pageList(ArrayList<?> allList) throws Exception {
		if (allList == null || allList.size() == 0) {
			return;
		}
		@SuppressWarnings("rawtypes")
		ArrayList pagedList;
		pagedList = getFilteredRecords(allList);
		pagedList = getOrderedRecords(pagedList);
		pagedList = getPageRecords(pagedList);
		attributesPaginator.setNumberElements(pagedList.size());
		allList.clear();
		allList.addAll(pagedList);
	}

	private ArrayList<?> getFilteredRecords(ArrayList<?> list) throws Exception {
		ArrayList<? super Object> filteredList = new ArrayList<Object>();
		if (attributesPaginator.getFilter() == null || attributesPaginator.getFilter().isEmpty()) {
			return list;
		}
		if (attributesPaginator.getFieldsToFliter() == null || attributesPaginator.getFieldsToFliter().length == 0) {
			return list;
		}
		List<Method> classMethodsToFliter = new ArrayList<Method>();
		Class<?> _clazz = list.get(0).getClass();
		Method[] allMethods = _clazz.getDeclaredMethods();
		String nameMethodToFliter;
		for (String attributeToFilter : attributesPaginator.getFieldsToFliter()) {
			nameMethodToFliter = getNameMethodGetter(attributeToFilter);
			for (Method method : allMethods) {
				if (method.getName().equals(nameMethodToFliter)) {
					classMethodsToFliter.add(method);
					break;
				}
			}
		}
		boolean filterStartsAsterisk = attributesPaginator.getFilter().startsWith("*");
		boolean filterEndsAsterisk = attributesPaginator.getFilter().endsWith("*");
		String filter = attributesPaginator.getFilter().toUpperCase();
		if (filterStartsAsterisk && filter.length() > 1) {
			filter = filter.substring(1, filter.length());
		}
		if (filterEndsAsterisk && filter.length() > 1) {
			filter = filter.substring(0, filter.length() - 1);
		}
		String returnGetMethod;
		for (Object object : list) {
			for (Method method : classMethodsToFliter) {
				returnGetMethod = (String) method.invoke(object);
				if (filterStartsAsterisk && filterEndsAsterisk) {
					if (returnGetMethod != null && returnGetMethod.toUpperCase().contains(filter.equals("*") ? "" : filter)) {
						filteredList.add(object);
						break;
					}
				} else if (filterStartsAsterisk) {
					if (returnGetMethod != null && returnGetMethod.toUpperCase().endsWith(filter)) {
						filteredList.add(object);
						break;
					}
				} else if (filterEndsAsterisk) {
					if (returnGetMethod != null && returnGetMethod.toUpperCase().startsWith(filter)) {
						filteredList.add(object);
						break;
					}
				} else {
					if (returnGetMethod != null && returnGetMethod.toUpperCase().equals(filter)) {
						filteredList.add(object);
						break;
					}
				}
			}
		}
		return filteredList;
	}

	private ArrayList<?> getOrderedRecords(ArrayList<?> list) throws Exception {
		if (list == null || list.size() == 0) {
			return list;
		}
		if (attributesPaginator.getSortColumn() == null || attributesPaginator.getSortColumn().isEmpty()) {
			return list;
		}
		Method sortMethod = null;
		Class<?> _clazz = list.get(0).getClass();
		Method[] allMethods = _clazz.getDeclaredMethods();
		String nameMethodOrdering = getNameMethodGetter(attributesPaginator.getSortColumn());
		for (Method method : allMethods) {
			if (method.getName().equals(nameMethodOrdering)) {
				sortMethod = method;
				break;
			}
		}
		if (sortMethod == null) {
			return list;
		}
		Map<String, Object> map = new TreeMap<String, Object>();
		int uniqueIndex = 0;
		String value = null;
		for (Object object : list) {
			value = (String) sortMethod.invoke(object);
			if (value == null) {
				value = "";
			}
			map.put(value + uniqueIndex, object);
			uniqueIndex++;
		}
		ArrayList<? super Object> sortLits = new ArrayList<Object>(map.values());
		if (attributesPaginator.getSortOrden() != null && attributesPaginator.getSortOrden().equals("DESC")) {
			Collections.reverse(sortLits);
		}
		return sortLits;
	}

	private ArrayList<?> getPageRecords(ArrayList<?> list) {
		ArrayList<? super Object> pearsonsPageList = new ArrayList<Object>();
		if (list.size() == 0) {
			return pearsonsPageList;
		}
		attributesPaginator.setPagesNumber(list.size() / attributesPaginator.getPageSize());
		if (list.size() % attributesPaginator.getPageSize() != 0) {
			attributesPaginator.setPagesNumber(attributesPaginator.getPagesNumber() + 1);
		}
		if (attributesPaginator.getPageNumber() > attributesPaginator.getPagesNumber()) {
			attributesPaginator.setPageNumber(attributesPaginator.getPagesNumber());
		}
		int idxPrimerRegistro = (attributesPaginator.getPageNumber() - 1) * attributesPaginator.getPageSize();
		if (list.size() <= idxPrimerRegistro) {
			return pearsonsPageList;
		}
		int addedRecords = 0;
		for (int idx = idxPrimerRegistro; idx < list.size(); idx++) {
			pearsonsPageList.add(list.get(idx));
			if (++addedRecords == attributesPaginator.getPageSize()) {
				break;
			}
		}
		return pearsonsPageList;
	}

	private static String getNameMethodGetter(String nameAttribute) {
		return "get" + Character.toString(nameAttribute.charAt(0)).toUpperCase() + nameAttribute.substring(1);
	}

}
