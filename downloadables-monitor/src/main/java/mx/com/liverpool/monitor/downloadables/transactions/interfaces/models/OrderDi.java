/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.models;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [05/03/2018]
 * @updated:
 * @description: model-class that mapping with Order Object
 * @since-version: 1.0
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderDi implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -3659750782074600522L;
	private String statusCode;
	private String shippingGroupId;
	private String fulfillSystemReference;
	private List<Item> items;
	private String refId;
	private String loginId;
	private String profileId;
	private String customerFirstName;
	private String customerPaterno;
	private String customerMaterno;
	private String customerEmail;
	private String customerCellPhone;
	private String channel;

	/** Used by Monitor */
	private String orderNumber;
	private Boolean isMonitor;
	private String productKey;

}
