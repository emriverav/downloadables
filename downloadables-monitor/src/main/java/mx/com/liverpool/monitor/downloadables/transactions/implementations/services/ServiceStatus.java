/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.implementations.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Service;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.services.IServiceNotifications;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.tos.ToMail;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.tos.ToMail.Recipient;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.services.IServiceProperties;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoStatus;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.services.IServiceStatus;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToStatus;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Mauricio Rivera [28/03/2018]
 * @description: bussiness logic ServiceProducts
 * @since-version: 1.0
 */
@Slf4j
@Stateless
public class ServiceStatus implements IServiceStatus {

	@EJB
	IDaoStatus daoStatus;

	@EJB
	IServiceNotifications serviceNotifications;

	@EJB
	IServiceProperties serviceProperties;

	private static final String MSG_BODY_MAIL_NOTIFICATION_SEND_CODE = "MSG_BODY_MAIL_NOTIFICATION_SEND_CODE";
	private static final String MSG_SUBJECT_MAIL_NOTIFICATION_SEND_CODE = "MSG_SUBJECT_MAIL_NOTIFICATION_SEND_CODE";

	private Service serviceGenerateCode = new Service();
	private Service serviceSendMessageMq = new Service();

	public ToStatus findAll(Object object) {
		ToStatus toStatus = (ToStatus) object;
		try {
			toStatus = (ToStatus) daoStatus.findAll(toStatus);
			log.info("Has been find all transactions");
		} catch (Exception e) {
			log.error("Can't obtain the transactions");
		}
		return toStatus;
	}

	public ToStatus sendCode(ToStatus toStatus) {
		try {
			ToMail toMail = new ToMail();
			List<Recipient> recipients = new ArrayList<Recipient>();
			ToMail.Recipient recipient = toMail.new Recipient();
			String body = serviceProperties.getValue(MSG_BODY_MAIL_NOTIFICATION_SEND_CODE);
			toMail.setSubject(serviceProperties.getValue(MSG_SUBJECT_MAIL_NOTIFICATION_SEND_CODE));
			String email = toStatus.getStatus().getCustomerSecondChangeEmail();
			if (email == null) {
				email = toStatus.getStatus().getCustomerFirstChangeEmail();
			}
			if (email == null) {
				email = toStatus.getStatus().getCustomerEmail();
			}
			body = body.replaceAll("{0}", email);
			body = body.replaceAll("{1}", toStatus.getStatus().getCustomerFirstName());
			toMail.setBody(body);
			recipient.setAlias(toStatus.getStatus().getCustomerFirstName() + " " + toStatus.getStatus().getCustomerApaterno());
			recipient.setEmail(email);
			recipients.add(recipient);
			toMail.setRecipients(recipients);
			toMail.setIsHtml(true);
			serviceNotifications.sendMail(toMail);
			toStatus.setSuccessfully(true);
		} catch (Exception e) {
			toStatus.setSuccessfully(false);
			log.error("Can't send the email");
		}
		return toStatus;
	}

	public ToStatus generateCode(ToStatus toStatus) {
		String target = serviceGenerateCode.getProtocol() + "://" + serviceGenerateCode.getHost() + "/" + serviceGenerateCode.getContext() + "/" + serviceGenerateCode.getResource();
		for (int i = 0; i < new Integer(serviceGenerateCode.getRetries()).intValue(); i++) {
			try {
				Response response = ClientBuilder.newBuilder().build().target(target).request().post(null);
				response.close();
				break;
			} catch (Exception e) {
				log.warn("Error invoking RestService > " + serviceGenerateCode.getName() + " | Retry > " + i + " " + e.getMessage());
			}
		}
		return toStatus;
	}

	public ToStatus sendMessageMq(ToStatus toStatus) {
		String target = serviceSendMessageMq.getProtocol() + "://" + serviceSendMessageMq.getHost() + "/" + serviceSendMessageMq.getContext() + "/" + serviceSendMessageMq.getResource();
		for (int i = 0; i < new Integer(serviceGenerateCode.getRetries()).intValue(); i++) {
			try {
				Response response = ClientBuilder.newBuilder().build().target(target).request().post(null);
				response.close();
				break;
			} catch (Exception e) {
				log.warn("Error invoking RestService > " + serviceGenerateCode.getName() + " | Retry > " + i + " " + e.getMessage());
			}
		}
		return toStatus;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

	@Override
	public Object save(Object object) {
		return null;
	}

}