/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.interfaces.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: entity-class that mapping with Properties Table
 * @since-version: 1.0
 */
@Data
public class Property implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -1940137932953437714L;
	private Integer propertyId;
	private String propertyKey;
	private String propertyValue;
	private String description;

}
