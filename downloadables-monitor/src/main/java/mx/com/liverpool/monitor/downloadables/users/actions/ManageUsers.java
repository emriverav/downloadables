/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.users.actions;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPTable;
import com.opensymphony.xwork2.ActionContext;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.models.Audit;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.commons.implementations.services.ServiceLocator;
import mx.com.liverpool.monitor.downloadables.users.interfaces.models.User;
import mx.com.liverpool.monitor.downloadables.users.interfaces.tos.ToUsers;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: Genaro Bermúdez [01/32/2018]
 * @description: struts controller ManageUsers
 * @since-version: 1.0
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = false)
public class ManageUsers extends GapsiCommonAction {

	private static final long serialVersionUID = 861061212714652765L;
	private ToUsers toUsers;
	private ToUsers toUsersPDF;
	private String userId;
	private String[] first_name= {"FirstName"};

	public String execute() throws Exception {
		toUsers = new ToUsers();
		super.readPopup();
		if (super.getOperation() != null && super.getOperation().equals("delete")) {
			delete();
		}
		toUsers = (ToUsers) ServiceLocator.getInstance().getServiceUsers().findAll(toUsers);
		
		if(this.getAttributesPaginator()!=null) {
		this.getAttributesPaginator().setFieldsToFliter(first_name);
		}
		this.setToUsers(toUsers);
		super.pageList(toUsers.getUsers());
		ActionContext.getContext().getSession().put("toUsers", toUsers);
		log.info("Has been ending ManageUsers action");
		return "success";
	}

	private void delete() throws Exception {
		if ((userId != null) && (!userId.trim().isEmpty())) {
			User user = new User();
			user.setUserId(new Integer(userId));
			toUsers.setUser(user);
			toUsers.setAudit(new Audit(null, null, "7", null, super.getUserLogued().getUserId()));
			toUsers = (ToUsers) ServiceLocator.getInstance().getServiceUsers().delete(toUsers);
		}
		log.info("Has been ending delete method");
	}

	public String buildReport() throws Exception {
		PdfPTable table = new PdfPTable(3);
		toUsers = new ToUsers();
		table.addCell(cell("Nombres", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Apellidos", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Login", Element.ALIGN_CENTER, Font.BOLD));
		toUsersPDF = (ToUsers) ServiceLocator.getInstance().getServiceUsers().findAll(toUsers);
		for (User user :  toUsersPDF.getUsers()) {
			table.addCell(cell(user.getFirstName(), Element.ALIGN_LEFT, Font.NORMAL));
			table.addCell(cell(user.getLastName(), Element.ALIGN_LEFT, Font.NORMAL));
			table.addCell(cell(user.getLogin(), Element.ALIGN_LEFT, Font.NORMAL));
		}
		log.info("Has been ending buildReport method");
		return super.buildReport("Listado de usuarios", table);
	}

}
