/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.actions;

import java.util.ArrayList;

import com.opensymphony.xwork2.ActionContext;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.models.Audit;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.commons.implementations.services.ServiceLocator;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Button;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.models.Property;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.tos.ToProperties;

/**
 * @author: Genaro Bermúdez [19/02/2018]
 * @updated: Genaro Bermúdez [01/32/2018]
 * @description: struts controller EditProperty
 * @since-version: 1.0
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = false)
public class EditProperty extends GapsiCommonAction {

	private static final long serialVersionUID = -5558864782366142415L;
	private ToProperties toProperties;
	private Property property;
	private String propertyId;

	public String execute() throws Exception {

		toProperties = new ToProperties();

		/** Operation: << edit >> */
		if (super.getOperation().equals("edit")) {
			if ((propertyId != null) && (!propertyId.trim().isEmpty())) {
				property = new Property();
				property.setPropertyId(new Integer(propertyId));
				toProperties.setProperty(property);
				super.setFormTitle("Editar propiedad");
			}
			toProperties = (ToProperties) ServiceLocator.getInstance().getServiceProperties().findAll(toProperties);
			this.setProperty(toProperties.getProperty());
			this.setToProperties(toProperties);
			ActionContext.getContext().getSession().put("toProperties", toProperties);
			return "edit";
		}

		/** Operation: << save >> */
		if (super.getOperation().equals("save")) {
			if (!validator()) {
				toProperties.setProperty(this.getProperty());
				toProperties.setAudit(new Audit(null, null, "3", null, super.getUserLogued().getUserId()));
				toProperties = (ToProperties) ServiceLocator.getInstance().getServiceProperties().save(toProperties);
				if (toProperties.getSuccessfully()) {
					ArrayList<Button> buttons = new ArrayList<Button>();
					buttons.add(new Button("Ok", "showHidePopup();"));
					super.showHidePopup("Se ha guardado la propiedad correctamente", "Mensaje de la aplicación", buttons);
				} else {
					ArrayList<Button> buttons = new ArrayList<Button>();
					buttons.add(new Button("Ok", "showHidePopup();"));
					super.showHidePopup("Ha ocurrido un error al intentar guardar la propiedad", "Mensaje de la aplicación", buttons);
				}
			} else {
				this.setToProperties((ToProperties) ActionContext.getContext().getSession().get("toProperties"));
				return "input";
			}
		}

		ActionContext.getContext().getSession().remove("toProperties");
		log.info("Has been ending EditProperty action");
		return "manage";

	}

	private boolean validator() {
		boolean emptyFileds = false;
		boolean shortLength = false;
		super.setErrorMessages(new ArrayList<String>());

		/** Validations cases << empty fileds >> */
		if (property.getPropertyValue() == null || property.getPropertyValue().trim().equals("")) {
			super.getErrorMessages().add("El valor de la propiedad es requerida");
			emptyFileds = true;
		}
		if (property.getDescription() == null || property.getDescription().trim().equals("")) {
			super.getErrorMessages().add("La descripción de la propiedad es requerida");
			emptyFileds = true;
		}
		if (emptyFileds) {
			return emptyFileds;
		}

		/** Validations cases << short length >> */
		if (property.getDescription().trim().length() < 5) {
			super.getErrorMessages().add("La descripción debe ser mas larga");
			shortLength = true;
		}
		if (shortLength) {
			return shortLength;
		}

		return false;

	}

}
