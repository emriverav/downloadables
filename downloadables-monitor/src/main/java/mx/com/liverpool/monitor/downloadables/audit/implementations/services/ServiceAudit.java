package mx.com.liverpool.monitor.downloadables.audit.implementations.services;

import javax.ejb.EJB;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import mx.com.liverpool.monitor.downloadables.audit.interfaces.daos.IDaoAudit;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.tos.ToAudit;

/**
 * @author: Genaro Bermúdez [01/03/2018]
 * @updated: ---
 * @description: Audit async implementation
 * @since-version: 1.0
 */
public class ServiceAudit {

	@EJB
	IDaoAudit daoAudit;

	@AroundInvoke
	public Object intercept(InvocationContext context) throws Exception {

		ToAudit toAudit = null;
		if (context.getParameters().length > 0 && context.getParameters()[0] instanceof ToAudit) {
			try {
				toAudit = (ToAudit) context.getParameters()[0];
				if ((toAudit != null) && (toAudit.getAudit() != null) && (toAudit.getAudit().getUserId() != null) && (toAudit.getAudit().getModificationType() != null)) {
					daoAudit.insert(toAudit.getAudit());
				}
			} catch (Exception e) {
			}
		}

		Object result = context.proceed();
		return result;
	}

}
