/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.services;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.IAbstract;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToStatus;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated:
 * @description: Interface to the ServiceStatus
 * @since-version: 1.0
 */
public interface IServiceStatus extends IAbstract {

	/**
	 * @autor: Genaro Bermúdez [24/02/2016]
	 * @updated: ---
	 * @description: Method that send email to client with redeem code
	 * @params - toStatus: object that constains the sku code
	 * @return - toStatus: return the result
	 */
	public ToStatus sendCode(ToStatus toStatus);

	/**
	 * @autor: Genaro Bermúdez [24/02/2016]
	 * @updated: ---
	 * @description: Method that generate redeem code
	 * @params - toStatus: object that constains the sku code
	 * @return - toStatus: return the result
	 */
	public ToStatus generateCode(ToStatus toStatus);

	/**
	 * @autor: Genaro Bermúdez [24/02/2016]
	 * @updated: ---
	 * @description: Method that send a message to MQ
	 * @params - toStatus: object that constains the info
	 * @return - toStatus: return the result
	 */
	public ToStatus sendMessageMq(ToStatus toStatus);

}
