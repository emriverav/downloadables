/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: class that represents Menu object
 * @since-version: 1.0
 */
@Data
public class Menu implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -6751183000785578855L;
	private Integer idMenu;
	private Integer idParentMenu;
	private String name;
	private String action;
	private Integer order;
	private String modal;
	private String url;

}
