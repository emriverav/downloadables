/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.tos.ToAudit;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.OrderDi;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Product;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.ProductDetail;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Genaro Bermúdez [01/03/2018]
 * @description: class that implements TransferObject pattern
 * @since-version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ToProducts extends ToAudit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 7943961886486955820L;
	private Product product;
	private ArrayList<Product> products;
	private ArrayList<ProductDetail> productsDetail;
	private OrderDi orderDi;
	private Boolean successfully;

}
