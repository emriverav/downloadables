/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.implementations.services;

import java.util.Properties;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.services.IServiceNotifications;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.tos.ToMail;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.tos.ToMail.MailType;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.services.IServiceProperties;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: service notification to send emails
 * @since-version: 1.0
 */
@Slf4j
@Stateless
public class ServiceNotifications implements IServiceNotifications {

	@Inject
	IServiceProperties serviceProperties;
	private final static String PREFIX = "CONF_MAIL_SMTP_";
	private final static String CONF_MAIL_SMTP_MAIN_TYPE = "CONF_MAIL_SMTP_MAIN_TYPE";
	private final static String CONF_MAIL_SMTP_AUTENTICATE = "CONF_MAIL_SMTP_AUTENTICATE";
	private final static String CONF_MAIL_SMTP_JNDI_LOOKUP = "CONF_MAIL_SMTP_JNDI_LOOKUP";
	private Session session;
	private Context conext;

	public ToMail sendMail(ToMail toMail) {
		try {
			log.info("Preparing send mail");
			for (ToMail.Recipient recipient : toMail.getRecipients()) {
				Properties properties = new Properties();
				serviceProperties.getValues(PREFIX).forEach((k, v) -> {
					if (v.getPropertyValue().split("\\|").length >= 2 && !v.getPropertyValue().split("\\|")[1].equals("") && !v.getPropertyValue().split("\\|")[1].trim().toUpperCase().equals("NULL")) {
						properties.put(v.getPropertyValue().split("\\|")[0], v.getPropertyValue().split("\\|")[1]);
					}
				});
				if (serviceProperties.getValue(CONF_MAIL_SMTP_MAIN_TYPE).trim().equals("0")) {
					conext = new InitialContext();
					session = (Session) conext.lookup(serviceProperties.getValue(CONF_MAIL_SMTP_JNDI_LOOKUP));
				} else {
					session = Session.getDefaultInstance(properties);
				}
				if (serviceProperties.getValue(CONF_MAIL_SMTP_AUTENTICATE).trim().toUpperCase().equals("S")) {
					session = Session.getInstance(properties, new javax.mail.Authenticator() {
						@Override
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(properties.getProperty("mail.smtp.user"), properties.getProperty("mail.smtp.password"));
						}
					});
				}
				MimeMessage mensaje = new MimeMessage(session);
				if (properties.getProperty("mail.smtp.alias") == null || properties.getProperty("mail.smtp.alias").trim().equals("") || properties.getProperty("mail.smtp.alias").trim().toUpperCase().equals("NULL")) {
					mensaje.setFrom((Address) new InternetAddress(properties.getProperty("mail.smtp.user")));
				} else {
					mensaje.setFrom((Address) new InternetAddress(properties.getProperty("mail.smtp.user"), properties.getProperty("mail.smtp.alias")));
				}
				mensaje.setRecipients(Message.RecipientType.TO, (Address[]) InternetAddress.parse((String) recipient.getEmail()));
				mensaje.setSubject(toMail.getSubject());
				if (toMail.getIsHtml()) {
					mensaje.setContent(toMail.getBody(), MailType.HTML.getContentType());
				} else {
					mensaje.setText(toMail.getBody(), MailType.TEXT.getContentType());
				}
				log.info("Mail info . To: " + recipient.getEmail());
				log.info("Mail info. Subject: " + mensaje.getSubject());
				Transport transport;
				if (properties.getProperty("mail.smtp.type") == null || properties.getProperty("mail.smtp.type").trim().equals("") || properties.getProperty("mail.smtp.type").trim().toUpperCase().equals("NULL")) {
					transport = session.getTransport();
				} else {
					transport = session.getTransport(properties.getProperty("mail.smtp.type"));
				}
				transport.connect();
				transport.sendMessage((Message) mensaje, mensaje.getAllRecipients());
				transport.close();
				log.info("Send mail to > " + recipient.getAlias() + " | Mail: " + recipient.getEmail());
			}
			toMail.setSuccessfully(true);
		} catch (Exception e) {
			log.error("Has been occurred a error during sendMail operations > " + e.getLocalizedMessage());
			toMail.setSuccessfully(false);
		}
		return toMail;
	}

}
