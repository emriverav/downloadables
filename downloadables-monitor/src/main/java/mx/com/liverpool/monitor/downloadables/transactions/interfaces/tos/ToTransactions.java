/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.tos.ToAudit;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Transaction;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.TransactionsFilters;

/**
 * @author: Genaro Bermúdez [19/02/2018]
 * @updated: Genaro Bermúdez [01/02/2018]
 * @description: class that implements TransferObject pattern
 * @since-version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ToTransactions extends ToAudit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 5708099905894715243L;
	private ArrayList<Transaction> transactions;
	private TransactionsFilters transactionsFilters;
	private Boolean successfully;

}
