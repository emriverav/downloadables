/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.actions;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Button;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Popup;
import mx.com.liverpool.monitor.downloadables.login.interfaces.tos.ToLogin;
import mx.com.liverpool.monitor.downloadables.users.interfaces.models.User;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: base action for all modules
 * @since-version: 1.0
 */
public class GapsiCommonAction extends ActionSupport {

	private static final long serialVersionUID = -6660695487921569495L;

	@Getter
	@Setter
	private String operation;

	@Getter
	@Setter
	private ArrayList<String> errorMessages;

	@Getter
	@Setter
	private Popup popup;

	@Getter
	@Setter
	private InputStream inputStream;

	@Getter
	@Setter
	private AttributesPaginator attributesPaginator;

	@Setter
	private String resourcesPath;

	@Getter
	@Setter
	private String formTitle;

	public GapsiCommonAction() {
		super();
	}

	public GapsiCommonAction(String operation, ArrayList<String> errorMessages, Popup popup, InputStream inputStream, AttributesPaginator attributesPaginator, String resourcesPath, String formTitle) {
		super();
		this.operation = operation;
		this.errorMessages = errorMessages;
		this.popup = popup;
		this.inputStream = inputStream;
		this.attributesPaginator = attributesPaginator;
		this.resourcesPath = resourcesPath;
		this.formTitle = formTitle;
	}

	public String getResourcesPath() {
		return ((ToLogin) ActionContext.getContext().getSession().get("toLogin")).getResourcesPath();
	}

	public User getUserLogued() {
		return ((ToLogin) ActionContext.getContext().getSession().get("toLogin")).getUser();
	}

	public void showHidePopup(String message, String title, ArrayList<Button> buttons) {
		Popup popup = new Popup();
		popup.setTitle(title);
		popup.setMessage(message);
		popup.setButtons(buttons);
		ActionContext.getContext().getSession().put("popup", popup);
	}

	public void readPopup() {
		Popup popup = (Popup) ActionContext.getContext().getSession().get("popup");
		ActionContext.getContext().getSession().remove("popup");
		this.setPopup(popup);
	}

	public void pageList(ArrayList<?> lista) throws Exception {
		if (attributesPaginator == null) {
			attributesPaginator = new AttributesPaginator();
		}
		Paginator paginator = new Paginator(attributesPaginator);
		paginator.pageList(lista);
	}

	public PdfPCell cell(String text, int align, int type) {
		final int TOP = -1;
		PdfPCell celda = new PdfPCell(new Phrase(text, FontFactory.getFont(FontFactory.HELVETICA, 9, type, BaseColor.DARK_GRAY)));
		celda.setHorizontalAlignment(align);
		if (type == TOP) {
			celda.setBorderColor(BaseColor.WHITE);
			celda.setBorderColorTop(BaseColor.BLACK);
			celda.setBorderWidthTop(1);
		} else if (type != Font.BOLD) {
			celda.setBorderColor(BaseColor.WHITE);
		}
		celda.setVerticalAlignment(Element.ALIGN_MIDDLE);
		return celda;
	}

	public String buildReport(String title, PdfPTable table) throws Exception {
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		Document document = new Document(PageSize.A4, 5, 5, 20, 20);
		PdfWriter.getInstance(document, buffer);
		document.open();
		Paragraph titleReport = new Paragraph();
		titleReport.add(new Phrase(title, FontFactory.getFont(FontFactory.HELVETICA, 10, Font.BOLD, BaseColor.DARK_GRAY)));
		titleReport.setAlignment(Element.ALIGN_CENTER);
		titleReport.add(new Phrase(Chunk.NEWLINE));
		titleReport.add(new Phrase(Chunk.NEWLINE));
		document.add(titleReport);
		document.add(table);
		document.close();
		this.setInputStream(new ByteArrayInputStream(buffer.toByteArray()));
		return "success";
	}

	private HSSFCellStyle getHSSFCellStyle(HSSFWorkbook workbook, int alignment, int bold) {
		HSSFCellStyle styleCenterBold = workbook.createCellStyle();
		if (bold == HSSFFont.BOLDWEIGHT_BOLD) {
			HSSFFont fontBold = workbook.createFont();
			fontBold.setBold(true);
			styleCenterBold.setFont(fontBold);
		}
		styleCenterBold.setAlignment((short) alignment);
		return styleCenterBold;
	}

	public String buildReportExcel(String title, List<List<CellExcel>> table) throws Exception {
		int colIdx = 0;
		int rowIdx = 0;
		HSSFRow row;
		HSSFCell cell;
		final int CHARACTER_SIZE = 256;
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet(title);
		HSSFFont fontBold = workbook.createFont();
		fontBold.setBold(true);
		Map<Integer, HSSFCellStyle> styles = new HashMap<Integer, HSSFCellStyle>();
		styles.put((int) CellStyle.ALIGN_LEFT, getHSSFCellStyle(workbook, CellStyle.ALIGN_LEFT, HSSFFont.BOLDWEIGHT_NORMAL));
		styles.put((int) CellStyle.ALIGN_RIGHT, getHSSFCellStyle(workbook, CellStyle.ALIGN_RIGHT, HSSFFont.BOLDWEIGHT_NORMAL));
		styles.put((int) CellStyle.ALIGN_CENTER, getHSSFCellStyle(workbook, CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
		styles.put(CellStyle.ALIGN_CENTER + HSSFFont.BOLDWEIGHT_BOLD, getHSSFCellStyle(workbook, CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		styles.put(CellStyle.ALIGN_LEFT + HSSFFont.BOLDWEIGHT_BOLD, getHSSFCellStyle(workbook, CellStyle.ALIGN_LEFT, HSSFFont.BOLDWEIGHT_BOLD));
		styles.put(CellStyle.ALIGN_RIGHT + HSSFFont.BOLDWEIGHT_BOLD, getHSSFCellStyle(workbook, CellStyle.ALIGN_RIGHT, HSSFFont.BOLDWEIGHT_BOLD));
		for (List<CellExcel> cells : table) {
			colIdx = 0;
			row = sheet.createRow(rowIdx++);
			for (CellExcel celda : cells) {
				cell = row.createCell(colIdx++);
				if (celda.getTipo() == HSSFFont.BOLDWEIGHT_BOLD) {
					cell.setCellStyle(styles.get(celda.getAlineacion() + HSSFFont.BOLDWEIGHT_BOLD));
				} else if (celda.getTipo() == HSSFFont.BOLDWEIGHT_NORMAL) {
					cell.setCellStyle(styles.get(celda.getAlineacion()));
				}
				cell.setCellValue(celda.getText());
			}
		}
		HSSFFont titleFont = workbook.createFont();
		titleFont.setBold(true);
		HSSFRow header = sheet.getRow(0);
		for (int jdx = 0; jdx < header.getPhysicalNumberOfCells(); jdx++) {
			sheet.setColumnWidth(jdx, 20 * CHARACTER_SIZE);
		}
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		workbook.write(buffer);
		workbook.close();
		this.setInputStream(new ByteArrayInputStream(buffer.toByteArray()));
		return "success";
	}

	public CellExcel cellExcel(String text, int alineacion, int tipo) {
		return new CellExcel(text, alineacion, tipo);
	}

	@Data
	@AllArgsConstructor
	public class CellExcel {
		private String text;
		private int alineacion;
		private int tipo;
	}

}
