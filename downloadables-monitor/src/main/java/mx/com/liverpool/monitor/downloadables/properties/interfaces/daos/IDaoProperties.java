/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.interfaces.daos;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.IAbstract;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: Interface to access the persistence tier
 * @since-version: 1.0
 */
public interface IDaoProperties extends IAbstract {

}
