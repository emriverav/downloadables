/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [27/02/2018]
 * @updated:
 * @description: model class to represent Item object
 * @since-version: 1.0
 */
@Data
public class Item implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 6062231969346600071L;
	@XmlElement
	private String sku;
	@XmlElement
	private String description;
	@XmlElement
	private int quantity;
	@XmlElement
	private String external_id;
	@XmlElement
	private int supplier_id;

}
