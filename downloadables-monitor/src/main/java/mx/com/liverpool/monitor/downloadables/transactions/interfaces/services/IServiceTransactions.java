/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.services;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.IAbstract;

/**
 * @author: Genaro Bermúdez [19/02/2018]
 * @updated:
 * @description: Interface to the ServiceTransactions
 * @since-version: 1.0
 */
public interface IServiceTransactions extends IAbstract {

}
