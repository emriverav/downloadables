/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.services;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.tos.ToMail;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: Interface to the ServiceNotifications
 * @since-version: 1.0
 */
public interface IServiceNotifications {

	/**
	 * @author: Genaro Bermúdez [02/02/2016]
	 * @updated: ---
	 * @description: Method that send a email
	 * @params toMail:
	 * @return toMail:
	 */
	public ToMail sendMail(ToMail toMail);

}
