/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [18/02/2018]
 * @description: Class to response the JSON Result
 * @since-version: 1.0
 */
@Data
@XmlRootElement
public class Response implements Serializable {

	/** Class mambers */
	private static final long serialVersionUID = -479209253337146641L;
	private Integer httStatusCode;
	private String description;

}
