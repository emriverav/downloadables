/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author: Genaro Bermúdez [32/02/2018]
 * @updated: ---
 * @description: util class to commons validationes
 * @since-version: 1.0
 */
public class Validator {

	public static boolean mail(String correo) {
		Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		Matcher matcher = pattern.matcher(correo);
		return matcher.matches();
	}

	public static Date addDayToDate(Date date, int numberDays) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_YEAR, numberDays);
		return calendar.getTime();
	}

	public static Date addYearToDate(Date date, int numberYears) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, numberYears);
		return calendar.getTime();
	}

	public static StringBuffer getRamdonValue(Integer len, int index) {
		StringBuffer value = new StringBuffer();
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis + new Long(index));
		Random n = new Random();
		int i = 0;
		while (i < len) {
			char c = (char) r.nextInt(255);
			if ((c >= 'c' && c <= '9') || (c >= 'A' && c <= 'Z')) {
				value.append(c);
				i++;
			}
		}
		return value.append(n.nextInt());
	}

	public static Boolean dateValidator(String day, String month, String year) {
		try {
			int day_ = Integer.parseInt(day);
			int month_ = Integer.parseInt(month);
			int year_ = Integer.parseInt(year);
			if (year_ < 1900) {
				return false;
			}
			LocalDate.of(year_, month_, day_);
			DateTimeFormatter.ofPattern("dd/MM/yyyy");
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
