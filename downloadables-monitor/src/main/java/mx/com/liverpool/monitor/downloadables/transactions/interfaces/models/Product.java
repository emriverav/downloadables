/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Mauricio Rivera [26/03/2018]
 * @description: entity-class that mapping with Products table
 * @since-version: 1.0
 */
@Data
public class Product implements Serializable {

	private static final long serialVersionUID = -6825455742419111674L;

	/** ORDERS info */
	private Date creation_date;
	private String shipping_group;
	private String channel;
	private String orderStatus;
	private Boolean showGenerateOrderButton;
	private Boolean showSupplyOrderButton;
	private Boolean showSendCodesButton;
	private Boolean showEditEmail;
	private String errorCode;
	private Integer send_code_retries;

	/** PRODUCTS info */
	private Integer idNumber;
	private Integer refTransId;
	private String sku;
	private String description;
	private Integer quantity;
	private String externalId;
	private Integer supplierId;
	private String supplierName;
	private String orderId;
	private String codeMessage;

	/** PRODUCTS_DETAILS info */
	private String redeemStatus;
	private String redeemDate;
	private String productKey;
	private Date redeemDateBegin;
	private Date redeemDateEnd;
	
	/** CLIENTS info */
	private String loginId;
	private String customerFirstName;
	private String customerApaterno;
	private String customerAMaterno;
	private String customerEmail;
	private String customerCelphone;
	private String customerFirstChangeEmail;
	private String customerSecondChangeEmail;
	private Boolean saveFirstEmail;

}
