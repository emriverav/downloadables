/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.daos;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated:
 * @description: Abstract connection to database
 * @since-version: 1.0
 */
@Slf4j
public abstract class AbstracDao {

	/** Class members */
	protected static volatile JdbcTemplate jdbcTemplate;
	private static final String JNDI = "java:/jdbc/di";

	@Getter
	@Setter
	private int rowsAffected;

	/** Specialized builder :: Singleton :: */
	public AbstracDao() {
		if (jdbcTemplate == null || jdbcTemplate.getDataSource() == null) {
			try {
				InitialContext context = new InitialContext();
				DataSource dataSource = (DataSource) context.lookup(JNDI);
				AbstracDao.jdbcTemplate = new JdbcTemplate(dataSource);
				log.info("Connection sucessfully");
			} catch (Exception e) {
				log.error("Error to connection database > " + e.getMessage());
			}
		}
	}

}
