/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.implementations.services;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.login.interfaces.services.IServiceLogin;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.services.IServiceProperties;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.services.IServiceProducts;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.services.IServiceStatus;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.services.IServiceTransactions;
import mx.com.liverpool.monitor.downloadables.users.interfaces.services.IServiceUsers;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: Genaro Bermúdez [21/02/2018]
 * @description: Class to return services instances
 * @since-version: 1.0
 */
@Slf4j
public class ServiceLocator {

	/** Member class */
	private static volatile ServiceLocator instance = null;
	private IServiceProperties serviceProperties;
	private IServiceLogin serviceLogin;
	private IServiceUsers serviceUsers;
	private IServiceTransactions serviceTransactions;
	private IServiceProducts serviceProducts;
	private IServiceStatus serviceStatus;
	private Context context;

	private ServiceLocator() throws Exception {
		serviceProperties = null;
		serviceLogin = null;
		serviceUsers = null;
		serviceTransactions = null;
		serviceProducts = null;
		serviceStatus = null;
		context = new InitialContext();
	}

	public static ServiceLocator getInstance() throws Exception {
		if (instance == null) {
			synchronized (ServiceLocator.class) {
				instance = new ServiceLocator();
				instance.context = new InitialContext();
				log.info("New ServiceLocator created");
			}
		}
		return instance;
	}

	public IServiceLogin getServiceLogin() throws NamingException {
		if (serviceLogin == null) {
			serviceLogin = (IServiceLogin) context.lookup("java:module/ServiceLogin");
		}
		return serviceLogin;
	}

	public IServiceProperties getServiceProperties() throws NamingException {
		if (serviceProperties == null) {
			serviceProperties = (IServiceProperties) context.lookup("java:module/ServiceProperties");
		}
		return serviceProperties;
	}

	public IServiceUsers getServiceUsers() throws NamingException {
		if (serviceUsers == null) {
			serviceUsers = (IServiceUsers) context.lookup("java:module/ServiceUsers");
		}
		return serviceUsers;
	}

	public IServiceTransactions getServiceTransactions() throws NamingException {
		if (serviceTransactions == null) {
			serviceTransactions = (IServiceTransactions) context.lookup("java:module/ServiceTransactions");
		}
		return serviceTransactions;
	}

	public IServiceProducts getServiceProducts() throws NamingException {
		if (serviceProducts == null) {
			serviceProducts = (IServiceProducts) context.lookup("java:module/ServiceProducts");
		}
		return serviceProducts;
	}

	public IServiceStatus getServiceStatus() throws NamingException {
		if (serviceStatus == null) {
			serviceStatus = (IServiceStatus) context.lookup("java:module/ServiceStatus");
		}
		return serviceStatus;
	}

}
