/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [27/02/2018]
 * @updated:
 * @description: entity-class that mapping with Products table
 * @since-version: 1.0
 */
@Data
public class ProductDetail implements Serializable {

	private static final long serialVersionUID = 2098960945055148946L;

	/** PRODUCTS info */
	private Integer refTransId;
	private String sku;

	/** PRODUCTS_DETAILS info */
	private String productKey;
	private String redeemStatus;
	private String redeemDate;

}
