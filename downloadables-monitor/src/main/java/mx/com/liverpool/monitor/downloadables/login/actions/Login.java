/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.login.actions;

import java.util.ArrayList;

import com.opensymphony.xwork2.ActionContext;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.commons.implementations.services.ServiceLocator;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Menu;
import mx.com.liverpool.monitor.downloadables.login.interfaces.tos.ToLogin;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: Mauricio Rivera [15/05/2018]
 * @description: action to controller the request/response view to Login module
 * @since-version: 1.0
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = false)
public class Login extends GapsiCommonAction {

	private static final long serialVersionUID = 2017223278895378682L;
	private ToLogin toLogin;
	private String path;
	private static final String CONF_RESOURCES_PATH = "CONF_RESOURCES_PATH";
	private static final String ADMIN = "ADMIN";
	private static final String AGENT = "AGENT";
	private static final String FULL = "FULL";

	public String execute() throws Exception {

		log.info("Beginning execute LoginAction");
		super.readPopup();
		path = ServiceLocator.getInstance().getServiceProperties().getValue(CONF_RESOURCES_PATH);
		if ((super.getOperation() != null) && !super.getOperation().isEmpty() && super.getOperation().trim().equals("userValidate")) {
			if (!validator()) {
				toLogin.setResourcesPath(path);
				toLogin = prepareMenu(toLogin);
				toLogin.getUser().setUserId(toLogin.getAudit().getUserId());
				if (toLogin.getUser().getProfile().equals(AGENT)) {
					toLogin.setFirstTime(true);
				}
				ActionContext.getContext().getSession().put("toLogin", toLogin);
				return "home";
			} else {
				return "input";
			}
		} else {
			toLogin = new ToLogin();
			toLogin.setResourcesPath(path);
		}

		/** Always clear session info */
		ActionContext.getContext().getSession().clear();

		/** Put the object in session cleanly */
		ActionContext.getContext().getSession().put("toLogin", toLogin);

		log.info("Ending execute LoginAction");
		return "success";

	}

	private ToLogin prepareMenu(ToLogin toLogin) {

		toLogin.setMenus(new ArrayList<Menu>());
		if (toLogin.getUser().getProfile().equals(ADMIN)) {
			Menu menu = new Menu();
			menu.setIdParentMenu(0);
			menu.setName("Usuarios");
			menu.setAction("manageUsers");
			menu.setUrl("N");
			menu.setModal("N");
			menu.setOrder(1);
			toLogin.getMenus().add(menu);

			menu = new Menu();
			menu.setIdParentMenu(0);
			menu.setName("Propiedades");
			menu.setAction("manageProperties");
			menu.setUrl("N");
			menu.setModal("N");
			menu.setOrder(2);
			toLogin.getMenus().add(menu);
		}

		if (toLogin.getUser().getProfile().equals(AGENT)) {
			Menu menu = new Menu();
			menu.setIdParentMenu(0);
			menu.setName("Monitor");
			menu.setAction("manageTransactions");
			menu.setUrl("N");
			menu.setModal("N");
			menu.setOrder(1);
			toLogin.getMenus().add(menu);
		}

		if (toLogin.getUser().getProfile().equals(FULL)) {
			Menu menu = new Menu();
			menu.setIdParentMenu(0);
			menu.setName("Usuarios");
			menu.setAction("manageUsers");
			menu.setUrl("N");
			menu.setModal("N");
			menu.setOrder(1);
			toLogin.getMenus().add(menu);

			menu = new Menu();
			menu.setIdParentMenu(0);
			menu.setName("Propiedades");
			menu.setAction("manageProperties");
			menu.setUrl("N");
			menu.setModal("N");
			menu.setOrder(2);
			toLogin.getMenus().add(menu);

			menu = new Menu();
			menu.setIdParentMenu(0);
			menu.setName("Monitor");
			menu.setAction("manageTransactions");
			menu.setUrl("N");
			menu.setModal("N");
			menu.setOrder(3);
			toLogin.getMenus().add(menu);
		}
		return toLogin;
	}

	public boolean validator() {

		/** Prepare local vars to validations */
		boolean emptyFields = false;
		boolean invalidCredentials = false;
		super.setErrorMessages(new ArrayList<String>());

		if (toLogin.getUser().getLogin() == null || toLogin.getUser().getLogin().trim().equals("")) {
			super.getErrorMessages().add("El login es requerido");
			emptyFields = true;
		}
		if (toLogin.getUser().getPassword() == null || toLogin.getUser().getPassword().trim().equals("")) {
			super.getErrorMessages().add("La clave es requerida");
			emptyFields = true;
		}
		if (emptyFields) {
			return emptyFields;
		}

		try {
			toLogin = ServiceLocator.getInstance().getServiceLogin().userValidation(toLogin);
			if (!toLogin.getSuccessfully()) {
				super.getErrorMessages().add("Las creenciales son inválidas");
				invalidCredentials = true;
			}
		} catch (Exception e) {
			super.getErrorMessages().add("Disculpe en este momento no podemos realizar esta operación");
			invalidCredentials = true;
			log.info("Ha ocurrido un error al intentar validar el usuario");
		}
		if (invalidCredentials) {
			return invalidCredentials;
		}

		return false;
	}

}
