/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.users.interfaces.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: entity-class that mapping with Users table
 * @since-version: 1.0
 */
@Data
public class User implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 2278243592381207333L;
	private Integer userId;
	private String profile;
	private String firstName;
	private String lastName;
	private String password;
	private String login;
	private String email;
	private Date created;

}
