/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.models;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: class that represents Popup object
 * @since-version: 1.0
 */
@Data
public class Popup implements Serializable {

	/** Preparación de las variables locales de la clase */
	private static final long serialVersionUID = 7144536409654605869L;
	private String message;
	private String title;
	private ArrayList<Button> buttons;

}
