/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.tos;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.tos.ToAudit;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: class that implements TransferObject pattern
 * @since-version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ToMail extends ToAudit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 6519742545983634493L;
	private String subject;
	private String body;
	private Boolean isHtml;
	private List<Recipient> recipients;
	private Boolean successfully;

	@Getter
	@AllArgsConstructor
	public enum MailType {

		HTML("0", "text/html; charset=utf-8"), TEXT("1", "text/plain");

		/** Sub-member class */
		private String id;
		private String contentType;

	}

	@Data
	public class Recipient implements Serializable {

		/** Sub-members classs */
		private static final long serialVersionUID = 3716597293536609782L;
		private String alias;
		private String email;

	}

}
