/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.implementations.services;

import java.util.Map;

import javax.ejb.Singleton;

import lombok.Data;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.models.Property;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: singleton to manage PropertiesCache
 * @since-version: 1.0
 */
@Singleton
@Data
public class PropertiesCache {

	/** Class members */
	private static volatile PropertiesCache instance = null;
	private Map<String, Property> properties;

	public static PropertiesCache getInstance() throws Exception {
		if (instance == null) {
			synchronized (PropertiesCache.class) {
				if (instance == null) {
					instance = new PropertiesCache();
				}
			}
		}
		return instance;
	}

}
