/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: class that represents Search object
 * @since-version: 1.0
 */
@Data
public class Search implements Serializable {

	/** Preparación de las variables locales de la clase */
	private static final long serialVersionUID = -9074111063045697976L;
	private String criteria;
	private String search;

}
