/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.audit.interfaces.models;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author: Genaro Bermúdez [01/03/2018]
 * @updated: ---
 * @description: entity-class that mapping with Audit table
 * @since-version: 1.0
 */
@Data
@AllArgsConstructor
public class Audit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 5855748574466125856L;
	private Integer refTransId;
	private Date creationDate;
	private String modificationType;
	private String modificationIds;
	private Integer userId;

}
