/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.users.actions;

import java.util.ArrayList;

import com.opensymphony.xwork2.ActionContext;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.models.Audit;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.commons.implementations.services.ServiceLocator;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Button;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.utils.Validator;
import mx.com.liverpool.monitor.downloadables.users.interfaces.models.User;
import mx.com.liverpool.monitor.downloadables.users.interfaces.tos.ToUsers;

/**
 * @author: Genaro Bermúdez [32/02/2018]
 * @updated: Genaro Bermúdez [01/03/2018]
 * @description: struts controller EditUser
 * @since-version: 1.0
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = false)
public class EditUser extends GapsiCommonAction {

	private static final long serialVersionUID = 4174416522009782746L;
	private ToUsers toUsers;
	private User user;
	private String userId;

	public String execute() throws Exception {

		toUsers = new ToUsers();

		/** Operation: << edit >> */
		if (super.getOperation().equals("edit")) {
			if ((userId != null) && (!userId.trim().isEmpty())) {
				user = new User();
				user.setUserId(new Integer(userId));
				toUsers.setUser(user);
				super.setFormTitle("Editar usuario");
			} else {
				super.setFormTitle("Agregar usuario");
			}
			toUsers = (ToUsers) ServiceLocator.getInstance().getServiceUsers().findAll(toUsers);
			this.setUser(toUsers.getUser());
			this.setToUsers(toUsers);
			ActionContext.getContext().getSession().put("toUsers", toUsers);
			return "edit";
		}

		/** Operation: << save >> */
		if (super.getOperation().equals("save")) {
			if (!validator()) {
				toUsers.setUser(this.getUser());
				toUsers.setAudit(new Audit(null, null, (toUsers.getUser().getUserId() != null && toUsers.getUser().getUserId() != 0) ? "6" : "5", null, super.getUserLogued().getUserId()));
				toUsers = (ToUsers) ServiceLocator.getInstance().getServiceUsers().save(toUsers);
				if (toUsers.getSuccessfully()) {
					ArrayList<Button> buttons = new ArrayList<Button>();
					buttons.add(new Button("Ok", "showHidePopup();"));
					super.showHidePopup("Se ha guardado el usuario correctamente", "Mensaje de la aplicación", buttons);
				} else {
					ArrayList<Button> buttons = new ArrayList<Button>();
					buttons.add(new Button("Ok", "showHidePopup();"));
					super.showHidePopup("Ha ocurrido un error al intentar guardar el usuario", "Mensaje de la aplicación", buttons);
				}
			} else {
				this.setToUsers((ToUsers) ActionContext.getContext().getSession().get("toUsers"));
				return "input";
			}
		}

		ActionContext.getContext().getSession().remove("toUsers");
		log.info("Has been ending EditUser action");
		return "manage";

	}

	private boolean validator() {
		boolean emptyFileds = false;
		boolean shortLength = false;
		boolean specialCases = false;
		super.setErrorMessages(new ArrayList<String>());

		/** Validations cases << empty fileds >> */
		if (user.getLogin() == null || user.getLogin().trim().equals("")) {
			super.getErrorMessages().add("El login del usuario es requerido");
			emptyFileds = true;
		}
		if (user.getFirstName() == null || user.getFirstName().trim().equals("")) {
			super.getErrorMessages().add("El nombre del usuario son requerido");
			emptyFileds = true;
		}
		if (user.getLastName() == null || user.getLastName().trim().equals("")) {
			super.getErrorMessages().add("El apellido del usuario es requerido");
			emptyFileds = true;
		}
		if (user.getEmail() == null || user.getEmail().trim().equals("")) {
			super.getErrorMessages().add("El correo del usuario es requerido");
			emptyFileds = true;
		}
		if (user.getProfile() == null || user.getProfile().trim().equals("0")) {
			super.getErrorMessages().add("El perfil del usuario es requerido");
			emptyFileds = true;
		}
		if (emptyFileds) {
			return emptyFileds;
		}

		/** Validations cases << short length >> */
		if (user.getLogin().trim().length() < 3) {
			super.getErrorMessages().add("El login del usuario debe ser mas largo");
			shortLength = true;
		}
		if (user.getFirstName().trim().length() < 3) {
			super.getErrorMessages().add("Los nombres del usuario deben ser mas largos");
			shortLength = true;
		}
		if (user.getLastName().trim().length() < 3) {
			super.getErrorMessages().add("Los apellidos del usuario deben ser mas largos");
			shortLength = true;
		}
		if (user.getEmail().trim().length() < 3) {
			super.getErrorMessages().add("El correo del usuario debe ser mas largo");
			shortLength = true;
		}
		if (shortLength) {
			return shortLength;
		}

		/** Validations cases << special cases >> */
		if (!Validator.mail(user.getEmail())) {
			super.getErrorMessages().add("El correo del usuario debe tener un formato adecuado");
			specialCases = true;
		}
		for (User user : ((ToUsers) ActionContext.getContext().getSession().get("toUsers")).getUsers()) {
			if (this.getUser().getUserId() == null) {
				if (user.getLogin().trim().toLowerCase().equals(this.getUser().getLogin().trim().toLowerCase())) {
					specialCases = true;
					super.getErrorMessages().add("El login indicado ya está siendo utilizado por otro usuario");
					break;
				}
			} else if (this.getUser().getUserId().intValue() != user.getUserId().intValue()) {
				if (user.getLogin().trim().toLowerCase().equals(this.getUser().getLogin().trim().toLowerCase())) {
					specialCases = true;
					super.getErrorMessages().add("El login indicado ya está siendo utilizado por otro usuario");
					break;
				}
			}
		}
		for (User usuario : ((ToUsers) ActionContext.getContext().getSession().get("toUsers")).getUsers()) {
			if (this.getUser().getUserId() == null) {
				if (usuario.getEmail().trim().toLowerCase().equals(this.getUser().getEmail().trim().toLowerCase())) {
					specialCases = true;
					super.getErrorMessages().add("El correo indicado ya está siendo utilizado por otro usuario");
					break;
				}
			} else if (this.getUser().getUserId().intValue() != usuario.getUserId().intValue()) {
				if (usuario.getEmail().trim().toLowerCase().equals(this.getUser().getEmail().trim().toLowerCase())) {
					specialCases = true;
					super.getErrorMessages().add("El correo indicado ya está siendo utilizado por otro usuario");
					break;
				}
			}
		}
		if (specialCases) {
			return specialCases;
		}

		return false;

	}

}
