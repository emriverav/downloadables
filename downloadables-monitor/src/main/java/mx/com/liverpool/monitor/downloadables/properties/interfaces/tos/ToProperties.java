/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.interfaces.tos;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.tos.ToAudit;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.models.Property;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: Genaro Bermúdez [01/02/2018]
 * @description: class that implements TransferObject pattern
 * @since-version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ToProperties extends ToAudit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 5876667003252886777L;
	private Property property;
	private ArrayList<Property> properties;
	private Boolean successfully;

}
