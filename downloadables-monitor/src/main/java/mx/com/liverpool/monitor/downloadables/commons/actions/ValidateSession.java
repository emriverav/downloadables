/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.actions;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.login.interfaces.tos.ToLogin;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: Class for pagination logic manager in the views
 * @since-version: 1.0
 */
@Slf4j
public class ValidateSession implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		log.info("Beginning java filter");

		try {

			/** Get the request to filter */
			HttpServletRequest hr = (HttpServletRequest) request;
			hr.setCharacterEncoding("UTF-8");
			String uri = hr.getRequestURI();
			String contextoApp = hr.getContextPath();
			HttpSession sesion = hr.getSession(false);
			Boolean isRequeridoValidar = true;
			if (sesion == null) {
				sesion = hr.getSession(true);
			}

			/** No validations pages */
			ArrayList<String> listaPaginas = new ArrayList<String>();
			listaPaginas.add("index.jsp");
			listaPaginas.add("login");
			listaPaginas.add("logout");
			listaPaginas.add("login.action");
			listaPaginas.add("logout.action");
			listaPaginas.add("manual");
			listaPaginas.add("pageNotAllowed.jsp");
			listaPaginas.add("errorUnespected.jsp");
			listaPaginas.add("report");


			/** When is root path then redirect to index.jsp page */
			if (uri.endsWith(contextoApp + "/") || uri.endsWith(contextoApp)) {
				((HttpServletResponse) response).sendRedirect(contextoApp + "/index.jsp");
				return;
			}

			/** Resources excluded */
			if (uri.endsWith(".png") || uri.endsWith(".jpg") || uri.endsWith(".gif") || uri.endsWith(".ico") || uri.endsWith(".jpeg") || uri.endsWith(".js") || uri.endsWith(".css") || uri.endsWith(".pdf")) {
				isRequeridoValidar = false;
			}

			/** Multipart Form excluded */
			if (ServletFileUpload.isMultipartContent((HttpServletRequest) request)) {
				isRequeridoValidar = false;
			}

			/** Pages no validatings excluded */
			for (String pagina : listaPaginas) {
				if (uri.endsWith(pagina)) {
					isRequeridoValidar = false;
					break;
				}
			}

			/** LDAP Validation */
			if (isRequeridoValidar) {

				/** Validación de usuarios internos */
				ToLogin toLogin = (ToLogin) sesion.getAttribute("toLogin");
				if ((toLogin == null) || (toLogin.getUser() == null)) {
					sesion.invalidate();
					((HttpServletResponse) response).sendRedirect(contextoApp);
					return;
				}

			}

			chain.doFilter(request, response);
		} catch (Exception e) {
			log.error("Has ocurred the following error: " + e.getMessage());
		}
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
