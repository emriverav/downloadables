/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.implementations.daos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.ejb.Stateless;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.AbstracDao;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoTransactions;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Transaction;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToTransactions;

/**
 * @author: Genaro Bermúdez [19/02/2018]
 * @updated: Mauricio Rivera [23/04/2018]
 * @description: Persistence implementation
 * @since-version: 1.0
 */
@Repository
@Slf4j
@Stateless
public class DaoTransactions extends AbstracDao implements IDaoTransactions {

	/** DAO query constans */
	protected final static String FIND_ALL = "SELECT O.REF_TRANS_ID, O.SHIPPING_GROUP, O.CREATION_DATE, O.CHANNEL, (CASE WHEN C.LOGIN_ID IS NULL THEN 'Invitado' ELSE 'Registrado' END) LOGIN_ID, C.CUSTOMER_FIRST_NAME, C.CUSTOMER_EMAIL, C.CUSTOMER_CELPHONE, C.CUSTOMER_APATERNO, C.CUSTOMER_AMATERNO,P.ORDER_ID FROM ORDERS O INNER JOIN CLIENTS C ON O.REF_TRANS_ID = C.REF_TRANS_ID INNER JOIN PRODUCTS P ON O.REF_TRANS_ID = P.REF_TRANS_ID";

	public ToTransactions findAll(Object object) {
		ToTransactions toTransactions = (ToTransactions) object;
		toTransactions.setTransactions(new ArrayList<Transaction>());

		try {
			String filtros = "";

			if (toTransactions.getTransactionsFilters().getStart() != null && !toTransactions.getTransactionsFilters().getStart().toString().isEmpty() && toTransactions.getTransactionsFilters().getEnd() != null && !toTransactions.getTransactionsFilters().getEnd().toString().isEmpty()) {
				filtros = filtros + " WHERE TRUNC(O.CREATION_DATE) BETWEEN TO_DATE('" + new SimpleDateFormat("dd/MM/yyyy").format(toTransactions.getTransactionsFilters().getStart()) + "', 'dd/mm/yyyy')" + " AND TO_DATE('" + new SimpleDateFormat("dd/MM/yyyy").format(toTransactions.getTransactionsFilters().getEnd()) + "', 'dd/mm/yyyy')";
			}
			if (!(toTransactions.getTransactionsFilters().getChannelWeb() != null && toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelCsc())) {
				if (toTransactions.getTransactionsFilters().getChannelWeb() != null && toTransactions.getTransactionsFilters().getChannelWeb() && !toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND O.CHANNEL = 'WEB'";
				} else if (toTransactions.getTransactionsFilters().getChannelWeb() != null && toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'WEB' OR O.CHANNEL = 'Mobile')";
				} else if (toTransactions.getTransactionsFilters().getChannelWeb() != null && toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelWap() && toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'WEB' OR O.CHANNEL = 'Mobile' OR O.CHANNEL = 'APP')";
				} else if (toTransactions.getTransactionsFilters().getChannelWeb() != null && toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelWap() && toTransactions.getTransactionsFilters().getChannelApp() && toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'WEB' OR O.CHANNEL = 'Mobile' OR O.CHANNEL = 'APP' OR O.CHANNEL = 'CSC')";
				}
				if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelWeb() && !toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND O.CHANNEL = 'Mobile'";
				} else if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'Mobile' OR O.CHANNEL = 'APP')";
				} else if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelApp() && toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'Mobile' OR O.CHANNEL = 'APP' OR O.CHANNEL = 'CSC')";
				}else if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelWeb() && !toTransactions.getTransactionsFilters().getChannelApp() && toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'Mobile' OR O.CHANNEL = 'CSC')";
				}
				if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelWeb() && !toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND O.CHANNEL = 'APP'";
				} else if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'APP' OR O.CHANNEL = 'CSC')";
				}else if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelWap() && toTransactions.getTransactionsFilters().getChannelWeb() && !toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'APP' OR O.CHANNEL = 'WEB')";
				}else if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelWap() && toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelCsc()) {
					filtros = filtros + " AND (O.CHANNEL = 'APP' OR O.CHANNEL = 'WEB' OR O.CHANNEL = 'CSC')";
				}
				if (toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelCsc() && !toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelWap() && !toTransactions.getTransactionsFilters().getChannelWeb()) {
					filtros = filtros + " AND O.CHANNEL = 'CSC'";
				}else if(toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelCsc() && !toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelWap() && toTransactions.getTransactionsFilters().getChannelWeb()) {
					filtros = filtros + " AND (O.CHANNEL = 'CSC' OR O.CHANNEL = 'WEB')";
				}
			}else if(toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelWap() && toTransactions.getTransactionsFilters().getChannelWeb() && toTransactions.getTransactionsFilters().getChannelCsc()) {
				filtros = filtros + " AND (O.CHANNEL = 'APP' OR O.CHANNEL = 'WEB' OR O.CHANNEL = 'CSC')";
			}else if(toTransactions.getTransactionsFilters().getChannelCsc() != null && toTransactions.getTransactionsFilters().getChannelCsc() && !toTransactions.getTransactionsFilters().getChannelApp() && !toTransactions.getTransactionsFilters().getChannelWap() && toTransactions.getTransactionsFilters().getChannelWeb()) {
				filtros = filtros + " AND (O.CHANNEL = 'CSC' OR O.CHANNEL = 'WEB')";
			}
			if (toTransactions.getTransactionsFilters().getCustomerFirstName() != null && !toTransactions.getTransactionsFilters().getCustomerFirstName().isEmpty()) {
				filtros = filtros + " AND LOWER(C.CUSTOMER_FIRST_NAME) = '" + toTransactions.getTransactionsFilters().getCustomerFirstName().toLowerCase() + "'";
			}
			if (toTransactions.getTransactionsFilters().getCustomerApaterno() != null && !toTransactions.getTransactionsFilters().getCustomerApaterno().isEmpty()) {
				filtros = filtros + " AND LOWER(C.CUSTOMER_APATERNO) = '" + toTransactions.getTransactionsFilters().getCustomerApaterno().toLowerCase() + "'";
			}
			if (toTransactions.getTransactionsFilters().getCustomerEmail() != null && !toTransactions.getTransactionsFilters().getCustomerEmail().isEmpty()) {
				filtros = filtros + " AND LOWER(C.CUSTOMER_EMAIL) = '" + toTransactions.getTransactionsFilters().getCustomerEmail().toLowerCase() + "'";
			}
			if (toTransactions.getTransactionsFilters().getCustomerCelphone() != null && !toTransactions.getTransactionsFilters().getCustomerCelphone().isEmpty()) {
				filtros = filtros + " AND LOWER(C.CUSTOMER_CELPHONE) = '" + toTransactions.getTransactionsFilters().getCustomerCelphone().toLowerCase() + "'";
			}
			if (!(toTransactions.getTransactionsFilters().getUserTypeInvited() != null && toTransactions.getTransactionsFilters().getUserTypeInvited() && toTransactions.getTransactionsFilters().getUserTypeRegistered() != null && toTransactions.getTransactionsFilters().getUserTypeRegistered())) {
				if (toTransactions.getTransactionsFilters().getUserTypeInvited() != null && toTransactions.getTransactionsFilters().getUserTypeInvited()) {
					filtros = filtros + " AND LOGIN_ID IS NULL";
				}
				if (toTransactions.getTransactionsFilters().getUserTypeRegistered() != null && toTransactions.getTransactionsFilters().getUserTypeRegistered()) {
					filtros = filtros + " AND LOGIN_ID IS NOT NULL";
				}
			}
			if (toTransactions.getTransactionsFilters().getSku() != null && !toTransactions.getTransactionsFilters().getSku().isEmpty()) {
				filtros = filtros + " AND LOWER(P.SKU) = '" + toTransactions.getTransactionsFilters().getSku().toLowerCase() + "'";
			}
			if (toTransactions.getTransactionsFilters().getRefTransId() != null && !toTransactions.getTransactionsFilters().getRefTransId().isEmpty()) {
				filtros = filtros + " AND O.REF_TRANS_ID = " + toTransactions.getTransactionsFilters().getRefTransId();
			}
			if (toTransactions.getTransactionsFilters().getOrderId() != null && !toTransactions.getTransactionsFilters().getOrderId().isEmpty()) {
				filtros = filtros + " AND LOWER(P.ORDER_ID) = '" + toTransactions.getTransactionsFilters().getOrderId().toLowerCase() + "'";
			}
			// filtros = filtros + " AND ROWNUM <= " + toTransactions.getTransactionsFilters().getRows();
			filtros = filtros + " GROUP BY O.REF_TRANS_ID, O.SHIPPING_GROUP, O.CREATION_DATE, O.CHANNEL, C.LOGIN_ID, C.CUSTOMER_FIRST_NAME, C.CUSTOMER_EMAIL, C.CUSTOMER_CELPHONE, C.CUSTOMER_APATERNO, C.CUSTOMER_AMATERNO, P.ORDER_ID";
			log.info("Filtro query::: " + FIND_ALL + filtros);
			toTransactions.setTransactions((ArrayList<Transaction>) jdbcTemplate.query(FIND_ALL + filtros, new BeanPropertyRowMapper<Transaction>(Transaction.class)));
			setRowsAffected(toTransactions.getTransactions().size());
			log.info("Has been find all rows from database");
		} catch (Exception e) {
			log.info("An error has been ocurred during findAll operation with this message > " + e.getMessage());
			throw e;

		}
		return toTransactions;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

}
