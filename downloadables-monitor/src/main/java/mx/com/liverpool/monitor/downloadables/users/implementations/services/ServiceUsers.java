/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.users.implementations.services;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.implementations.services.ServiceAudit;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.services.IServiceProperties;
import mx.com.liverpool.monitor.downloadables.users.interfaces.daos.IDaoUsers;
import mx.com.liverpool.monitor.downloadables.users.interfaces.models.User;
import mx.com.liverpool.monitor.downloadables.users.interfaces.services.IServiceUsers;
import mx.com.liverpool.monitor.downloadables.users.interfaces.tos.ToUsers;

/**
 * @author: Genaro Bermúdez [13/02/2018]
 * @updated: ---
 * @description: bussiness logic UsersService
 * @since-version: 1.0
 */
@Slf4j
@Stateless
@Interceptors(ServiceAudit.class)
public class ServiceUsers implements IServiceUsers {

	@EJB
	IDaoUsers daoUsers;
	@EJB
	IServiceProperties serviceProperties;

	public ToUsers findAll(Object object) {
		ToUsers toUsers = (ToUsers) object;
		try {
			toUsers = (ToUsers) daoUsers.findAll(toUsers);
			if (toUsers.getUser() != null) {
				for (User user : toUsers.getUsers()) {
					if (user.getUserId().intValue() == toUsers.getUser().getUserId().intValue()) {
						toUsers.setUser(user);
						break;
					}
				}
			}
			log.info("Has been find all users");
		} catch (Exception e) {
			log.error("Can't obtain the users");
		}
		return toUsers;
	}

	public ToUsers save(Object object) {
		ToUsers toUsers = (ToUsers) object;
		try {
			if (toUsers.getUser().getUserId() != null && toUsers.getUser().getUserId() != 0) {
				toUsers = (ToUsers) daoUsers.update(toUsers);
			} else {
				toUsers = (ToUsers) daoUsers.insert(toUsers);
			}
			log.info("Has been save the user");
		} catch (Exception e) {
			log.error("Can't save the user");
		}
		return toUsers;
	}

	public ToUsers delete(Object object) {
		ToUsers toUsers = (ToUsers) object;
		try {
			toUsers = (ToUsers) daoUsers.delete(toUsers);
			log.info("Has been delete the user");
		} catch (Exception e) {
			log.error("Can't delete the user");
		}
		return toUsers;
	}

	public ToUsers find(Object object) {
		ToUsers toUsers = (ToUsers) object;
		try {
			if (toUsers != null && toUsers.getUser() != null) {
				toUsers = (ToUsers) daoUsers.find(toUsers);
				log.info("Has been find user");
			} else {
				log.warn("The user object is null");
			}
		} catch (Exception e) {
			log.error("Can't obtain the user");
		}
		return toUsers;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

}
