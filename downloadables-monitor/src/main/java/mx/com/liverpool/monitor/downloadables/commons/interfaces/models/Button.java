/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: class that represents Button object
 * @since-version: 1.0
 */
@Data
@AllArgsConstructor
public class Button implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -3556863962288889014L;
	private String label;
	private String callback;

}
