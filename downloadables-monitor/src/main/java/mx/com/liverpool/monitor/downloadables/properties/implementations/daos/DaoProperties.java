/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.implementations.daos;

import java.util.ArrayList;

import javax.ejb.Stateless;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.AbstracDao;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.daos.IDaoProperties;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.models.Property;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.tos.ToProperties;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: Persistence implementation
 * @since-version: 1.0
 */
@Repository
@Slf4j
@Stateless
public class DaoProperties extends AbstracDao implements IDaoProperties {

	/** DAO query constans */
	protected final static String UPDATE = "UPDATE PROPERTIES SET PROPERTY_VALUE = ?, DESCRIPTION = ? WHERE PROPERTY_ID = ?";
	protected final static String FIND_ALL = "SELECT * FROM PROPERTIES";

	public ToProperties update(Object object) {

		/** Prepare local objects */
		ToProperties toProperties = (ToProperties) object;

		try {

			/** Update operation */
			setRowsAffected(jdbcTemplate.update(UPDATE, new Object[] { toProperties.getProperty().getPropertyValue(), toProperties.getProperty().getDescription(), toProperties.getProperty().getPropertyId() }));
			toProperties.setSuccessfully(true);
			log.info("Has been updated the row at database");

		} catch (Exception e) {

			/** When fails, write log and set the attribute */
			toProperties.setSuccessfully(false);
			log.info("An error has been ocurred during update operation with this message > " + e.getMessage());

		}

		/** Return value */
		return toProperties;
	}

	public ToProperties findAll(Object object) {

		/** Prepare local objects */
		ToProperties toPropiedades = (ToProperties) object;
		toPropiedades.setProperties(new ArrayList<Property>());

		try {

			/** Find all operation */
			toPropiedades.setProperties((ArrayList<Property>) jdbcTemplate.query(FIND_ALL, new BeanPropertyRowMapper<Property>(Property.class)));
			setRowsAffected(toPropiedades.getProperties().size());
			log.info("Has been find all rows from database");

		} catch (Exception e) {

			/** When fails, write log and set the attribute */
			log.info("An error has been ocurred during findAll operation with this message > " + e.getMessage());
			throw e;

		}

		/** Return value */
		return toPropiedades;

	}

	public ToProperties healthCheck(Object object) {

		/** Prepare local objects */
		ToProperties toPropiedades = (ToProperties) object;

		try {

			/** HealthCheck operation */
			toPropiedades.setSuccessfully(true);
			log.info("Persistencia de Propiedades funcionando correctamente");

		} catch (Exception e) {

			/** When fails, write log and set the attribute */
			toPropiedades.setSuccessfully(false);
			log.info("Persistencia de Propiedades no se encuentra saludable debido al siguiente error > " + e.getMessage());
		}

		/** Return value */
		return toPropiedades;

	}

	public Object insert(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

}
