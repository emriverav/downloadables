/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.actions;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.CellStyle;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPTable;
import com.opensymphony.xwork2.ActionContext;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.commons.implementations.services.ServiceLocator;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Button;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.utils.Validator;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Status;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToStatus;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Genaro Bermúdez [24/02/2018]
 * @description: struts controller ManageStatus
 * @since-version: 1.0
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = false)
public class ManageStatus extends GapsiCommonAction {

	private static final long serialVersionUID = -6485436993072307016L;
	private ToStatus toStatus;
	private String shippingGroup;
	private String sku;
	private String operationEmail;

	public String execute() throws Exception {
		super.readPopup();
		if (toStatus == null) {
			if (ActionContext.getContext().getSession().get("toStatus") != null) {
				shippingGroup = ((ToStatus) ActionContext.getContext().getSession().get("toStatus")).getStatus().getShippingGroup();
				sku = ((ToStatus) ActionContext.getContext().getSession().get("toStatus")).getStatus().getSku();
			}
			toStatus = new ToStatus();
			toStatus.setStatus(new Status());
			operationEmail = null;
			super.setOperation(null);
		}
		toStatus.getStatus().setShippingGroup(shippingGroup);
		toStatus.getStatus().setSku(sku);
		if (super.getOperation() != null && super.getOperation().equals("saveEmail") && validator()) {
			this.setToStatus((ToStatus) ActionContext.getContext().getSession().get("toStatus"));
			return "input";
		}
		if (super.getOperation() != null && super.getOperation().equals("saveEmail")) {
			toStatus.getStatus().setSaveFirstEmail(operationEmail.equals("1") ? true : false);
			ServiceLocator.getInstance().getServiceStatus().save(toStatus);
		}
		if (super.getOperation() != null && super.getOperation().equals("sendCode")) {
			toStatus = ServiceLocator.getInstance().getServiceStatus().sendCode((ToStatus) ActionContext.getContext().getSession().get("toStatus"));
			if (toStatus.getSuccessfully()) {
				ArrayList<Button> buttons = new ArrayList<Button>();
				buttons.add(new Button("Ok", "showHidePopup();"));
				super.showHidePopup("Se ha enviado el correo correctamente", "Mensaje de la aplicación", buttons);
			} else {
				ArrayList<Button> buttons = new ArrayList<Button>();
				buttons.add(new Button("Ok", "showHidePopup();"));
				super.showHidePopup("Ha ocurrido una falla al intentar enviar el correo", "Mensaje de la aplicación", buttons);
			}
			return "sendCode";
		}
		this.setToStatus((ToStatus) ServiceLocator.getInstance().getServiceStatus().findAll(toStatus));
		ActionContext.getContext().getSession().put("toStatus", toStatus);
		log.info("Has been ending ManageStatus action");
		return "success";
	}

	private boolean validator() {
		boolean emptyFileds = false;
		boolean shortLength = false;
		boolean specialCases = false;
		super.setErrorMessages(new ArrayList<String>());

		/** Validations cases << empty fileds >> */
		if (operationEmail.equals("1")) {
			if (toStatus.getStatus().getCustomerFirstChangeEmail() == null || toStatus.getStatus().getCustomerFirstChangeEmail().trim().equals("")) {
				super.getErrorMessages().add("El correo del usuario es requerido");
				emptyFileds = true;
			}
		} else {
			if (toStatus.getStatus().getCustomerSecondChangeEmail() == null || toStatus.getStatus().getCustomerSecondChangeEmail().trim().equals("")) {
				super.getErrorMessages().add("El correo del usuario es requerido");
				emptyFileds = true;
			}
		}
		if (emptyFileds) {
			return emptyFileds;
		}

		/** Validations cases << short length >> */
		if (operationEmail.equals("1")) {
			if (toStatus.getStatus().getCustomerFirstChangeEmail().trim().length() < 3) {
				super.getErrorMessages().add("El correo del usuario debe ser mas largo");
				shortLength = true;
			}
		} else {
			if (toStatus.getStatus().getCustomerSecondChangeEmail().trim().length() < 3) {
				super.getErrorMessages().add("El correo del usuario debe ser mas largo");
				shortLength = true;
			}
		}

		if (shortLength) {
			return shortLength;
		}

		/** Validations cases << special cases >> */
		if (operationEmail.equals("1")) {
			if (!Validator.mail(toStatus.getStatus().getCustomerFirstChangeEmail())) {
				super.getErrorMessages().add("El correo del usuario debe tener un formato adecuado");
				specialCases = true;
			}
		} else {
			if (!Validator.mail(toStatus.getStatus().getCustomerSecondChangeEmail())) {
				super.getErrorMessages().add("El correo del usuario debe tener un formato adecuado");
				specialCases = true;
			}
		}
		if (specialCases) {
			return specialCases;
		}

		return false;

	}

	public String buildReport() throws Exception {
		PdfPTable table = new PdfPTable(7);
		table.addCell(cell("Sku Liverpool", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Descripción", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Sku Proveedor", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Índice", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Estatus", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Fecha redención", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Intentos entrega", Element.ALIGN_CENTER, Font.BOLD));
		for (Status status : ((ToStatus) ActionContext.getContext().getSession().get("toStatus")).getStatusList()) {
			table.addCell(cell(status.getSku(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(status.getDescription(), Element.ALIGN_LEFT, Font.NORMAL));
			table.addCell(cell(status.getExternalId(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(status.getIndexId().toString(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(status.getRedeemStatus() == null ? "" : status.getRedeemStatus(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(status.getRedeemDate() == null ? "" : status.getRedeemDate().toString(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(status.getRedeemRetries() == null ? "" : status.getRedeemRetries(), Element.ALIGN_CENTER, Font.NORMAL));
		}
		log.info("Has been ending buildReport method");
		return super.buildReport("Listado de estatus", table);
	}

	public String buildReportExcel() throws Exception {
		List<List<CellExcel>> table = new ArrayList<List<CellExcel>>();
		List<CellExcel> title = new ArrayList<CellExcel>();
		title.add(cellExcel("Sku Liverpool", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Descripción", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Sku Proveedor", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Índice", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Estatus", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Fecha redención", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Intentos entrega", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		List<List<CellExcel>> rows = new ArrayList<List<CellExcel>>();
		for (Status status : ((ToStatus) ActionContext.getContext().getSession().get("toStatus")).getStatusList()) {
			List<CellExcel> row = new ArrayList<CellExcel>();
			row.add(cellExcel(status.getSku(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(status.getDescription(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(status.getExternalId(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(status.getIndexId().toString(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(status.getRedeemStatus() == null ? "" : status.getRedeemStatus(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(status.getRedeemDate() == null ? "" : status.getRedeemDate().toString(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(status.getRedeemRetries() == null ? "" : status.getRedeemRetries(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			rows.add(row);
		}
		table.add(title);
		table.addAll(rows);
		log.info("Has been ending buildReportExcel method");
		return super.buildReportExcel("Listado de estatus", table);
	}

}
