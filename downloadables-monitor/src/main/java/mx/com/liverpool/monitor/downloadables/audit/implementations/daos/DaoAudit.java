/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.audit.implementations.daos;

import java.util.Date;
import java.util.concurrent.Executors;

import javax.ejb.Stateless;

import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.daos.IDaoAudit;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.models.Audit;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.AbstracDao;

/**
 * @author: Genaro Bermúdez [01/03/2018]
 * @updated: ---
 * @description: Persistence implementation
 * @since-version: 1.0
 */
@Repository
@Slf4j
@Stateless
public class DaoAudit extends AbstracDao implements IDaoAudit {

	protected final static String INSERT = "INSERT INTO AUDIT_MONITOR (REF_TRANS_ID, CREATION_DATE, MODIFICATION_TYPE, MODIFICATION_IDS, USER_ID) VALUES (?,?,?,?,?)";

	public Object insert(Object object) {
		Audit audit = (Audit) object;
		Executors.newSingleThreadExecutor().submit(new Runnable() {
			public void run() {
				Audit audit = (Audit) object;
				try {
					setRowsAffected(jdbcTemplate.update(INSERT, new Object[] { audit.getRefTransId(), new Date(), audit.getModificationType(), audit.getModificationIds(), audit.getUserId() }));
					log.info("Has been insert audit into database");

				} catch (Exception e) {
					log.error("An error occurred while attempting to insert audit into database whose cause is: " + e.getLocalizedMessage());
				}
			}
		});
		return audit;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

}
