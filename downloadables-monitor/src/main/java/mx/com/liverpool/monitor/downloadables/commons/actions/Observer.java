/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.actions;

import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.services.IServiceTimer;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: listener to start - stop the timer
 * @since-version: 1.0
 */
@WebListener
@Slf4j
public class Observer implements ServletContextListener {

	@Inject
	IServiceTimer servicioTimer;

	public void contextInitialized(ServletContextEvent sce) {
		try {
			Executors.newSingleThreadExecutor().submit(new Runnable() {
				public void run() {
					servicioTimer.start();
					log.info("Context Initialized");
				}
			});
		} catch (Exception e) {
			log.error("Can't initialize the timer");
		}
	}

	public void contextDestroyed(ServletContextEvent arg0) {
		try {
			servicioTimer.stop();
			log.info("Has been stopped the timer");
		} catch (Exception e) {
			log.error("Can't stop the timer");
		}
	}

}
