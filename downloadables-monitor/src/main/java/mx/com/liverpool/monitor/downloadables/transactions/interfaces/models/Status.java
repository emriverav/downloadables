/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Genaro Bermúdez [24/02/2018]
 * @description: entity-class that mapping with Products table
 * @since-version: 1.0
 */
@Data
public class Status implements Serializable {

	private static final long serialVersionUID = -338030891754014708L;

	/** ORDERS info */
	private Date creationDate;
	private String shippingGroup;
	private String channel;

	/** PRODUCTS info */
	private Integer idNumber;
	private Integer refTransId;
	private String sku;
	private String description;
	private Integer quantity;
	private String externalId;
	private Integer supplierId;
	private String supplierName;
	private String orderId;
	private String status;
	private String lastStatus;

	/** PRODUCTS_DETAIL info */
	private Integer indexId;
	private String productKey;
	private String url;
	private String instructions;
	private String redeemStatus;
	private Date redeemDate;
	private String redeemRetries;

	/** CLIENTS info */
	private String loginId;
	private String customerFirstName;
	private String customerApaterno;
	private String customerAMaterno;
	private String customerEmail;
	private String customerFirstChangeEmail;
	private String customerSecondChangeEmail;
	private String customerCelphone;
	private Boolean saveFirstEmail;

}
