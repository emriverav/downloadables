/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.implementations.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.implementations.services.ServiceAudit;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Service;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.services.IServiceNotifications;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.tos.ToMail;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.tos.ToMail.Recipient;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.utils.Encryptor;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.services.IServiceProperties;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoOrder;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoProducts;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoProductsDetail;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Item;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.OrderDi;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Product;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.ProductDetail;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.services.IServiceProducts;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToProducts;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Mauricio Rivera [09/04/2018]
 * @description: bussiness logic ServiceProducts
 * @since-version: 1.0
 */
@Slf4j
@Stateless
@Interceptors(ServiceAudit.class)
public class ServiceProducts implements IServiceProducts {

	@EJB
	IDaoProducts daoProducts;

	@EJB
	IDaoProductsDetail daoProductsDetail;

	@EJB
	IServiceProperties serviceProperties;

	@EJB
	IServiceNotifications serviceNotifications;

	@EJB
	IDaoOrder daoOrder;

	private final static String CONF_ADAPTER_SERVICE_GENERATE_CODE_NAME = "CONF_ADAPTER_SERVICE_GENERATE_CODE_NAME";
	private final static String CONF_ADAPTER_SERVICE_GENERATE_CODE_PROTOCOL = "CONF_ADAPTER_SERVICE_GENERATE_CODE_PROTOCOL";
	private final static String CONF_ADAPTER_SERVICE_GENERATE_CODE_HOST = "CONF_ADAPTER_SERVICE_GENERATE_CODE_HOST";
	private final static String CONF_ADAPTER_SERVICE_GENERATE_CODE_PORT = "CONF_ADAPTER_SERVICE_GENERATE_CODE_PORT";
	private final static String CONF_ADAPTER_SERVICE_GENERATE_CODE_CONTEXT = "CONF_ADAPTER_SERVICE_GENERATE_CODE_CONTEXT";
	private final static String CONF_ADAPTER_SERVICE_GENERATE_CODE_RESOURCE = "CONF_ADAPTER_SERVICE_GENERATE_CODE_RESOURCE";
	private final static String CONF_ADAPTER_SERVICE_GENERATE_CODE_RETRIES = "CONF_ADAPTER_SERVICE_GENERATE_CODE_RETRIES";
	private final static String CONF_ADAPTER_SERVICE_GENERATE_CODE_DELAY = "CONF_ADAPTER_SERVICE_GENERATE_CODE_DELAY";
	private final static String CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_NAME = "CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_NAME";
	private final static String CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_PROTOCOL = "CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_PROTOCOL";
	private final static String CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_HOST = "CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_HOST";
	private final static String CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_PORT = "CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_PORT";
	private final static String CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_CONTEXT = "CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_CONTEXT";
	private final static String CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_RESOURCE = "CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_RESOURCE";
	private final static String CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_RETRIES = "CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_RETRIES";
	private final static String CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_DELAY = "CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_DELAY";
	private Service serviceGenerateCodes;
	private Service serviceGetOrderStatus;
	private final static String ERROR_CODE_1001 = "1001";
	private final static String ERROR_CODE_1000 = "1000";
	private final static String ERROR_CODE_1002 = "1002";
	private final static String ERROR_CODE_1003 = "1003";
	private static final String MSG_SUBJECT_MAIL_NOTIFICATION_SEND_CODE = "MSG_SUBJECT_MAIL_NOTIFICATION_SEND_CODE";
	private static final String CONF_MAX_RETRIES_SEND_CODES = "CONF_MAX_RETRIES_SEND_CODES";

	public ToProducts findAll(Object object) {
		ToProducts toProducts = (ToProducts) object;
		try {
			toProducts = (ToProducts) daoProducts.findAll(toProducts);
			if (toProducts.getProduct().getSend_code_retries() == null) {
				toProducts.getProduct().setSend_code_retries(0);
			}
			boolean isRedeem = false;
			toProducts = getOrderStatus(toProducts);
			for (Product product : toProducts.getProducts()) {
				ToProducts toProductsLocal = new ToProducts();
				toProductsLocal.setProduct(new Product());
				toProductsLocal.getProduct().setRefTransId(product.getRefTransId());
				toProductsLocal.getProduct().setSku(product.getExternalId());
				toProductsLocal = (ToProducts) daoProductsDetail.findAll(toProductsLocal);
				boolean someIsNullRedeem = false;
				boolean someIsNotNullRedeem = false;

				for (ProductDetail productDetail : toProductsLocal.getProductsDetail()) {
					if (productDetail.getProductKey() == null || productDetail.getProductKey().isEmpty()) {
						product.setCodeMessage("No");
					} else {
						product.setCodeMessage("Si");
					}
					if (productDetail.getRedeemStatus() == null || productDetail.getRedeemStatus().isEmpty()) {
						someIsNullRedeem = true;
						isRedeem = false;
						product.setRedeemStatus("No");
					} else if (productDetail.getRedeemStatus() != null && productDetail.getRedeemStatus().equals("Redeemed")) {
						someIsNotNullRedeem = false;
						isRedeem = true;
						product.setRedeemStatus("Si");
					} else {
						someIsNotNullRedeem = true;
						product.setRedeemStatus("No");
						isRedeem = false;
					}
					if (productDetail.getRedeemDate() != null && !productDetail.getRedeemDate().isEmpty()) {
						product.setRedeemDate(productDetail.getRedeemDate());
					}
				}

				if (toProductsLocal.getProductsDetail().size() <= 0) {
					someIsNullRedeem = true;
					someIsNotNullRedeem = true;
				}

				if (someIsNullRedeem && someIsNotNullRedeem) {
					product.setRedeemStatus("No");
					product.setCodeMessage("No");
					isRedeem = false;
					if (toProducts.getProduct().getSend_code_retries() < new Integer(serviceProperties.getValue(CONF_MAX_RETRIES_SEND_CODES))) {
						toProducts.getProduct().setShowSendCodesButton(false);
						toProducts.getProduct().setShowEditEmail(false);
					}
				} else {
					if (someIsNullRedeem) {
						product.setRedeemStatus("No");
						isRedeem = false;
						if (toProducts.getProduct().getSend_code_retries() < new Integer(serviceProperties.getValue(CONF_MAX_RETRIES_SEND_CODES))) {
							toProducts.getProduct().setShowSendCodesButton(true);
							toProducts.getProduct().setShowEditEmail(true);
						}
					}
				}

			}
			if (toProducts.getProduct().getOrderId() == null || toProducts.getProduct().getOrderId().isEmpty()) {
				if (toProducts.getProduct().getErrorCode() != null && toProducts.getProduct().getErrorCode().equals(ERROR_CODE_1001)) {
					toProducts.getProduct().setShowGenerateOrderButton(true);
					toProducts.getProduct().setShowSupplyOrderButton(false);
					toProducts.getProduct().setShowSendCodesButton(false);
				} else if (toProducts.getProduct().getErrorCode() != null && toProducts.getProduct().getErrorCode().equals(ERROR_CODE_1003)) {
					toProducts.getProduct().setShowEditEmail(false);
				}
			} else {
				if (toProducts.getProduct().getErrorCode() != null && toProducts.getProduct().getErrorCode().equals(ERROR_CODE_1002)) {
					toProducts.getProduct().setShowSupplyOrderButton(true);
					toProducts.getProduct().setShowSendCodesButton(false);
					toProducts.getProduct().setShowGenerateOrderButton(false);
				} else if (toProducts.getProduct().getErrorCode() != null && toProducts.getProduct().getErrorCode().equals(ERROR_CODE_1000) && !isRedeem) {
					if (toProducts.getProduct().getSend_code_retries() < new Integer(serviceProperties.getValue(CONF_MAX_RETRIES_SEND_CODES))) {
						toProducts.getProduct().setShowEditEmail(true);
						toProducts.getProduct().setShowSendCodesButton(true);
					} else {
						toProducts.getProduct().setShowEditEmail(false);
						toProducts.getProduct().setShowSendCodesButton(false);
					}
				} else {
					toProducts.getProduct().setShowEditEmail(false);
					toProducts.getProduct().setShowSendCodesButton(false);
				}
			}
			log.info("Has been find all transactions");
		} catch (Exception e) {
			log.error("Can't obtain the transactions");
		}
		return toProducts;
	}

	public Object save(Object object) {
		ToProducts toProducts = (ToProducts) object;
		try {
			toProducts = (ToProducts) daoProducts.update(toProducts);
			log.info("Has been save the email");
		} catch (Exception e) {
			log.error("Can't save the email");
		}
		return toProducts;
	}

	public ToProducts generateOrder(ToProducts toProducts) {
		OrderDi orderDi = new OrderDi();
		orderDi.setIsMonitor(true);
		orderDi.setOrderNumber(toProducts.getProduct().getOrderId());
		orderDi.setShippingGroupId(toProducts.getProduct().getShipping_group());
		if (orderDi.getShippingGroupId() == null || orderDi.getShippingGroupId().isEmpty()) {
			toProducts = (ToProducts) daoProducts.findAll(toProducts);
			orderDi.setShippingGroupId(toProducts.getProduct().getShipping_group());
		}
		orderDi.setRefId(String.valueOf(toProducts.getProduct().getRefTransId()));
		orderDi.setItems(new ArrayList<Item>());
		toProducts.setOrderDi(orderDi);
		toProducts = daoProducts.findAllGenerate(toProducts);
		if (toProducts.getOrderDi().getItems().size() > 0) {
			String ids = "";
			for (Item product : toProducts.getOrderDi().getItems()) {
				ids = ids + product.getSku() + ", ";
			}
			if (ids.endsWith(", ")) {
				ids = ids.substring(0, ids.length() - 2);
			}
			String request = "{\"orderDi\": { \"shippingGroupId\": \"" + orderDi.getShippingGroupId() + "\",\"refId\": \"" + orderDi.getRefId() + "\",\"isMonitor\":\"" + orderDi.getIsMonitor() + "\",\"orderNumber\": \"" + orderDi.getOrderNumber() + "\",\"items\": [";

			for (int i = 0; i < toProducts.getOrderDi().getItems().size(); i++) {
				request += "{\"sku\":\"" + toProducts.getOrderDi().getItems().get(i).getSku() + "\",";
				request += "\"externalId\":\"" + toProducts.getOrderDi().getItems().get(i).getExternal_id() + "\",";
				request += "\"supplierId\":\"" + toProducts.getOrderDi().getItems().get(i).getSupplier_id() + "\",";
				request += "\"quantity\":\"" + toProducts.getOrderDi().getItems().get(i).getQuantity() + "\",";
				request += "\"description\":\"" + toProducts.getOrderDi().getItems().get(i).getDescription() + "\"},";
			}
			request = request.substring(0, request.length() - 1);
			request = request.concat("]}}");
			log.info("Request Place Order:: " + request);
			toProducts.getAudit().setModificationIds(ids);
			toProducts.getAudit().setModificationType((toProducts.getProduct().getOrderId() != null && !toProducts.getProduct().getOrderId().isEmpty() && !toProducts.getProduct().getOrderId().equals(0)) ? "6" : "");
			if (toProducts.getAudit().getModificationType().equals("6")) {
				toProducts.getAudit().setModificationIds("Surtir Orden");
			}
			if (serviceGenerateCodes == null) {
				serviceGenerateCodes = new Service();
				serviceGenerateCodes.setName(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GENERATE_CODE_NAME));
				serviceGenerateCodes.setProtocol(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GENERATE_CODE_PROTOCOL));
				serviceGenerateCodes.setHost(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GENERATE_CODE_HOST));
				serviceGenerateCodes.setPort(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GENERATE_CODE_PORT));
				serviceGenerateCodes.setContext(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GENERATE_CODE_CONTEXT));
				serviceGenerateCodes.setResource(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GENERATE_CODE_RESOURCE));
				serviceGenerateCodes.setDelay(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GENERATE_CODE_DELAY));
				serviceGenerateCodes.setRetries(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GENERATE_CODE_RETRIES));
			}
			String target = serviceGenerateCodes.getProtocol() + "://" + serviceGenerateCodes.getHost() + ":" + serviceGenerateCodes.getPort() + "/" + serviceGenerateCodes.getContext() + "/" + serviceGenerateCodes.getResource();
			log.info("Llamado al adapter URL: " + target);
			for (int i = 0; i < new Integer(serviceGenerateCodes.getRetries()).intValue(); i++) {
				try {
					Response response = ClientBuilder.newBuilder().build().target(target).request().post(Entity.entity(request, MediaType.APPLICATION_JSON), Response.class);
					log.info("Status Code of Response: " + response.getStatus() + response);
					response.close();
					break;
				} catch (Exception e) {
					log.warn("Error invoking RestService > " + serviceGenerateCodes.getName() + " | Retry > " + i + " " + e.getMessage());
				}
			}
		} else {
			/** si no hay Items esta nulo o es cero poner a nivel orden ErrorCode = 1005 editEmail(); */
			daoOrder.update(toProducts);
		}
		return toProducts;
	}

	private ToProducts getOrderStatus(ToProducts toProducts) {
		if (toProducts.getProduct().getErrorCode().equals("1000") || toProducts.getProduct().getErrorCode().equals("1005")) {
			if (serviceGetOrderStatus == null) {
				serviceGetOrderStatus = new Service();
				serviceGetOrderStatus.setName(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_NAME));
				serviceGetOrderStatus.setProtocol(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_PROTOCOL));
				serviceGetOrderStatus.setHost(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_HOST));
				serviceGetOrderStatus.setPort(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_PORT));
				serviceGetOrderStatus.setContext(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_CONTEXT));
				serviceGetOrderStatus.setResource(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_RESOURCE));
				serviceGetOrderStatus.setDelay(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_DELAY));
				serviceGetOrderStatus.setRetries(serviceProperties.getValue(CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_RETRIES));
			}
			OrderDi orderDi = new OrderDi();
			orderDi.setIsMonitor(true);
			orderDi.setOrderNumber(toProducts.getProduct().getOrderId());
			orderDi.setShippingGroupId(toProducts.getProduct().getShipping_group());
			if (orderDi.getShippingGroupId() == null || orderDi.getShippingGroupId().isEmpty()) {
				toProducts = (ToProducts) daoProducts.findAll(toProducts);
				orderDi.setShippingGroupId(toProducts.getProduct().getShipping_group());
			}
			orderDi.setRefId(String.valueOf(toProducts.getProduct().getRefTransId()));
			orderDi.setItems(new ArrayList<Item>());
			toProducts.setOrderDi(orderDi);
			String request = "{\"orderDi\": { \"shippingGroupId\": \"" + orderDi.getShippingGroupId() + "\",\"refId\": \"" + orderDi.getRefId() + "\",\"isMonitor\":\"" + orderDi.getIsMonitor() + "\",\"orderNumber\": \"" + orderDi.getOrderNumber() + "\",\"productKey\": \"";
			String requestAux = "{\"orderDi\": { \"shippingGroupId\": \"" + orderDi.getShippingGroupId() + "\",\"refId\": \"" + orderDi.getRefId() + "\",\"isMonitor\":\"" + orderDi.getIsMonitor() + "\",\"orderNumber\": \"" + orderDi.getOrderNumber() + "\",\"productKey\": \"";
			ArrayList<String> productKey = new ArrayList<String>();
			for (Product product : toProducts.getProducts()) {
				ToProducts toProductsLocal = new ToProducts();
				toProductsLocal.setProduct(new Product());
				toProductsLocal.getProduct().setRefTransId(product.getRefTransId());
				toProductsLocal.getProduct().setSku(product.getExternalId());
				toProductsLocal = (ToProducts) daoProductsDetail.findAll(toProductsLocal);

				for (ProductDetail productDetail : toProductsLocal.getProductsDetail()) {
					if (productDetail.getProductKey() == null || productDetail.getProductKey().isEmpty()) {
						log.info("No existe productKey para el SKU: " + productDetail.getSku() + " REF_TRANS_ID: " + productDetail.getRefTransId());
					} else {
						productKey.add(productDetail.getProductKey());
					}
				}
			}
			String target = serviceGetOrderStatus.getProtocol() + "://" + serviceGetOrderStatus.getHost() + ":" + serviceGetOrderStatus.getPort() + "/" + serviceGetOrderStatus.getContext() + "/" + serviceGetOrderStatus.getResource();
			for (int i = 0; i < new Integer(serviceGetOrderStatus.getRetries()).intValue(); i++) {
				try {
					for (String key : productKey) {
						request += key + "\"";
						request = request.concat("}}");
						log.info("Request Place Order:: " + request);

						Response response = ClientBuilder.newBuilder().build().target(target).request().post(Entity.entity(request, MediaType.APPLICATION_JSON), Response.class);
						response.close();
						request = requestAux;
					}
					break;
				} catch (Exception e) {
					log.warn("Error invoking RestService > serviceGetOrderStatus  | Retry > " + i + " " + e.getMessage());
				}
			}
		} else {
			log.info("Not Invoke ");
		}
		return toProducts;

	}

	public ToProducts sendCodes(ToProducts toProducts) {
		try {
			OrderDi orderDi = new OrderDi();
			orderDi.setItems(new ArrayList<Item>());
			toProducts.setOrderDi(orderDi);
			toProducts = (ToProducts) daoProducts.findAll(toProducts);
			toProducts = daoProducts.findAllSend(toProducts);
			ToMail toMail = new ToMail();
			List<Recipient> recipients = new ArrayList<Recipient>();
			ToMail.Recipient recipient = toMail.new Recipient();
			String body = "<!DOCTYPE html> <html lang=\"en\"> <head> <meta charset=\"utf-8\"> <title>title</title> <link rel=\"stylesheet\" href=\"style.css\"> <script src=\"script.js\"></script> </head> <body> <table width=\"760\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin:0px auto;width:760px;font-family:Arial;font-size:14px\"> <tbody> <tr> <td> <table width=\"760\"> <tbody> <tr> <td align=\"left\" valign=\"middle\"><img src=\"https://ci6.googleusercontent.com/proxy/rDSP1UeRQ8FbsDN97QAe2XTb7Mfw56M5FfbuG3uiTCjFn7x8cuBxA-M8rw5vBuZBVvub0ZvNjq3UkgT5M3QPXYEaGSCKAHse91Uj0LMDpPFQ6-v5pxHyhBiqAHv4xxoj=s0-d-e1-ft#https://assetsqa.liverpool.com.mx/assets/images/mailing/liverpool_logo.png\" alt=\"liverpool\" class=\"CToWUd\"></td> <td align=\"right\" valign=\"middle\" style=\"text-align:right\"> <div style=\"font-family:Arial;font-style:normal;font-weight:normal;color:#e10098;display:block;margin:0px;text-align:right;float:right\"> <a href=\"https://assets.liverpool.com.mx/ayuda/\" style=\"text-decoration:none;color:#81898b\" rel=\"nofollow\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=es&amp;q=https://assets.liverpool.com.mx/ayuda/&amp;source=gmail&amp;ust=1516227535027000&amp;usg=AFQjCNFwRuJ7AxPm-pR_YSi6GnPiPqHesQ\"> <span style=\"display:block;text-align:right;float:right\"><img src=\"https://ci6.googleusercontent.com/proxy/ghgm5Rno-MSKTRjhPpiT1dQHRWda2d0AZDzkUfgL-_lt8fFj23szfKGU6S6szs_8XQgGgNnSsjFkpREx7N8Z5x1ELNc8aCkVAEoAC3TD9xAx9ulQXeHzcnKUFdYfHxrT=s0-d-e1-ft#https://assetsqa.liverpool.com.mx/assets/staticPages/images/ayuda_logo.gif\" class=\"CToWUd\"></span></a> </div> </td> </tr> </tbody> </table> <table width=\"760\"> <tbody> <tr> <td colspan=\"2\"> <p style=\"font-family:Arial;font-weight:400;font-size:20px;color:#000;margin:0px;\">Hola <span> {0}</span> </p> </td> </tr> <tr> <td colspan=\"2\" height=\"33\"> <h2 style=\"font-family:Arial;font-style:normal;font-weight:400;text-align:left;color:#000;margin:0px;font-size:20px\">Te reenviamos la compra digital que nos solicitaste</h2> </td> </tr> <tr> <td colspan=\"2\" height=\"31\"> <p style=\"font-family:Arial;font-size:14px;color:#000;text-align:left;margin:0px\"> Conserva este correo para futuras aclaraciones. </p> </td> </tr> </tbody> </table> <hr> </td> </tr> <tr></tr> <tr> <td></td> </tr> <tr> <td> <table width=\"100%\"> <tbody> <tr> <td> <p>{3}<br> No. de referencia: {2}<br> </p> </td> <tr> <td style=\"font-family:Arial\"> <h3 style=\"font-size:15px;font-family:Arial;font-style:normal;text-align:left;color:#000;margin:5px 0px 0px 0px\"> Detalle de Compra </h3> <p style=\"text-align:left;color:#ff0084\"><b>Tus productos digitales</b></p> </td> </tr> </tr> </tbody> </table> <table width=\"760\" align=\"center\" style=\"margin:0px auto\"> <tbody> <tr align=\"center\"> <td width=\"760\"> <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-left:1px solid #cdd0d4;border-right:1px solid #cdd0d4;border-top:1px solid #cdd0d4;border-bottom:1px solid #cdd0d4\"> <tbody> <tr> <td style=\"font-family:Arial;font-size:13px;color:#000\"> <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"> <tbody> <tr bgcolor=\"#f2f2f2\" style=\"min-height:40px\"> <td align=\"left\" bgcolor=\"#f2f2f2\" > <p style=\"padding-left:10px\"><b>Producto</b></p> </td> </tr> <tr align=\"left\" style=\"text-align:left\"> <td colspan=\"2\" style=\"text-align:left;border-top:1px solid #cdd0d4;border-bottom:1px solid #cdd0d4;padding-top:5px;padding-bottom:5px\"> <p style=\"padding-left:10px;font-size:15px;color: #81868b;\">{1}</p> <p style=\"padding-left:10px;font-size:15px;color: #81868b;\">Tipo de compra: Digital</p> </td> <td colspan=\"2\" style=\"text-align:left;border-top:1px solid #cdd0d4;border-bottom:1px solid #cdd0d4;padding-top:5px;padding-bottom:5px\"> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> <tr> <td></td> </tr> <tr> <td> </td> </tr> <tr style=\"font-family:Arial;font-size:12px\"> <td width=\"760\" align=\"center\" valign=\"middle\" style=\"text-align:center;padding-top:10px;padding-bottom:10px;border-bottom:1px solid #cdd0d4\"> <p style=\"margin:3px 0px;font-size:12px;font-style:normal;font-weight:normal;color:#000\"><b>Ventas y Centro de atención telefónica (CAT)</b> 52.62.99.99 <b>D.F. y del interior </b> 01 800 713 55 55 </p> </td> </tr> <tr> <td width=\"760\"> <table width=\"760\" align=\"center\"> <tbody> <tr> <td align=\"right\" style=\"text-align:left;font-size:12px;color:#000;padding-left:5px\"> <span style=\"margin-left:2px;margin-right:2px\"><span style=\"text-align:left\"><a style=\"color:#000;text-decoration:none;font-size:11px\"></a></span><a style=\"color:#000;text-decoration:none;font-size:11px\">DEVOLUCIONES</a></span> <span> • </span> <span style=\"margin-left:2px;margin-right:2px\"><a style=\"color:#000;text-decoration:none;font-size:11px\">AVISO DE PRIVACIDAD</a></span> <span> • </span> <span style=\"margin-left:2px;margin-right:2px\"><a style=\"color:#000;text-decoration:none;font-size:11px\">CONDICIONES DE ENTREGA</a></span> <span> • </span> <span style=\"margin-left:2px;margin-right:2px\"><a style=\"color:#000;text-decoration:none;font-size:11px\">GARANTÍA DE SERVICIO</a></span> </td> <td align=\"right\" valign=\"middle\"> <table width=\"100%\" align=\"right\" cellpadding=\"0\" cellspacing=\"0\"> <tbody> <tr style=\"text-align:center\"> <td>&nbsp;</td> <td width=\"28\" height=\"28\"><a href=\"https://www.facebook.com/liverpoolmexico\" rel=\"nofollow\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=es&amp;q=https://www.facebook.com/liverpoolmexico&amp;source=gmail&amp;ust=1516227535028000&amp;usg=AFQjCNHezyQjhvsAmdcj2t5oQGLV97PPhw\"><img src=\"https://ci3.googleusercontent.com/proxy/JIuWgqC7VLo9rleBW_tfO2iEy4tYgYr4qgLjafq2USh_4z5jjz_UJdiEjwEcOkdxETgyYEIcbjRmKZeLFRBZPHbnaeiPSypxJ2ZkftujGBcdR2LAFgA2Hyu0zQ=s0-d-e1-ft#https://assetsqa.liverpool.com.mx/assets/images/mailing/icon_face.jpg\" alt=\"facebook\" border=\"0\" class=\"CToWUd\"></a></td> <td width=\"28\" height=\"28\"><a href=\"https://twitter.com/LiverpoolMexico\" rel=\"nofollow\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=es&amp;q=https://twitter.com/LiverpoolMexico&amp;source=gmail&amp;ust=1516227535028000&amp;usg=AFQjCNEjdvckc6OMwUI9AnYyjRlkT25Biw\"><img src=\"https://ci3.googleusercontent.com/proxy/qs8PllpHn9hgv2o_VIASycS4wJsJwaBfmzDqmXIpm3r7gCP7oLTj3bb2Hf70szVNk9eF-eQp5EncBhJyGrzglxuEJ2SdAZUC5WN5hPnFH8Zl6JrrO30s9OY=s0-d-e1-ft#https://assetsqa.liverpool.com.mx/assets/images/mailing/icon_tw.jpg\" alt=\"twitter\" border=\"0\" class=\"CToWUd\"></a></td> <td width=\"28\" height=\"28\"><a href=\"https://www.pinterest.com/liverpoolmexico/pins/\" rel=\"nofollow\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=es&amp;q=https://www.pinterest.com/liverpoolmexico/pins/&amp;source=gmail&amp;ust=1516227535028000&amp;usg=AFQjCNH7D6c_uBti8FUulIhSn6GHAgSwng\"><img src=\"https://ci4.googleusercontent.com/proxy/is1yOcHZcgzfNK1n_69Wtn3H5b1nIgUOTJ03R5jnoTMJ01B3jV3Q1L9OgQcJz752xOygudqiCO7iOqBp4hMmzq_uoLxn-vsR0oRJQqFSUpw8KomaP87sWX0JSw=s0-d-e1-ft#https://assetsqa.liverpool.com.mx/assets/images/mailing/icon_pint.jpg\" alt=\"pinterest\" border=\"0\" class=\"CToWUd\"></a></td> <td width=\"28\" height=\"28\"><a href=\"https://plus.google.com/+liverpoolmexico/posts\" rel=\"nofollow\" target=\"_blank\" data-saferedirecturl=\"https://www.google.com/url?hl=es&amp;q=https://plus.google.com/%2Bliverpoolmexico/posts&amp;source=gmail&amp;ust=1516227535028000&amp;usg=AFQjCNEK99PlT_7EaIh41aMFIdFE97KDmA\"><img src=\"https://ci6.googleusercontent.com/proxy/0unSC9dW_rQu08QBMgWKoq11LeV2EDDdrghqpc85xknAbo-SnNHwJpDG0miuRNiTZsxtYrGo1if3JVuGNvnVu0bYRNiHRCIwnZki18uJoOINerevldJAM24ixQ=s0-d-e1-ft#https://assetsqa.liverpool.com.mx/assets/images/mailing/icon_gmas.jpg\" alt=\"google+\" border=\"0\" class=\"CToWUd\"></a></td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </td> </tr> </tbody> </table> </body> </html>";
			toMail.setSubject(serviceProperties.getValue(MSG_SUBJECT_MAIL_NOTIFICATION_SEND_CODE));
			String email = toProducts.getProduct().getCustomerSecondChangeEmail();
			if (email == null || email.isEmpty()) {
				email = toProducts.getProduct().getCustomerFirstChangeEmail();
			}
			if (email == null || email.isEmpty()) {
				email = toProducts.getProduct().getCustomerEmail();
			}
			String skus = "";
			for (Item item : toProducts.getOrderDi().getItems()) {
				toProducts.getProduct().setSku(item.getExternal_id());
				toProducts = (ToProducts) daoProductsDetail.findAll(toProducts);
				String details = "";
				String description = "";
				description = item.getDescription();
				for (ProductDetail productDetail : toProducts.getProductsDetail()) {
					details = details + Encryptor.decrypt(productDetail.getProductKey());
				}
				skus = skus + "<br><p style=\"padding-left:10px;font-size:15px;color: #81868b;\">" + description + "<br>SKU: " + item.getSku() + "<br><br>Cantidad: " + item.getQuantity() + "<br><br>Codigo de Descaga: </p>" + "<p style=\"padding-left:10px;font-size:15px\"><b>" + details + "<span style=\"color:#ff0084\"> más información</span></b></p><br>";
			}
			String nombreCompleto = toProducts.getProduct().getCustomerFirstName() + " " + toProducts.getProduct().getCustomerApaterno();
			if (toProducts.getProduct().getCustomerAMaterno() != null && !toProducts.getProduct().getCustomerAMaterno().equals("")) {
				nombreCompleto.concat(" " + toProducts.getProduct().getCustomerAMaterno());
			}
			body = body.replace("{0}", toProducts.getProduct().getCustomerFirstName());
			body = body.replace("{1}", skus);
			body = body.replace("{2}", toProducts.getProduct().getRefTransId().toString() + "<br>Fecha de creación: " + toProducts.getProduct().getCreation_date());
			body = body.replace("{3}", nombreCompleto);
			toMail.setBody(body);
			recipient.setAlias(toProducts.getProduct().getCustomerFirstName());
			recipient.setEmail(email);
			recipients.add(recipient);
			toMail.setRecipients(recipients);
			toMail.setIsHtml(true);
			toMail = serviceNotifications.sendMail(toMail);
			toProducts.setSuccessfully(toMail.getSuccessfully());
			if (toMail.getSuccessfully()) {
				daoProducts.updateRetriesSendCodes(toProducts);
			}
		} catch (Exception e) {
			toProducts.setSuccessfully(false);
			log.error("Can't send the email");
		}

		return toProducts;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

}