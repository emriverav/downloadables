/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.audit.interfaces.services;

/**
 * @author: Genaro Bermúdez [28/02/2018]
 * @updated: ---
 * @description: Interface to the ServiceAudit
 * @since-version: 1.0
 */
public interface IServiceAudit {

}
