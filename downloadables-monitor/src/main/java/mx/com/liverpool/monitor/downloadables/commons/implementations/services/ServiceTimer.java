/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.implementations.services;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;
import javax.inject.Singleton;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.services.IServiceTimer;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.services.IServiceProperties;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: Timer to trigger all the task in this application
 * @since-version: 1.0
 */
@Singleton
@Slf4j
public class ServiceTimer implements IServiceTimer {

	/** Class members */
	Timer timer = new Timer();

	@Inject
	IServiceProperties serviceProperties;

	public ServiceTimer() {
		super();
	}

	class Job extends TimerTask {
		public void run() {

			/** Task 1: Monitor refresh PropertiesCache */
			try {
				serviceProperties.refreshCache();
			} catch (Exception e) {
				log.warn("Task 1 > Incompleted at > " + new Date());
			}

		}
	}

	public void start() {
		timer.schedule(new Job(), 60000, 3600000);
		log.info("Has been initialize the timer successfuly");
		log.info("WAR Downloadables-Monitor Version PRE-PRODUCTIVA 29/05/2018 ------");
	}

	public void stop() {
		timer.cancel();
		log.info("Has been stoped the timer successfuly");
	}

}
