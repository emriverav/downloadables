/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.users.implementations.daos;

import java.util.ArrayList;
import java.util.Date;

import javax.ejb.Stateless;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.AbstracDao;
import mx.com.liverpool.monitor.downloadables.users.interfaces.daos.IDaoUsers;
import mx.com.liverpool.monitor.downloadables.users.interfaces.models.User;
import mx.com.liverpool.monitor.downloadables.users.interfaces.tos.ToUsers;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: Persistence implementation
 * @since-version: 1.0
 */
@Repository
@Slf4j
@Stateless
public class DaoUsers extends AbstracDao implements IDaoUsers {

	protected final static String INSERT = "INSERT INTO USERS (PROFILE, FIRST_NAME, LAST_NAME, LOGIN, EMAIL, CREATED) VALUES (?,?,?,?,?,?)";
	protected final static String UPDATE = "UPDATE USERS SET PROFILE = ?, FIRST_NAME = ?, LAST_NAME = ?, EMAIL = ?, LOGIN = ? WHERE USER_ID = ?";
	protected final static String DELETE = "DELETE FROM USERS WHERE USER_ID = ?";
	protected final static String FIND = "SELECT * FROM USERS WHERE LOGIN = ?";
	protected final static String FIND_ALL = "SELECT * FROM USERS";

	public Object insert(Object object) {
		ToUsers toUsers = (ToUsers) object;
		try {
			setRowsAffected(jdbcTemplate.update(INSERT, new Object[] { toUsers.getUser().getProfile(), toUsers.getUser().getFirstName(), toUsers.getUser().getLastName(), toUsers.getUser().getLogin(), toUsers.getUser().getEmail(), new Date() }));
			toUsers.setSuccessfully(true);
			log.info("Has been updated the row at database");
		} catch (Exception e) {
			toUsers.setSuccessfully(false);
			log.error("An error occurred while attempting to insert audit into database whose cause is: " + e.getLocalizedMessage());
		}
		return toUsers;
	}

	public Object update(Object object) {
		ToUsers toUsers = (ToUsers) object;
		try {
			setRowsAffected(jdbcTemplate.update(UPDATE, new Object[] { toUsers.getUser().getProfile(), toUsers.getUser().getFirstName(), toUsers.getUser().getLastName(), toUsers.getUser().getEmail(), toUsers.getUser().getLogin(), toUsers.getUser().getUserId() }));
			toUsers.setSuccessfully(true);
			log.info("Has been updated the row at database");
		} catch (Exception e) {
			toUsers.setSuccessfully(false);
			log.info("An error has been ocurred during update operation with this message > " + e.getMessage());
		}
		return toUsers;
	}

	public Object delete(Object object) {
		ToUsers toUsers = (ToUsers) object;
		try {
			setRowsAffected(jdbcTemplate.update(DELETE, new Object[] { toUsers.getUser().getUserId() }));
			toUsers.setSuccessfully(true);
		} catch (Exception e) {
			toUsers.setSuccessfully(false);
		}
		return toUsers;
	}

	public Object find(Object object) {
		ToUsers toUsers = (ToUsers) object;
		try {
			toUsers.setUser((User) jdbcTemplate.queryForObject(FIND, new Object[] { toUsers.getUser().getLogin() }, new BeanPropertyRowMapper<User>(User.class)));
			toUsers.setSuccessfully(true);
			log.info("Has been find user from database");
		} catch (Exception e) {
			toUsers.setSuccessfully(false);
			log.info("An error has been ocurred during find operation with this message > " + e.getMessage());
		}
		return toUsers;
	}

	public Object findAll(Object object) {
		ToUsers toUsers = (ToUsers) object;
		toUsers.setUsers(new ArrayList<User>());
		try {
			toUsers.setUsers((ArrayList<User>) jdbcTemplate.query(FIND_ALL, new BeanPropertyRowMapper<User>(User.class)));
			setRowsAffected(toUsers.getUsers().size());
			toUsers.setSuccessfully(true);
			log.info("Has been find all rows from database");
		} catch (Exception e) {
			toUsers.setSuccessfully(false);
			log.info("An error has been ocurred during findAll operation with this message > " + e.getMessage());
		}
		return toUsers;
	}

	public Object save(Object object) {
		return null;
	}

}
