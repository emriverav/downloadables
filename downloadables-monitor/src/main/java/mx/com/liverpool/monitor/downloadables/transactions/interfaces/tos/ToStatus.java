/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Status;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated:
 * @description: class that implements TransferObject pattern
 * @since-version: 1.0
 */
@Data
public class ToStatus implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 7943961886486955820L;
	private Status status;
	private ArrayList<Status> statusList;
	private Boolean successfully;

}
