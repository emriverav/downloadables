/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.actions;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPTable;
import com.opensymphony.xwork2.ActionContext;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.models.Audit;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.commons.implementations.services.ServiceLocator;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.models.Property;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.tos.ToProperties;

/**
 * @author: Genaro Bermúdez [19/02/2018]
 * @updated: Genaro Bermúdez [01/32/2018]
 * @description: struts controller ManageProperties
 * @since-version: 1.0
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = false)
public class ManageProperties extends GapsiCommonAction {

	private static final long serialVersionUID = -6485436993072307016L;
	private ToProperties toProperties;
	private String propertyId;
	private String[] propertyKey= {"PropertyKey"};

	public String execute() throws Exception {
		toProperties = new ToProperties();
		super.readPopup();
		toProperties.setAudit(new Audit(null, null, "2", null, super.getUserLogued().getUserId()));
		toProperties = (ToProperties) ServiceLocator.getInstance().getServiceProperties().findAll(toProperties);
		if(this.getAttributesPaginator()!=null) {
			this.getAttributesPaginator().setFieldsToFliter(propertyKey);
			}
		this.setToProperties(toProperties);
		super.pageList(toProperties.getProperties());
		ActionContext.getContext().getSession().put("toProperties", toProperties);
		log.info("Has been ending ManageProperties action");
		return "success";
	}

	public String buildReport() throws Exception {
		PdfPTable table = new PdfPTable(2);
		table.addCell(cell("Clave", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Valor", Element.ALIGN_CENTER, Font.BOLD));
		for (Property property : ((ToProperties) ActionContext.getContext().getSession().get("toProperties")).getProperties()) {
			table.addCell(cell(property.getPropertyKey(), Element.ALIGN_LEFT, Font.NORMAL));
			table.addCell(cell(property.getPropertyValue(), Element.ALIGN_LEFT, Font.NORMAL));
		}
		log.info("Has been ending buildReport method");
		return super.buildReport("Listado de propiedades", table);
	}

}
