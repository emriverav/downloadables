/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.users.interfaces.daos;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.IAbstract;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: ---
 * @description: Interface to access the persistence tier
 * @since-version: 1.0
 */
public interface IDaoUsers extends IAbstract {

}
