/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.actions;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.CellStyle;

import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.PdfPTable;
import com.opensymphony.xwork2.ActionContext;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.interfaces.models.Audit;
import mx.com.liverpool.monitor.downloadables.commons.actions.GapsiCommonAction;
import mx.com.liverpool.monitor.downloadables.commons.implementations.services.ServiceLocator;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.models.Button;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.utils.Validator;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Product;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToProducts;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Mauricio Rivera [26/03/2018]
 * @description: struts controller ManageTransactions
 * @since-version: 1.0
 */
@Slf4j
@Data
@EqualsAndHashCode(callSuper = false)
public class ManageProducts extends GapsiCommonAction {

	private static final long serialVersionUID = -6485436993072307016L;
	private ToProducts toProducts;
	private Integer refTransId;
	private String operationEmail;
	private String shippingGroup;

	public String execute() throws Exception {
		super.readPopup();
		if (toProducts == null) {
			toProducts = new ToProducts();
			toProducts.setProduct(new Product());
			toProducts.getProduct().setRefTransId(refTransId);
		}
		if (super.getOperation() != null && super.getOperation().equals("saveEmail") && validator()) {
			this.setToProducts((ToProducts) ActionContext.getContext().getSession().get("toProducts"));
			return "input";
		}
		if (super.getOperation() != null && super.getOperation().equals("saveEmail")) {
			toProducts.setAudit(new Audit(refTransId, null, "5", "Cambio de mail", super.getUserLogued().getUserId()));
			toProducts.getProduct().setSaveFirstEmail(operationEmail.equals("1") ? true : false);
			if (toProducts.getAudit().getModificationType().equals("5")) {
				ServiceLocator.getInstance().getServiceProducts().save(toProducts);
			}
			toProducts.setAudit(null);
			this.setToProducts((ToProducts) ServiceLocator.getInstance().getServiceProducts().findAll(toProducts));
		}
		if (super.getOperation() != null && super.getOperation().equals("generateOrder")) {
			toProducts.setAudit(new Audit(refTransId, null, "3", "Genera orden", super.getUserLogued().getUserId()));
			toProducts.getProduct().setRefTransId(refTransId);
			ServiceLocator.getInstance().getServiceProducts().generateOrder(toProducts);
			this.setToProducts((ToProducts) ServiceLocator.getInstance().getServiceProducts().findAll(toProducts));
		}
		if (super.getOperation() != null && super.getOperation().equals("sendCodes")) {
			toProducts.setAudit(new Audit(refTransId, null, "4", "Reenviar codigo", super.getUserLogued().getUserId()));
			toProducts.getProduct().setRefTransId(refTransId);
			this.setToProducts((ToProducts) ServiceLocator.getInstance().getServiceProducts().findAll(toProducts));
			toProducts.setAudit(null);
			ServiceLocator.getInstance().getServiceProducts().sendCodes(toProducts);
			ArrayList<Button> buttons = new ArrayList<Button>();
			if (toProducts.getSuccessfully()) {
				buttons.add(new Button("Ok", "showHidePopup();"));
				super.showHidePopup("Se ha enviado el correo exitosamente", "Mensaje de la aplicación", buttons);
			} else {
				buttons.add(new Button("Ok", "showHidePopup();"));
				super.showHidePopup("No se pudo enviar el correo", "Mensaje de la aplicación", buttons);
			}
			super.setOperation(null);
			super.readPopup();
		}
		toProducts.setAudit(new Audit(refTransId, null, "2", "Detalle de la orden", super.getUserLogued().getUserId()));
		this.setToProducts((ToProducts) ServiceLocator.getInstance().getServiceProducts().findAll(toProducts));
		ActionContext.getContext().getSession().put("toProducts", toProducts);
		log.info("Has been ending ManageProducts action");
		return "success";
	}

	private boolean validator() {
		boolean emptyFileds = false;
		boolean shortLength = false;
		boolean specialCases = false;
		super.setErrorMessages(new ArrayList<String>());

		/** Validations cases << empty fileds >> */
		if (operationEmail.equals("1")) {
			if (toProducts.getProduct().getCustomerFirstChangeEmail() == null || toProducts.getProduct().getCustomerFirstChangeEmail().trim().equals("")) {
				super.getErrorMessages().add("El correo del usuario es requerido");
				emptyFileds = true;
			}
		} else {
			if (toProducts.getProduct().getCustomerSecondChangeEmail() == null || toProducts.getProduct().getCustomerSecondChangeEmail().trim().equals("")) {
				super.getErrorMessages().add("El correo del usuario es requerido");
				emptyFileds = true;
			}
		}
		if (emptyFileds) {
			return emptyFileds;
		}

		/** Validations cases << short length >> */
		if (operationEmail.equals("1")) {
			if (toProducts.getProduct().getCustomerFirstChangeEmail().trim().length() < 3) {
				super.getErrorMessages().add("El correo del usuario debe ser mas largo");
				shortLength = true;
			}
		} else {
			if (toProducts.getProduct().getCustomerSecondChangeEmail().trim().length() < 3) {
				super.getErrorMessages().add("El correo del usuario debe ser mas largo");
				shortLength = true;
			}
		}

		if (shortLength) {
			return shortLength;
		}

		/** Validations cases << special cases >> */
		if (operationEmail.equals("1")) {
			if (!Validator.mail(toProducts.getProduct().getCustomerFirstChangeEmail())) {
				super.getErrorMessages().add("El correo del usuario debe tener un formato adecuado");
				specialCases = true;
			}
		} else {
			if (!Validator.mail(toProducts.getProduct().getCustomerSecondChangeEmail())) {
				super.getErrorMessages().add("El correo del usuario debe tener un formato adecuado");
				specialCases = true;
			}
		}
		if (specialCases) {
			return specialCases;
		}

		return false;

	}

	public String buildReport() throws Exception {
		PdfPTable table = new PdfPTable(9);
		table.addCell(cell("Sku Liverpool", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Descripción", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Sku Proveedor", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Cantidad", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Fecha creación", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Orden", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Proveedor", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("¿Redimido?", Element.ALIGN_CENTER, Font.BOLD));
		table.addCell(cell("Fecha", Element.ALIGN_CENTER, Font.BOLD));
		for (Product product : ((ToProducts) ActionContext.getContext().getSession().get("toProducts")).getProducts()) {
			table.addCell(cell(product.getSku(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(product.getDescription(), Element.ALIGN_LEFT, Font.NORMAL));
			table.addCell(cell(product.getSku(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(product.getQuantity().toString(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(product.getCreation_date().toString(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(((product.getOrderId() == null) ? "" : product.getOrderId()), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(product.getSupplierName(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(product.getRedeemStatus(), Element.ALIGN_CENTER, Font.NORMAL));
			table.addCell(cell(((product.getRedeemDate() == null) ? "" : product.getRedeemDate().toString()), Element.ALIGN_CENTER, Font.NORMAL));
		}
		log.info("Has been ending buildReport method");
		return super.buildReport("Listado de productos", table);
	}

	public String buildReportExcel() throws Exception {
		List<List<CellExcel>> table = new ArrayList<List<CellExcel>>();
		List<CellExcel> title = new ArrayList<CellExcel>();
		title.add(cellExcel("Sku Liverpool", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Descripción", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Sku Proveedor", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Cantidad", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Fecha creación", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Orden", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Proveedor", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("¿Redimido?", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		title.add(cellExcel("Fecha", CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_BOLD));
		List<List<CellExcel>> rows = new ArrayList<List<CellExcel>>();
		for (Product product : ((ToProducts) ActionContext.getContext().getSession().get("toProducts")).getProducts()) {
			List<CellExcel> row = new ArrayList<CellExcel>();
			row.add(cellExcel(product.getSku(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(product.getDescription(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(product.getSku(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(product.getQuantity().toString(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(product.getCreation_date().toString(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(((product.getOrderId() == null) ? "" : product.getOrderId()), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(product.getRedeemStatus(), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			row.add(cellExcel(((product.getRedeemDate() == null) ? "" : product.getRedeemDate().toString()), CellStyle.ALIGN_CENTER, HSSFFont.BOLDWEIGHT_NORMAL));
			rows.add(row);
		}
		table.add(title);
		table.addAll(rows);
		log.info("Has been ending buildReportExcel method");
		return super.buildReportExcel("Listado de productos", table);
	}

}
