/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.implementations.daos;

import java.util.ArrayList;

import javax.ejb.Stateless;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.AbstracDao;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoProductsDetail;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.ProductDetail;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToProducts;

/**
 * @author: Genaro Bermúdez [27/02/2018]
 * @updated:
 * @description: Persistence implementation
 * @since-version: 1.0
 */
@Repository
@Slf4j
@Stateless
public class DaoProductsDetail extends AbstracDao implements IDaoProductsDetail {

	/** DAO query constans */
	protected final static String FIND_ALL = "SELECT * FROM PRODUCTS_DETAIL WHERE REF_TRANS_ID = ? AND SKU = ?";

	public ToProducts findAll(Object object) {
		ToProducts toProducts = (ToProducts) object;
		try {
			toProducts.setProductsDetail((ArrayList<ProductDetail>) jdbcTemplate.query(FIND_ALL, new Object[] { toProducts.getProduct().getRefTransId(), toProducts.getProduct().getSku() }, new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class)));
			setRowsAffected(toProducts.getProductsDetail().size());
			log.info("Has been find all rows from database");
		} catch (Exception e) {
			log.info("An error has been ocurred during findAll operation with this message > " + e.getMessage());
			throw e;
		}
		return toProducts;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

}
