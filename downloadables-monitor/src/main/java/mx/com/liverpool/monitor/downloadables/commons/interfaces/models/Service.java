/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [24/02/2018]
 * @updated: Genaro Bermúdez [26/02/2018]
 * @description: class that represents Service object
 * @since-version: 1.0
 */
@Data
public class Service implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 8970798839614895270L;
	private String name;
	private String protocol;
	private String host;
	private String port;
	private String context;
	private String resource;
	private String retries;
	private String delay;

}
