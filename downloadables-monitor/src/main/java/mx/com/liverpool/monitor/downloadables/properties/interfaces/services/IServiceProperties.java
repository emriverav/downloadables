/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.interfaces.services;

import java.util.Map;

import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.IAbstract;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.models.Property;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: Interface to the ServiceProperties
 * @since-version: 1.0
 */
public interface IServiceProperties extends IAbstract {

	/**
	 * @author: Genaro Bermúdez [02/02/2016]
	 * @updated: ---
	 * @description: Method that return the value/key part representation
	 * @params - key: string with the key to get
	 * @return - value: string with the value
	 */
	public String getValue(String key);

	/**
	 * @author: Genaro Bermúdez [02/02/2016]
	 * @updated: ---
	 * @description: Method that return the groups value/key part representation
	 * @params - prefix: string with the prefix-key to get
	 * @return - map: map with all values returned
	 */
	public Map<String, Property> getValues(String prefix);

	/**
	 * @author: Genaro Bermúdez [02/02/2016]
	 * @updated: ---
	 * @description: Method that refresh the PropertiesCache
	 * @params - N/A:
	 * @return - N/A:
	 */
	public void refreshCache();

}
