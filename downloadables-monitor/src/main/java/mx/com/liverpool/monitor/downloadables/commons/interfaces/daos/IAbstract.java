/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.commons.interfaces.daos;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: ---
 * @description: Abstrac interface to use with Service / Persistence tier
 * @since-version: 1.0
 */
public interface IAbstract {

	/**
	 * @author: Genaro Bermúdez [04/02/2018]
	 * @updated: ---
	 * @description: Method to implement insert operations
	 * @params object: castable object that represents TransferObject pattern
	 * @return: same TransferObject with results
	 */
	public Object insert(Object object);

	/**
	 * @author: Genaro Bermúdez [04/02/2018]
	 * @updated: ---
	 * @description: Method to implement update operations
	 * @params object: castable object that represents TransferObject pattern
	 * @return: same TransferObject with results
	 */
	public Object update(Object object);

	/**
	 * @author: Genaro Bermúdez [04/02/2018]
	 * @updated: ---
	 * @description: Method to implement save operations (insert or update)
	 * @params object: castable object that represents TransferObject pattern
	 * @return: same TransferObject with results
	 */
	public Object save(Object object);

	/**
	 * @author: Genaro Bermúdez [04/02/2018]
	 * @updated: ---
	 * @description: Method to implement delete operations
	 * @params object: castable object that represents TransferObject pattern
	 * @return: same TransferObject with results
	 */
	public Object delete(Object object);

	/**
	 * @author: Genaro Bermúdez [04/02/2018]
	 * @updated: ---
	 * @description: Method to implement find operations
	 * @params object: castable object that represents TransferObject pattern
	 * @return: same TransferObject with results
	 */
	public Object find(Object object);

	/**
	 * @author: Genaro Bermúdez [04/02/2018]
	 * @updated: ---
	 * @description: Method to implement findAll operations
	 * @params object: castable object that represents TransferObject pattern
	 * @return: same TransferObject with results
	 */
	public Object findAll(Object object);

	/**
	 * @author: Genaro Bermúdez [04/02/2018]
	 * @updated: ---
	 * @description: Method that get the rows affected after query
	 * @params N/A:
	 * @return: int with the rows number
	 */
	public int getRowsAffected();

}
