/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.properties.implementations.services;

import java.util.Hashtable;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.audit.implementations.services.ServiceAudit;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.daos.IDaoProperties;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.models.Property;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.services.IServiceProperties;
import mx.com.liverpool.monitor.downloadables.properties.interfaces.tos.ToProperties;

/**
 * @author: Genaro Bermúdez [04/02/2018]
 * @updated: Genaro Bermúdez [19/02/2018]
 * @description: bussiness logic Properties Service
 * @since-version: 1.0
 */
@Slf4j
@Stateless
@Interceptors(ServiceAudit.class)
public class ServiceProperties implements IServiceProperties {

	@EJB
	IDaoProperties daoProperties;

	public ToProperties findAll(Object object) {
		ToProperties toProperties = (ToProperties) object;
		try {
			toProperties = (ToProperties) daoProperties.findAll(toProperties);
			if (toProperties.getProperty() != null) {
				for (Property property : toProperties.getProperties()) {
					if (property.getPropertyId().intValue() == toProperties.getProperty().getPropertyId().intValue()) {
						toProperties.setProperty(property);
						break;
					}
				}
			}
			log.info("Has been find all properties");
		} catch (Exception e) {
			log.error("Can't obtain the properties");
		}
		return toProperties;
	}

	public ToProperties save(Object object) {
		ToProperties toProperties = (ToProperties) object;
		try {
			if (toProperties.getProperty().getPropertyId() != null && toProperties.getProperty().getPropertyId() != 0) {
				toProperties = (ToProperties) daoProperties.update(toProperties);
			}
			log.info("Has been save the property");
		} catch (Exception e) {
			log.error("Can't save the property");
		}
		return toProperties;
	}

	public String getValue(String key) {
		String value = "";
		try {
			if (PropertiesCache.getInstance().getProperties() == null) {
				Map<String, Property> properties = new Hashtable<String, Property>();
				for (Property p : ((ToProperties) daoProperties.findAll(new ToProperties())).getProperties()) {
					properties.put(p.getPropertyKey(), p);
				}
				PropertiesCache.getInstance().setProperties(properties);
			}
			value = ((Property) PropertiesCache.getInstance().getProperties().get(key)).getPropertyValue();
		} catch (Exception e) {
			log.error("Can't obtain the value");
		}
		return value;
	}

	public void refreshCache() {
		try {
			PropertiesCache.getInstance().setProperties(null);
		} catch (Exception e) {
			log.error("Can't refresh the cache");
		}
	}

	public Map<String, Property> getValues(String prefix) {
		Map<String, Property> properties = new Hashtable<String, Property>();
		try {
			PropertiesCache.getInstance().getProperties().forEach((k, v) -> {
				if (k.startsWith(prefix))
					properties.put(k, v);
			});
			log.info("Has been obtain the values");
		} catch (Exception e) {
			log.error("Can't obtain tha values");
		}
		return properties;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

}