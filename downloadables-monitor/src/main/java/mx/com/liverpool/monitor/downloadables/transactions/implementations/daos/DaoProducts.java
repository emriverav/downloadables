/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.com.liverpool.monitor.downloadables.transactions.implementations.daos;

import java.util.ArrayList;

import javax.ejb.Stateless;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import mx.com.liverpool.monitor.downloadables.commons.interfaces.daos.AbstracDao;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.daos.IDaoProducts;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Item;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.models.Product;
import mx.com.liverpool.monitor.downloadables.transactions.interfaces.tos.ToProducts;

/**
 * @author: Genaro Bermúdez [21/02/2018]
 * @updated: Mauricio Rivera [13/03/2018]
 * @description: Persistence implementation
 * @since-version: 1.0
 */
@Repository
@Slf4j
@Stateless
public class DaoProducts extends AbstracDao implements IDaoProducts {

	/** DAO query constans */
	protected final static String FIND_ALL = "SELECT O.REF_TRANS_ID, O.SHIPPING_GROUP, O.CREATION_DATE, O.CHANNEL, O.ERROR_CODE, O.SEND_CODE_RETRIES, (CASE WHEN C.LOGIN_ID IS NULL THEN 'Invitado' ELSE 'Registrado' END) LOGIN_ID, C.CUSTOMER_FIRST_NAME, C.CUSTOMER_EMAIL, C.CUSTOMER_CELPHONE, C.CUSTOMER_APATERNO, C.CUSTOMER_AMATERNO, C.CUSTOMER_FIRST_CHANGE_EMAIL, C.CUSTOMER_SECOND_CHANGE_EMAIL, P.SKU, P.ORDER_ID, P.DESCRIPTION, P.QUANTITY, P.EXTERNAL_ID, (SELECT S.SUPPLIER_VALUE FROM SUPPLIERS S WHERE S.SUPPLIER_KEY = TO_CHAR(P.SUPPLIER_ID)) SUPPLIER_NAME, (SELECT E.LIVERPOOL_DESCRIPTION FROM ERRORS_CODES E WHERE E.ERROR_CODE = O.ERROR_CODE) ORDER_STATUS FROM PRODUCTS P INNER JOIN ORDERS O ON P.REF_TRANS_ID = O.REF_TRANS_ID INNER JOIN CLIENTS C ON P.REF_TRANS_ID = C.REF_TRANS_ID WHERE O.REF_TRANS_ID = ?";
	protected final static String UPDATE_FIRST = "UPDATE CLIENTS SET CUSTOMER_FIRST_CHANGE_EMAIL = ? WHERE REF_TRANS_ID = ?";
	protected final static String UPDATE_SECOND = "UPDATE CLIENTS SET CUSTOMER_SECOND_CHANGE_EMAIL = ? WHERE REF_TRANS_ID = ?";
	protected final static String FIND_ALL_GENERATE = "SELECT * FROM PRODUCTS WHERE REF_TRANS_ID = ? AND ERROR_CODE IS NULL OR REF_TRANS_ID = ? AND ERROR_CODE <> 1004 AND ERROR_CODE <>21";
	protected final static String FIND_ALL_SEND = "SELECT * FROM PRODUCTS WHERE REF_TRANS_ID = ? AND ERROR_CODE IS NULL";
	protected final static String UPDATE_RETRIES = "UPDATE ORDERS SET SEND_CODE_RETRIES = (SELECT (CASE WHEN SEND_CODE_RETRIES IS NULL THEN 1 ELSE (SEND_CODE_RETRIES+1) END) SEND_CODE_RETRIES FROM ORDERS WHERE REF_TRANS_ID = ?) WHERE REF_TRANS_ID = ?";

	public ToProducts findAll(Object object) {
		ToProducts toProducts = (ToProducts) object;
		toProducts.setProducts(new ArrayList<Product>());
		try {
			toProducts.setProducts((ArrayList<Product>) jdbcTemplate.query(FIND_ALL, new Object[] { toProducts.getProduct().getRefTransId() }, new BeanPropertyRowMapper<Product>(Product.class)));
			if (toProducts.getProducts().size() > 0) {
				toProducts.setProduct(toProducts.getProducts().get(0));
			}
			setRowsAffected(toProducts.getProducts().size());
			log.info("Has been find all rows from database");
		} catch (Exception e) {
			log.info("An error has been ocurred during findAll operation with this message > " + e.getMessage());
			throw e;
		}
		return toProducts;
	}

	public ToProducts findAllGenerate(ToProducts toProducts) {
		try {
			toProducts.getOrderDi().setItems((ArrayList<Item>) jdbcTemplate.query(FIND_ALL_GENERATE, new Object[] { toProducts.getProduct().getRefTransId(), toProducts.getProduct().getRefTransId()  }, new BeanPropertyRowMapper<Item>(Item.class)));
			setRowsAffected(toProducts.getOrderDi().getItems().size());
			log.info("Has been find all rows from database");
		} catch (Exception e) {
			log.info("An error has been ocurred during findAll operation with this message > " + e.getMessage());
			throw e;
		}
		return toProducts;
	}

	public ToProducts findAllSend(ToProducts toProducts) {
		try {
			toProducts.getOrderDi().setItems((ArrayList<Item>) jdbcTemplate.query(FIND_ALL_SEND, new Object[] { toProducts.getProduct().getRefTransId() }, new BeanPropertyRowMapper<Item>(Item.class)));
			setRowsAffected(toProducts.getOrderDi().getItems().size());
			log.info("Has been find all rows from database");
		} catch (Exception e) {
			log.info("An error has been ocurred during findAll operation with this message > " + e.getMessage());
			throw e;
		}
		return toProducts;
	}

	public ToProducts update(Object object) {
		ToProducts toProducts = (ToProducts) object;
		try {
			String UPDATE = toProducts.getProduct().getSaveFirstEmail() ? UPDATE_FIRST : UPDATE_SECOND;
			String VALUE = toProducts.getProduct().getSaveFirstEmail() ? toProducts.getProduct().getCustomerFirstChangeEmail() : toProducts.getProduct().getCustomerSecondChangeEmail();
			setRowsAffected(jdbcTemplate.update(UPDATE, new Object[] { VALUE, toProducts.getProduct().getRefTransId() }));
			log.info("Has been update the detail database");
		} catch (Exception e) {
			log.info("An error has been ocurred during update operation with this message > " + e.getMessage());
			throw e;
		}
		return toProducts;
	}

	public ToProducts updateRetriesSendCodes(ToProducts toProducts) {
		try {
			setRowsAffected(jdbcTemplate.update(UPDATE_RETRIES, new Object[] { toProducts.getProduct().getRefTransId(), toProducts.getProduct().getRefTransId() }));
			log.info("Has been update the detail database");
		} catch (Exception e) {
			log.info("An error has been ocurred during update operation with this message > " + e.getMessage());
			throw e;
		}
		return toProducts;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

}
