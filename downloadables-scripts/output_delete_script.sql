--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: ---
-- description: Delete data to table PROPERTIES
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.PROPERTIES;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: ---
-- description: Delete data to table USERS
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.USERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [10/01/2018]
-- updated: ---
-- description: Delete data to table SUPPLIERS
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.SUPPLIERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [10/01/2018]
-- updated: ---
-- description: Delete data to table APPLICATIONS
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.APPLICATIONS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [27/01/2018]
-- updated: ---
-- description: Delete data to table ERRORS_CODES
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.ERRORS_CODES;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: ---
-- description: Drop table PROPERTIES
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.PROPERTIES DROP CONSTRAINT PK_PROPERTIES;
DROP TABLE APL_DI.PROPERTIES;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [08/01/2018]
-- updated: ---
-- description: Drop table USERS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.USERS DROP CONSTRAINT PK_USERS;
DROP TABLE APL_DI.USERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: ---
-- description: Drop table SUPPLIERS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.SUPPLIERS DROP CONSTRAINT PK_SUPPLIERS;
DROP TABLE APL_DI.SUPPLIERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [08/01/2018]
-- updated: ---
-- description: Drop table APPLICATIONS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.APPLICATIONS DROP CONSTRAINT PK_APPLICATIONS;
DROP TABLE APL_DI.APPLICATIONS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table PRODUCTS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.PRODUCTS DROP CONSTRAINT PK_PRODUCTS;
DROP TABLE APL_DI.PRODUCTS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table PRODUCTS_DETAIL
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.PRODUCTS_DETAIL DROP CONSTRAINT PK_PRODUCTS_DETAIL;
DROP TABLE APL_DI.PRODUCTS_DETAIL;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table ORDERS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.ORDERS DROP CONSTRAINT PK_ORDERS;
DROP TABLE APL_DI.ORDERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table CLIENTS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.CLIENTS DROP CONSTRAINT PK_CLIENTS;
DROP TABLE APL_DI.CLIENTS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table AUDIT_ADAPTER
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.AUDIT_ADAPTER DROP CONSTRAINT PK_AUDIT_ADAPTER;
DROP TABLE APL_DI.AUDIT_ADAPTER;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [27/02/2018]
-- updated: ---
-- description: Drop table AUDIT_MONITOR
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.AUDIT_MONITOR DROP CONSTRAINT PK_AUDIT_MONITOR;
DROP TABLE APL_DI.AUDIT_MONITOR;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table AUDIT_ADAPTER
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.ERRORS_CODES DROP CONSTRAINT PK_ERRORS_CODES;
DROP TABLE APL_DI.ERRORS_CODES;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
