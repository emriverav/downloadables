--------------------------------------------------------
-- author: Genaro Bermúdez [10/01/2018]
-- updated: ---
-- description: Delete data to table APPLICATIONS
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.APPLICATIONS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
