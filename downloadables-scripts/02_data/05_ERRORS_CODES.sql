--------------------------------------------------------
-- author: Genaro Bermúdez [10/02/2018]
-- updated: Perla Martínez [07/03/2018]
-- description: Insert data to table ERRORS_CODES
-- schema: APL_DI
--------------------------------------------------------


-- INTCOMEX CODES --
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (10, 'Invalid signature', 																							'Firma inválida');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (11, 'Invalid time stamp', 																							'Fecha y hora inválida');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (12, 'Request time stamp no longer valid', 																			'Fecha y hora del request inválida');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (13, 'Invalid API Key', 																							'Llave inválida');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (14, 'IP address not registered', 																					'IP no registrada');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (20, 'Invalid product', 																							'Producto inválido');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (21, 'Unable to add product to an order SKU <SKU> is inactive or is not authorized for the customer specified', 	'No es posible agregar uno(s) a la orden');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (22, 'Customer not authorized to receive product sales data', 														'Liverpool no está autorizado para recibir datos de venta');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (30, 'Invalid order', 																								'Orden inválida');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (31, 'Invalid order specified Make sure that the order is properly formatted in the request body', 					'Orden mal formada');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (32, 'Invalid list of items specified Make sure that the list of items is properly formatted in the request body', 	'Artículos inválidos');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (40, 'Invalid customer', 																							'Contacte a Intcomex');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (41, 'Customer is invalid', 																						'Contacte a Intcomex');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (50, 'No downloadable products found in the order specified', 														'No hay artículos descargables en la orden');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (51, 'Invalid product key', 																						'Llave de producto inválida');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (60, 'Catalog file not found', 																						'No es posible recuperar el catálogo de producto');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (100, 'Excessive request attempts detected', 																		'Peticiones excesivas');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (900, 'An error has ocurred Please contact support for assistance', 												'Ocurrio un error, contacte a Intcomex');

-- ADAPTER / MONITOR CODES --
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1000, '', 'Orden generada y surtida exitosamente');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1001, '', 'Error al procesar la orden');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1002, '', 'Error al procesar códigos de la orden');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1003, '', 'Ya no es posible generar la petición');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1004, '', 'Proveedor inválido');

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
