--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: ---
-- description: Insert data to table USERS
-- schema: APL_DI
--------------------------------------------------------

INSERT INTO APL_DI.USERS(PROFILE, FIRST_NAME, LAST_NAME, LOGIN, EMAIL, CREATED) VALUES ('FULL', 'Perla', 'Martínez', 'pmartinez', 'perlamartinez@liverpool.com.mx', SYSTIMESTAMP);
INSERT INTO APL_DI.USERS(PROFILE, FIRST_NAME, LAST_NAME, LOGIN, EMAIL, CREATED) VALUES ('ADMIN', 'Genaro', 'Bermúdez', 'gbermudez', 'gbermudez@grupoasesores.com.mx', SYSTIMESTAMP);
INSERT INTO APL_DI.USERS(PROFILE, FIRST_NAME, LAST_NAME, LOGIN, EMAIL, CREATED) VALUES ('AGENT', 'Mauricio', 'Rivera', 'mrivera', 'mrivera@grupoasesores.com.mx', SYSTIMESTAMP);

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
