@Echo OFF

chcp 1250
copy /B ".\01_tables\*.sql" + ".\02_data_QA\*.sql" "output_create_script_QA.sql"
copy /B ".\02_data_QA\rollbacks\*.sql" + ".\01_tables\rollbacks\*.sql"  "output_delete_script_QA.sql"

Exit