@Echo OFF

chcp 1250
copy /B ".\01_tables\*.sql" + ".\02_data\*.sql" "output_create_script.sql"
copy /B ".\02_data\rollbacks\*.sql" + ".\01_tables\rollbacks\*.sql"  "output_delete_script.sql"

Exit