--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table PROPERTIES
-- schema: APL_DI
--------------------------------------------------------

CREATE TABLE APL_DI.PROPERTIES
(PROPERTY_ID	NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
PROPERTY_KEY	VARCHAR(500) UNIQUE,
PROPERTY_VALUE	VARCHAR(2000),
DESCRIPTION		VARCHAR(2000));

ALTER TABLE APL_DI.PROPERTIES ADD CONSTRAINT PK_PROPERTIES PRIMARY KEY (PROPERTY_ID);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
