--------------------------------------------------------
-- author: Genaro Bermúdez [27/02/2018]
-- updated: Genaro Bermúdez [01/32/2018]
-- description: Create table AUDIT_MONITOR
-- schema: APL_DI
---------------------------------------------------------

CREATE TABLE AUDIT_MONITOR
(ID					NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
REF_TRANS_ID		NUMBER(20),
CREATION_DATE		TIMESTAMP NOT NULL,
MODIFICATION_TYPE	CHAR(2) NOT NULL,
MODIFICATION_IDS	VARCHAR(1000),
USER_ID				NUMBER NOT NULL);

ALTER TABLE APL_DI.AUDIT_MONITOR ADD CONSTRAINT PK_AUDIT_MONITOR PRIMARY KEY (ID);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
