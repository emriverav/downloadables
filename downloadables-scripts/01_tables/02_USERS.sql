--------------------------------------------------------
-- author: Genaro Bermúdez [08/01/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table USERS
-- schema: APL_DI
--------------------------------------------------------

CREATE TABLE APL_DI.USERS
(USER_ID	NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
PROFILE		VARCHAR(100),
FIRST_NAME	VARCHAR(50),
LAST_NAME	VARCHAR(50),
LOGIN		VARCHAR(50) UNIQUE,
EMAIL		VARCHAR(50) UNIQUE,
CREATED		TIMESTAMP);

ALTER TABLE APL_DI.USERS ADD CONSTRAINT PK_USERS PRIMARY KEY (USER_ID);

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
