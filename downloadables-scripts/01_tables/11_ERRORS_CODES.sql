--------------------------------------------------------
-- author: Genaro Bermúdez [27/02/2018]
-- updated: ---
-- description: Create table ERRORS_CODES
-- schema: APL_DI
--------------------------------------------------------

CREATE TABLE APL_DI.ERRORS_CODES
(ID						NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
ERROR_CODE				NUMBER(5) UNIQUE, 
INTCOMEX_DESCRIPTION	VARCHAR(250),
LIVERPOOL_DESCRIPTION	VARCHAR(250));

ALTER TABLE APL_DI.ERRORS_CODES ADD CONSTRAINT PK_ERRORS_CODES PRIMARY KEY (ID);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
