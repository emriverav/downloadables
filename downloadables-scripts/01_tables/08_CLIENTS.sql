--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table CLIENTS
-- schema: APL_DI
---------------------------------------------------------

CREATE TABLE CLIENTS
(ID								NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
REF_TRANS_ID					NUMBER(20) NOT NULL,
CUSTOMER_FIRST_NAME				VARCHAR(60),
CUSTOMER_APATERNO				VARCHAR(60),
CUSTOMER_AMATERNO				VARCHAR(60),
CUSTOMER_FIRST_CHANGE_EMAIL 	VARCHAR(150),
CUSTOMER_SECOND_CHANGE_EMAIL	VARCHAR(150),
LOGIN_ID						VARCHAR(255),
PROFILE_ID						VARCHAR(255),
CUSTOMER_EMAIL					VARCHAR(150),
CUSTOMER_CELPHONE				VARCHAR(30));

ALTER TABLE APL_DI.CLIENTS ADD CONSTRAINT PK_CLIENTS PRIMARY KEY (ID, REF_TRANS_ID);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
