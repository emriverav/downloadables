--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table PRODUCTS_DETAIL
-- schema: APL_DI
---------------------------------------------------------

CREATE TABLE PRODUCTS_DETAIL
(ID				NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
REF_TRANS_ID	NUMBER(20) NOT NULL,
SKU				VARCHAR(40) NOT NULL,
INDEX_ID		NUMBER(3) NOT NULL,
PRODUCT_KEY		VARCHAR(300),
URL				VARCHAR(500),
REDEEM_STATUS   VARCHAR(15),
REDEEM_DATE		VARCHAR(40),
INSTRUCTIONS	VARCHAR(1000));

ALTER TABLE APL_DI.PRODUCTS_DETAIL ADD CONSTRAINT PK_PRODUCTS_DETAIL PRIMARY KEY (ID, REF_TRANS_ID, SKU, INDEX_ID);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
