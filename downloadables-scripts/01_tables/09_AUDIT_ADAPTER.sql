--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table AUDIT_ADAPTER
-- schema: APL_DI
---------------------------------------------------------

CREATE TABLE AUDIT_ADAPTER
(ID				NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
REF_TRANS_ID	NUMBER(20) NOT NULL,
CREATION_DATE	TIMESTAMP NOT NULL,
REQUEST_HEADER	VARCHAR(2000) NOT NULL,
REQUEST_BODY	VARCHAR(2000) NOT NULL,
REQUEST_TYPE 	NUMBER(1),
RESPONSE_HEADER	VARCHAR(2000),
RESPONSE_BODY	VARCHAR(2000),
ENDPOINT		VARCHAR(500));

ALTER TABLE APL_DI.AUDIT_ADAPTER ADD CONSTRAINT PK_AUDIT_ADAPTER PRIMARY KEY (ID, REF_TRANS_ID);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
