--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table CLIENTS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.CLIENTS DROP CONSTRAINT PK_CLIENTS;
DROP TABLE APL_DI.CLIENTS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
