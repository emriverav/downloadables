--------------------------------------------------------
-- author: Genaro Bermúdez [08/01/2018]
-- updated: ---
-- description: Drop table USERS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.USERS DROP CONSTRAINT PK_USERS;
DROP TABLE APL_DI.USERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
