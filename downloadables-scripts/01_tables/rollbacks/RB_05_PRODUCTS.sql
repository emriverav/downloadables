--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table PRODUCTS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.PRODUCTS DROP CONSTRAINT PK_PRODUCTS;
DROP TABLE APL_DI.PRODUCTS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
