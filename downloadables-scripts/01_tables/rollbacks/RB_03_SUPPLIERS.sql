--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: ---
-- description: Drop table SUPPLIERS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.SUPPLIERS DROP CONSTRAINT PK_SUPPLIERS;
DROP TABLE APL_DI.SUPPLIERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
