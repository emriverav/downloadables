--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table AUDIT_ADAPTER
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.ERRORS_CODES DROP CONSTRAINT PK_ERRORS_CODES;
DROP TABLE APL_DI.ERRORS_CODES;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
