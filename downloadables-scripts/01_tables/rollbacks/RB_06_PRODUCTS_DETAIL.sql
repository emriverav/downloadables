--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table PRODUCTS_DETAIL
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.PRODUCTS_DETAIL DROP CONSTRAINT PK_PRODUCTS_DETAIL;
DROP TABLE APL_DI.PRODUCTS_DETAIL;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
