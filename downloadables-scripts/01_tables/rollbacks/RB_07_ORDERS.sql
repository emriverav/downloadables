--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table ORDERS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.ORDERS DROP CONSTRAINT PK_ORDERS;
DROP TABLE APL_DI.ORDERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
