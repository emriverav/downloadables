--------------------------------------------------------
-- author: Genaro Bermúdez [08/01/2018]
-- updated: ---
-- description: Drop table APPLICATIONS
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.APPLICATIONS DROP CONSTRAINT PK_APPLICATIONS;
DROP TABLE APL_DI.APPLICATIONS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
