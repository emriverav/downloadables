--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: ---
-- description: Drop table PROPERTIES
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.PROPERTIES DROP CONSTRAINT PK_PROPERTIES;
DROP TABLE APL_DI.PROPERTIES;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
