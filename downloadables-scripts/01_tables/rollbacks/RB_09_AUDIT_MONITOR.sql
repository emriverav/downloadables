--------------------------------------------------------
-- author: Genaro Bermúdez [27/02/2018]
-- updated: ---
-- description: Drop table AUDIT_MONITOR
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.AUDIT_MONITOR DROP CONSTRAINT PK_AUDIT_MONITOR;
DROP TABLE APL_DI.AUDIT_MONITOR;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
