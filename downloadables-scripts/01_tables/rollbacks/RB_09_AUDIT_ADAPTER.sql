--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: ---
-- description: Drop table AUDIT_ADAPTER
-- schema: APL_DI
--------------------------------------------------------

ALTER TABLE APL_DI.AUDIT_ADAPTER DROP CONSTRAINT PK_AUDIT_ADAPTER;
DROP TABLE APL_DI.AUDIT_ADAPTER;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
