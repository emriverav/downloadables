--------------------------------------------------------
-- author: Genaro Bermúdez [10/02/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table SUPPLIERS
-- schema: APL_DI
--------------------------------------------------------

CREATE TABLE APL_DI.SUPPLIERS
(SUPPLIER_ID	NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
SUPPLIER_KEY	VARCHAR(100) UNIQUE,
SUPPLIER_VALUE	VARCHAR(100),
DESCRIPTION		VARCHAR(500));

ALTER TABLE APL_DI.SUPPLIERS ADD CONSTRAINT PK_SUPPLIERS PRIMARY KEY (SUPPLIER_ID);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
