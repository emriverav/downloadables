--------------------------------------------------------
-- author: Genaro Bermúdez [10/02/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table PRODUCTS
-- schema: APL_DI
--------------------------------------------------------

CREATE TABLE APL_DI.PRODUCTS
(ID				NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
REF_TRANS_ID	NUMBER(20) NOT NULL,
SKU				VARCHAR(40) NOT NULL,
DESCRIPTION		VARCHAR(300),
QUANTITY		NUMBER(3) NOT NULL,
EXTERNAL_ID		VARCHAR(40) NOT NULL,
SUPPLIER_ID		NUMBER(20) NOT NULL,
ORDER_ID		VARCHAR(60),
ERROR_CODE		NUMBER(5));

ALTER TABLE APL_DI.PRODUCTS ADD CONSTRAINT PK_PRODUCTS PRIMARY KEY (ID, REF_TRANS_ID, SKU);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
