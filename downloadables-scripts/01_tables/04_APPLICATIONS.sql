--------------------------------------------------------
-- author: Genaro Bermúdez [10/02/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table APPLICATIONS
-- schema: APL_DI
--------------------------------------------------------

CREATE TABLE APL_DI.APPLICATIONS
(APPLICATION_ID		NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
APPLICATION_NAME	VARCHAR(100) UNIQUE);

ALTER TABLE APL_DI.APPLICATIONS ADD CONSTRAINT PK_APPLICATIONS PRIMARY KEY (APPLICATION_ID);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
