--------------------------------------------------------
-- author: Genaro Bermúdez [14/02/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Create table ORDERS
-- schema: APL_DI
---------------------------------------------------------

CREATE TABLE ORDERS
(ID					NUMBER GENERATED ALWAYS AS IDENTITY MINVALUE 1 MAXVALUE 99999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE  NOT NULL ENABLE,
CREATION_DATE		TIMESTAMP DEFAULT SYSDATE NOT NULL,
REF_TRANS_ID		NUMBER(20) NOT NULL,
SHIPPING_GROUP		VARCHAR(254) NOT NULL,
CHANNEL				VARCHAR(10) NOT NULL,
SEND_CODE_RETRIES	NUMBER(1),
ERROR_CODE			NUMBER(4));

ALTER TABLE APL_DI.ORDERS ADD CONSTRAINT PK_ORDERS PRIMARY KEY (ID, REF_TRANS_ID,SHIPPING_GROUP);
	  
--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
