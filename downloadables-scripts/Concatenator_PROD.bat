@Echo OFF

chcp 1250
copy /B ".\01_tables\*.sql" + ".\02_data_PROD\*.sql" "output_create_script_PROD.sql"
copy /B ".\02_data_PROD\rollbacks\*.sql" + ".\01_tables\rollbacks\*.sql"  "output_delete_script_PROD.sql"

Exit