--------------------------------------------------------
-- author: Genaro Bermúdez [10/02/2018]
-- updated: ---
-- description: Insert data to table ERRORS_CODES
-- schema: APL_DI
--------------------------------------------------------


-- INTCOMEX CODES --
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (10, 'Error 10 Intcomex', 'Error 10 Liverpool');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (21, 'Error 21 Intcomex', 'Error 21 Liverpool');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (990, 'Error 990 Intcomex', 'Error 990 Liverpool');

-- ADAPTER / MONITOR CODES --
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1000, '', 'Orden generada y surtida exitosamente');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1001, '', 'Error al procesar la orden');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1002, '', 'Error al procesar códigos de la orden');
INSERT INTO APL_DI.ERRORS_CODES(ERROR_CODE, INTCOMEX_DESCRIPTION, LIVERPOOL_DESCRIPTION) VALUES (1003, '', 'Ya no es posible generar la petición');

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
