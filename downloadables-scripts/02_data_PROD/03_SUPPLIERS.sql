--------------------------------------------------------
-- author: Genaro Bermúdez [10/02/2018]
-- updated: Genaro Bermúdez [27/02/2018]
-- description: Insert data to table SUPPLIERS
-- schema: APL_DI
--------------------------------------------------------

INSERT INTO APL_DI.SUPPLIERS(SUPPLIER_KEY, SUPPLIER_VALUE, DESCRIPTION) VALUES ('102', 'Intcomex', 'Any description for this supplier 01');

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
