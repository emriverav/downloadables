--------------------------------------------------------
-- author: Genaro Bermúdez [02/03/2018]
-- updated: ---
-- description: Script to view audit monitor
-- schema: APL_DI
--------------------------------------------------------


SELECT U.FIRST_NAME, U.LAST_NAME, 
(CASE A.MODIFICATION_TYPE 
  WHEN '1' THEN 'Login' 
  WHEN '2' THEN 'Consultar propiedades'
  WHEN '3' THEN 'Mofificar propiedades'
  WHEN '4' THEN 'Consultar usuarios'
  WHEN '5' THEN 'Agregar usuario'
  WHEN '6' THEN 'Modificar usuario'
  WHEN '7' THEN 'Eliminar usuario'
  WHEN '8' THEN 'Consultar transacciones'
  WHEN '9' THEN 'Consultar orden > REF_TRANS_ID debe mostrarese'
  WHEN '10' THEN 'Cambiar correo > REF_TRANS_ID debe mostrarse'
  WHEN '11' THEN 'Generar orden > REF_TRANS_ID debe mostrarse y MODIFICATION_IDS debe mostrar los SKUs separados por coma'
  WHEN '12' THEN 'Surtir orden > REF_TRANS_ID debe mostrase y MODIFICATION_IDS debe mostrar los SKUs separados por coma'
  WHEN '13' THEN 'Reenviar notificación'
END) OPERATION_TYPE, A.REF_TRANS_ID, A.MODIFICATION_IDS, A.CREATION_DATE
FROM AUDIT_MONITOR A INNER JOIN USERS U ON A.USER_ID = U.USER_ID WHERE ROWNUM <= 100 ORDER BY A.CREATION_DATE ASC;	

--------------------------------------------------------
-- End
--------------------------------------------------------

