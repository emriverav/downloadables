--------------------------------------------------------
-- author: Genaro Bermúdez [10/01/2018]
-- updated: ---
-- description: Delete data to table SUPPLIERS
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.SUPPLIERS;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
