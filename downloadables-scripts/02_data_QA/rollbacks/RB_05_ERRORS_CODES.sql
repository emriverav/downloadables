--------------------------------------------------------
-- author: Genaro Bermúdez [27/01/2018]
-- updated: ---
-- description: Delete data to table ERRORS_CODES
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.ERRORS_CODES;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
