--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: ---
-- description: Delete data to table PROPERTIES
-- schema: APL_DI
--------------------------------------------------------

TRUNCATE TABLE APL_DI.PROPERTIES;

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
