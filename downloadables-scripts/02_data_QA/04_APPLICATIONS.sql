--------------------------------------------------------
-- author: Genaro Bermúdez [10/02/2018]
-- updated: ---
-- description: Insert data to table APPLICATIONS
-- schema: APL_DI
--------------------------------------------------------

INSERT INTO APL_DI.APPLICATIONS(APPLICATION_NAME) VALUES ('App-01');
INSERT INTO APL_DI.APPLICATIONS(APPLICATION_NAME) VALUES ('App-02');
INSERT INTO APL_DI.APPLICATIONS(APPLICATION_NAME) VALUES ('App-03');

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
