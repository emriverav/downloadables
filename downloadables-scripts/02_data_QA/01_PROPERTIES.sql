--------------------------------------------------------
-- author: Genaro Bermúdez [04/01/2018]
-- updated: Genaro Bermúdez [28/02/2018]
-- description: Insert data to table PROPERTIES
-- schema: APL_DI
--------------------------------------------------------

-- GENERAL SETTINGS --
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_URL_APP', 'http://localhost:8080/downloadables-monitor', 'Main URL to use when send mail notifications');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_RESOURCES_PATH', 'http://localhost:8080/downloadables-monitor/', 'Main URL to use when send mail notifications');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('MSG_BODY_MAIL_NOTIFICATION_SEND_CODE', 'Hola {0}, <br><br>Tus sku son los siguientes: {1}<br><br>Referencia: {2}<br><br>Gracias por usar la plataforma Liverpool', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('MSG_SUBJECT_MAIL_NOTIFICATION_SEND_CODE', 'Downloadables Plus 2018', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('ENVIRONMENT_LOCAL', 'false', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAX_RETRIES_SEND_CODES', '3', 'Any description');

-- INCOMEX SERVICE PLACE ORDER SETTINGS --
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_NAME', 'Incomex place order', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_PROTOCOL', 'https', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_HOST', 'intcomex-test.apigee.net', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_CONTEXT', 'v1', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_RETRIES', '3', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_DELAY', '1000', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_API_KEY', 'be01c955-83e0-4f06-989a-68c7ee943d6d', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_ACCES_KEY', '1afc48d5-e3e7-43ae-a680-9122c07eea66', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_RESOURCE', 'placeorder', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PLACE_ORDER_PORT', '8080', 'Any description');

-- INCOMEX SERVICE PURCHASE ORDER SETTINGS --
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_NAME', 'Incomex place order', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_PROTOCOL', 'https', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_HOST', 'intcomex-test.apigee.net', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_CONTEXT', 'v1', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_RETRIES', '3', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_DELAY', '1000', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_API_KEY', 'be01c955-83e0-4f06-989a-68c7ee943d6d', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_ACCES_KEY', '1afc48d5-e3e7-43ae-a680-9122c07eea66', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_RESOURCE', 'purchaseesdproducts', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_PURCHASE_ORDER_PORT', '8080', 'Any description');

-- INCOMEX SERVICE GET STATUS KEY STATUS SETTINGS --
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_NAME', 'Incomex place order', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_PROTOCOL', 'https', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_HOST', 'intcomex-test.apigee.net', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_CONTEXT', 'v1', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_RETRIES', '3', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_DELAY', '1000', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_API_KEY', 'be01c955-83e0-4f06-989a-68c7ee943d6d', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_ACCES_KEY', '1afc48d5-e3e7-43ae-a680-9122c07eea66', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_RESOURCE', 'getproductkeystatus', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_PORT', '8080', 'Any description');

-- ADAPTER SERVICE GENERATE CODE --
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GENERATE_CODE_NAME', 'Adapter Generate Code', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GENERATE_CODE_PROTOCOL', 'http', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GENERATE_CODE_HOST', 'localhost', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GENERATE_CODE_PORT', '8080', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GENERATE_CODE_CONTEXT', 'adapter/api/v1', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GENERATE_CODE_RESOURCE', 'orders', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GENERATE_CODE_RETRIES', '2', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GENERATE_CODE_DELAY', '500', 'Any description');

-- ADAPTER SERVICE GET ORDER STATUS --
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_NAME', 'Adapter Get Order Status', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_PROTOCOL', 'http', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_HOST', 'localhost', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_PORT', '8080', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_CONTEXT', 'adapter/api/v1', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_RESOURCE', 'orders', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_RETRIES', '2', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ADAPTER_SERVICE_GET_ORDER_STATUS_DELAY', '500', 'Any description');

-- ATG MQ SETTINGS --
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ATG_MQ_NAME', 'QM.GW.BRK.WBI.QA', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ATG_MQ_HOST', '172.16.204.99', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ATG_MQ_CHANNEL', 'CH.T.ATG.WMB.SVRCONN', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ATG_MQ_PORT', '1416', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ATG_MQ_QUEUE', 'QA.VAD.ATG.DI.UPDATE', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ATG_MQ_DELAY', '1000', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ATG_MQ_RETRIES', '2', 'Any description');

-- ACTIVE DIRECTORY SETTINGS --
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ACTIVE_DIRECTORY_USERNAME', 'valijas', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ACTIVE_DIRECTORY_PASSWORD', 'V4l1j4s4dm1n', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ACTIVE_DIRECTORY_BASE', 'DC=Puerto,DC=liverpool,DC=com,DC=mx', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ACTIVE_DIRECTORY_DN', 'CN=valijas,OU=Aplicaciones y Servicios,OU=Administracion,DC=Puerto,DC=liverpool,DC=com,DC=mx', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ACTIVE_DIRECTORY_URL', 'ldap://172.17.201.81:389', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ACTIVE_DIRECTORY_FACTORY', 'com.sun.jndi.ldap.LdapCtxFactory', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ACTIVE_DIRECTORY_AUTHENTICATION', 'Simple', 'Any description');
INSERT INTO APL_DI.PROPERTIES(PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_ACTIVE_DIRECTORY_REFERRAL', 'follow', 'Any description');		

-- SMTP SETTINGS --
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_MAIN_TYPE', '0', 'Indicates the type of connection to the server. Arguments=1 | JNDI=0. This field is required by the application and can not be omitted');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_AUTENTICATE', 'N', 'Indicates if auth is required to the service mail. This field is required by the application and can not be omitted');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_JNDI_NAME', 'mail/downloadbles', 'Indicates the resource MAPPEDNAME attribute');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_JNDI_LOOKUP', 'java:/mail/downloadbles', 'Indicates resource JNDI attribute');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_HOST', 'mail.smtp.host|smtp.gmail.com', 'Indicate smtp value corresponding to: mail.smtp.host and its value');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_PORT', 'mail.smtp.port|465', 'Indicate smtp value  corresponding to: mail.smtp.por and its value');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_AUTH', 'mail.smtp.auth|true', 'Indicate smtp corresponding to: mail.smtp.auth and its value ');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_USER', 'mail.smtp.user|gapsiliverpool@gmail.com', 'Indicate smtp corresponding to: mail.smtp.user and its value. This field is required by the application and can not be omitted');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_PASSWORD', 'mail.smtp.password|g4p5i_321', 'Indicate smtp corresponding to: mail.smtp.password and its value');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_ALIAS', 'mail.smtp.alias|Downloadables Plus', 'Indicate smtp corresponding to: mail.smtp.alias and its value');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_TYPE', 'mail.smtp.type|NULL', 'Indicate smtp corresponding to: mail.smtp.type and its value');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_TLS', 'mail.smtp.starttls.enable|true', 'Indicate smtp corresponding to: mail.smtp.starttls.enable and its value');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_SSL', 'mail.smtp.EnableSSL.enable|true', 'Indicate smtp corresponding to: mail.smtp.EnableSSL.enable and its value');
INSERT INTO APL_DI.PROPERTIES (PROPERTY_KEY, PROPERTY_VALUE, DESCRIPTION) VALUES ('CONF_MAIL_SMTP_FACTORY', 'mail.smtp.socketFactory.class|javax.net.ssl.SSLSocketFactory', 'Indicate smtp corresponding to: mail.smtp.socketFactory.class and its value');

--------------------------------------------------------
-- Confirmation
--------------------------------------------------------
COMMIT;
