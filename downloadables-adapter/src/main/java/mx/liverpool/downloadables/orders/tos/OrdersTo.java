/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.tos;

import java.io.Serializable;

import lombok.Data;
import mx.liverpool.downloadables.commons.models.Request;
import mx.liverpool.downloadables.commons.models.Response;
import mx.liverpool.downloadables.intcomex.placeOrder.models.IntcomexPlaceOrderOUT;
import mx.liverpool.downloadables.intcomex.purchaseesdProducts.models.IntcomexPurchaseesdProductsOut;
import mx.liverpool.downloadables.orders.models.Order;
import mx.liverpool.downloadables.orders.models.OrderDi;
import mx.liverpool.downloadables.orders.models.OrderDiOut;

/**
 * @author: Genaro Bermúdez [19/01/2018]
 * @updated: Genaro Bermúdez [19/02/2018]
 * @description: Wrapping to many models or pojo's class. Its represents the TransferObject Design Pattern
 * @since-version: 1.0
 */
@Data
public class OrdersTo implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -2179154572620335233L;
	private OrderDi orderDi;
	private OrderDiOut orderDiOut;
	private Boolean successfully;
	private Order order;
	private Response response;
	private Request request;
	private OrdersTo2 ordersTo2;
	private IntcomexPlaceOrderOUT intcomexPlaceOrderOut;
	private IntcomexPurchaseesdProductsOut intcomexPurchaseesdProductsOut;

}
