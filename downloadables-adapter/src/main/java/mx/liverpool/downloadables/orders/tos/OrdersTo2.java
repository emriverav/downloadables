/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.tos;

import java.io.Serializable;

import lombok.Data;
import mx.liverpool.downloadables.orders.models.OrderDiOut;

/**
 * @author: Genaro Bermúdez [19/01/2018]
 * @updated: Genaro Bermúdez [19/02/2018]
 * @description: Wrapping to many models or pojo's class. Its represents the TransferObject Design Pattern
 * @since-version: 1.0
 */
@Data
public class OrdersTo2 implements Serializable {
	private static final long serialVersionUID = 3481401915546273982L;
	/** Class members */
	private OrderDiOut orderDi;

}
