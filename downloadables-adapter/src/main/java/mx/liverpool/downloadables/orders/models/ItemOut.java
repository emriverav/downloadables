package mx.liverpool.downloadables.orders.models;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [18/02/2018]
 * @updated: Mauricio Rivera [28/02/2018]
 * @description: model class to represent Item object
 * @since-version: 1.0
 */
@Data
public class ItemOut implements Serializable {
	
	private static final long serialVersionUID = 4374563823804563400L;
	/** Class members */
	private String status;
	private String sku;
	private List<FulfillData> fulfillData;
}
