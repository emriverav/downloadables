/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [18/02/2018]
 * @updated:
 * @description: model class to represent FulfillData object
 * @since-version: 1.0
 */
@Data
public class FulfillData implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 5618726903140520343L;
	private String instructions;
	private String productKey;
	private String url;

}
