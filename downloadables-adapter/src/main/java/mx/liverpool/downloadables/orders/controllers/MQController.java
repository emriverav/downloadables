/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.controllers;

import java.util.Enumeration;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.commons.models.Request;
import mx.liverpool.downloadables.orders.services.MQService;
import mx.liverpool.downloadables.orders.tos.OrdersTo;
import mx.liverpool.downloadables.orders.validators.OrdersValidator;
import mx.liverpool.downloadables.suppliers.services.SuppliersService;

/**
 * @author: Mauricio Rivera [01/02/2018]
 * @updated: Mauricio Rivera [08/02/2018]
 * @description: Class to manage the http(s) requets to the Downloadables Interface
 * @since-version: 1.0
 */
@Path("api/MQ")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@Slf4j
public class MQController {

	@Inject
	MQService mqService;
	@Inject
	SuppliersService providersService;
	@Inject
	OrdersValidator orderValidator;

	public static void Discover() {
		log.info("DownloadablesController discovered");
	}

	@GET
	@Path("conection/mq")
	public void conectionMq(OrdersTo ordersTo, @Context HttpServletRequest httpServletRequest) throws Exception {
		ordersTo = new OrdersTo();
		ordersTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		ordersTo.setRequest(new Request());
		String headers = "";
		Enumeration<?> names = httpServletRequest.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			Enumeration<?> values = httpServletRequest.getHeaders(name);
			if (values != null) {
				while (values.hasMoreElements()) {
					String value = (String) values.nextElement();
					headers = headers + name + "=" + value + " | ";
				}
			}
		}
		ordersTo.getRequest().setHeaders(headers);
		try {
		log.info("Beginning downloadablePurchaseOrder method");
		ordersTo=mqService.execute(ordersTo.getOrderDiOut());
		} catch (Exception e) {
			log.info("Ending sendResponseMQ with errors");
		}
	}
}