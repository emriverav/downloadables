/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.controllers;

import java.util.Enumeration;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.commons.models.Request;
import mx.liverpool.downloadables.orders.models.OrderDi;
import mx.liverpool.downloadables.orders.services.IOrdersService;
import mx.liverpool.downloadables.orders.tos.OrdersTo;
import mx.liverpool.downloadables.orders.validators.OrdersValidator;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Mauricio Rivera [12/03/2018]
 * @description: Class to manage the http(s) requets to Orders module
 * @since-version: 1.0
 */
@Path("api/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@Slf4j
public class OrdersController {

	@Inject
	IOrdersService ordersService;

	@Inject
	OrdersValidator orderValidator;

	public static void Discover() {
		log.info("OdersController discovered");
	}

	@POST
	@Path("/orders")
	public Response placeOrder(OrdersTo ordersTo, @Context HttpServletRequest httpServletRequest) {
		ordersTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		ordersTo.setRequest(new Request());
		ordersTo.getRequest().setEndPoint(httpServletRequest.getMethod() + ": " + httpServletRequest.getRequestURL() + ((httpServletRequest.getQueryString() == null || httpServletRequest.getQueryString().isEmpty()) ? "" : "?" + httpServletRequest.getQueryString()));
		String headers = "";
		Enumeration<?> names = httpServletRequest.getHeaderNames();
		while (names.hasMoreElements()) {
			String name = (String) names.nextElement();
			Enumeration<?> values = httpServletRequest.getHeaders(name);
			if (values != null) {
				while (values.hasMoreElements()) {
					String value = (String) values.nextElement();
					headers = headers + name + "=" + value + " | ";
				}
			}
		}
		ordersTo.getRequest().setHeaders(headers);
		try {
			ordersTo = orderValidator.execute(ordersTo);
			if (ordersTo.getResponse().getHttStatusCode().equals(Response.Status.OK.getStatusCode())) {
				if (ordersTo.getOrderDi().getIsMonitor() != null && ordersTo.getOrderDi().getIsMonitor() == true) {
					if(ordersTo.getOrderDi().getProductKey()!=null && !ordersTo.getOrderDi().getProductKey().isEmpty() && !ordersTo.getOrderDi().getProductKey().equals(""))
					ordersTo = ordersService.stautsProductKey(ordersTo);
					else {
						ordersTo = ordersService.executeMonitor(ordersTo);
					}
				} else {
					ordersTo = ordersService.executeAdapter(ordersTo);
				}
			}
			log.info("Ending placeOrder succesfully");
		} catch (Exception e) {
			log.info("Ending placeOrder with errors");
		}
		log.info("Respuesta del adpatador:: " + Response.status(ordersTo.getResponse().getHttStatusCode()).entity(ordersTo.getOrdersTo2()).build().toString());
		return Response.status(ordersTo.getResponse().getHttStatusCode()).entity(ordersTo.getOrdersTo2()).build();
	}

	@POST
	@Path("/productKey")
	public Response find(String productKey) {
		OrdersTo ordersTo = new OrdersTo();
		ordersTo.setOrderDi(new OrderDi());
		ordersTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		try {
			// TODO: se debe invocar la lógica de negocio
//			ordersTo = ordersService.stautsProductKey(ordersTo,productKey);
			ordersTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
			ordersTo.getResponse().setDescription("Has been completed received Order Number > " + productKey);
			// TODO: la respuesta se debe armar dentro de la lógica de negocio
			log.info("Ending find succesfully");
		} catch (Exception e) {
			log.info("Ending find with errors");
		}
		return Response.status(ordersTo.getResponse().getHttStatusCode()).entity(ordersTo.getResponse()).build();
	}

}
