/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.validators;

import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.orders.models.Item;
import mx.liverpool.downloadables.orders.tos.OrdersTo;

/**
 * @author: Genaro Bermúdez [19/01/2018]
 * @updated: Genaro Bermúdez [18/02/2018]
 * @description: Class to manage the http(s) valitations
 * @since-version: 1.0
 */
@Slf4j
public class OrdersValidator {

	public OrdersTo execute(OrdersTo ordersTo) {

		if(!ordersTo.getOrderDi().getIsMonitor() || ordersTo.getOrderDi().getIsMonitor()==null) {
		/** Validation case 1 */
		if (ordersTo.getOrderDi() == null || (ordersTo.getOrderDi().getShippingGroupId().trim().equals("") || ordersTo.getOrderDi().getRefId().trim().equals("") || ordersTo.getOrderDi().getCustomerFirstName().trim().equals("") || ordersTo.getOrderDi().getCustomerPaterno().trim().equals("") || ordersTo.getOrderDi().getCustomerEmail().trim().equals("") || ordersTo.getOrderDi().getChannel().trim().equalsIgnoreCase("")
				|| ordersTo.getOrderDi().getItems() == null)) {
			ordersTo.getResponse().setHttStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
			ordersTo.getResponse().setDescription("The OrderDi is not complete");
			log.error("The following error has been ocurred > " + ordersTo.getResponse().getDescription());
			return ordersTo;
		}

		/** Validation case 2 */
		for (Item item : ordersTo.getOrderDi().getItems()) {
			if (item.getExternalId() == null || item.getExternalId().equals("")) {
				ordersTo.getResponse().setHttStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
				ordersTo.getResponse().setDescription("The ExternalId is missing");
				log.error("The following error has been ocurred validating ExternalId > " + ordersTo.getResponse().getDescription());
				return ordersTo;
			}
			if (item.getQuantity() == null || item.getQuantity().equals("")) {
				ordersTo.getResponse().setHttStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
				ordersTo.getResponse().setDescription("The Quantity is missing");
				log.error("The following error has been ocurred validating Quantity > " + ordersTo.getResponse().getDescription());
				return ordersTo;
			}
		}
		}
		ordersTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		ordersTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
		ordersTo.getResponse().setDescription("All validations are ok");

		/** Return value */
		return ordersTo;

	}

}