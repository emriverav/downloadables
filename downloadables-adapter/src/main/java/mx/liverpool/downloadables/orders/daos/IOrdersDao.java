/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.daos;

import mx.liverpool.downloadables.commons.daos.IAbstract;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated:
 * @description: Orders interface to dao tier
 * @since-version: 1.0
 */
public interface IOrdersDao extends IAbstract {

}
