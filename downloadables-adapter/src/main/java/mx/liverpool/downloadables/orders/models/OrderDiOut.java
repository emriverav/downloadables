package mx.liverpool.downloadables.orders.models;

import java.io.Serializable;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

/**
 * @author:Mauricio Rivera [06/01/2018]
 * @updated: Mauricio Rivera [06/01/2018]
 * @description: Wrapping to many models or pojo's class. Its represents the output response only with attributes requireds
 * @since-version: 1.0
 */
@Data
public class OrderDiOut implements Serializable {
	private static final long serialVersionUID = -810640125825565115L;
	private String statusCode;
	private String shippingGroupId;
	private String fulfillSystemReference;
	private ItemOut[] items;
	
	public String toJson() {
		String json = "";
		try {
			json = new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
		}
		return json;
	}
}
