/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.constants;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: Genaro Bermúdez [18/02/2018]
 * @updated:
 * @description: calss-enum that contains all the constants values
 * @since-version: 1.0
 */
public class Enums {

	@Getter
	@AllArgsConstructor
	public enum CHANNELS {
		WEB(1, "WEB"), 
		IOS(0, "IOS"),
		ANDROID(0, "ANDROID");
		private Integer codigo;
		private String descripcion;
	}
	
}
