/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.daos;

import org.springframework.stereotype.Repository;

import mx.liverpool.downloadables.commons.daos.AbstracDao;
import mx.liverpool.downloadables.orders.tos.OrdersTo;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated:
 * @description: Dao implementation of Orders
 * @since-version: 1.0
 */
@Repository
public class OrdersDao extends AbstracDao implements IOrdersDao {

	/** Query constants for all operations in this implementation */
	protected final static String INSERT = "INSERT INTO ORDERS (CREATION_DATE, REF_TRANS_ID, SHIPPING_GROUP, CHANNEL, ERROR_CODE) VALUES (?,?,?,?,?)";
	protected final static String UPDATE_ERROR_CODE = "UPDATE ORDERS SET ERROR_CODE  = ? WHERE REF_TRANS_ID = ? AND SHIPPING_GROUP = ?";
	
	public Object insert(Object object) {
		OrdersTo ordersTo = (OrdersTo) object;
		try {
			setRowsAffected(jdbcTemplate.update(INSERT, new Object[] { ordersTo.getOrder().getCreationDate(), ordersTo.getOrder().getRefTransId(), ordersTo.getOrder().getShippingGroup(), ordersTo.getOrder().getChannel(), ordersTo.getOrder().getErrorCode() }));
		} catch (Exception e) {
			throw e;
		}
		return ordersTo;
	}

	public Object update(Object object) {
		OrdersTo ordersTo = (OrdersTo) object;
		try {
			setRowsAffected(jdbcTemplate.update(UPDATE_ERROR_CODE, new Object[] { ordersTo.getOrder().getErrorCode(), ordersTo.getOrder().getRefTransId(), ordersTo.getOrder().getShippingGroup()}));
		} catch (Exception e) {
			throw e;
		}
		return ordersTo;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

}
