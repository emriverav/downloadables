/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.services;

import javax.inject.Inject;
import javax.jms.Connection;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.ibm.msg.client.jms.JmsConnectionFactory;
import com.ibm.msg.client.jms.JmsFactoryFactory;
import com.ibm.msg.client.wmq.WMQConstants;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.commons.models.Config;
import mx.liverpool.downloadables.commons.tos.ApiTo;
import mx.liverpool.downloadables.orders.models.OrderDiOut;
import mx.liverpool.downloadables.orders.tos.OrdersTo;
import mx.liverpool.downloadables.properties.services.IPropertiesService;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: Mauricio Rivera [01/03/2018]
 * @description: bussiness logic MQ Service
 * @since-version: 1.0
 */
@Slf4j
public class MQService {

	@Inject
	IPropertiesService propertiesService;
	Session session;
	Connection connection;
	JmsFactoryFactory jmsFactoryFactory;
	JmsConnectionFactory connectionFactory;
	Queue cola;

	static final String CONF_ATG_MQ_NAME = "CONF_ATG_MQ_NAME";
	static final String CONF_ATG_MQ_HOST = "CONF_ATG_MQ_HOST";
	static final String CONF_ATG_MQ_CHANNEL = "CONF_ATG_MQ_CHANNEL";
	static final String CONF_ATG_MQ_PORT = "CONF_ATG_MQ_PORT";
	static final String CONF_ATG_MQ_QUEUE = "CONF_ATG_MQ_QUEUE";
	static final String CONF_ATG_MQ_DELAY = "CONF_ATG_MQ_DELAY";
	static final String CONF_ATG_MQ_RETRIES = "CONF_ATG_MQ_RETRIES";

	private void loadInfo() {
		Config.QueueManager queueManager = ApiTo.getInstance().getConfig().new QueueManager();
		queueManager.setName(propertiesService.getValue(CONF_ATG_MQ_NAME));
		queueManager.setHost(propertiesService.getValue(CONF_ATG_MQ_HOST));
		queueManager.setChannel(propertiesService.getValue(CONF_ATG_MQ_CHANNEL));
		queueManager.setPort(new Integer(propertiesService.getValue(CONF_ATG_MQ_PORT)));
		queueManager.setQueue(propertiesService.getValue(CONF_ATG_MQ_QUEUE));
		queueManager.setDelay(new Integer(propertiesService.getValue(CONF_ATG_MQ_DELAY)));
		queueManager.setRetries(new Integer(propertiesService.getValue(CONF_ATG_MQ_RETRIES)));
		ApiTo.getInstance().getConfig().setQueueManager(queueManager);
		log.info("Has been loaded database info");
	}

	public OrdersTo execute(OrderDiOut mensaje) throws Exception {
		loadInfo();
		jmsFactoryFactory = JmsFactoryFactory.getInstance(WMQConstants.WMQ_PROVIDER);
		connectionFactory = jmsFactoryFactory.createConnectionFactory();
		connectionFactory.setBooleanProperty(WMQConstants.USER_AUTHENTICATION_MQCSP, true);
		connectionFactory.setIntProperty(WMQConstants.WMQ_CONNECTION_MODE, WMQConstants.WMQ_CM_CLIENT);
		connectionFactory.setStringProperty(WMQConstants.WMQ_HOST_NAME, ApiTo.getInstance().getConfig().getQueueManager().getHost());
		connectionFactory.setIntProperty(WMQConstants.WMQ_PORT, ApiTo.getInstance().getConfig().getQueueManager().getPort());
		connectionFactory.setStringProperty(WMQConstants.WMQ_CHANNEL, ApiTo.getInstance().getConfig().getQueueManager().getChannel());
		connectionFactory.setStringProperty(WMQConstants.WMQ_QUEUE_MANAGER, ApiTo.getInstance().getConfig().getQueueManager().getName());
		connection = connectionFactory.createConnection();
		session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
		cola = session.createQueue(ApiTo.getInstance().getConfig().getQueueManager().getQueue());
		MessageProducer productorJms = session.createProducer(cola);
		TextMessage tmo = session.createTextMessage();
		connection.start();
		if (connection != null) {
			log.info("Se ha realizado la conexión al MQ de manera correcta!!");
		}
		log.info("Mensaje que se enviará al MQ:   " + mensaje.toJson());
		tmo.setText("{\"orderDi\": "+mensaje.toJson() + "}");
		log.info("Mensaje completo: " + "{\"orderDi\": "+mensaje.toJson() + "}");
		productorJms.send(tmo);
		productorJms.close();
		log.info("Has been sent the message to MQ");
		return null;
	}

}
