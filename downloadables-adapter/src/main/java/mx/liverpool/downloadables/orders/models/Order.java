/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [19/01/2018]
 * @updated:
 * @description: config-file to set app values. Its uses YAML mapping with jackson library
 * @since-version: 1.0
 */
@Data
public class Order implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 7233449605235940596L;
	private Integer idNumber;
	private Date creationDate;
	private Integer refTransId;
	private String shippingGroup;
	private String channel;
	private Integer errorCode;

}
