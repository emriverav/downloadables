/**
 * Copyright (c) 2018 by Banco FAMSA All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
@XmlRootElement
public class Message implements Serializable {

	/** Miembros de la clase */
	private static final long serialVersionUID = 5579799927938218525L;
	private String attribute1;
	private String attribute2;
	private OrderDi orderDi;

}
