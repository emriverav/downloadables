/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.models;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [18/02/2018]
 * @updated: Mauricio Rivera [28/02/2018]
 * @description: model class to represent Item object
 * @since-version: 1.0
 */
@Data
public class Item implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -7287983944360331786L;
	private String status;
	private String sku;
	private List<FulfillData> fulfillData;
	private String externalId;
	private String supplierId;
	private String quantity;
	private String description;

}
