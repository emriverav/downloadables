/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.services;

import mx.liverpool.downloadables.commons.daos.IAbstract;
import mx.liverpool.downloadables.orders.tos.OrdersTo;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated: Mauricio Rivera [27/02/2018]
 * @description: Orders interface to dao tier
 * @since-version: 1.0
 */
public interface IOrdersService extends IAbstract {

	/**
	 * @author: Genaro Bermúdez [11/02/2018]
	 * @updated:
	 * @description: method to execute the TX operation
	 * @since-version: 1.0
	 */
	public OrdersTo executeAdapter(OrdersTo ordersTo);

	public OrdersTo executeMonitor(OrdersTo ordersTo);

	public OrdersTo stautsProductKey(OrdersTo ordersTo);

}
