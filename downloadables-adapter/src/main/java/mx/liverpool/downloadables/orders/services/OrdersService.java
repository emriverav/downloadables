/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.services;

import java.security.MessageDigest;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.clients.daos.IClientsDao;
import mx.liverpool.downloadables.clients.models.Client;
import mx.liverpool.downloadables.clients.tos.ClientsTo;
import mx.liverpool.downloadables.commons.daos.IAuditDao;
import mx.liverpool.downloadables.commons.models.Audit;
import mx.liverpool.downloadables.commons.models.Config;
import mx.liverpool.downloadables.commons.models.Config.Signature;
import mx.liverpool.downloadables.commons.models.Service;
import mx.liverpool.downloadables.commons.tos.ApiTo;
import mx.liverpool.downloadables.commons.tos.AuditTo;
import mx.liverpool.downloadables.commons.utils.Encryptor;
import mx.liverpool.downloadables.intcomex.placeOrder.models.IncomexPlaceOrderIN;
import mx.liverpool.downloadables.intcomex.placeOrder.models.IntcomexPlaceOrderOUT;
import mx.liverpool.downloadables.intcomex.productKeyStatus.models.productKeyStatusOut;
import mx.liverpool.downloadables.intcomex.purchaseesdProducts.models.EsdFulfillmentError;
import mx.liverpool.downloadables.intcomex.purchaseesdProducts.models.EsdFulfillments;
import mx.liverpool.downloadables.intcomex.purchaseesdProducts.models.IntcomexPurchaseesdProductsIN;
import mx.liverpool.downloadables.intcomex.purchaseesdProducts.models.IntcomexPurchaseesdProductsOut;
import mx.liverpool.downloadables.intcomex.purchaseesdProducts.models.Tokens;
import mx.liverpool.downloadables.orders.daos.IOrdersDao;
import mx.liverpool.downloadables.orders.models.FulfillData;
import mx.liverpool.downloadables.orders.models.Item;
import mx.liverpool.downloadables.orders.models.ItemOut;
import mx.liverpool.downloadables.orders.models.Order;
import mx.liverpool.downloadables.orders.models.OrderDiOut;
import mx.liverpool.downloadables.orders.tos.OrdersTo;
import mx.liverpool.downloadables.orders.tos.OrdersTo2;
import mx.liverpool.downloadables.products.daos.IProductsDao;
import mx.liverpool.downloadables.products.daos.IProductsDetailDao;
import mx.liverpool.downloadables.products.models.Product;
import mx.liverpool.downloadables.products.models.ProductDetail;
import mx.liverpool.downloadables.products.tos.ProductsTo;
import mx.liverpool.downloadables.properties.services.IPropertiesService;
import mx.liverpool.downloadables.suppliers.models.Supplier;
import mx.liverpool.downloadables.suppliers.services.SuppliersService;
import mx.liverpool.downloadables.suppliers.tos.SuppliersTo;

/**
 * @author: Genaro Bermúdez [12/02/2018]
 * @updated: Mauricio Rivera [25/05/2018]
 * @description: bussiness logic Orders Service
 * @since-version: 1.0
 */
@Slf4j
public class OrdersService implements IOrdersService {

	@Inject
	IAuditDao auditDao;

	@Inject
	IOrdersDao ordersDao;

	@Inject
	IProductsDao productsDao;

	@Inject
	IProductsDetailDao productsDetailDao;

	@Inject
	IClientsDao clientsDao;

	@Inject
	IPropertiesService propertiesService;

	@Inject
	MQService mqService;

	@Inject
	SuppliersService suppliersService;

	/** Service > PlaceOrder */
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_NAME = "CONF_INCOMEX_SERVICE_PLACE_ORDER_NAME";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_PROTOCOL = "CONF_INCOMEX_SERVICE_PLACE_ORDER_PROTOCOL";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_HOST = "CONF_INCOMEX_SERVICE_PLACE_ORDER_HOST";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_CONTEXT = "CONF_INCOMEX_SERVICE_PLACE_ORDER_CONTEXT";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_RETRIES = "CONF_INCOMEX_SERVICE_PLACE_ORDER_RETRIES";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_DELAY = "CONF_INCOMEX_SERVICE_PLACE_ORDER_DELAY";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_ACCES_KEY = "CONF_INCOMEX_SERVICE_PLACE_ORDER_ACCES_KEY";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_API_KEY = "CONF_INCOMEX_SERVICE_PLACE_ORDER_API_KEY";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_PORT = "CONF_INCOMEX_SERVICE_PLACE_ORDER_PORT";
	static final String CONF_INCOMEX_SERVICE_PLACE_ORDER_RESOURCE = "CONF_INCOMEX_SERVICE_PLACE_ORDER_RESOURCE";

	/** Service > PurchaseOrder */
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_NAME = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_NAME";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_PROTOCOL = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_PROTOCOL";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_HOST = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_HOST";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_CONTEXT = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_CONTEXT";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_RETRIES = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_RETRIES";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_DELAY = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_DELAY";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_ACCES_KEY = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_ACCES_KEY";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_API_KEY = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_API_KEY";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_PORT = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_PORT";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_RESOURCE = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_RESOURCE";
	static final String CONF_INCOMEX_SERVICE_PURCHASE_ORDER_STATUS_OVERRIDE = "CONF_INCOMEX_SERVICE_PURCHASE_ORDER_STATUS_OVERRIDE";

	/** Service > getproductkeystatus */
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_NAME = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_NAME";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_PROTOCOL = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_PROTOCOL";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_HOST = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_HOST";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_CONTEXT = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_CONTEXT";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_RETRIES = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_RETRIES";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_DELAY = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_DELAY";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_ACCES_KEY = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_ACCES_KEY";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_API_KEY = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_API_KEY";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_PORT = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_PORT";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_RESOURCE = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_RESOURCE";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_OVERRIDE = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_OVERRIDE";
	static final String CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_CODE = "CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_CODE";

	/** Service > getproductkeystatus */
	static final String CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_NAME = "CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_NAME";
	static final String CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_PROTOCOL = "CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_PROTOCOL";
	static final String CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_HOST = "CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_HOST";
	static final String CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_CONTEXT = "CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_CONTEXT";
	static final String CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_RETRIES = "CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_RETRIES";
	static final String CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_DELAY = "CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_DELAY";
	static final String CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_PORT = "CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_PORT";
	static final String CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_RESOURCE = "CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_RESOURCE";

	static final String SUPPLIER_KEY = "SUPPLIER_KEY";
	static Boolean unsupportProvider = false;

	static String supplier = "";

	public OrdersTo executeAdapter(OrdersTo ordersTo) {
		try {

			log.info("WAR Downloadables-Adapter Version PRE-PRODUCTIVA 29/05/2018 ------");
			/** 1.- Load info from database */
			loadInfo();
			/** 2.- Save data into database */
			saveInfo(ordersTo);

			Supplier suppliers = new Supplier();
			suppliers.setSupplierKey(suppliersService.getValue(SUPPLIER_KEY));
			SuppliersTo suppliersTo = new SuppliersTo();
			suppliersTo.setSupplier(suppliers);
			/** 3.- Validate suplierId */
			/** Invoke PalceOrder Intcomex service --- */
			supplier = suppliers.getSupplierKey();
			log.info("Suppliers obtained: " + supplier);
			if (supplier.isEmpty() || supplier.equals("")) {
				do {
					supplier = suppliersService.getValue(SUPPLIER_KEY);
				} while (supplier.isEmpty() || supplier.equals(""));
			}
			placeOrder(ordersTo);
			/** Invoke Audit Method Response */
			if (!unsupportProvider) {
				saveAudit(ordersTo, 3);
				/** Invoke PurchaseOrder Intcomex service */
				purchaseOrder(ordersTo);
				saveAudit(ordersTo, 5);
			}
			/** 4.- Create Response of Adapter to Client */
			ordersTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
			ordersTo.getResponse().setDescription("Order succesfully");
			ordersTo.setSuccessfully(true);
			ordersTo.getOrderDi().setStatusCode("0");
			OrderDiOut ordersDiOut = new OrderDiOut();
			ordersDiOut.setFulfillSystemReference(ordersTo.getOrderDi().getFulfillSystemReference());
			ordersDiOut.setShippingGroupId(ordersTo.getOrderDi().getShippingGroupId());
			ordersDiOut.setStatusCode(ordersTo.getOrderDi().getStatusCode());
			ItemOut[] itemOut = new ItemOut[ordersTo.getOrderDi().getItems().length];
			for (int i = 0; i < itemOut.length; i++) {
				itemOut[i] = new ItemOut();
				itemOut[i].setSku(ordersTo.getOrderDi().getItems()[i].getSku());
				if (ordersTo.getOrderDi().getItems()[i].getFulfillData() != null) {
					itemOut[i].setFulfillData(ordersTo.getOrderDi().getItems()[i].getFulfillData());
				} else {
					itemOut[i].setFulfillData(new ArrayList<>());
				}
				itemOut[i].setStatus(ordersTo.getOrderDi().getItems()[i].getStatus());
			}
			ordersDiOut.setItems(itemOut);
			ordersTo.setOrderDiOut(ordersDiOut);
			OrdersTo2 ordersTo2 = new OrdersTo2();
			ordersTo2.setOrderDi(ordersTo.getOrderDiOut());
			ordersTo.setOrdersTo2(ordersTo2);

			/** 5.- Persistence of Data Base */
			log.info("Se procede a guardar en auditoria el response del Adaptador");
			AuditTo auditTo = new AuditTo();
			auditTo.setAudit(new Audit());
			auditTo.getAudit().setCreationDate(new Date());
			auditTo.getAudit().setEndPoint(ordersTo.getRequest().getEndPoint());
			auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
			auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
			auditTo.getAudit().setRequestBody(ordersTo.getOrderDi().toJson());
			auditTo.getAudit().setResponseBody(ordersTo.getOrderDiOut().toJson());
			auditTo.getAudit().setRequestType(6);
			auditDao.insert(auditTo);
			log.info("Se ha guardado correctamente la informacion del response del Adaptador en la tabla de auditoria");
		} catch (Exception e) {
			ordersTo.getResponse().setHttStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
			ordersTo.getResponse().setDescription("Can't complete de PlaceOrder for Adapter due this error > " + e.getMessage());
			ordersTo.setSuccessfully(false);
			ordersTo.getOrderDi().setStatusCode("2");
			log.info(ordersTo.getResponse().getDescription());
			log.info("Se procede a guardar en auditoria el response del Adaptador");
			AuditTo auditTo = new AuditTo();
			auditTo.setAudit(new Audit());
			auditTo.getAudit().setCreationDate(new Date());
			auditTo.getAudit().setEndPoint(ordersTo.getRequest().getEndPoint());
			auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
			auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
			auditTo.getAudit().setRequestBody(ordersTo.getOrderDi().toJson());
			auditTo.getAudit().setResponseBody("Can't complete de PlaceOrder for Monitor due this error >");
			auditTo.getAudit().setRequestType(6);
			auditDao.insert(auditTo);
			log.info("Se ha guardado correctamente la informacion del response del Adaptador en la tabla de auditoria");
		}
		return ordersTo;
	}

	public OrdersTo executeMonitor(OrdersTo ordersTo) {
		try {

			/** 1.- Load info from database */
			loadInfo();
			/** 2.- Save AUDIT_ADAPTER info */
			AuditTo auditTo = new AuditTo();
			auditTo.setAudit(new Audit());
			auditTo.getAudit().setCreationDate(new Date());
			auditTo.getAudit().setEndPoint(ordersTo.getRequest().getEndPoint());
			auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
			auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
			auditTo.getAudit().setRequestBody(ordersTo.getOrderDi().toJson());
			auditTo.getAudit().setRequestType(1);
			auditDao.insert(auditTo);
			/** 3.- Validate attribute orderNumber of request */
			Supplier suppliers = new Supplier();
			suppliers.setSupplierKey(suppliersService.getValue(SUPPLIER_KEY));
			SuppliersTo suppliersTo = new SuppliersTo();
			suppliersTo.setSupplier(suppliers);
			/** 4.- Validate suplierId */
			/** Invoke PalceOrder Intcomex service --- */
			supplier = suppliers.getSupplierKey();
			log.info("Suppliers obtained: " + supplier);
			if (supplier.isEmpty() || supplier.equals("")) {
				do {
					supplier = suppliersService.getValue(SUPPLIER_KEY);
				} while (supplier.isEmpty() || supplier.equals(""));
			}
			if (ordersTo.getOrderDi().getOrderNumber() != null && !ordersTo.getOrderDi().getOrderNumber().equals("")) {
				ordersTo.setIntcomexPlaceOrderOut(new IntcomexPlaceOrderOUT());
				ordersTo.getIntcomexPlaceOrderOut().setOrderNumber(ordersTo.getOrderDi().getOrderNumber());
				ordersTo.getOrderDi().setItemsIntcomex(ordersTo.getOrderDi().getItems());
				/** Invoke PurchaseOrder Intcomex service */
				purchaseOrder(ordersTo);
				/** Invoke Audit Method Response */
				saveAudit(ordersTo, 5);
			} else {
				/** 3.- Validate suplierId */
				/** Invoke PalceOrder Intcomex service --- */
				placeOrder(ordersTo);
				/** Invoke Audit Method Response */
				saveAudit(ordersTo, 3);
				if (!unsupportProvider) {
					/** Invoke PurchaseOrder Intcomex service */
					purchaseOrder(ordersTo);
					saveAudit(ordersTo, 5);
				}
			}
			/** 5.- Create Response of Adapter to Client */
			ordersTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
			ordersTo.getResponse().setDescription("Order succesfully");
			ordersTo.setSuccessfully(true);
			ordersTo.getOrderDi().setStatusCode("0");
			OrderDiOut ordersDiOut = new OrderDiOut();
			ordersDiOut.setFulfillSystemReference(ordersTo.getOrderDi().getFulfillSystemReference());
			ordersDiOut.setShippingGroupId(ordersTo.getOrderDi().getShippingGroupId());
			ordersDiOut.setStatusCode(ordersTo.getOrderDi().getStatusCode());
			ItemOut[] itemOut = new ItemOut[ordersTo.getOrderDi().getItems().length];
			for (int i = 0; i < ordersTo.getOrderDi().getItems().length; i++) {
				itemOut[i] = new ItemOut();
				itemOut[i].setSku(ordersTo.getOrderDi().getItems()[i].getSku());
				itemOut[i].setFulfillData(ordersTo.getOrderDi().getItems()[i].getFulfillData());
				itemOut[i].setStatus(ordersTo.getOrderDi().getItems()[i].getStatus());
			}
			ordersDiOut.setItems(itemOut);
			ordersTo.setOrderDiOut(ordersDiOut);
			OrdersTo2 ordersTo2 = new OrdersTo2();
			ordersTo2.setOrderDi(ordersTo.getOrderDiOut());
			ordersTo.setOrdersTo2(ordersTo2);

			/** 6.- Persistence of Data Base */
			log.info("Se procede a guardar en auditoria el response del Adaptador");
			auditTo = new AuditTo();
			auditTo.setAudit(new Audit());
			auditTo.getAudit().setCreationDate(new Date());
			auditTo.getAudit().setEndPoint(ordersTo.getRequest().getEndPoint());
			auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
			auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
			auditTo.getAudit().setRequestBody(ordersTo.getOrderDi().toJson());
			auditTo.getAudit().setResponseBody(ordersTo.getOrderDiOut().toJson());
			auditTo.getAudit().setRequestType(6);
			auditDao.insert(auditTo);
			log.info("Se ha guardado correctamente la informacion del response del Adaptador en la tabla de auditoria");
		} catch (Exception e) {
			ordersTo.getResponse().setHttStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
			ordersTo.getResponse().setDescription("Can't complete de PlaceOrder for Monitor due this error > " + e.getMessage());
			ordersTo.setSuccessfully(false);
			ordersTo.getOrderDi().setStatusCode("2");
			log.info(ordersTo.getResponse().getDescription());
			log.info("Se procede a guardar en auditoria el response del Adaptador");
			AuditTo auditTo = new AuditTo();
			auditTo.setAudit(new Audit());
			auditTo.getAudit().setCreationDate(new Date());
			auditTo.getAudit().setEndPoint(ordersTo.getRequest().getEndPoint());
			auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
			auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
			auditTo.getAudit().setRequestBody(ordersTo.getOrderDi().toJson());
			auditTo.getAudit().setResponseBody("Can't complete de PlaceOrder for Monitor due this error >");
			auditTo.getAudit().setRequestType(6);
			auditDao.insert(auditTo);
			log.info("Se ha guardado correctamente la informacion del response del Adaptador en la tabla de auditoria");
			log.info(ordersTo.getResponse().getDescription());
		}
		return ordersTo;
	}

	public OrdersTo stautsProductKey(OrdersTo ordersTo) {
		/** 1.- Load info from database */
		loadInfo();
		/** 2.- Invoke getProductStatusKey */
		getproductkeystatus(ordersTo);
		saveAudit(ordersTo, 8);
		return ordersTo;
	}

	private void loadInfo() {
		if (ApiTo.getInstance().getConfig().getServices() == null) {
			ApiTo.getInstance().getConfig().setServices(new ArrayList<Service>());
			Service service = new Service();
			service.setName(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_NAME));
			service.setProtocol(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_PROTOCOL));
			service.setHost(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_HOST));
			service.setContext(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_CONTEXT));
			service.setRetries(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_RETRIES));
			service.setDelay(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_DELAY));
			service.setResource(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_RESOURCE));
			service.setPort(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_PORT));
			ApiTo.getInstance().getConfig().getServices().add(service);
			Config.Signature signature = ApiTo.getInstance().getConfig().new Signature();
			signature.setAccessKey(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_ACCES_KEY));
			signature.setApiKey(propertiesService.getValue(CONF_INCOMEX_SERVICE_PLACE_ORDER_API_KEY));
			ApiTo.getInstance().getConfig().setSignature(signature);
			service = new Service();
			service.setName(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_NAME));
			service.setProtocol(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_PROTOCOL));
			service.setHost(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_HOST));
			service.setContext(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_CONTEXT));
			service.setRetries(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_RETRIES));
			service.setDelay(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_DELAY));
			service.setResource(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_RESOURCE));
			service.setPort(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_PORT));
			service.setOverride(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_STATUS_OVERRIDE));
			ApiTo.getInstance().getConfig().getServices().add(service);
			signature = ApiTo.getInstance().getConfig().new Signature();
			signature.setAccessKey(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_ACCES_KEY));
			signature.setApiKey(propertiesService.getValue(CONF_INCOMEX_SERVICE_PURCHASE_ORDER_API_KEY));
			ApiTo.getInstance().getConfig().setSignature(signature);
			service = new Service();
			service.setName(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_NAME));
			service.setProtocol(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_PROTOCOL));
			service.setHost(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_HOST));
			service.setContext(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_CONTEXT));
			service.setRetries(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_RETRIES));
			service.setDelay(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_DELAY));
			service.setResource(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_RESOURCE));
			service.setPort(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_PORT));
			service.setOverride(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_OVERRIDE));
			service.setCode(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_CODE));
			ApiTo.getInstance().getConfig().getServices().add(service);
			signature = ApiTo.getInstance().getConfig().new Signature();
			signature.setAccessKey(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_ACCES_KEY));
			signature.setApiKey(propertiesService.getValue(CONF_INCOMEX_SERVICE_GET_PRODUCT_KEY_STATUS_API_KEY));
			ApiTo.getInstance().getConfig().setSignature(signature);
			service = new Service();
			service.setName(propertiesService.getValue(CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_NAME));
			service.setProtocol(propertiesService.getValue(CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_PROTOCOL));
			service.setHost(propertiesService.getValue(CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_HOST));
			service.setContext(propertiesService.getValue(CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_CONTEXT));
			service.setRetries(propertiesService.getValue(CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_RETRIES));
			service.setDelay(propertiesService.getValue(CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_DELAY));
			service.setResource(propertiesService.getValue(CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_RESOURCE));
			service.setPort(propertiesService.getValue(CONF_ADAPTER_SERVICE_SEND_MESSAGE_MQ_PORT));
			ApiTo.getInstance().getConfig().getServices().add(service);
		}
	}

	private void saveInfo(OrdersTo ordersTo) {
		Executors.newSingleThreadExecutor().submit(new Runnable() {
			public void run() {

				/** 1.- Save AUDIT_ADAPTER info */
				AuditTo auditTo = new AuditTo();
				auditTo.setAudit(new Audit());
				auditTo.getAudit().setCreationDate(new Date());
				auditTo.getAudit().setEndPoint(ordersTo.getRequest().getEndPoint());
				auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
				auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
				auditTo.getAudit().setRequestBody(ordersTo.getOrderDi().toJson());
				auditTo.getAudit().setRequestType(1);
				auditDao.insert(auditTo);

				/** 2.- Save ORDERS info */
				ordersTo.setOrder(new Order());
				ordersTo.getOrder().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
				ordersTo.getOrder().setShippingGroup(ordersTo.getOrderDi().getShippingGroupId());
				ordersTo.getOrder().setChannel(ordersTo.getOrderDi().getChannel());
				ordersTo.getOrder().setCreationDate(new Date());
				ordersTo.getOrder().setErrorCode(null);
				ordersDao.insert(ordersTo);

				/** 3.- Save PRODUCTS info */
				ProductsTo productsTo = new ProductsTo();
				for (Item item : ordersTo.getOrderDi().getItems()) {
					productsTo.setProduct(new Product());
					productsTo.getProduct().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
					productsTo.getProduct().setSupplierId(new Integer(item.getSupplierId()));
					productsTo.getProduct().setSku(item.getSku());
					productsTo.getProduct().setQuantity(new Integer(item.getQuantity()));
					productsTo.getProduct().setOrderId(null); // Consultar con Perla
					productsTo.getProduct().setExternalId(item.getExternalId());
					productsTo.getProduct().setDescription(item.getDescription());
					productsTo.getProduct().setErrorCode(null);
					productsDao.insert(productsTo);
				}

				/** 4.- Save CLIENTS info */
				ClientsTo clientsTo = new ClientsTo();
				clientsTo.setClient(new Client());
				clientsTo.getClient().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
				clientsTo.getClient().setProfileId(ordersTo.getOrderDi().getProfileId());
				clientsTo.getClient().setLoginId(ordersTo.getOrderDi().getLoginId());
				clientsTo.getClient().setCustomerFirstName(ordersTo.getOrderDi().getCustomerFirstName());
				clientsTo.getClient().setCustomerApaterno(ordersTo.getOrderDi().getCustomerPaterno());
				clientsTo.getClient().setCustomerAMaterno(ordersTo.getOrderDi().getCustomerMaterno());
				clientsTo.getClient().setCustomerEmail(ordersTo.getOrderDi().getCustomerEmail());
				clientsTo.getClient().setCustomerCelphone(ordersTo.getOrderDi().getCustomerCellPhone());
				clientsDao.insert(clientsTo);

			}
		});
	}

	/**
	 * @param ordersTo
	 * @param requestType
	 */
	private void saveAudit(OrdersTo ordersTo, int requestType) {
		/** 1.- Save AUDIT_ADAPTER info */
		AuditTo auditTo = new AuditTo();
		auditTo.setAudit(new Audit());
		auditTo.getAudit().setCreationDate(new Date());
		auditTo.getAudit().setEndPoint("NA");
		auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
		auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
		auditTo.getAudit().setRequestBody("Request of typeRequest: " + String.valueOf(requestType));
		auditTo.getAudit().setRequestType(requestType);
		auditTo.getAudit().setResponseBody(ordersTo.getResponse().getDescription());
		auditDao.insert(auditTo);
		log.info("Se ha insertado correctamente la auditoria para el typeRequest: " + requestType);
	}

	private Object placeOrder(OrdersTo ordersTo) throws InterruptedException {
		IncomexPlaceOrderIN intcomexPlaceOrderIN = new IncomexPlaceOrderIN();
		Item[] arrayItems = ordersTo.getOrderDi().getItems();
		int sizeIntcomex = 0;
		int sizeUnsupport = 0;
		int indexArrayIntcomex = 0;
		int IntdexArrayUnsupport = 0;
		for (int i = 0; i < arrayItems.length; i++) {
			if (arrayItems[i].getSupplierId().equals(supplier)) {
				sizeIntcomex += 1;
			} else {
				sizeUnsupport += 1;
			}
		}
		Item[] arrayItemsIntcomex = new Item[sizeIntcomex];
		Item[] arrayItemsIntcomexValidator = new Item[sizeIntcomex];
		Item[] arrayItemsUnsupport = new Item[sizeUnsupport];
		Item[] arrayItemsIntcomex2 = new Item[sizeIntcomex];
		int quantity = 0;
		for (int i = 0; i < arrayItems.length; i++) {
			if (arrayItems[i].getSupplierId().equals(supplier)) {
				arrayItemsIntcomex[indexArrayIntcomex] = (arrayItems[i]);
				indexArrayIntcomex++;
			} else {
				arrayItemsUnsupport[IntdexArrayUnsupport] = (arrayItems[i]);
				IntdexArrayUnsupport++;
			}
		}
		arrayItemsIntcomexValidator = arrayItemsIntcomex;
		for (int i = 0; i < arrayItemsIntcomexValidator.length; i++) {
			quantity = 0;
			for (int j = 0; j < arrayItemsIntcomex.length; j++) {
				if (i - 1 >= 0) {
					if (arrayItemsIntcomexValidator[i].getExternalId().equals(arrayItemsIntcomex[j].getExternalId()) && !arrayItemsIntcomexValidator[i].getExternalId().equals(arrayItemsIntcomexValidator[i - 1].getExternalId())) {
						quantity = quantity + 1;
					}
				} else {
					if (arrayItemsIntcomexValidator[i].getExternalId().equals(arrayItemsIntcomex[j].getExternalId())) {
						quantity = quantity + 1;
					}
				}
				if (quantity > 0) {
					arrayItemsIntcomex2[i] = new Item();
					arrayItemsIntcomex2[i].setExternalId(arrayItemsIntcomexValidator[i].getExternalId());
					arrayItemsIntcomex2[i].setSku(arrayItemsIntcomexValidator[i].getSku());
					arrayItemsIntcomex2[i].setSupplierId(arrayItemsIntcomexValidator[i].getSupplierId());
					arrayItemsIntcomex2[i].setDescription(arrayItemsIntcomexValidator[i].getDescription());
					arrayItemsIntcomex2[i].setQuantity(String.valueOf(quantity));
				}
			}
		}
		if (arrayItemsIntcomex2.length <= 0) {
			arrayItemsIntcomex2 = arrayItemsIntcomex;
		} else {
			ordersTo.getOrderDi().setItemsIntcomex(arrayItemsIntcomex2);
			ordersTo.getOrderDi().setItemsUnsupport(arrayItemsUnsupport);
		}
		if (arrayItemsIntcomex.length > 0) {
			unsupportProvider = false;
			String request = "[";

			int j = 0;
			for (int i = 0; i < arrayItemsIntcomex.length; i++) {
				if (arrayItemsIntcomex[i] != null) {
					intcomexPlaceOrderIN = new IncomexPlaceOrderIN();
					intcomexPlaceOrderIN.setSku(arrayItemsIntcomex[i].getExternalId());
					intcomexPlaceOrderIN.setQuantity(arrayItemsIntcomex[i].getQuantity());
					if (arrayItemsIntcomex.length <= j) {
						request += intcomexPlaceOrderIN.toJson(new ObjectMapper());
					} else {
						request += intcomexPlaceOrderIN.toJson(new ObjectMapper()).concat(",");
					}
				}
				j++;
			}
			request = request.substring(0, request.length() - 1);
			request = request.concat("]");
			log.info("Request Place Order:: " + request);
			Service restService = ApiTo.getInstance().getConfig().getServices().get(0);
			Signature signatureService = ApiTo.getInstance().getConfig().getSignature();
			Instant timeStamp = Instant.now();
			String signature = signatureService.getApiKey() + "," + signatureService.getAccessKey() + "," + timeStamp;
			String signatureP = signature;
			StringBuffer signatureF = getGenerateCodeSHA(signatureP);
			String target = restService.getProtocol() + "://" + restService.getHost() + "/" + restService.getContext() + "/" + restService.getResource() + "?apiKey=" + signatureService.getApiKey() + "&utcTimeStamp=" + timeStamp + "&signature=" + signatureF + "&CustomerOrderNumber=" + ordersTo.getOrderDi().getRefId();
			log.info("EndPoint Place Order: " + target);
			for (int i = 0; i < new Integer(restService.getRetries()).intValue(); i++) {
				try {
					Response response = ClientBuilder.newBuilder().build().target(target).request().post(Entity.entity(request, MediaType.APPLICATION_JSON), Response.class);
					if (response.getStatus() == Response.Status.OK.getStatusCode()) {
						ordersTo.setIntcomexPlaceOrderOut(response.readEntity(IntcomexPlaceOrderOUT.class));
						log.info("Response: " + ordersTo.getIntcomexPlaceOrderOut());
						log.info("Order Number obtained by Intcomex: " + ordersTo.getIntcomexPlaceOrderOut().getOrderNumber());
						/** Validate fiel Order Number */
						if (ordersTo.getIntcomexPlaceOrderOut().getOrderNumber() != null && !ordersTo.getIntcomexPlaceOrderOut().getOrderNumber().equals("")) {
							ordersTo.getOrderDi().setFulfillSystemReference(ordersTo.getIntcomexPlaceOrderOut().getOrderNumber());
							for (int index = 0; index < arrayItems.length; index++) {
								if (arrayItems[index].getSupplierId().equals(supplier)) {
									/** Update Error Code of products of Orders */
									ProductsTo productsTo = new ProductsTo();
									productsTo.setProduct(new Product());
									productsTo.getProduct().setSku(arrayItems[index].getSku());
									productsTo.getProduct().setSupplierId(new Integer(arrayItems[index].getSupplierId()));
									productsTo.getProduct().setOrderId(ordersTo.getIntcomexPlaceOrderOut().getOrderNumber());
									productsTo.getProduct().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
									productsTo.getProduct().setExternalId(arrayItems[index].getExternalId());
									productsDao.update(productsTo);
								} else {
									/** Update Error Code of products */
									log.info("Unsupport provider");
									ProductsTo productsTo = new ProductsTo();
									productsTo.setProduct(new Product());
									productsTo.getProduct().setSku(arrayItems[index].getSku());
									productsTo.getProduct().setSupplierId(new Integer(arrayItems[index].getSupplierId()));
									productsTo.getProduct().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
									productsTo.getProduct().setExternalId(arrayItems[index].getExternalId());
									productsTo.getProduct().setErrorCode(1004);// Error
									productsDao.update(productsTo);

									ordersTo.getOrderDi().getItems()[index].setStatus("2");
									ordersTo.getOrderDi().getItems()[index].setFulfillData(new ArrayList<>());
								}
							}
						} else {
							log.info("El StatusCode es 200, pero no se devolvió un orderNumber");
						}
						ordersTo.getResponse().setHttStatusCode(response.getStatus());
						ordersTo.getResponse().setDescription(response.toString());
					} else {
						ordersTo.setIntcomexPlaceOrderOut(response.readEntity(IntcomexPlaceOrderOUT.class));
						ordersTo.getResponse().setHttStatusCode(response.getStatus());
						ordersTo.getResponse().setDescription("An error occurred, whose cause is:::" + response.getStatus());
						log.info("An error occurred, whose cause is:::" + response.getStatus());
						/** If Error Code of Intcomex is 21, put this Error code only on SKU conflict --- Read fiel "Message" */
						if (ordersTo.getIntcomexPlaceOrderOut().getErrorCode() == 21) {
							String SKU = "";
							int contador = 0;
							String[] busquedaSku = ordersTo.getIntcomexPlaceOrderOut().getMessage().split(" ");
							for (String iter : busquedaSku) {
								if (iter.contains("SE")) {
									SKU = iter;
								}
							}
							for (int index = 0; index < arrayItemsIntcomex.length; index++) {
								/** Update Error Code 21 on SKU conflict */
								log.info("SKU Invalid: " + SKU);
								ProductsTo productsTo = new ProductsTo();
								productsTo.setProduct(new Product());
								productsTo.getProduct().setExternalId(SKU);
								productsTo.getProduct().setSupplierId(new Integer(arrayItemsIntcomex[index].getSupplierId()));
								productsTo.getProduct().setErrorCode(ordersTo.getIntcomexPlaceOrderOut().getErrorCode());
								productsDao.update(productsTo);
								ordersTo.getOrderDi().getItems()[index].setStatus("2");
								ordersTo.getOrderDi().getItems()[index].setFulfillData(new ArrayList<>());
								contador += 1;
							}
							if (arrayItemsUnsupport.length > 0) {
								for (int index = 0; index < arrayItemsUnsupport.length; index++) {
									log.info("Update unsupprot provider");
									ProductsTo productsTo = new ProductsTo();
									productsTo.setProduct(new Product());
									productsTo.getProduct().setExternalId(arrayItemsUnsupport[index].getExternalId());
									productsTo.getProduct().setSku(arrayItemsUnsupport[index].getSku());
									productsTo.getProduct().setSupplierId(new Integer(arrayItemsUnsupport[index].getSupplierId()));
									productsTo.getProduct().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
									productsTo.getProduct().setErrorCode(1004);
									productsDao.update(productsTo);
									ordersTo.getOrderDi().getItems()[contador].setStatus("2");
									ordersTo.getOrderDi().getItems()[contador].setFulfillData(new ArrayList<>());
									contador += 1;
								}
							}
						} else {
							for (int index = 0; index < arrayItemsIntcomex.length; index++) {
								/** Update Error Code of Intcomex for all products */
								log.info("Unsupport provider");
								ProductsTo productsTo = new ProductsTo();
								productsTo.setProduct(new Product());
								productsTo.getProduct().setSku(arrayItemsIntcomex[index].getSku());
								productsTo.getProduct().setSupplierId(new Integer(arrayItemsIntcomex[index].getSupplierId()));
								productsTo.getProduct().setErrorCode(ordersTo.getIntcomexPlaceOrderOut().getErrorCode());
								productsDao.update(productsTo);
								ordersTo.getOrderDi().getItems()[index].setStatus("2");
								ordersTo.getOrderDi().getItems()[index].setFulfillData(new ArrayList<>());
							}
						}
						/** Update Error Code of products of Orders */
						ordersTo.setOrder(new Order());
						ordersTo.getOrder().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
						ordersTo.getOrder().setShippingGroup(ordersTo.getOrderDi().getShippingGroupId());
						ordersTo.getOrder().setErrorCode(1001);
						ordersDao.update(ordersTo);
					}
					log.info("Se intentara guardar en auditoria con el request de place order Intcomex, el codigo devuelto por el servicio fue: " + response.getStatus());
					response.close();
					AuditTo auditTo = new AuditTo();
					auditTo.setAudit(new Audit());
					auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
					auditTo.getAudit().setCreationDate(new Date());
					auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
					auditTo.getAudit().setRequestType(2);
					auditTo.getAudit().setRequestBody(request);
					auditTo.getAudit().setEndPoint(target);
					auditDao.insert(auditTo);
					log.info("Se guardo correctamente la informacion de auditoria para la respuesta del ws placeOrder de Intcomex :) ");
					break;
				} catch (Exception e) {
					unsupportProvider = true;
					log.warn("Error invoking RestService > placeOrder of Intcomex | Retry > " + i + " " + e.getMessage());
					for (int index = 0; index < arrayItemsIntcomex.length; index++) {
						/** Update Error Code of Intcomex for all products */
						ProductsTo productsTo = new ProductsTo();
						productsTo.setProduct(new Product());
						productsTo.getProduct().setSku(arrayItemsIntcomex[index].getSku());
						productsTo.getProduct().setSupplierId(new Integer(arrayItemsIntcomex[index].getSupplierId()));
						productsTo.getProduct().setErrorCode(503);
						productsDao.update(productsTo);
						ordersTo.getOrderDi().getItems()[index].setStatus("2");
						ordersTo.getOrderDi().getItems()[index].setFulfillData(new ArrayList<>());
						AuditTo auditTo = new AuditTo();
						auditTo.setAudit(new Audit());
						auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
						auditTo.getAudit().setCreationDate(new Date());
						auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
						auditTo.getAudit().setRequestType(2);
						auditTo.getAudit().setRequestBody(request);
						auditTo.getAudit().setEndPoint(target);
						auditDao.insert(auditTo);
					}
				}

			}
		} else {
			unsupportProvider = true;
			Thread.sleep(2000);
			for (int i = 0; i < arrayItemsUnsupport.length; i++) {
				/** Update Error Code of products */
				log.info("Unsupport provider");
				ProductsTo productsTo = new ProductsTo();
				productsTo.setProduct(new Product());
				productsTo.getProduct().setSku(arrayItemsUnsupport[i].getSku());
				productsTo.getProduct().setSupplierId(new Integer(arrayItemsUnsupport[i].getSupplierId()));
				productsTo.getProduct().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
				productsTo.getProduct().setExternalId(arrayItemsUnsupport[i].getExternalId());
				productsTo.getProduct().setErrorCode(1004);// Error
				productsDao.update(productsTo);

				ordersTo.getOrderDi().getItems()[i].setStatus("2");
				ordersTo.getOrderDi().getItems()[i].setFulfillData(new ArrayList<>());
			}
			/** Update Error Code of products of Orders */
			ordersTo.setOrder(new Order());
			ordersTo.getOrder().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
			ordersTo.getOrder().setShippingGroup(ordersTo.getOrderDi().getShippingGroupId());
			ordersTo.getOrder().setErrorCode(1001);
			ordersDao.update(ordersTo);
		}

		return ordersTo;
	}

	private Object purchaseOrder(OrdersTo ordersTo) {
		IntcomexPurchaseesdProductsIN purchaseIn = new IntcomexPurchaseesdProductsIN();
		purchaseIn.setOrderNumber(ordersTo.getIntcomexPlaceOrderOut().getOrderNumber());
		Service restService = ApiTo.getInstance().getConfig().getServices().get(1);
		Signature signatureService = ApiTo.getInstance().getConfig().getSignature();
		Instant timeStamp = Instant.now();
		String signature = signatureService.getApiKey() + "," + signatureService.getAccessKey() + "," + timeStamp;
		String signatureP = signature;
		StringBuffer signatureF = getGenerateCodeSHA(signatureP);
		String target = "";
		if (restService.getOverride() != null) {
			target = restService.getProtocol() + "://" + restService.getHost() + "/" + restService.getContext() + "/" + restService.getResource() + "?apiKey=" + signatureService.getApiKey() + "&utcTimeStamp=" + timeStamp + "&signature=" + signatureF + restService.getOverride();
		} else {
			target = restService.getProtocol() + "://" + restService.getHost() + "/" + restService.getContext() + "/" + restService.getResource() + "?apiKey=" + signatureService.getApiKey() + "&utcTimeStamp=" + timeStamp + "&signature=" + signatureF;
		}
		log.info("Endpoint: " + target);
		Boolean controlStatus = false;
		String control = "";
		for (int i = 0; i < new Integer(restService.getRetries()).intValue(); i++) {
			try {
				Response response = ClientBuilder.newBuilder().build().target(target).request().post(Entity.entity(purchaseIn, MediaType.APPLICATION_JSON), Response.class);
				if (response.getStatus() == Response.Status.OK.getStatusCode()) {
					IntcomexPurchaseesdProductsOut[] resultMock = response.readEntity(IntcomexPurchaseesdProductsOut[].class);
					Item mainItem[] = new Item[resultMock.length];
					for (int i2 = 0; i2 < resultMock.length; i2++) {
						List<FulfillData> listFulfillData = new ArrayList<FulfillData>();
						EsdFulfillments[] esdFulfillments = resultMock[i2].getEsdFulfillments();
						Tokens[] tokens = resultMock[i2].getTokens();
						if (tokens.length > 0) {
							for (int p = 0; p < tokens.length; p++) {
								FulfillData fulfillData = new FulfillData();
								fulfillData.setProductKey(tokens[p].getTokenCode());
								if (tokens.length > 0) {
									fulfillData.setUrl(tokens[p].getLinkUri());
								} else {
									fulfillData.setUrl("");
								}
								fulfillData.setInstructions(esdFulfillments[0].getInstructions());
								if (fulfillData.getProductKey() != null && !fulfillData.getProductKey().equals("")) {
									ProductsTo productsTo = new ProductsTo();
									productsTo.setProductDetail(new ProductDetail());
									productsTo.getProductDetail().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
									productsTo.getProductDetail().setSku(resultMock[i2].getSku());
									productsTo.getProductDetail().setIndexId(p + 1);
									productsTo.getProductDetail().setProductKey(Encryptor.encriptarAES(fulfillData.getProductKey()));
									productsTo.getProductDetail().setUrl(fulfillData.getUrl());
									productsTo.getProductDetail().setInstructions(fulfillData.getInstructions());
									productsDetailDao.insert(productsTo);
									controlStatus = true;
								} else {
									EsdFulfillmentError esdFulfillmentError = resultMock[i2].getEsdFulfillmentError();
									ProductsTo productsTo = new ProductsTo();
									productsTo.setProduct(new Product());
									productsTo.getProduct().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
									productsTo.getProduct().setSku(resultMock[i2].getSku());
									productsTo.getProduct().setSupplierId(new Integer(ordersTo.getOrderDi().getItems()[i2].getSupplierId()));
									productsTo.getProduct().setErrorCode(esdFulfillmentError.getErrorCode());
									productsDao.update(productsTo);
									controlStatus = false;
									control = "1002";

								}
								listFulfillData.add(fulfillData);
							}
						} else {
							EsdFulfillmentError esdFulfillmentError = resultMock[i2].getEsdFulfillmentError();
							ProductsTo productsTo = new ProductsTo();
							productsTo.setProduct(new Product());
							productsTo.getProduct().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
							productsTo.getProduct().setSku(resultMock[i2].getSku());
							productsTo.getProduct().setSupplierId(new Integer(ordersTo.getOrderDi().getItems()[i2].getSupplierId()));
							productsTo.getProduct().setErrorCode(esdFulfillmentError.getErrorCode());
							productsDao.update(productsTo);
							controlStatus = false;
							control = "1002";
						}

						for (int it = 0; it < ordersTo.getOrderDi().getItems().length; it++) {
							if (ordersTo.getOrderDi().getItems()[it].getExternalId().equals(resultMock[i2].getSku())) {
								ordersTo.getOrderDi().getItems()[it].setFulfillData(listFulfillData);
								if (controlStatus) {
									ordersTo.getOrderDi().getItems()[it].setStatus("0");
								} else {
									ordersTo.getOrderDi().getItems()[it].setStatus("2");
									ordersTo.getOrderDi().getItems()[it].setFulfillData(listFulfillData);
								}
								mainItem[i2] = ordersTo.getOrderDi().getItems()[it];
							}
						}
					} // Aqui termina el for de resultmock
					for (int x = 0; x < mainItem.length; x++) {
						if (mainItem[x] == null) {
							mainItem[x] = new Item();
							if (ordersTo.getOrderDi().getItems()[x].getSupplierId() != supplier) {
								mainItem[x].setSku(ordersTo.getOrderDi().getItems()[x].getSku());
								mainItem[x].setStatus("2");
								mainItem[x].setFulfillData(new ArrayList<>());
							}
						}
					}
					ordersTo.getOrderDi().setItems(mainItem);
					/** Update Error Code of products of Orders */
					ordersTo.setOrder(new Order());
					ordersTo.getOrder().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
					ordersTo.getOrder().setShippingGroup(ordersTo.getOrderDi().getShippingGroupId());
					/** Update ErroCode for Order if all de product Key were generated */
					if (ordersTo.getOrderDi().getIsMonitor() == null || !ordersTo.getOrderDi().getIsMonitor()) {
						if (controlStatus && !control.equals("1002")) {
							ordersTo.getOrder().setErrorCode(1000);
						} else {
							ordersTo.getOrder().setErrorCode(1002);
						}
						ordersDao.update(ordersTo);
					} else {
						if (controlStatus && !control.equals("1002")) {
							ordersTo.getOrder().setErrorCode(1000);
							ordersDao.update(ordersTo);
							OrderDiOut ordersDiOut = new OrderDiOut();
							ordersDiOut.setFulfillSystemReference(ordersTo.getOrderDi().getFulfillSystemReference());
							ordersDiOut.setShippingGroupId(ordersTo.getOrderDi().getShippingGroupId());
							ordersDiOut.setStatusCode(ordersTo.getOrderDi().getStatusCode());
							ItemOut[] itemOut = new ItemOut[ordersTo.getOrderDi().getItems().length];
							for (int i2 = 0; i2 < itemOut.length; i2++) {
								itemOut[i2] = new ItemOut();
								itemOut[i2].setSku(ordersTo.getOrderDi().getItems()[i2].getSku());
								if (ordersTo.getOrderDi().getItems()[i2].getFulfillData() != null) {
									itemOut[i2].setFulfillData(ordersTo.getOrderDi().getItems()[i2].getFulfillData());
								} else {
									itemOut[i2].setFulfillData(new ArrayList<>());
								}
								itemOut[i2].setStatus(ordersTo.getOrderDi().getItems()[i2].getStatus());
							}
							ordersDiOut.setItems(itemOut);
							ordersTo.setOrderDiOut(ordersDiOut);
							ordersTo.getOrderDiOut().setStatusCode("0");
							mqService.execute(ordersTo.getOrderDiOut());
						} else {
							log.info("The Field ErrorCode of Orders not update because one or more product key failed");
						}
					}
					ordersTo.getResponse().setHttStatusCode(response.getStatus());
					IntcomexPurchaseesdProductsOut[] responseAudit = resultMock;
					String respuesta = "";
					for (int audit = 0; audit < responseAudit.length; audit++) {
						Tokens[] tokens = resultMock[audit].getTokens();
						for (int audit2 = 0; audit2 < tokens.length; audit2++) {
							responseAudit[audit].getTokens()[audit2].setTokenCode(Encryptor.encriptarAES(tokens[audit2].getTokenCode()));
							responseAudit[audit].setEsdFulfillments(null);
						}
						respuesta += responseAudit[audit];
					}
					ordersTo.getResponse().setDescription(respuesta);
				} else {
					EsdFulfillmentError resultMock = response.readEntity(EsdFulfillmentError.class);
					ordersTo.getResponse().setHttStatusCode(response.getStatus());
					ordersTo.getResponse().setDescription("An error occurred, whose cause is::: StatusCode" + response.getStatus() + " {\"Message\": \"" + resultMock.getMessage() + "\",\"ErrorCode\": " + resultMock.getErrorCode() + ",\"Reference\": \"" + resultMock.getReference() + "\"}");
				}
				log.info("Se intentara guardar en auditoria con el request de purchaseEsdProducts order Intcomex, el codigo devuelto por el servicio fue: " + response.getStatus());
				response.close();
				break;
			} catch (Exception e) {
				log.warn("Error invoking RestService >  PurchaseEsdProducts of Intcomex | Retry > " + i + " " + e.getMessage());
				ordersTo.getOrder().setErrorCode(1002);
				ordersDao.update(ordersTo);
				ProductsTo productsTo = new ProductsTo();
				productsTo.setProduct(new Product());
				productsTo.getProduct().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
				productsTo.getProduct().setErrorCode(500);
				productsDao.update(productsTo);
			}
		}
		AuditTo auditTo = new AuditTo();
		auditTo.setAudit(new Audit());
		auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
		auditTo.getAudit().setCreationDate(new Date());
		auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
		auditTo.getAudit().setRequestType(4);
		auditTo.getAudit().setRequestBody("{ 'OrderNumber':" + purchaseIn.getOrderNumber() + "}");
		auditTo.getAudit().setEndPoint(target);
		auditDao.insert(auditTo);
		log.info("Se guardo correctamente la informacion de auditoria para la respuesta del ws purchaseEsdProducts de Intcomex :) ");
		return ordersTo;
	}

	private Object getproductkeystatus(OrdersTo ordersTo) {
		productKeyStatusOut productKeyStatusOut = new productKeyStatusOut();
		Service restService = ApiTo.getInstance().getConfig().getServices().get(2);
		Signature signatureService = ApiTo.getInstance().getConfig().getSignature();
		Instant timeStamp = Instant.now();
		String signature = signatureService.getApiKey() + "," + signatureService.getAccessKey() + "," + timeStamp;
		String signatureP = signature;
		StringBuffer signatureF = getGenerateCodeSHA(signatureP);
		String target = "";
		ProductsTo productsTo = new ProductsTo();
		productsTo.setProductDetail(new ProductDetail());
		productsTo.getProductDetail().setProductKey(ordersTo.getOrderDi().getProductKey());
		productsDetailDao.find(productsTo);
		String status = productsTo.getProductsDetail().get(0).toString();
		if (!status.contains("Redeemed")) {
			String productKey = Encryptor.desencriptarAES(ordersTo.getOrderDi().getProductKey());
			if (restService.getOverride() != null && restService.getCode() != null) {
				target = restService.getProtocol() + "://" + restService.getHost() + "/" + restService.getContext() + "/" + restService.getResource() + "?apiKey=" + signatureService.getApiKey() + "&utcTimeStamp=" + timeStamp + "&signature=" + signatureF + "&productkey=" + productKey + restService.getOverride() + restService.getCode();
			} else {
				target = restService.getProtocol() + "://" + restService.getHost() + "/" + restService.getContext() + "/" + restService.getResource() + "?apiKey=" + signatureService.getApiKey() + "&utcTimeStamp=" + timeStamp + "&signature=" + signatureF + "&productkey=" + productKey;
			}
			log.info("Llamado a EndPoint: " + target);
			for (int j = 0; j < new Integer(restService.getRetries()).intValue();) {
				try {
					Response response = ClientBuilder.newBuilder().build().target(target).request().get();
					if (response.getStatus() == Response.Status.OK.getStatusCode()) {
						productKeyStatusOut = response.readEntity(productKeyStatusOut.class);
						productsTo.setProductDetail(new ProductDetail());
						productsTo.getProductDetail().setRedeemStatus(productKeyStatusOut.getStatus());
						productsTo.getProductDetail().setRedeemDate(productKeyStatusOut.getRedemptionDate());
						productsTo.getProductDetail().setProductKey(Encryptor.encriptarAES(productKeyStatusOut.getProductKey()));
						productsDetailDao.update(productsTo);
					} else {
						ordersTo.getResponse().setHttStatusCode(response.getStatus());
						ordersTo.getResponse().setDescription("An error occurred, whose cause is::: ");
					}
					ordersTo.getResponse().setHttStatusCode(response.getStatus());
					ordersTo.getResponse().setDescription(productKeyStatusOut.toString());
					response.close();
					break;
				} catch (Exception e) {
					log.info("Se intentara guardar en auditoria con el request de getProductKeyStatus de Intcomex, la respuesta del WS no fue exitosa: ");
					productKeyStatusOut.setStatus("Invalid");
					AuditTo auditTo = new AuditTo();
					auditTo.setAudit(new Audit());
					auditTo.getAudit().setCreationDate(new Date());
					auditTo.getAudit().setEndPoint(target);
					auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
					auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
					auditTo.getAudit().setRequestBody("No aplica para este servicio de Intcomex");
					auditTo.getAudit().setRequestType(7);
					auditDao.insert(auditTo);
					log.info("Se guardo correctamente la informacion de auditoria para la respuesta del ws getProductKeyStatus de Intcomex con una respuesta incorrecta del WS ");
					return ordersTo;
				}
			}
		} else {
			log.info("ProductKey ya redimido, no se consultará el WS");
		}
		log.info("Se intentara guardar en auditoria con el request de getProductKeyStatus de Intcomex");
		AuditTo auditTo = new AuditTo();
		auditTo.setAudit(new Audit());
		auditTo.getAudit().setCreationDate(new Date());
		auditTo.getAudit().setEndPoint(target);
		auditTo.getAudit().setRequestHeader(ordersTo.getRequest().getHeaders());
		auditTo.getAudit().setRefTransId(new Integer(ordersTo.getOrderDi().getRefId()));
		auditTo.getAudit().setRequestBody("No aplica para este servicio de Intcomex");
		auditTo.getAudit().setRequestType(7);
		auditDao.insert(auditTo);
		log.info("Se guardo correctamente la informacion de auditoria para la respuesta del ws getProductKeyStatus de Intcomex :) ");
		return ordersTo;
	}

	/** Method that generate the code SHA for intcomex service */
	private StringBuffer getGenerateCodeSHA(String signatureP) {
		StringBuffer signatureF = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(signatureP.getBytes());
			byte byteData[] = md.digest();
			/** Convert the byte to hex format method */
			for (int i = 0; i < byteData.length; i++) {
				signatureF.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (Exception e) {
			log.error("" + e.getLocalizedMessage());
		}
		return signatureF;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

	public String execute(String propertieKey) {
		return null;
	}

}