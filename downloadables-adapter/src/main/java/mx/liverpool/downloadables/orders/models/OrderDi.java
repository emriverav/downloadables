/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.orders.models;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [18/01/2018]
 * @updated: Mauricio Rivera [06/03/2018]
 * @description: model-class that mapping with Order Object
 * @since-version: 1.0
 */
@Data
public class OrderDi implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -3659750782074600522L;
	private String statusCode;
	private String shippingGroupId;
	private String fulfillSystemReference;
	private Item[] items;
	@JsonIgnore
	private Item[] itemsIntcomex;
	@JsonIgnore
	private Item[] itemsUnsupport;
	private String refId;
	private String loginId;
	private String profileId;
	private String customerFirstName;
	private String customerPaterno;
	private String customerMaterno;
	private String customerEmail;
	private String customerCellPhone;
	private String channel;
	private String productKey;

	/** Used by Monitor */
	private String orderNumber;
	private Boolean isMonitor = false;

	public String toJson() {
		String json = "";
		try {
			json = new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
		}
		return json;
	}
}
