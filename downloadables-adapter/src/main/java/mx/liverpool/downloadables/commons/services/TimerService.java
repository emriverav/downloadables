/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.services;

import java.util.Timer;
import java.util.TimerTask;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.applications.services.ApplicationsService;
import mx.liverpool.downloadables.applications.tos.ApplicationsTo;
import mx.liverpool.downloadables.commons.models.Response;
import mx.liverpool.downloadables.commons.tos.ApiTo;
import mx.liverpool.downloadables.properties.services.PropertiesService;
import mx.liverpool.downloadables.properties.tos.PropertiesTo;
import mx.liverpool.downloadables.suppliers.services.SuppliersService;
import mx.liverpool.downloadables.suppliers.tos.SuppliersTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated:
 * @description: timer service to run all task
 * @since-version: 1.0
 */
@Slf4j
public class TimerService {

	Timer timer = new Timer();

	class Job extends TimerTask {
		public void run() {
			try {

				/** Task 1: Invocar el garbage collector */
				(Runtime.getRuntime()).gc();

				/** Task 2: Refresh Properties cache */
				PropertiesTo propertiesTo = new PropertiesTo();
				propertiesTo.setResponse(new Response());
				new PropertiesService().refresh(propertiesTo);

				/** Task 3: Refresh Suppliers cache */
				SuppliersTo suppliersTo = new SuppliersTo();
				suppliersTo.setResponse(new Response());
				new SuppliersService().refresh(suppliersTo);

				/** Task 4: Refresh Applications cache */
				ApplicationsTo applicationsTo = new ApplicationsTo();
				applicationsTo.setResponse(new Response());
				new ApplicationsService().refresh(applicationsTo);

			} catch (Exception e) {
				log.error("Some task is failed");
			}
		}
	}

	public void start(int milisegundos) {
		timer.schedule(new Job(), 60000, ApiTo.getInstance().getConfig().getApi().getPolling());
		log.info("Has been started the timer service");
	}

	public void stop() {
		timer.cancel();
		log.info("Has been stopped the timer service");
	}

}
