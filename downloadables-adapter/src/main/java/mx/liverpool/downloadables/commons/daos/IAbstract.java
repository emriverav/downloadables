/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.daos;

/**
 * @author: Genaro Bermúdez [30/01/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Abstract interface for CRUD Operations
 * @since-version: 1.0
 */
public interface IAbstract {

	/**
	 * @Description: interface to do insert operations
	 * @param object
	 * @return
	 */
	public Object insert(Object object);

	/**
	 * @Description: interface to do update operations
	 * @param object
	 * @return
	 */
	public Object update(Object object);

	/**
	 * @Description: interface to do save operations
	 * @param object
	 * @return
	 */
	public Object save(Object object);

	/**
	 * @Description: interface to do delete operations
	 * @param object
	 */
	public Object delete(Object object);

	/**
	 * @Description: interface to do find operations
	 * @param object
	 * @return
	 */
	public Object find(Object object);

	/**
	 * @Description: interface to do findAll operations
	 * @return
	 */
	public Object findAll(Object object);

	/**
	 * @Description: interface to set rows returned from operations
	 * @return
	 */
	public int getRowsAffected();

}
