/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [19/02/2018]
 * @updated:
 * @description: Class to manage request attributes
 * @since-version: 1.0
 */
@Data
public class Request implements Serializable {

	/** Class mambers */
	private static final long serialVersionUID = 4765756201080160650L;
	private String endPoint;
	private String headers;

}
