/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.daos;

import org.springframework.stereotype.Repository;

import mx.liverpool.downloadables.commons.tos.AuditTo;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated: Genaro Bermúdez [18/02/2018]
 * @description: Dao implementation of Orders
 * @since-version: 1.0
 */
@Repository
public class AuditDao extends AbstracDao implements IAuditDao {

	/** Query constants for all operations in this implementation */
	protected final static String INSERT = "INSERT INTO AUDIT_ADAPTER (REF_TRANS_ID, CREATION_DATE, REQUEST_HEADER, REQUEST_BODY, RESPONSE_HEADER, RESPONSE_BODY, ENDPOINT, REQUEST_TYPE) VALUES (?,?,?,?,?,?,?,?)";

	public Object insert(Object object) {
		AuditTo auditTo = (AuditTo) object;
		try {
			setRowsAffected(jdbcTemplate.update(INSERT, new Object[] { auditTo.getAudit().getRefTransId(), auditTo.getAudit().getCreationDate(), auditTo.getAudit().getRequestHeader(), auditTo.getAudit().getRequestBody(), auditTo.getAudit().getResponseHeader(), auditTo.getAudit().getResponseBody(), auditTo.getAudit().getEndPoint(), auditTo.getAudit().getRequestType() }));
		} catch (Exception e) {
			throw e;
		}
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

}
