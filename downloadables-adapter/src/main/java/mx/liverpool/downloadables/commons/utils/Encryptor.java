/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.utils;

import java.security.Key;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import lombok.extern.slf4j.Slf4j;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Utilitarian class that is responsible for encrypting a chain
 * @since-version: 1.0
 */
@Slf4j
public class Encryptor {

	/** Preparation of the local variables of the class | 128-bit key for the AES algorithm */
	private static final String ALGORITMO_AES = "AES";
	private static final String AES_KEY = "SD8DJ34HE9DLSJSD";

	public static String encriptarAES(String entrada) {
		log.info("Beginning method to encryptAES entry: ");
		String resultado = "";
		try {
			if (entrada == null) {
				log.error("Can't encrypt an empty string!");
				return null;
			}
			Key aesKey = new SecretKeySpec(AES_KEY.getBytes(), ALGORITMO_AES);
			Cipher cipher = Cipher.getInstance(ALGORITMO_AES);
			cipher.init(Cipher.ENCRYPT_MODE, aesKey);
			byte[] encrypted = cipher.doFinal(entrada.getBytes());
			StringBuilder sb = new StringBuilder();
			for (byte b : encrypted) {
				sb.append((char) b);
			}
			String enc = sb.toString();
			byte[] bb = new byte[enc.length()];
			for (int i = 0; i < enc.length(); i++) {
				bb[i] = (byte) enc.charAt(i);
			}
			resultado = Base64.getEncoder().encodeToString(bb);
		} catch (Exception e) {
			log.info("An error occurred while trying to encrypt the string with AES. Cause: " + e.getCause());
		}
		return resultado;
	}

	public static String desencriptarAES(String entrada) {
		log.info("Beginning method to decryptAES entry: ");
		String resultado = "";
		try {
			if (entrada == null) {
				log.warn("Can't decrypt an empty string!");
				return null;
			}
			Key aesKey = new SecretKeySpec(AES_KEY.getBytes(), ALGORITMO_AES);
			Cipher cipher = Cipher.getInstance(ALGORITMO_AES);
			byte[] bb = Base64.getDecoder().decode(entrada);
			cipher.init(Cipher.DECRYPT_MODE, aesKey);
			resultado = new String(cipher.doFinal(bb));
		} catch (Exception e) {
			log.info("An error occurred while trying to decrypt the string with AES. Cause: " + e.getCause());
		}
		return resultado;
	}

}