/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [19/01/2018]
 * @updated: Mauricio Rivera [20/02/2018]
 * @description: config-file to set app values. Its uses YAML mapping with jackson library
 * @since-version: 1.0
 */
@Data
public class Service implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 8970798839614895270L;
	private String name;
	private String protocol;
	private String host;
	private String port;
	private String context;
	private String resource;
	private String retries;
	private String delay;
	private String override;
	private String code;

}
