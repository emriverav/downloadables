/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.models;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [19/01/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: config-file to set app values. Its uses YAML mapping with jackson library
 * @since-version: 1.0
 */
@Data
public class Config implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 3046852161050907933L;
	private Api api;
	private DataBase dataBase;
	private List<Service> services;
	private Signature signature;
	private QueueManager queueManager;

	@Data
	public class Api {

		/** Sub-class members */
		private String name;
		private int polling;
		private String version;
		private List<String> exclusions;

	}

	@Data
	public class DataBase {

		/** Sub-class members */
		private String jndi;

	}

	@Data
	public class Signature {

		/** Sub-class members */
		private String apiKey;
		private String accessKey;

	}

	@Data
	public class QueueManager {

		/** Sub-class members */
		private String name;
		private String host;
		private String channel;
		private String application;
		private String user;
		private String password;
		private Integer port;
		private String queue;
		private Integer delay;
		private Integer retries;

	}

}
