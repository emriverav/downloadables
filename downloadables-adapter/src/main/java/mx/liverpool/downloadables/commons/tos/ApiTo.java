/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.tos;

import java.io.Serializable;

import javax.inject.Singleton;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.commons.models.Config;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Wrapping to many models or pojo's class. Its represents the TransferObject Design Pattern
 * @since-version: 1.0
 */
@Slf4j
@Data
@Singleton
public class ApiTo implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -9170996066079091191L;
	private static volatile ApiTo instance = null;
	private Config config;

	public static ApiTo getInstance() {
		if (instance == null) {
			synchronized (ApiTo.class) {
				if (instance == null) {
					instance = new ApiTo();
					log.info("New ApiTo created");
				}
			}
		}
		return instance;
	}

}
