/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.tos;

import java.io.Serializable;

import lombok.Data;
import mx.liverpool.downloadables.commons.models.Audit;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated:
 * @description: Wrapping to many models or pojo's class. Its represents the TransferObject Design Pattern
 * @since-version: 1.0
 */
@Data
public class AuditTo implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 2967315032346211864L;
	private Audit audit;
	private Boolean successfully;

}
