/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.daos;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated:
 * @description: Audit interface to dao tier
 * @since-version: 1.0
 */
public interface IAuditDao extends IAbstract {

}
