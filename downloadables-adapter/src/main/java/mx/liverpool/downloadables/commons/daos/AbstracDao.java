/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.daos;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.commons.tos.ApiTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Abstract connection to database en JDBC-Template
 * @since-version: 1.0
 */
@Slf4j
public abstract class AbstracDao {

	/** Class members */
	protected static volatile JdbcTemplate jdbcTemplate;

	@Getter
	@Setter
	private boolean operationSuccessfully;

	@Getter
	@Setter
	private int rowsAffected;

	/** Default construct */
	public AbstracDao() {
		if (jdbcTemplate == null || jdbcTemplate.getDataSource() == null) {
			try {
				InitialContext context = new InitialContext();
				DataSource ds = (DataSource) context.lookup(ApiTo.getInstance().getConfig().getDataBase().getJndi());
				AbstracDao.jdbcTemplate = new JdbcTemplate(ds);
				log.info("Database Connection successfully");
			} catch (Exception e) {
				log.error("Error while connect to database");
			}
		}
	}

}
