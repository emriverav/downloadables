/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.models;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: config-file to set app values. Its uses YAML mapping with jackson library
 * @since-version: 1.0
 */
@Data
public class Audit implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 7997920380838236881L;
	private int id;
	private int refTransId;
	private Date creationDate;
	private String requestHeader;
	private String requestBody;
	private String responseHeader;
	private String responseBody;
	private String endPoint;
	private int requestType;
}
