/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.commons.app;

import javax.inject.Singleton;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.applications.controllers.ApplicationsController;
import mx.liverpool.downloadables.commons.models.Config;
import mx.liverpool.downloadables.commons.services.TimerService;
import mx.liverpool.downloadables.commons.tos.ApiTo;
import mx.liverpool.downloadables.orders.controllers.OrdersController;
import mx.liverpool.downloadables.products.controllers.ProductsController;
import mx.liverpool.downloadables.properties.controllers.PropertiesController;
import mx.liverpool.downloadables.security.controllers.SecurityController;
import mx.liverpool.downloadables.suppliers.controllers.SuppliersController;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [12/02/2018]
 * @description: discover others Controllers
 * @since-version: 1.0
 */
@ApplicationPath("/")
@Singleton
@Slf4j
public class App extends Application {

	public App() {
		try {
			ApiTo.getInstance().setConfig(new ObjectMapper(new YAMLFactory()).readValue(getClass().getClassLoader().getResourceAsStream("config.yml"), Config.class));
			new TimerService().start(ApiTo.getInstance().getConfig().getApi().getPolling());
			PropertiesController.Discover();
			SuppliersController.Discover();
			SecurityController.Discover();
			ProductsController.Discover();
			ApplicationsController.Discover();
			OrdersController.Discover();
			log.info("Has been loaded all controllers and Yam-file loaded");
		} catch (Exception e) {
			log.error("Can't load the controllers or can't load the Yaml-file");
		}
	}

}
