/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.applications.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: entity-class that mapping with Application table
 * @since-version: 1.0
 */
@Data
public class Application implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -772327916805234801L;
	private String applicationId;
	private String applicationName;

}
