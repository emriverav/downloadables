/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.applications.tos;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.liverpool.downloadables.applications.models.Application;
import mx.liverpool.downloadables.commons.models.Response;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [18/02/2018]
 * @description: Wrapping to many models or pojo's class. Its represents the TransferObject Design Pattern
 * @since-version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ApplicationsTo implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 5876667003252886777L;
	private Application application;
	private ArrayList<Application> applications;
	private Boolean successfully;
	private Response response;

}
