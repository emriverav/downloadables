/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.applications.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.applications.services.IApplicationsService;
import mx.liverpool.downloadables.applications.tos.ApplicationsTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [18/02/2018]
 * @description: Class to manage the http(s) requets to Application module
 * @since-version: 1.0
 */
@Path("api/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@Slf4j
public class ApplicationsController {

	@Inject
	IApplicationsService applicationsService;

	public static void Discover() {
		log.info("ApplicationsController discovered");
	}

	@GET
	@Path("/applications/cache/refresh")
	public Response refresh() {
		ApplicationsTo applicationsTo = new ApplicationsTo();
		applicationsTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		try {
			applicationsTo = applicationsService.refresh(applicationsTo);
		} catch (Exception e) {
			log.info("Ending refresh method with errors");
		}
		return Response.status(applicationsTo.getResponse().getHttStatusCode()).entity(applicationsTo.getResponse()).build();
	}

}
