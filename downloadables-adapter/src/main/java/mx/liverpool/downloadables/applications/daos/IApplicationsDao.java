/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.applications.daos;

import mx.liverpool.downloadables.commons.daos.IAbstract;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Properties interface to dao tier
 * @since-version: 1.0
 */
public interface IApplicationsDao extends IAbstract {

}
