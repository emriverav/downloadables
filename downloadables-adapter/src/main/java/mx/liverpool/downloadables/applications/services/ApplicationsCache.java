/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.applications.services;

import java.util.Map;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.applications.models.Application;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: singleton to manage ApplicationsCache
 * @since-version: 1.0
 */
@Slf4j
@Data
public class ApplicationsCache {

	/** Class members */
	private static volatile ApplicationsCache instance = null;
	private Map<String, Application> applications;

	public static ApplicationsCache getInstance() throws Exception {
		if (instance == null) {
			synchronized (ApplicationsCache.class) {
				instance = new ApplicationsCache();
			}
		}
		return instance;
	}

}
