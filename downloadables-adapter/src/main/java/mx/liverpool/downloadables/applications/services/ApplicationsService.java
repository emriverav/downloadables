/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.applications.services;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.applications.daos.IApplicationsDao;
import mx.liverpool.downloadables.applications.models.Application;
import mx.liverpool.downloadables.applications.tos.ApplicationsTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [18/02/2018]
 * @description: bussiness logic Properties Service
 * @since-version: 1.0
 */
@Slf4j
public class ApplicationsService implements IApplicationsService {

	@Inject
	IApplicationsDao applicationsDao;

	public boolean validate(String applicationName) {
		boolean exits = false;
		try {
			if (ApplicationsCache.getInstance().getApplications() == null) {
				Map<String, Application> applications = new Hashtable<String, Application>();
				for (Application p : ((ApplicationsTo) applicationsDao.findAll(new ApplicationsTo())).getApplications()) {
					applications.put(p.getApplicationName(), p);
				}
				ApplicationsCache.getInstance().setApplications(applications);
			}
			if (ApplicationsCache.getInstance().getApplications().get(applicationName) != null) {
				exits = true;
			}
		} catch (Exception e) {
			log.info("Can't validate the application");
		}
		return exits;
	}

	public ApplicationsTo refresh(ApplicationsTo applicationsTo) {
		try {
			ApplicationsCache.getInstance().setApplications(null);
			applicationsTo.setSuccessfully(true);
			applicationsTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
			applicationsTo.getResponse().setDescription("Has been refreshed the applications cache");
		} catch (Exception e) {
			applicationsTo.setSuccessfully(false);
			applicationsTo.getResponse().setHttStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
			applicationsTo.getResponse().setDescription("Can't refresh the applications cache");
			log.info("Can't refresh the applications cache > " + e.getMessage());
		}
		return applicationsTo;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public List<? extends Object> findAll() {
		return null;
	}

	public boolean isOperationSuccessfully() {
		return false;
	}

	public int getRowsAffected() {
		return 0;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

}