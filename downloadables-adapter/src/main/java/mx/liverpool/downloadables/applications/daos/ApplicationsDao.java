/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.applications.daos;

import java.util.ArrayList;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import mx.liverpool.downloadables.applications.models.Application;
import mx.liverpool.downloadables.applications.tos.ApplicationsTo;
import mx.liverpool.downloadables.commons.daos.AbstracDao;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Dao implementation of Applications
 * @since-version: 1.0
 */
@Repository
public class ApplicationsDao extends AbstracDao implements IApplicationsDao {

	/** Query constants for all operations in this implementation */
	protected final static String SQL_FIND_ALL = "SELECT * FROM APPLICATIONS";

	public ApplicationsTo findAll(Object object) {
		ApplicationsTo applicationsTo = (ApplicationsTo) object;
		applicationsTo.setApplications(new ArrayList<Application>());
		try {
			applicationsTo.setApplications((ArrayList<Application>) jdbcTemplate.query(SQL_FIND_ALL, new BeanPropertyRowMapper<Application>(Application.class)));
			setRowsAffected(applicationsTo.getApplications().size());
		} catch (Exception e) {
			throw e;
		}
		return applicationsTo;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

}
