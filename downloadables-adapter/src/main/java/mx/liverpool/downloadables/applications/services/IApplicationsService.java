/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.applications.services;

import mx.liverpool.downloadables.applications.tos.ApplicationsTo;
import mx.liverpool.downloadables.commons.daos.IAbstract;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Applications interface to dao tier
 * @since-version: 1.0
 */
public interface IApplicationsService extends IAbstract {

	/**
	 * @author: Genaro Bermúdez [10/02/2018]
	 * @updated: Genaro Bermúdez [10/02/2018]
	 * @description: method to refresh the ApplicationsCache
	 * @since-version: 1.0
	 */
	public ApplicationsTo refresh(ApplicationsTo applicationsTo);

	/**
	 * @author: Genaro Bermúdez [10/02/2018]
	 * @updated: Genaro Bermúdez [10/02/2018]
	 * @description: method to validate the Application
	 * @since-version: 1.0
	 */
	public boolean validate(String applicationName);

}
