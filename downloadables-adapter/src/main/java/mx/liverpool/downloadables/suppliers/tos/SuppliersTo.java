/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.suppliers.tos;

import java.io.Serializable;
import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.liverpool.downloadables.commons.models.Response;
import mx.liverpool.downloadables.suppliers.models.Supplier;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [18/02/2018]
 * @description: Wrapping to many models or pojo's class. Its represents the TransferObject Design Pattern
 * @since-version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SuppliersTo implements Serializable {

	/** Class members */
	private static final long serialVersionUID = 5876667003252886777L;
	private Supplier supplier;
	private ArrayList<Supplier> suppliers;
	private Boolean successfully;
	private Response response;

}
