/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.suppliers.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.suppliers.models.Supplier;
import mx.liverpool.downloadables.suppliers.services.ISuppliersService;
import mx.liverpool.downloadables.suppliers.tos.SuppliersTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Class to manage the http(s) requets to Suppliers
 * @since-version: 1.0
 */
@Path("api/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@Slf4j
public class SuppliersController {

	@Inject
	ISuppliersService suppliersService;

	public static void Discover() {
		log.info("SuppliersController discovered");
	}

	@GET
	@Path("/suppliers/{supplierKey}")
	public Response find(@PathParam("supplierKey") String supplierKey) {
		SuppliersTo suppliersToTo = new SuppliersTo();
		suppliersToTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		suppliersToTo.setSupplier(new Supplier());
		suppliersToTo.getSupplier().setSupplierKey(supplierKey);
		try {
			suppliersToTo = (SuppliersTo) suppliersService.find(suppliersToTo);
			log.info("Ending find succesfully");
		} catch (Exception e) {
			log.info("Ending find with errors");
		}
		return Response.status(suppliersToTo.getResponse().getHttStatusCode()).entity(suppliersToTo.getResponse()).build();
	}

	@GET
	@Path("/suppliers/cache/refresh")
	public Response refresh() {
		SuppliersTo suppliersTo = new SuppliersTo();
		suppliersTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		try {
			suppliersTo = suppliersService.refresh(suppliersTo);
			log.info("Ending refresh method succesfully");
		} catch (Exception e) {
			log.info("Ending refresh method with errors");
		}
		return Response.status(suppliersTo.getResponse().getHttStatusCode()).entity(suppliersTo.getResponse()).build();
	}

}
