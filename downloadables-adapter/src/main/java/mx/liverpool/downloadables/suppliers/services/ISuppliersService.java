/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.suppliers.services;

import mx.liverpool.downloadables.commons.daos.IAbstract;
import mx.liverpool.downloadables.suppliers.tos.SuppliersTo;

/**
 * @author: Genaro Bermúdez [10/01/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Properties interface to dao tier
 * @since-version: 1.0
 */
public interface ISuppliersService extends IAbstract {

	/**
	 * @author: Genaro Bermúdez [10/02/2018]
	 * @updated: Genaro Bermúdez [10/02/2018]
	 * @description: method to get the Supplier
	 * @since-version: 1.0
	 */
	public String getValue(String suppliersTo);

	/**
	 * @author: Genaro Bermúdez [10/02/2018]
	 * @updated: Genaro Bermúdez [10/02/2018]
	 * @description: method to refresh the SuppliersCache
	 * @since-version: 1.0
	 */
	public SuppliersTo refresh(SuppliersTo suppliersTo);

}
