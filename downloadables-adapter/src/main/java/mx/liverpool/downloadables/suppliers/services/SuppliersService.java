/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.suppliers.services;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.suppliers.daos.ISuppliersDao;
import mx.liverpool.downloadables.suppliers.models.Supplier;
import mx.liverpool.downloadables.suppliers.tos.SuppliersTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Mauricio Rivera [21/03/2018]
 * @description: bussiness logic Supliers Service
 * @since-version: 1.0
 */
@Slf4j
public class SuppliersService implements ISuppliersService {

	@Inject
	ISuppliersDao suppliersDao;

	public String getValue(String supplierKey) {
		String value = "";
		try {
			ArrayList<Supplier> p = ((SuppliersTo) suppliersDao.findAll(new SuppliersTo())).getSuppliers();
			value = p.get(0).getSupplierKey();
		} catch (Exception e) {
			log.info("Can't obtained the value: " + value);
		}
		return value;
	}

	public SuppliersTo find(Object object) {
		SuppliersTo suppliersTo = (SuppliersTo) object;
		try {
			String supplierValue = getValue(suppliersTo.getSupplier().getSupplierKey());
			suppliersTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
			suppliersTo.getResponse().setDescription("The supplierValue is > " + supplierValue);
			suppliersTo.setSuccessfully(true);
		} catch (Exception e) {
			suppliersTo.getResponse().setHttStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
			suppliersTo.getResponse().setDescription("Can't find supplierValue due this error > " + e.getMessage());
			suppliersTo.setSuccessfully(false);
			log.info(suppliersTo.getResponse().getDescription());
		}
		return suppliersTo;
	}

	public SuppliersTo refresh(SuppliersTo suppliersTo) {
		try {
			SuppliersCache.getInstance().setSuppliers(null);
			suppliersTo.setSuccessfully(true);
			suppliersTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
		} catch (Exception e) {
			suppliersTo.setSuccessfully(false);
			suppliersTo.getResponse().setHttStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
			suppliersTo.getResponse().setDescription("Can't refresh the suppliers cache");
			log.info("Can't refresh the suppliers cache > " + e.getMessage());
		}
		return suppliersTo;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

}