/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.suppliers.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Jonathan Garcia [19/01/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: entity-class that mapping with Provider table
 * @since-version: 1.0
 */
@Data
public class Supplier implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -5699932265634140537L;
	private String supplierId;
	private String supplierKey;
	private String supplierValue;
	private String description;

}