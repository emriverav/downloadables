/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.suppliers.daos;

import java.util.ArrayList;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import mx.liverpool.downloadables.commons.daos.AbstracDao;
import mx.liverpool.downloadables.suppliers.models.Supplier;
import mx.liverpool.downloadables.suppliers.tos.SuppliersTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Dao implementation of Supliers
 * @since-version: 1.0
 */
@Repository
public class SuppliersDao extends AbstracDao implements ISuppliersDao {

	/** Query constants for all operations in this implementation */
	protected final static String SQL_FIND_ALL = "SELECT SUPPLIER_KEY FROM SUPPLIERS";

	public SuppliersTo findAll(Object object) {
		SuppliersTo suppliersTo = (SuppliersTo) object;
		suppliersTo.setSuppliers(new ArrayList<Supplier>());
		try {
			suppliersTo.setSuppliers((ArrayList<Supplier>) jdbcTemplate.query(SQL_FIND_ALL, new BeanPropertyRowMapper<Supplier>(Supplier.class)));
			setRowsAffected(suppliersTo.getSuppliers().size());
		} catch (Exception e) {
			throw e;
		}
		return suppliersTo;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

}
