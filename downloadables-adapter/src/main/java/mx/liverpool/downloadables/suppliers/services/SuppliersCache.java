/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.suppliers.services;

import java.util.Map;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.suppliers.models.Supplier;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: singleton to manage SuppliersCache
 * @since-version: 1.0
 */
@Slf4j
@Data
public class SuppliersCache {

	/** Class members */
	private static volatile SuppliersCache instance = null;
	private Map<String, Supplier> suppliers;

	public static SuppliersCache getInstance() throws Exception {
		if (instance == null) {
			synchronized (SuppliersCache.class) {
				instance = new SuppliersCache();
			}
		}
		return instance;
	}

}
