package mx.liverpool.downloadables.security.services;

import java.io.IOException;
import java.net.URI;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.applications.services.IApplicationsService;
import mx.liverpool.downloadables.commons.daos.IAuditDao;
import mx.liverpool.downloadables.commons.tos.ApiTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated:
 * @description: interceptor service
 * @since-version: 1.0
 */
@Slf4j
@Provider
@PreMatching
public class InterceptorService implements ContainerRequestFilter {

	@Inject
	IApplicationsService applicationsService;

	@Inject
	IAuditDao auditDao;

	public void filter(ContainerRequestContext request) throws IOException {
		String applicationId = request.getHeaderString(HttpHeaders.AUTHORIZATION);
		if (ApiTo.getInstance().getConfig().getApi().getExclusions() != null && !ApiTo.getInstance().getConfig().getApi().getExclusions().isEmpty()) {
			for (String path : ApiTo.getInstance().getConfig().getApi().getExclusions()) {
				if (request.getUriInfo().getPath().endsWith(path) || request.getUriInfo().getPath().contains(path)) {
					return;
				}
			}
		}
		if (applicationId == null || applicationId.isEmpty() || !applicationsService.validate(applicationId)) {
			log.error("This request can not be addressed because the Client Application that you are trying to access does not have a valid applicationId");
			request.setRequestUri(URI.create("/api/v1/security/unauthorized"));
		}
	}

}
