/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.security.controllers;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated:
 * @description: Class to manage the http(s) requets to Security
 * @since-version: 1.0
 */
@Path("api/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@Slf4j
public class SecurityController {

	public static void Discover() {
		log.info("SecurityController discovered");
	}

	@GET
	@Path("/security/unauthorized")
	public Response unauthorized() {
		mx.liverpool.downloadables.commons.models.Response response = new mx.liverpool.downloadables.commons.models.Response();
		try {
			response.setHttStatusCode(Response.Status.UNAUTHORIZED.getStatusCode());
			response.setDescription(Response.Status.UNAUTHORIZED.getReasonPhrase() + " - This request can not be addressed because the Client Application that you are trying to access does not have a valid applicationId");
			log.info("Se ha realizado la operación solicitada");

		} catch (Exception e) {
			log.info("No se ha podido realizar la operación solicitada");
		}
		return Response.status(response.getHttStatusCode()).entity(response).build();

	}

}
