package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExtendedPrice implements Serializable {
	private static final long serialVersionUID = -6670233446498812107L;
	@XmlElement
	private String Amount;
	@XmlElement
	private String HomeAmount;
	@XmlElement
	private String Currency;
}
