package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Manufacturer implements Serializable {
	private static final long serialVersionUID = 861466407391223090L;
	@XmlElement
	private String Description;
	@XmlElement
	private String ManufacturerId;

}
