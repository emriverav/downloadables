package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Items implements Serializable{
	private static final long serialVersionUID = 8415267792296491989L;
	@XmlElement
	private String LocationId;
	@XmlElement
	private String Comments;
	@XmlElement
	private String Quantity;
	@XmlElement
	private String LineNumber;
	@XmlElement
	private Product Product;
	@XmlElement
	private ExtendedPrice ExtendedPrice;
	@XmlElement
	private UnitPrice UnitPrice;
}
