/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.intcomex.purchaseesdProducts.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @author: Mauricio Rivera [23/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EsdFulfillments implements Serializable {
	private static final long serialVersionUID = 5071267102895296542L;

	@XmlElement
	private Data Data;

	@XmlElement
	private String DescriptionKey;

	@XmlElement
	private Products[] Products;

	@XmlElement
	private String PoNumber;

	@XmlElement
	private String Instructions;

	@XmlElement
	private String FulfillmentDate;

}
