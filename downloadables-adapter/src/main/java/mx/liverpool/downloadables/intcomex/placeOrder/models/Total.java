package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Total implements Serializable {
	private static final long serialVersionUID = -8720279142802146789L;
	@XmlElement
	private String Amount;
	@XmlElement
	private String HomeAmount;
	@XmlElement
	private String Currency;
}
