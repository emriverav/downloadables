package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import lombok.Data;

@Data
public class Brand implements Serializable {
	private static final long serialVersionUID = 8299736793741320475L;
	@XmlElement
	private String BrandId;
	@XmlElement
	private String Description;
	@XmlElement
	private String ManufacturerId;
}
