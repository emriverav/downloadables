package mx.liverpool.downloadables.intcomex.purchaseesdProducts.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EsdFulfillmentError implements Serializable {
	
	private static final long serialVersionUID = 714948318491961224L;

	@XmlElement
	private String Message;
	@XmlElement
	private String ExceptionType;
	@XmlElement
	private int ErrorCode;
	@XmlElement
	private String Reference;
}
