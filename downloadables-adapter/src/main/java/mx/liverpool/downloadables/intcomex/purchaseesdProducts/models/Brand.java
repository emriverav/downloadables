/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.intcomex.purchaseesdProducts.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * @author: Mauricio Rivera [23/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Brand implements Serializable {
	private static final long serialVersionUID = -830512785859584162L;

	@XmlElement
	private String BrandId;

	@XmlElement
	private String Description;

	@XmlElement
	private String ManufacturerId;
}
