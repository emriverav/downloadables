/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.intcomex.purchaseesdProducts.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author: Mauricio Rivera [23/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@lombok.Data
public class Data implements Serializable {
	private static final long serialVersionUID = -1089494392296073038L;

	@XmlElement
	private String EsdFulfillmentId;

	@XmlElement
	private String FullApiResponse;

	@XmlElement
	private String FulfillmentType;

	@XmlElement
	private String ClientTransactionId;
}
