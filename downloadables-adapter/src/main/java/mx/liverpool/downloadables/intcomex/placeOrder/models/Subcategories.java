package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Subcategories implements Serializable {
	private static final long serialVersionUID = -1408605044828240870L;
	@XmlElement
	private String Description;
	@XmlElement
	private String CategoryId;
	@XmlElement
	private String[] Subcategories;
}
