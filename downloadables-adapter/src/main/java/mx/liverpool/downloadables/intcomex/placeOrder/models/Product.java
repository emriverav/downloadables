package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product implements Serializable {
	private static final long serialVersionUID = -7243702238855083832L;
	@XmlElement
	private Brand Brand;
	@XmlElement
	private String Description;
	@XmlElement
	private Category Category;
	@XmlElement
	private Manufacturer Manufacturer;
	@XmlElement
	private String Type;
	@XmlElement
	private String Mpn;
	@XmlElement
	private String Sku;
}
