package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
class Category implements Serializable{
	private static final long serialVersionUID = -3083774111174835327L;
	@XmlElement
	private String Description;
	@XmlElement
	private String CategoryId;
	@XmlElement
	private Subcategories[] Subcategories;
}
