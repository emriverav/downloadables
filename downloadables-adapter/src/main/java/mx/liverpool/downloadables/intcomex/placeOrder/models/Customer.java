package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer implements Serializable {

	private static final long serialVersionUID = -5339676658120719315L;

	@XmlElement
	private String Name;
	@XmlElement
	private String CustomerId;
	@XmlElement
	private Country Country;
	@XmlElement
	private Company Company;
	@XmlElement
	private Currency Currency;
}
