/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.intcomex.purchaseesdProducts.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * @author: Mauricio Rivera [23/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@lombok.Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Products implements Serializable {
	private static final long serialVersionUID = 6587018969781292958L;

	@XmlElement
	private Data Data;

	@XmlElement
	private Links[] Links;

	@XmlElement
	private String Mpn;

	@XmlElement
	private Tokens[] Tokens;

}
