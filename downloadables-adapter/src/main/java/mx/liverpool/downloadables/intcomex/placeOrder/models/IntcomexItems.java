package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

@Data
@XmlRootElement
public class IntcomexItems implements Serializable{
	private static final long serialVersionUID = -8332403889147257939L;
	private ArrayList<IncomexPlaceOrderIN> items;
}
