/**
 * Copyright (c) 2018 by Banco FAMSA All rights reserved.
 *
**/
package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

/**
 * @author: Mauricio Rivera [20/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
@XmlRootElement
public class IncomexPlaceOrderIN implements Serializable {

	private static final long serialVersionUID = 5320798652326139381L;
	private String Sku;
	private String Quantity;

	public String toJson(ObjectMapper objectMapper) {
		String json = "";
		try {
			json = objectMapper.writeValueAsString(this);
		} catch (Exception e) {
		}
		return json;
	}
}
