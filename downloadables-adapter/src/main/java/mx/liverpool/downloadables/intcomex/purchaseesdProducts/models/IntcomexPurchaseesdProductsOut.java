package mx.liverpool.downloadables.intcomex.purchaseesdProducts.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * @author: Mauricio Rivera [23/02/2018]
 * @updated: Mauricio Rivera [08/03/2018]
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IntcomexPurchaseesdProductsOut implements Serializable, Cloneable {
	private static final long serialVersionUID = 147923512499553015L;
	@XmlElement
	private IntcomexPurchaseesdProductsOut[] intcomexPurchaseesdProductsOut;
	@XmlElement
	private Brand Brand;
	@XmlElement
	private String Description;
	@XmlElement
	private String TokenDescription;
	@XmlElement
	private String LinkExpirationDate;
	@XmlElement
	private String Type;
	@XmlElement
	private String EsdFulfillmentStatus;
	@XmlElement
	private String TokenType;
	@XmlElement
	private String Sku;
	@XmlElement
	private Category Category;
	@XmlElement
	private Manufacturer Manufacturer;
	@XmlElement
	private String LinkDescription;
	@XmlElement
	private String LinkType;
	@XmlElement
	private String Mpn;
	@XmlElement
	private String EsdFulfillmentType;
	@XmlElement
	private String TokenCount;
	@XmlElement
	private Tokens[] Tokens;
	@XmlElement
	private EsdFulfillmentError EsdFulfillmentError;
	@XmlElement
	private EsdFulfillments[] EsdFulfillments;

}
