package mx.liverpool.downloadables.intcomex.purchaseesdProducts.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Data;

/**
 * @author: Mauricio Rivera [20/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
@XmlRootElement
public class IntcomexPurchaseesdProductsIN implements Serializable {
	private static final long serialVersionUID = 5531492223338711473L;
	private String OrderNumber;
}
