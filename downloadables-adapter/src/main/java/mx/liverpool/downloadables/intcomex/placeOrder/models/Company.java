package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Company implements Serializable {
	private static final long serialVersionUID = 6855643559856294245L;
	@XmlElement
	private String Name;
	@XmlElement
	private String Id;

}
