package mx.liverpool.downloadables.intcomex.purchaseesdProducts.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
/**
 * @author: Mauricio Rivera [23/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tokens implements Serializable {
	private static final long serialVersionUID = -3675036963013374967L;
	@XmlElement
	private String LinkExpirationDate;
	@XmlElement
	private String LinkType;
	@XmlElement
	private String TokenCode;
	@XmlElement
	private String TokenType;
	@XmlElement
	private String LinkUri;
	@XmlElement
	private String Status;
	@XmlElement
	private String Description;
	@XmlElement
	private String ProductKey;
	@XmlElement
	private String Type;
}
