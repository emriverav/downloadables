package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Currency implements Serializable {
	private static final long serialVersionUID = 4791113103943611748L;
	@XmlElement
	private String Description;
	@XmlElement
	private String UsExchangeRate;
	@XmlElement
	private String Symbol;
	@XmlElement
	private String CurrencyId;
	@XmlElement
	private String CurrencyCode;
}
