package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

/**
 * @author: Mauricio Rivera [20/02/2018]
 * @updated:
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Status implements Serializable {
	private static final long serialVersionUID = -1552346789194972885L;
	@XmlElement
	private String StatusCode;
	@XmlElement
	private String Description;
}
