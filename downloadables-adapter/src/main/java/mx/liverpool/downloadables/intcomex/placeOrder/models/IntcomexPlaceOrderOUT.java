package mx.liverpool.downloadables.intcomex.placeOrder.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;

/**
 * @author: Mauricio Rivera [20/02/2018]
 * @updated: Mauricio Rivera [23/02/2018]
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IntcomexPlaceOrderOUT implements Serializable {
	private static final long serialVersionUID = -4184780486370968000L;
	@XmlElement
	private Customer Customer;
	@XmlElement
	private Items[] Items;
	@XmlElement
	private Discounts Discounts;
	@XmlElement
	private String Comments;
	@XmlElement
	private String OrderDate;
	@XmlElement
	private String TransactionCode;
	@XmlElement
	private Subtotal Subtotal;
	@XmlElement
	private String ItemCount;
	@XmlElement
	private String CustomerOrderNumber;
	@XmlElement
	private Tax Tax;
	@XmlElement
	private String TaxRate;
	@XmlElement
	private String TransactionId;
	@XmlElement
	private Status Status;
	@XmlElement
	private String Tag;
	@XmlElement
	private String OrderNumber;
	@XmlElement
	private Currency Currency;
	@XmlElement
	private ShippingFee ShippingFee;
	@XmlElement
	private Total Total;
	@XmlElement
	private String OtherFees;
	@XmlElement
	private String Message;
	@XmlElement
	private int ErrorCode;
	@XmlElement
	private String Reference;
	@XmlElement
	private String Signature;

	public String toJson() {
		String json = "";
		try {
			json = new ObjectMapper().writeValueAsString(this);
		} catch (Exception e) {
		}
		return json;
	}
}
