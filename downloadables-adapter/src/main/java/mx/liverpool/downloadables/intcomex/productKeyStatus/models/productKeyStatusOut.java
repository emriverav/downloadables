package mx.liverpool.downloadables.intcomex.productKeyStatus.models;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;

import lombok.Data;

/**
 * @author: Mauricio Rivera [20/02/2018]
 * @updated: Mauricio Rivera [23/02/2018]
 * @description: model-class that mapping with Request Object
 * @since-version: 1.0
 */
@Data
public class productKeyStatusOut implements Serializable {
	private static final long serialVersionUID = 804386984156490653L;
	@XmlElement
	private String Status;
	@XmlElement
	private String Description;
	@XmlElement
	private String ProductKey;
	@XmlElement
	private String LastStatusUpdate;
	@XmlElement
	private String RedemptionDate;
	@XmlElement
	private String PromotionName;
}
