/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.clients.tos;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.liverpool.downloadables.clients.models.Client;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated:
 * @description: Wrapping to many models or pojo's class. Its represents the TransferObject Design Pattern
 * @since-version: 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ClientsTo implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -777246258880380477L;
	private Client client;
	private Boolean successfully;

}