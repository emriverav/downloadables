/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.clients.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated:
 * @description: entity-class that mapping with Clients table
 * @since-version: 1.0
 */
@Data
public class Client implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -772327916805234801L;
	private int id;
	private int refTransId;
	private String customerFirstName;
	private String customerApaterno;
	private String customerAMaterno;
	private String loginId;
	private String profileId;
	private String customerEmail;
	private String customerCelphone;

}
