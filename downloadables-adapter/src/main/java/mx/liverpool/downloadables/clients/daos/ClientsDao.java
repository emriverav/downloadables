/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.clients.daos;

import org.springframework.stereotype.Repository;

import mx.liverpool.downloadables.clients.tos.ClientsTo;
import mx.liverpool.downloadables.commons.daos.AbstracDao;

/**
 * @author: Genaro Bermúdez [14/02/2018]
 * @updated: Genaro Bermúdez [19/02/2018]
 * @description: Dao implementation of Clients
 * @since-version: 1.0
 */
@Repository
public class ClientsDao extends AbstracDao implements IClientsDao {

	/** Query constants for all operations in this implementation */
	protected final static String INSERT = "INSERT INTO CLIENTS (REF_TRANS_ID, CUSTOMER_FIRST_NAME, CUSTOMER_APATERNO, CUSTOMER_AMATERNO, LOGIN_ID, PROFILE_ID, CUSTOMER_EMAIL, CUSTOMER_CELPHONE) VALUES (?,?,?,?,?,?,?,?)";

	public Object insert(Object object) {
		ClientsTo clientsTo = (ClientsTo) object;
		try {
			setRowsAffected(jdbcTemplate.update(INSERT, new Object[] { clientsTo.getClient().getRefTransId(), clientsTo.getClient().getCustomerFirstName(), clientsTo.getClient().getCustomerApaterno(), clientsTo.getClient().getCustomerAMaterno(), clientsTo.getClient().getLoginId(), clientsTo.getClient().getProfileId(), clientsTo.getClient().getCustomerEmail(), clientsTo.getClient().getCustomerCelphone() }));
		} catch (Exception e) {
			throw e;
		}
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

}
