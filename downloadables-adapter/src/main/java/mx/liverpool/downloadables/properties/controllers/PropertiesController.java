/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.properties.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.properties.models.Property;
import mx.liverpool.downloadables.properties.services.IPropertiesService;
import mx.liverpool.downloadables.properties.tos.PropertiesTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Class to manage the http(s) requets to Properties module
 * @since-version: 1.0
 */
@Path("api/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@Slf4j
public class PropertiesController {

	@Inject
	IPropertiesService propertiesService;

	public static void Discover() {
		log.info("PropertiesController discovered");
	}

	@GET
	@Path("/properties/{propertyKey}")
	public Response find(@PathParam("propertyKey") String propertyKey) {
		PropertiesTo propertiesTo = new PropertiesTo();
		propertiesTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		propertiesTo.setProperty(new Property());
		propertiesTo.getProperty().setPropertyKey(propertyKey);
		try {
			propertiesTo = (PropertiesTo) propertiesService.find(propertiesTo);
			log.info("Ending find succesfully");
		} catch (Exception e) {
			log.info("Ending find with errors");
		}
		return Response.status(propertiesTo.getResponse().getHttStatusCode()).entity(propertiesTo.getResponse()).build();
	}

	@GET
	@Path("/properties/cache/refresh")
	public Response refresh() {
		PropertiesTo propertiesTo = new PropertiesTo();
		propertiesTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		try {
			propertiesTo = propertiesService.refresh(propertiesTo);
			log.info("Ending refresh method succesfully");
		} catch (Exception e) {
			log.info("Ending refresh method with errors");
		}
		return Response.status(propertiesTo.getResponse().getHttStatusCode()).entity(propertiesTo.getResponse()).build();
	}

}
