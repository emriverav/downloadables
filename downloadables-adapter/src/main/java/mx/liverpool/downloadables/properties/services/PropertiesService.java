/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.properties.services;

import java.util.Hashtable;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.properties.daos.IPropertiesDao;
import mx.liverpool.downloadables.properties.models.Property;
import mx.liverpool.downloadables.properties.tos.PropertiesTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [11/02/2018]
 * @description: bussiness logic Properties Service
 * @since-version: 1.0
 */
@Slf4j
public class PropertiesService implements IPropertiesService {

	@Inject
	IPropertiesDao propertiesDao;

	public String getValue(String key) {
		String value = "";
		try {
			if (PropertiesCache.getInstance().getProperties() == null) {
				Map<String, Property> properties = new Hashtable<String, Property>();
				for (Property p : ((PropertiesTo) propertiesDao.findAll(new PropertiesTo())).getProperties()) {
					properties.put(p.getPropertyKey(), p);
				}
				PropertiesCache.getInstance().setProperties(properties);
			}
			value = ((Property) PropertiesCache.getInstance().getProperties().get(key)).getPropertyValue();
		} catch (Exception e) {
			log.info("Can't obtain the value");
		}
		return value;
	}

	public PropertiesTo find(Object object) {
		PropertiesTo propertiesTo = (PropertiesTo) object;
		try {
			String propertyValue = getValue(propertiesTo.getProperty().getPropertyKey());
			propertiesTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
			propertiesTo.getResponse().setDescription("The propertyValue is > " + propertyValue);
			propertiesTo.setSuccessfully(true);
			log.info("Has been returned the propertyValue");
		} catch (Exception e) {
			propertiesTo.getResponse().setHttStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
			propertiesTo.getResponse().setDescription("Can't find propertyValue due this error > " + e.getMessage());
			propertiesTo.setSuccessfully(false);
			log.info(propertiesTo.getResponse().getDescription());
		}
		return propertiesTo;
	}

	public PropertiesTo refresh(PropertiesTo propertiesTo) {
		try {
			PropertiesCache.getInstance().setProperties(null);
			propertiesTo.setSuccessfully(true);
			propertiesTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
			propertiesTo.getResponse().setDescription("Has been refreshed the properties cache");
		} catch (Exception e) {
			propertiesTo.setSuccessfully(false);
			propertiesTo.getResponse().setHttStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
			propertiesTo.getResponse().setDescription("Can't refresh the properties cache");
			log.info("Can't refresh the properties cache > " + e.getMessage());
		}
		return propertiesTo;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

}