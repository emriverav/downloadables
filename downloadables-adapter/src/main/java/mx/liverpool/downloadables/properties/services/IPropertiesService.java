/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.properties.services;

import mx.liverpool.downloadables.commons.daos.IAbstract;
import mx.liverpool.downloadables.properties.tos.PropertiesTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Properties interface to dao tier
 * @since-version: 1.0
 */
public interface IPropertiesService extends IAbstract {

	/**
	 * @author: Genaro Bermúdez [11/02/2018]
	 * @updated:
	 * @description: method to get value the propertyKey
	 * @since-version: 1.0
	 */
	public String getValue(String propertieKey);

	/**
	 * @author: Genaro Bermúdez [10/02/2018]
	 * @updated: Genaro Bermúdez [10/02/2018]
	 * @description: method to refresh the PropertiesCache
	 * @since-version: 1.0
	 */
	public PropertiesTo refresh(PropertiesTo propertiesTo);

}
