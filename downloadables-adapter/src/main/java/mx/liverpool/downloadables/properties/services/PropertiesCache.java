/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.properties.services;

import java.util.Map;

import lombok.Data;
import mx.liverpool.downloadables.properties.models.Property;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: singleton to manage PropertiesCache
 * @since-version: 1.0
 */
@Data
public class PropertiesCache {

	/** Class members */
	private static volatile PropertiesCache instance = null;
	private Map<String, Property> properties;

	public static PropertiesCache getInstance() throws Exception {
		if (instance == null) {
			synchronized (PropertiesCache.class) {
				instance = new PropertiesCache();
			}
		}
		return instance;
	}

}
