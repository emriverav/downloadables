/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.properties.daos;

import java.util.ArrayList;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import mx.liverpool.downloadables.commons.daos.AbstracDao;
import mx.liverpool.downloadables.properties.models.Property;
import mx.liverpool.downloadables.properties.tos.PropertiesTo;

/**
 * @author: Genaro Bermúdez [10/02/2018]
 * @updated: Genaro Bermúdez [10/02/2018]
 * @description: Dao implementation of Properties
 * @since-version: 1.0
 */
@Repository
public class PropertiesDao extends AbstracDao implements IPropertiesDao {

	/** Query constants for all operations in this implementation */
	protected final static String SQL_FIND_ALL = "SELECT * FROM PROPERTIES";

	public PropertiesTo findAll(Object object) {
		PropertiesTo propertiesTo = (PropertiesTo) object;
		propertiesTo.setProperties(new ArrayList<Property>());
		try {
			propertiesTo.setProperties((ArrayList<Property>) jdbcTemplate.query(SQL_FIND_ALL, new BeanPropertyRowMapper<Property>(Property.class)));
			setRowsAffected(propertiesTo.getProperties().size());
		} catch (Exception e) {
			throw e;
		}
		return propertiesTo;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object find(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

}
