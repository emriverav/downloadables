/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.products.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [11/02/2018]
 * @updated:
 * @description: entity-class that mapping with Products table
 * @since-version: 1.0
 */
@Data
public class Product implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -772327916805234801L;
	private Integer idNumber;
	private Integer refTransId;
	private String sku;
	private String description;
	private Integer quantity;
	private String externalId;
	private Integer supplierId;
	private String orderId;
	private Integer errorCode;

}
