/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.products.controllers;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.products.models.Product;
import mx.liverpool.downloadables.products.services.IProductsService;
import mx.liverpool.downloadables.products.tos.ProductsTo;
import mx.liverpool.downloadables.products.valitators.ProductsValidator;

/**
 * @author: Genaro Bermúdez [11/02/2018]
 * @updated:
 * @description: Class to manage the http(s) requets to Products module
 * @since-version: 1.0
 */
@Path("api/v1")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Singleton
@Slf4j
public class ProductsController {

	@Inject
	IProductsService productsService;
	@Inject
	ProductsValidator productsValidator;

	public static void Discover() {
		log.info("ProductsController discovered");
	}

	@GET
	@Path("/products/{refTransId}/{sku}")
	public Response find(@PathParam("refTransId") Integer refTransId, @PathParam("sku") String sku) {
		ProductsTo productsTo = new ProductsTo();
		productsTo.setResponse(new mx.liverpool.downloadables.commons.models.Response());
		productsTo.setProduct(new Product());
		productsTo.getProduct().setRefTransId(refTransId);
		productsTo.getProduct().setSku(sku);
		try {
			productsTo = productsValidator.findValidate(productsTo);
			if (productsTo.getSuccessfully()) {
				productsTo = (ProductsTo) productsService.find(productsTo);
			}
			log.info("Ending find method");
		} catch (Exception e) {
			log.info("Can't perform the find method due this error > " + e.getMessage());
		}
		return Response.status(productsTo.getResponse().getHttStatusCode()).entity(productsTo.getResponse()).build();
	}

}
