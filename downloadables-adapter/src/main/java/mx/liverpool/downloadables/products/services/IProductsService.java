/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.products.services;

import mx.liverpool.downloadables.commons.daos.IAbstract;

/**
 * @author: Genaro Bermúdez [11/02/2018]
 * @updated:
 * @description: Products interface to dao tier
 * @since-version: 1.0
 */
public interface IProductsService extends IAbstract {

}
