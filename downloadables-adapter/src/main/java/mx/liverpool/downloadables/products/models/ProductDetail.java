/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.products.models;

import java.io.Serializable;

import lombok.Data;

/**
 * @author: Genaro Bermúdez [11/02/2018]
 * @updated:
 * @description: entity-class that mapping with Products table
 * @since-version: 1.0
 */
@Data
public class ProductDetail implements Serializable {

	/** Class members */
	private static final long serialVersionUID = -772327916805234801L;
	private Integer idNumber;
	private Integer refTransId;
	private String sku;
	private Integer indexId;
	private String productKey;
	private String url;
	private String instructions;
	/** PRODUCTS_DETAILS info */
	private String redeemStatus;
	private String redeemDate;

}
