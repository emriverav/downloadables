/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.products.valitators;

import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.products.tos.ProductsTo;

/**
 * @author: Genaro Bermúdez [11/02/2018]
 * @updated:
 * @description: Class to manage the http(s) valitations
 * @since-version: 1.0
 */
@Slf4j
public class ProductsValidator {

	public ProductsTo findValidate(ProductsTo productsTo) {

		/** Prepare the local vars */
		productsTo.setSuccessfully(false);

		try {

			/** Validation case 1 */
			if (productsTo.getProduct() == null) {
				productsTo.getResponse().setHttStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
				productsTo.getResponse().setDescription("Invalid param. Missing Product object attribute");
				throw new Exception("Invalid param. Missing Product object or productId attribute");
			}

			/** Validation case 2 */
			if (productsTo.getProduct().getRefTransId() == null || productsTo.getProduct().getRefTransId().intValue() == 0) {
				productsTo.getResponse().setHttStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
				productsTo.getResponse().setDescription("Invalid param. Missing RefTransId object or RefTransId attribute");
				throw new Exception("Invalid param. Missing Product object or productId attribute");
			}

			/** All validations are successfully */
			productsTo.setSuccessfully(true);
			productsTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
			productsTo.getResponse().setDescription("All validations are successfully");

		} catch (Exception e) {

			/** Error case */
			log.error("Can't perform the operation due this error > " + productsTo.getResponse());

		}

		/** Return value */
		return productsTo;

	}

}