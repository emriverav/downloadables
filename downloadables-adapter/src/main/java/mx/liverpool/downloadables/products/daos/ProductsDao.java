/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.products.daos;

import java.util.ArrayList;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import mx.liverpool.downloadables.commons.daos.AbstracDao;
import mx.liverpool.downloadables.products.models.Product;
import mx.liverpool.downloadables.products.tos.ProductsTo;

/**
 * @author: Genaro Bermúdez [11/02/2018]
 * @updated: Mauricio Rivera [25/05/2018]
 * @description: Dao implementation of Products
 * @since-version: 1.0
 */
@Repository
public class ProductsDao extends AbstracDao implements IProductsDao {

	/** Query constants for all operations in this implementation */
	protected final static String INSERT = "INSERT INTO PRODUCTS (REF_TRANS_ID, SKU, DESCRIPTION, QUANTITY, EXTERNAL_ID, SUPPLIER_ID, ORDER_ID, ERROR_CODE) VALUES (?,?,?,?,?,?,?,?)";
	protected final static String FIND = "SELECT * FROM PRODUCTS WHERE REF_TRANS_ID = ? AND SKU = ?";
	protected final static String UPDATE = "UPDATE PRODUCTS SET ORDER_ID  = ? WHERE REF_TRANS_ID = ?";
	protected final static String UPDATE_ERROR = "UPDATE PRODUCTS SET ERROR_CODE  = ? WHERE SKU = ? AND EXTERNAL_ID = ? AND SUPPLIER_ID = ? AND REF_TRANS_ID = ?";
	protected final static String UPDATE_ERROR_CODE21 = "UPDATE PRODUCTS SET ERROR_CODE  = ? WHERE EXTERNAL_ID = ? AND SUPPLIER_ID = ?";
	protected final static String UPDATE_ERROR_CODE500 = "UPDATE PRODUCTS SET ERROR_CODE  = ? WHERE REF_TRANS_ID = ?";

	public Object insert(Object object) {
		ProductsTo productsTo = (ProductsTo) object;
		try {
			setRowsAffected(jdbcTemplate.update(INSERT, new Object[] { productsTo.getProduct().getRefTransId(), productsTo.getProduct().getSku(), productsTo.getProduct().getDescription(), productsTo.getProduct().getQuantity(), productsTo.getProduct().getExternalId(), productsTo.getProduct().getSupplierId(), productsTo.getProduct().getOrderId(), productsTo.getProduct().getErrorCode() }));
		} catch (Exception e) {
			throw e;
		}
		return productsTo;
	}

	public Object find(Object object) {
		ProductsTo productsTo = (ProductsTo) object;
		try {
			productsTo.setProducts((ArrayList<Product>) jdbcTemplate.query(FIND, new Object[] { productsTo.getProduct().getRefTransId(), productsTo.getProduct().getSku() }, new BeanPropertyRowMapper<Product>(Product.class)));
			setRowsAffected(productsTo.getProducts().size());
			if (productsTo.getProducts().size() > 0) {
				productsTo.setProduct(productsTo.getProducts().get(0));
			}
		} catch (Exception e) {
			throw e;
		}
		return productsTo;
	}

	public Object update(Object object) {
		ProductsTo productsTo = (ProductsTo) object;
		try {
			if(productsTo.getProduct().getOrderId()!=null) {
			setRowsAffected(jdbcTemplate.update(UPDATE, new Object[] { productsTo.getProduct().getOrderId(), productsTo.getProduct().getRefTransId()}));
			}else if(productsTo.getProduct().getErrorCode()==21) {
				setRowsAffected(jdbcTemplate.update(UPDATE_ERROR_CODE21, new Object[] { productsTo.getProduct().getErrorCode(), productsTo.getProduct().getExternalId(), productsTo.getProduct().getSupplierId() }));
			}else if(productsTo.getProduct().getErrorCode()==500) {
				setRowsAffected(jdbcTemplate.update(UPDATE_ERROR_CODE500, new Object[] { productsTo.getProduct().getErrorCode(), productsTo.getProduct().getRefTransId()}));
			}
			else {
				setRowsAffected(jdbcTemplate.update(UPDATE_ERROR, new Object[] { productsTo.getProduct().getErrorCode(), productsTo.getProduct().getSku(), productsTo.getProduct().getExternalId(), productsTo.getProduct().getSupplierId(), productsTo.getProduct().getRefTransId() }));
			}
		} catch (Exception e) {
			throw e;
		}
		return productsTo;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

}
