/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.products.services;

import javax.inject.Inject;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;
import mx.liverpool.downloadables.products.daos.IProductsDao;
import mx.liverpool.downloadables.products.tos.ProductsTo;

/**
 * @author: Genaro Bermúdez [11/02/2018]
 * @updated: Genaro Bermúdez [18/02/2018]
 * @description: bussiness logic Products Service
 * @since-version: 1.0
 */
@Slf4j
public class ProductsService implements IProductsService {

	@Inject
	IProductsDao productsDao;

	public ProductsTo find(Object object) {
		ProductsTo productsTo = (ProductsTo) object;
		try {
			// TODO: aquí se debe invocar al servicio de Incomex o buscar en la BD
			productsTo = (ProductsTo) productsDao.find(productsTo);
			// TODO: aquí se deben aplicar las reglas de validación y negocio
			if (productsTo.getProducts().size() > 0) {
				productsTo.getResponse().setHttStatusCode(Response.Status.OK.getStatusCode());
				productsTo.getResponse().setDescription("The productCode is > " + productsTo.getProduct().getRefTransId());
			} else {
				productsTo.getResponse().setHttStatusCode(Response.Status.BAD_REQUEST.getStatusCode());
				productsTo.getResponse().setDescription("The productId > " + productsTo.getProduct().getRefTransId() + " doesn't exits");
			}
			productsTo.setSuccessfully(true);
			log.info(productsTo.getResponse().getDescription());
		} catch (Exception e) {
			productsTo.getResponse().setHttStatusCode(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode());
			productsTo.getResponse().setDescription("Can't find the productId due this error > " + e.getMessage());
			productsTo.setSuccessfully(false);
			log.info(productsTo.getResponse().getDescription());
		}
		return productsTo;
	}

	public Object insert(Object object) {
		return null;
	}

	public Object update(Object object) {
		return null;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

	public int getRowsAffected() {
		return 0;
	}

}