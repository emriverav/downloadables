/**
 * Copyright (c) 2018 by Liverpool All rights reserved.
 *
**/
package mx.liverpool.downloadables.products.daos;

import java.util.ArrayList;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import mx.liverpool.downloadables.commons.daos.AbstracDao;
import mx.liverpool.downloadables.products.models.Product;
import mx.liverpool.downloadables.products.models.ProductDetail;
import mx.liverpool.downloadables.products.tos.ProductsTo;

/**
 * @author: Genaro Bermúdez [11/02/2018]
 * @updated: Mauricio Rivera [04/04/2018]
 * @description: Dao implementation of Products
 * @since-version: 1.0
 */
@Repository
public class ProductsDetailDao extends AbstracDao implements IProductsDetailDao {

	/** Query constants for all operations in this implementation */
	protected final static String INSERT = "INSERT INTO PRODUCTS_DETAIL (REF_TRANS_ID, SKU, INDEX_ID, PRODUCT_KEY, URL, INSTRUCTIONS) VALUES (?,?,?,?,?,?)";
	protected final static String UPDATE_REDEM_STATUS = "UPDATE PRODUCTS_DETAIL SET REDEEM_STATUS  = ?, REDEEM_DATE = ? WHERE PRODUCT_KEY = ?";
	protected final static String FIND = "SELECT * FROM PRODUCTS_DETAIL WHERE PRODUCT_KEY = ?";

	public Object insert(Object object) {
		ProductsTo productsTo = (ProductsTo) object;
		try {
			setRowsAffected(jdbcTemplate.update(INSERT, new Object[] { productsTo.getProductDetail().getRefTransId(), productsTo.getProductDetail().getSku(), productsTo.getProductDetail().getIndexId(), productsTo.getProductDetail().getProductKey(), productsTo.getProductDetail().getUrl(), productsTo.getProductDetail().getInstructions() }));
		} catch (Exception e) {
			throw e;
		}
		return productsTo;
	}

	public Object update(Object object) {
		ProductsTo productsTo = (ProductsTo) object;
		try {
			setRowsAffected(jdbcTemplate.update(UPDATE_REDEM_STATUS, new Object[] { productsTo.getProductDetail().getRedeemStatus(), productsTo.getProductDetail().getRedeemDate(), productsTo.getProductDetail().getProductKey()}));
		} catch (Exception e) {
			throw e;
		}
		return productsTo;
	}
	
	public Object find(Object object) {
		ProductsTo productsTo = (ProductsTo) object;
		try {
			productsTo.setProductsDetail((ArrayList<ProductDetail>) jdbcTemplate.query(FIND, new Object[] { productsTo.getProductDetail().getProductKey()}, new BeanPropertyRowMapper<ProductDetail>(ProductDetail.class)));
		} catch (Exception e) {
			throw e;
		}
		return productsTo;
	}

	public Object save(Object object) {
		return null;
	}

	public Object delete(Object object) {
		return null;
	}

	public Object findAll(Object object) {
		return null;
	}

}
